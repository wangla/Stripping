###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from StrippingUtils.Utils import create_archive
from StrippingArchive import _duplicate_strippings as duplicate_strippings
import os

class CommonParticlesArchiveConf ( object ) :
    
    def isdir( self, z, name ):
        return any(x.startswith("%s/" % name.rstrip("/")) for x in z.namelist())

    def isValidArchive(self,path):

        zipname = "python.zip"

        if not zipname in path :

            # Standard file system
            import os
            return os.path.exists(path)

        else :

            # Python zip file. So manually handle this ..
            import zipfile
            splits = path.split(zipname+"/")
            zfile = splits[0] + zipname
            entry = splits[1]
            z = zipfile.ZipFile( zfile, 'r' )
            status = self.isdir(z,entry)
            z.close()
            return status

    def redirect(self,stripping):

        import os, sys

        # Handle 'alias' strippings
        stripName = stripping.capitalize()
        if stripName in duplicate_strippings.keys() :
            stripName = duplicate_strippings[stripName]

        # Construct the archive python path from this modules path
        pth = os.path.sep.join( __file__.split(os.path.sep)[0:-1] + [stripName] )

        # Check to see if CommonParticles has already been loaded.
        # If it has, throw an exception, as this method must be called
        # prior to any import of CommonParticles
        if 'CommonParticles' in sys.modules.keys() and not sys.modules['CommonParticles'].__file__.startswith(pth) :
            raise Exception( "Can't redirect CommonParticles to " + pth + ". Module " + str(sys.modules['CommonParticles']) +
                             " already loaded. " + 
                             "redirectCommonParticles must be called *BEFORE* any imports from CommonParticles" )

        # Check this is a known archived version
        if not self.isValidArchive(pth) :
            raise Exception( "CommonParticles archive '"+pth+"' is missing" )

        # Append the archive path, so it is picked up first
        log.info( "Redirecting CommonParticles to '" + pth + "'" )
        sys.path.insert(0,pth)

def create_common_particles_archive(stripping) :
    source = os.path.join(os.environ['COMMONPARTICLESROOT'], 'python', 'CommonParticles')
    dest = os.path.join(os.environ['COMMONPARTICLESARCHIVEROOT'], 'python', 'CommonParticlesArchive', stripping, 'CommonParticles')
    return create_archive(source, dest)
