/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/ToolFactory.h"
#include "RelInfoVeloTrackMatch.h"
#include "Kernel/RelatedInfoNamed.h"
#include "Event/Particle.h"
// kernel
#include "GaudiKernel/PhysicalConstants.h"
#include <Kernel/IDistanceCalculator.h>

#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>

#include "Kernel/IRelatedPVFinder.h"
#include "Math/Boost.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RelInfoVeloTrackMatch
//
// 2014-07-25 : Andrea Contu
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( RelInfoVeloTrackMatch )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RelInfoVeloTrackMatch::RelInfoVeloTrackMatch( const std::string& type,
                                            const std::string& name,
                                            const IInterface* parent) : GaudiTool ( type, name , parent )
                                            , m_dva(0)
                                            , m_slopediff(0)
                                            , m_momentumcone(false){
                                                declareInterface<IRelatedInfoTool>(this);
                                                
                                                declareProperty("ConeAngle", m_slopediff = 0.05);
                                                declareProperty("ConeAroundMomentum", m_momentumcone = false);
                                                declareProperty("Variables", m_variables, "List of variables to store (store all if empty)");
                                                declareProperty("MaxIP", m_maxVbestPVdist=0.5);
                                                declareProperty("InputTracks",      m_inputTracksLocation = "dummy");
                                                
                                                //sort these keys out by adding all
                                                m_keys.clear();
                                                
                                            }
                                            
//=============================================================================
// Destructor
//=============================================================================
RelInfoVeloTrackMatch::~RelInfoVeloTrackMatch() {}


//=============================================================================
// Initialize
//=============================================================================
StatusCode RelInfoVeloTrackMatch::initialize() {
    StatusCode sc = GaudiTool::initialize() ;
    if ( sc.isFailure() ) return sc ;
    
//     m_hitManager  = tool<FastVeloHitManager>( "FastVeloHitManager", m_hitManagerName );
    
    if ( m_variables.empty() ){
        m_keys.push_back( RelatedInfoNamed::VTM_R );
        m_keys.push_back( RelatedInfoNamed::VTM_NTRACKS );
        m_keys.push_back( RelatedInfoNamed::VTM_TX );
        m_keys.push_back( RelatedInfoNamed::VTM_TY );
        m_keys.push_back( RelatedInfoNamed::VTM_TPX );
        m_keys.push_back( RelatedInfoNamed::VTM_TPY );
        m_keys.push_back( RelatedInfoNamed::VTM_TPZ );
        m_keys.push_back( RelatedInfoNamed::VTM_IP );
    }
    else {

        for ( const auto& var : m_variables ){
            short int key = RelatedInfoNamed::indexByName( var );
            if (key != RelatedInfoNamed::UNKNOWN){
                m_keys.push_back( key );
                if ( msgLevel(MSG::DEBUG) )
                debug() << "Adding variable " << var << ", key = " << key << endmsg;
            }
            else{
                warning() << "Unknown variable " << var << ", skipping" << endmsg;
            }
        }
    }
    
    m_dva = Gaudi::Utils::getIDVAlgorithm ( contextSvc()) ;
    if (!m_dva) return Error("Couldn't get parent DVAlgorithm",StatusCode::FAILURE);
    
    return sc;
}

//=============================================================================
// Fill Extra Info structure
//=============================================================================
StatusCode RelInfoVeloTrackMatch::calculateRelatedInfo( const LHCb::Particle *top,
                                                        const LHCb::Particle *part ){
    
    if ( msgLevel( MSG::DEBUG ) )
    debug() << "==> Fill" << endmsg;

    // -- The vector m_decayParticles contains all the particles that belong to the given decay  according to the decay descriptor.

    // -- Clear the vector with the particles in the specific decay
    m_decayParticles.clear();

    // -- Add the mother (prefix of the decay chain) to the vector
    if ( msgLevel( MSG::DEBUG ) )
    debug() << "Filling particle with ID " << top->particleID().pid() << endmsg;
    m_decayParticles.push_back( top );

    // -- Save all particles that belong to the given decay in the vector m_decayParticles
    saveDecayParticles( top );

//     bool test = true;
    m_map.clear();
    
    StatusCode sc=StatusCode::SUCCESS;
    
//     const unsigned int maxVeloTracks = 2000 ;
    
    double ipcut=m_maxVbestPVdist;
    
    double bestdt=9999.;
    double bestip=9999.;
    double nvelotracks=-1.;
//     double bestconedist=9999.;
    Gaudi::XYZPoint bestfposition;
    Gaudi::SymMatrix3x3 bestfpositionerr;
    Gaudi::XYZPoint bestslopes;
    Gaudi::SymMatrix3x3 bestslopeserr;
    
    if(!part->isBasicParticle() && part->endVertex()){
        
        Gaudi::LorentzVector lorvectmother=part->momentum();
        
        bool conemom=m_momentumcone;
        const LHCb::VertexBase* aPV = NULL;
        
        if(!m_momentumcone) {
            aPV = m_dva->bestVertex (part);
            if(!aPV){
                conemom=true;
                ipcut=9999.;
                Warning("Could not find best PV, falling back to momentum cone!");
            }
        }
        else{
            conemom=true;
        }
        
        double mpx=9999.;
        double mpy=9999.;
        double mpz=9999.;
        
        if(!conemom){
            mpx=aPV->position().x()-part->endVertex()->position().x();
            mpy=aPV->position().y()-part->endVertex()->position().y();
            mpz=aPV->position().z()-part->endVertex()->position().z();
        }
        
        else{
            mpx=lorvectmother.Px();
            mpy=lorvectmother.Py();
            mpz=lorvectmother.Pz();
        }
        double msx=mpx/mpz;
        double msy=mpy/mpz;
        if ( exist<LHCb::Tracks>( m_inputTracksLocation ) ) {
            m_inputTracks = get<LHCb::Tracks>( m_inputTracksLocation );
        } 
        

        nvelotracks=m_inputTracks->size();

        if(nvelotracks==0){
            std::string notracksmsg = "Could not find any VELO tracks at "+m_inputTracksLocation+". You may need to create them!";
            return Warning(notracksmsg.c_str(),sc);
        }
        else{
            debug() << "found tracks at "<<m_inputTracksLocation<<endmsg;
        }
        
        for(LHCb::Tracks::const_iterator tri=m_inputTracks->begin(); tri!= m_inputTracks->end(); ++tri){
            const LHCb::Track *tmptrack=(*tri);
            if(tmptrack->type()==LHCb::Track::Velo){
                Gaudi::XYZPoint tmpfp;
                Gaudi::SymMatrix3x3 fperror;
                tmptrack->position(tmpfp,fperror);
                
                Gaudi::XYZVector tmpslope;
                Gaudi::SymMatrix3x3 slopeerror;
                tmptrack->slopes(tmpslope,slopeerror);
                double dtx = msx - tmpslope.X() ;
                double dty = msy - tmpslope.Y() ;
                double dt  = std::sqrt(dtx*dtx+dty*dty) ;
                double tmpR3d=sqrt(pow((1-tmpslope.X()/msx),2)+pow((1-tmpslope.Y()/msy),2));
                
                double ipbestpv=-1.;
                double tmpvect2=0.;
                double tmpscalar2=0.;
                
                if(!conemom){
                    tmpvect2= pow((tmpfp.X()-aPV->position().x()),2) + pow((tmpfp.Y()-aPV->position().y()),2) + pow((tmpfp.Z()-aPV->position().z()),2);
                    tmpscalar2=pow( ((tmpfp.X()-aPV->position().x())*tmpslope.X() + (tmpfp.Y()-aPV->position().y())*tmpslope.Y() + (tmpfp.Z()-aPV->position().z())*tmpslope.Z()),2)/(tmpslope.X()*tmpslope.X()+tmpslope.Y()*tmpslope.Y()+tmpslope.Z()*tmpslope.Z());
                    ipbestpv=sqrt(tmpvect2-tmpscalar2);
                }
                
                if(dt<m_slopediff && ipbestpv<ipcut){
                    if(tmpR3d<bestdt ) {
                        bestdt = tmpR3d ;
                        bestip = ipbestpv ;
                        bestfposition=tmpfp;
                        bestfpositionerr=fperror;
                        bestslopes=tmpslope;
                        bestslopeserr=slopeerror;
//                         bestconedist=dt;
                    }
                    ++nvelotracks;
                    
                }
            }
        }
        
        
        for ( const auto key : m_keys ) {
            float value = 0;
            bool filled = true;
            switch (key) {
                case RelatedInfoNamed::VTM_R : value = bestdt; break;
                case RelatedInfoNamed::VTM_NTRACKS : value = nvelotracks; break;
                case RelatedInfoNamed::VTM_TX : value = bestslopes.X(); break;
                case RelatedInfoNamed::VTM_TY : value = bestslopes.Y(); break;
                case RelatedInfoNamed::VTM_TPX : value = bestfposition.X(); break;
                case RelatedInfoNamed::VTM_TPY : value = bestfposition.Y(); break;
                case RelatedInfoNamed::VTM_TPZ : value = bestfposition.Z(); break;
                case RelatedInfoNamed::VTM_IP : value = bestip; break;
                default: filled = false; break; 
            }
            if (filled) {
                debug() << "  Inserting key = " << key << ", value = " << value << " into map" << endmsg;
                m_map.insert( std::make_pair(key,value) );
            }
        }
        
        
        
    }
    return StatusCode::SUCCESS;
}


//=============================================================================
// Save the particles in the decay chain (recursive function)
//=============================================================================
void RelInfoVeloTrackMatch::saveDecayParticles( const LHCb::Particle *top ) {

  // -- Get the daughters of the top particle
  const SmartRefVector< LHCb::Particle > daughters = top->daughters();
  
  // -- Fill all the daugthers in m_decayParticles
  for ( SmartRefVector< LHCb::Particle >::const_iterator ip = daughters.begin(); ip != daughters.end(); ++ip ) {
    
    // -- If the particle is stable, save it in the vector, or...
    if ( (*ip)->isBasicParticle() ) {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Filling particle with ID " << (*ip)->particleID().pid() << endmsg;
      m_decayParticles.push_back( (*ip) );
    }
    else {
      // -- if it is not stable, call the function recursively
      m_decayParticles.push_back( (*ip) );
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Filling particle with ID " << (*ip)->particleID().pid() << endmsg;
      saveDecayParticles( (*ip) );
    }
  }

  return;
  
}
//rel infor methods

LHCb::RelatedInfoMap* RelInfoVeloTrackMatch::getInfo(void) {
    return &m_map;
}

std::string RelInfoVeloTrackMatch::infoPath(void) {
    std::stringstream ss;
    ss << std::string("ParticleVeloTrackMatchRelations");
    return ss.str();
}