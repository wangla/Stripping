###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Exclusive lines for Xic+ -> Lambda0 KS0 pi+ and Xic0 -> Lambda0 K- pi+
 including partial reconstructed Xic -> Sigma0 K pi+ signals

Author: M. Stahl
'''
__author__ = ['Marian Stahl']

__all__ = ('Xic2LambdaKPiConf', 'default_config')

moduleName = 'Xic2LambdaKPi'

# Import Packages
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import AutomaticData
from PhysConf.Selections import FilterSelection, Combine3BodySelection, MergedSelection
from Configurables import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

# Default configuration dictionary
default_config = {
    'NAME': 'Xic2LambdaKPi',
    'BUILDERTYPE': 'Xic2LambdaKPiConf',
    'CONFIG': {
        'pi': {
            'TES':
            'Phys/StdAllNoPIDsPions/Particles',
            'Filter':
            "(P>2000*MeV) & (PT>200*MeV) & (PROBNNpi>0.2) & (MIPCHI2DV(PRIMARY)>0.2) & (TRGHOSTPROB<0.3)"
        },
        'K': {
            'TES':
            'Phys/StdAllNoPIDsKaons/Particles',
            'Filter':
            "(P>2000*MeV) & (PT>250*MeV) & (PROBNNk>0.2) & (MIPCHI2DV(PRIMARY)>0.4) & (TRGHOSTPROB<0.3)"
        },
        'KShortLL': {
            'TES':
            "Phys/StdVeryLooseKsLL/Particles",
            'Filter':
            "(ADMASS('KS0')<25*MeV) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.05) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>6) & (CHI2VXNDF<6) & (BPVVDCHI2>12)"
        },
        'KShortDD': {
            'TES':
            "Phys/StdLooseKsDD/Particles",
            'Filter':
            "(ADMASS('KS0')<25*MeV) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.05) & (CHI2VXNDF<6) & (BPVVDZ>500*mm) & (BPVVDCHI2>12)"
        },
        'LambdaLL': {
            'TES':
            'Phys/StdVeryLooseLambdaLL/Particles',
            'Filter':
            """(ADMASS('Lambda0')<20*MeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.1) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.05) & (CHI2VXNDF<6) & (BPVVDCHI2>48) &
               (MAXTREE('p+'==ABSID,PT)>250*MeV) & (MAXTREE('p+'==ABSID,P)>7500*MeV) & (MAXTREE('pi+'==ABSID,TRGHOSTPROB)<0.3) & (MAXTREE('p+'==ABSID,TRGHOSTPROB)<0.3) & (P>12*GeV) & (PT>600*MeV)"""
        },
        'LambdaDD': {
            'TES':
            'Phys/StdLooseLambdaDD/Particles',
            'Filter':
            """(ADMASS('Lambda0')<20*MeV) & (MAXTREE('p+'==ABSID,PROBNNp)>0.25) & (MAXTREE('pi+'==ABSID,PROBNNpi)>0.1) & (CHI2VXNDF<6) & (BPVVDZ>500*mm) &
               (MAXTREE('p+'==ABSID,PT)>300*MeV) & (MAXTREE('p+'==ABSID,P)>7500*MeV) & (MAXTREE('pi+'==ABSID,TRGHOSTPROB)<0.3) & (MAXTREE('p+'==ABSID,TRGHOSTPROB)<0.3) & (BPVVDCHI2>48) & (P>12*GeV) & (PT>600*MeV)"""
        },
        'Xic': {
            'XicpDDDs': ['[Xi_c+ -> Lambda0 KS0 pi+]cc'],
            'XiczDDDs': ['[Xi_c0 -> Lambda0 K- pi+]cc'],
            'Comb12Cut':
            "(ACHI2DOCA(1,2)<10) & (AMASS(1,2)<2460*MeV)",
            'CombCut':
            "(ACHI2DOCA(1,3)<8) & (ACHI2DOCA(2,3)<8) & (ASUM(PT)>1800*MeV) & (AMASS()>2260*MeV) & (AMASS()<2590*MeV)",
            'XicpMotherCut':
            "(CHI2VXNDF<6) & (in_range(2280*MeV,M,2560*MeV)) & (P>16*GeV) & (PT>1200*MeV) & (BPVDIRA>0.999) & (BPVIPCHI2()<12) & (BPVVDZ>-5*mm)",
            'XiczMotherCut':
            """(CHI2VXNDF<3) & (in_range(2290*MeV,M,2550*MeV)) & (P>25*GeV) & (PT>1400*MeV) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>15*mm) &
               (SUMTREE(PT, ISBASIC)>2000*MeV) & (BPVDIRA>0.9996) & (BPVIPCHI2()<5) & (BPVVDZ>-1*mm)""",
        },
    },
    'STREAMS': {
        'Charm': [
            'StrippingXic2LambdaKPi_XicpLine',
            'StrippingXic2LambdaKPi_XiczLine',
        ]
    },
    'WGs': ['Charm']
}


# Configure the LineBuilder
class Xic2LambdaKPiConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, moduleName, config):
        LineBuilder.__init__(self, moduleName, config)

        # inputs to build Xic candidates
        pi = FilterSelection(
            moduleName + "DetachedLongPions",
            [AutomaticData(config['pi']['TES'])],
            Code=config['pi']['Filter'])
        K = FilterSelection(
            moduleName + "DetachedLongKaons",
            [AutomaticData(config['K']['TES'])],
            Code=config['K']['Filter'])
        LambdaLL = FilterSelection(
            moduleName + "LambdaLL",
            [AutomaticData(config['LambdaLL']['TES'])],
            Code=config['LambdaLL']['Filter'])
        LambdaDD = FilterSelection(
            moduleName + "LambdaDD",
            [AutomaticData(config['LambdaDD']['TES'])],
            Code=config['LambdaDD']['Filter'])
        KShortLL = FilterSelection(
            moduleName + "KShortLL",
            [AutomaticData(config['KShortLL']['TES'])],
            Code=config['KShortLL']['Filter'])
        KShortDD = FilterSelection(
            moduleName + "KShortDD",
            [AutomaticData(config['KShortDD']['TES'])],
            Code=config['KShortDD']['Filter'])

        Lambda = MergedSelection(
            moduleName + "MergedLambdas",
            RequiredSelections=[LambdaLL, LambdaDD])

        # save some computing time by using multi-body combinations
        SelXicp_LLLL = Combine3BodySelection(
            moduleName + "Xicp_LLLL", [LambdaLL, KShortLL, pi],
            DecayDescriptors=config["Xic"]["XicpDDDs"],
            Combination12Cut=config["Xic"]["Comb12Cut"],
            CombinationCut=config["Xic"]["CombCut"],
            MotherCut=config["Xic"]["XicpMotherCut"])
        SelXicp_LLDD = Combine3BodySelection(
            moduleName + "Xicp_LLDD", [LambdaLL, KShortDD, pi],
            DecayDescriptors=config["Xic"]["XicpDDDs"],
            Combination12Cut=config["Xic"]["Comb12Cut"],
            CombinationCut=config["Xic"]["CombCut"],
            MotherCut=config["Xic"]["XicpMotherCut"])
        SelXicp_DDLL = Combine3BodySelection(
            moduleName + "Xicp_DDLL", [LambdaDD, KShortLL, pi],
            DecayDescriptors=config["Xic"]["XicpDDDs"],
            Combination12Cut=config["Xic"]["Comb12Cut"],
            CombinationCut=config["Xic"]["CombCut"],
            MotherCut=config["Xic"]["XicpMotherCut"])
        SelXicp = MergedSelection(
            moduleName + "Xicp",
            RequiredSelections=[SelXicp_LLLL, SelXicp_LLDD, SelXicp_DDLL])

        SelXicz = Combine3BodySelection(
            moduleName + "Xicz", [Lambda, K, pi],
            DecayDescriptors=config["Xic"]["XiczDDDs"],
            Combination12Cut=config["Xic"]["Comb12Cut"],
            CombinationCut=config["Xic"]["CombCut"],
            MotherCut=config["Xic"]["XiczMotherCut"])

        # Create the stripping line
        Xicpline = StrippingLine(moduleName + '_XicpLine', algos=[SelXicp])
        self.registerLine(Xicpline)
        Xiczline = StrippingLine(moduleName + '_XiczLine', algos=[SelXicz])
        self.registerLine(Xiczline)
