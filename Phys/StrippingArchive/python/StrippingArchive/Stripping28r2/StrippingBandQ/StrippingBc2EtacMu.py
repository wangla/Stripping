###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for B_c->eta_c mu

'''

__author__ = ['Valeriia Zhovkovska']
__date__ = '15/03/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ('Bc2EtacMuConf', 'default_config')

# If you have several configs in one module your default_config should
# be a dict of configs with the names of the configs as keys. The configs
# themselves then shouldn't have a 'NAME' element.
# M.A. 2017/05/02.
default_config = {
    'NAME': 'Bc2EtacMu',
    'BUILDERTYPE': 'Bc2EtacMuConf',
    'CONFIG': {
        'LinePrescale':
        1.,
        'LinePostscale':
        1.,
        'SpdMult':
        450.,  # dimensionless, Spd Multiplicy cut
        'MuonCuts':
        "(PROBNNmu > 0.1) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
        'ProtonCuts':
        "(PROBNNp > 0.1)  & (PT > 400*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
        'PionCuts':
        "(PROBNNpi > 0.1) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)",
        'KaonCuts':
        "(PROBNNk > 0.1)  & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)",
        'KsCuts':
        "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5) & (PT > 500*MeV) & (MAXTREE('pi-'==ABSID, PROBNNpi) > 0.1) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.4) & (MAXTREE('pi-'==ABSID, TRCHI2DOF) < 5)",
        'EtacCombCuts':
        "in_range(2.7*GeV, AM, 3.3*GeV)",
        'EtacMomCuts':
        "in_range(2.7*GeV, MM, 3.3*GeV) & (VFASPF(VCHI2/VDOF) < 9.)",
        'BcCombCuts':
        "in_range(3.2*GeV, AM, 6.8*GeV)",
        'BcMomCuts':
        "in_range(3.2*GeV, MM, 6.8*GeV) & in_range(3.2*GeV, BPVCORRM, 6.8*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)",
        'CCCut':
        ""
    },
    'STREAMS': ['CharmCompleteEvent'],
    'WGs': ['BandQ']
}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


class Bc2EtacMuConf(LineBuilder):

    #__configuration_keys__ = default_config['CONFIG'].keys()

    __configuration_keys__ = ('LinePrescale', 'LinePostscale', 'SpdMult',
                              'MuonCuts', 'ProtonCuts', 'PionCuts', 'KaonCuts',
                              'KsCuts', 'EtacCombCuts', 'EtacMomCuts',
                              'BcCombCuts', 'BcMomCuts', 'CCCut')

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        self.name = name
        self.config = config

        from PhysSelPython.Wrappers import MergedSelection

        self.InputKs = MergedSelection(
            self.name + "InputKs",
            RequiredSelections=[
                DataOnDemand(Location="Phys/StdLooseKsDD/Particles"),
                DataOnDemand(Location="Phys/StdVeryLooseKsLL/Particles")
            ])

        self.SelKs = self.createSubSel(
            OutputList=self.name + "SelKs",
            InputList=self.InputKs,
            Cuts=config['KsCuts'])

        self.SelKaons = self.createSubSel(
            OutputList=self.name + "SelKaons",
            InputList=DataOnDemand(Location='Phys/StdLooseKaons/Particles'),
            Cuts=config['KaonCuts'])

        self.SelPions = self.createSubSel(
            OutputList=self.name + "SelPions",
            InputList=DataOnDemand(
                Location='Phys/StdAllNoPIDsPions/Particles'),
            Cuts=config['PionCuts'])

        self.SelProtons = self.createSubSel(
            OutputList=self.name + "SelProtons",
            InputList=DataOnDemand(Location='Phys/StdLooseProtons/Particles'),
            Cuts=config['ProtonCuts'])

        self.SelMuons = self.createSubSel(
            OutputList=self.name + "SelMuons",
            InputList=DataOnDemand(Location='Phys/StdLooseMuons/Particles'),
            Cuts=config['MuonCuts'])

        # Eta_c -> KS0 K Pi
        self.SelEtac2KsKPi = self.createCombinationSel(
            OutputList=self.name + "SelEtac2KsKPi",
            DecayDescriptor="[eta_c(1S) -> KS0 K+ pi-]cc",
            DaughterLists=[self.SelKs, self.SelKaons, self.SelPions],
            PreVertexCuts=config['EtacCombCuts'],
            PostVertexCuts=config['EtacMomCuts'])

        # Eta_c -> p pbar
        self.SelEtac2Ppbar = self.createCombinationSel(
            OutputList=self.name + "SelEtac2Ppbar",
            DecayDescriptor="eta_c(1S) -> p+ p~-",
            DaughterLists=[self.SelProtons],
            PreVertexCuts=config['EtacCombCuts'],
            PostVertexCuts=config['EtacMomCuts'])

        SpdMultForCcbarCut = config['SpdMult']

        self.SelBc2EtacMu_Ppbar = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacMu_Ppbar",
            DecayDescriptor="[B_c+ -> eta_c(1S) mu+]cc",
            DaughterLists=[self.SelEtac2Ppbar, self.SelMuons],
            PreVertexCuts=config['BcCombCuts'],
            PostVertexCuts=config['BcMomCuts'])

        self.Bc2EtacMu_PpbarLine = StrippingLine(
            self.name + "_PpbarLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForCcbarCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacMu_Ppbar])

        self.registerLine(self.Bc2EtacMu_PpbarLine)

        self.SelBc2EtacMu_KsKpi = self.createCombinationSel(
            OutputList=self.name + "SelBc2EtacMu_KsKpi",
            DecayDescriptor="B_c+ -> eta_c(1S) mu+",
            DaughterLists=[self.SelEtac2KsKPi, self.SelMuons],
            PreVertexCuts=config['BcCombCuts'],
            PostVertexCuts=config['BcMomCuts'])

        self.Bc2EtacMu_KsKpiLine = StrippingLine(
            self.name + "_KsKpiLine",
            prescale=config['LinePrescale'],
            postscale=config['LinePostscale'],
            FILTER={
                'Code':
                " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMultForCcbarCut)s )"
                % locals(),
                'Preambulo': [
                    "from LoKiNumbers.decorators import *",
                    "from LoKiCore.basic import LHCb"
                ]
            },
            checkPV=True,
            algos=[self.SelBc2EtacMu_KsKpi])

        self.registerLine(self.Bc2EtacMu_KsKpiLine)

    def createSubSel(self, OutputList, InputList, Cuts):
        '''create a selection using a FilterDesktop'''
        filt = FilterDesktop(Code=Cuts)
        return Selection(
            OutputList, Algorithm=filt, RequiredSelections=[InputList])

    def createCombinationSel(self,
                             OutputList,
                             DecayDescriptor,
                             DaughterLists,
                             DaughterCuts={},
                             PreVertexCuts="ALL",
                             PostVertexCuts="ALL",
                             ReFitPVs=True):
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles(
            DecayDescriptor=DecayDescriptor,
            DaughtersCuts=DaughterCuts,
            MotherCut=PostVertexCuts,
            CombinationCut=PreVertexCuts,
            ReFitPVs=True)
        return Selection(
            OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)
