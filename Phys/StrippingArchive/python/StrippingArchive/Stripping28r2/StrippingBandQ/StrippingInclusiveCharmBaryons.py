###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Inclusive lines for single open charm baryons
Selection based on DfomB BDTs for Lc -> p K- pi+, Xic+ -> p K- pi+, Xic0 -> p K- K- pi+, Omegac0 -> p K- K- pi+
BDT cuts are chosen such that they are ~80 % efficient on their Xb->Xc pi calibration channels

Author: M. Stahl
Notes: Be creative!
'''
__author__ = ['Marian Stahl']

__all__ = ('InclusiveCharmBaryonsConf', 'default_config', 'applyMVA', 'getMVAVars')

moduleName = 'InclusiveCharmBaryons'

# Import Packages
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import Selection , AutomaticData
from PhysConf.Selections import FilterSelection, CombineSelection, Combine4BodySelection, Combine3BodySelection, LimitSelection, MergedSelection
from Configurables import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from LoKiPhys.decorators import *
from LoKiArrayFunctors.decorators import *
from LoKiProtoParticles.decorators import *
import math

# Default configuration dictionary
default_config = {
  'NAME' : 'InclusiveCharmBaryons',
  'BUILDERTYPE' : 'InclusiveCharmBaryonsConf',
  'CONFIG' : {
    'pi' : {
      'TES'    : 'Phys/StdAllNoPIDsPions/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'K' : {
      'TES'    : 'Phys/StdAllNoPIDsKaons/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'p' : {
      'TES'    : 'Phys/StdAllNoPIDsProtons/Particles',
      'Filter' : "(P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0)"
    },
    'CharmHadrons': {
      'Lc' : {
        'Daughters'       : {'p' : 1, 'K' : 2, 'pi' : 3},
        'Parent'          : 'Lc',
        'DecayDescriptors' : ['[Lambda_c+ -> p+ K- pi+]cc'],
        #'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/LcPi_2015_GBDT.weights.xml',
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/LcPi_2016_GBDT.weights.xml',
        'MVACut'          : '0.0', #2015 -0.4
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Lambda_c+') < 50*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Lambda_c+') < 32*MeV)",
      },
      'Xic' : {
        'Daughters'       : {'p' : 1, 'K' : 2, 'pi' : 3},
        'Parent'          : 'Xic',
        'DecayDescriptors' : ['[Xi_c+ -> p+ K- pi+]cc'],
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/XicPi_Run2_GBDT.weights.xml',
        'MVACut'          : '0.4', #2015 0.1
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Xi_c+') < 50*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Xi_c+') < 32*MeV)",
      },
      'Xic0' : {
        'Daughters'       : {'p' : 1, 'K1' : 2, 'K2' : 3, 'pi' : 4},
        'Parent'          : 'Xic0',
        'DecayDescriptors' : ['[Xi_c0 -> p+ K- K- pi+]cc'],
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/Xic0Pi_Run2_GBDT.weights.xml',
        'MVACut'          : '-0.3', #2015 -0.5
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'Comb123Cut'      : "(ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Xi_c0') < 40*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,4)<0.5*mm) & (ADOCA(2,4)<0.5*mm) & (ADOCA(3,4)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Xi_c0') < 24*MeV)",
      },
      'Omegac0' : {
        'Daughters'       : {'p' : 1, 'K1' : 2, 'K2' : 3, 'pi' : 4},
        'Parent'          : 'Xic0', # the name in the weights file
        'DecayDescriptors' : ['[Omega_c0 -> p+ K- K- pi+]cc'],
        'xmlFile'         : '$TMVAWEIGHTSROOT/data/DfromB/Xic0Pi_Run2_GBDT.weights.xml',
        'MVACut'          : '0.1', #2015 -0.1
        'Comb12Cut'       : "(ADOCA(1,2)<0.5*mm)",
        'Comb123Cut'      : "(ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm)",
        'CombCut'         : """(ASUM(PT)>1800*MeV) & (ADAMASS('Omega_c0') < 40*MeV) &
                               (AHASCHILD((ISBASIC & HASTRACK & (TRCHI2DOF<4.) & (PT > 500*MeV) & (P > 5000*MeV))|
                                ((ABSID=='KS0') & (PT > 500*MeV) & (P > 5000*MeV) & (BPVVDCHI2 > 1000)))) &
                               (ADOCA(1,4)<0.5*mm) & (ADOCA(2,4)<0.5*mm) & (ADOCA(3,4)<0.5*mm)""",
        'MotherCut'       : "(CHI2VXNDF<10) & (BPVVDCHI2>36) & (BPVDIRA>0) & (ADMASS('Omega_c0') < 24*MeV)",
      },
    },
  },
  'STREAMS' : {
    'BhadronCompleteEvent':[
      'StrippingInclusiveCharmBaryons_LcLine',
      'StrippingInclusiveCharmBaryons_XicLine',
      'StrippingInclusiveCharmBaryons_Xic0Line',
      'StrippingInclusiveCharmBaryons_Omegac0Line',
    ]
  },
  'WGs' : [ 'BandQ' ]
}


# Configure the LineBuilder
class InclusiveCharmBaryonsConf(LineBuilder):

  __configuration_keys__ = default_config['CONFIG'].keys()

  def __init__(self, moduleName, config):
    LineBuilder.__init__(self, moduleName, config)

    # inputs to build charm candidates
    pi = FilterSelection(moduleName + "DetachedLongPions",   [AutomaticData(config['pi']['TES'])], Code = config['pi']['Filter'])
    K  = FilterSelection(moduleName + "DetachedLongKaons",   [AutomaticData(config['K' ]['TES'])], Code = config['K' ]['Filter'])
    p  = FilterSelection(moduleName + "DetachedLongProtons", [AutomaticData(config['p' ]['TES'])], Code = config['p' ]['Filter'])
    mergedTracks  = MergedSelection(moduleName+"MergedTracks",RequiredSelections = [pi, K, p])

    # loop over all charm decays in the configuration and save their MVA-filtered selection in a list
    for name, cHad in config['CharmHadrons'].iteritems():

      # save some computing time by using multi-body combinations
      if "Comb123Cut" in cHad :
        SelcHad = Combine4BodySelection (moduleName+'Proto'+name, [mergedTracks], DecayDescriptors = cHad["DecayDescriptors"],
                                         Combination12Cut = cHad["Comb12Cut"], Combination123Cut = cHad["Comb123Cut"],
                                         CombinationCut = cHad["CombCut"], MotherCut = cHad["MotherCut"])
      else :
        SelcHad = Combine3BodySelection (moduleName+'Proto'+name, [mergedTracks], DecayDescriptors = cHad["DecayDescriptors"],
                                         Combination12Cut = cHad["Comb12Cut"], CombinationCut = cHad["CombCut"], MotherCut = cHad["MotherCut"])

      # Apply MVA cut
      MVASelcHad = self.applyMVA(moduleName+'_'+name, cHad['Parent'], cHad['Daughters'], [SelcHad], cHad['xmlFile'], cHad['MVACut'])

      line = StrippingLine(moduleName+'_'+name+'Line', algos = [MVASelcHad])
      self.registerLine(line)

  def applyMVA(self, selname, parentname, daughters, b2cSel, MVAxmlFile, MVACutValue):
    """
    Applies a BDT cut based on the input beauty 2 charm selection
    """
    from MVADictHelpers import addTMVAclassifierValue
    from Configurables import FilterDesktop

    _XcMVAFilter = FilterDesktop(selname+"Filter", Code = "VALUE('LoKi::Hybrid::DictValue/"+parentname+"_BDT')>"+MVACutValue)
    addTMVAclassifierValue(Component = _XcMVAFilter, XMLFile = MVAxmlFile,
                            Variables = self.getMVAVars(parentname, daughters), ToolName = parentname+"_BDT")
    return Selection(selname+'_MVAFilter', Algorithm = _XcMVAFilter, RequiredSelections = b2cSel)

  def getMVAVars(self, parentname, daugthers):
    """
    Return all variables required for the BDT
    Variable names MUST correspond exactly to what is needed by classifier (xml)
    """

    bdt_vars = {}
    # Variables for D and daughters;  prefixes added later
    vars_parent = {
      'log_P'              : 'math.log10(P)',
      'log_PT'             : 'math.log10(PT)',
      'log_ENDVERTEX_CHI2' : 'math.log10(VFASPF(VCHI2))',
      'log_IPCHI2_OWNPV'   : 'math.log10(MIPCHI2DV(PRIMARY))',
      'log_FDCHI2_OWNPV'   : 'math.log10(BPVVDCHI2)',
      'beta'               : '(SUMTREE(P,ISBASIC,0.)-(2.*CHILD(P,1)))/SUMTREE(P,ISBASIC,0.)',
    }
    vars_daughters = {
      'log_PT'                 : 'math.log10(CHILD(PT,{0}))',
      'log_IPCHI2_OWNPV'       : 'math.log10(CHILD(MIPCHI2DV(PRIMARY),{0}))',
      'log_TRACK_VeloCHI2NDOF' : 'math.log10(switch(CHILD(TINFO(LHCb.Track.FitVeloNDoF,-1),{0})>0,CHILD(TINFO(LHCb.Track.FitVeloChi2,-1),{0})/CHILD(TINFO(LHCb.Track.FitVeloNDoF,-1),{0}),-1))',
      'log_TRACK_TCHI2NDOF'    : 'math.log10(switch(CHILD(TINFO(LHCb.Track.FitTNDoF,-1),{0})>0,CHILD(TINFO(LHCb.Track.FitTChi2,-1),{0})/CHILD(TINFO(LHCb.Track.FitTNDoF,-1),{0}),-1))',
      'log_TRACK_MatchCHI2'    : 'math.log10(CHILD(TINFO(LHCb.Track.FitMatchChi2,-1.),{0}))',
      'log_TRACK_GhostProb'    : 'math.log10(CHILD(TRGHOSTPROB,{0}))',
      'UsedRichAerogel'        : 'switch(CHILDCUT(PPCUT(PP_USEDAEROGEL),{0}),1,0)',
      'UsedRich1Gas'           : 'switch(CHILDCUT(PPCUT(PP_USEDRICH1GAS),{0}),1,0)',
      'UsedRich2Gas'           : 'switch(CHILDCUT(PPCUT(PP_USEDRICH2GAS),{0}),1,0)',
      'RichAbovePiThres'       : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_PI),{0}),1,0)',
      'RichAboveKaThres'       : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_K),{0}),1,0)',
      'RichAbovePrThres'       : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_P),{0}),1,0)',
      'atan_RichDLLe'          : 'math.atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLe,-1000),{0}))',
      'atan_RichDLLmu'         : 'math.atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLmu,-1000),{0}))',
      'atan_RichDLLk'          : 'math.atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLk,-1000),{0}))',
      'atan_RichDLLp'          : 'math.atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLp,-1000),{0}))',
      'atan_RichDLLbt'         : 'math.atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLbt,-1000),{0}))',
      'atan_MuonLLbg'          : 'math.atan(switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonBkgLL,-10000),{0}),-1000))',
      'atan_MuonLLmu'          : 'math.atan(switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonMuLL,-10000),{0}),-1000))',
      'isMuon'                 : 'switch(CHILDCUT(ISMUON,{0}),1,0)',
      'MuonNShared'            : 'switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonNShared,0),{0}),-1)',
      'VeloCharge'             : 'CHILD(PPINFO(LHCb.ProtoParticle.VeloCharge,-1000),{0})',
    }
    # Add all parent variables to output
    for var, loki in vars_parent.iteritems():
      bdt_vars.update({'{}_{}'.format(parentname, var) : loki})
    # Add all daughter variables to output
    for daugthername, lab in daugthers.iteritems():
      for var, loki in vars_daughters.iteritems():
        bdt_vars.update({'{}_{}_{}'.format(parentname,daugthername, var) : loki.format(lab)})
    # Print out variables for sanity
    #for key in sorted(bdt_vars):
    #    print '{:<25} : {}'.format(key, bdt_vars[key].replace(',', ', '))
    #print 80 * '-'
    return bdt_vars
