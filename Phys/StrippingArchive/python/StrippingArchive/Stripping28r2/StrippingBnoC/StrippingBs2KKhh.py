###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Bs->quasi 2-body selection
Note: This script borrows heavily from that constructed by
Fred Blanc in the B2CharmlessQuasi2Body selection

Designed to look for KKhh final states with KK around the
phi mass

2016-11-21:
Updated for Run2 HLT line names, adding the HLT2 inclusive phi line.
2016-11-22:
Loosened kinematic cuts and track IP chi2 back to S20 values.
2018-03-15:
Changed to .DST for full event writeout to be used with new flavor-tagging algorithms in time-dependent measurements using Run 2 data
Rate estimated on 100K events using a 2017 data sample is: 0.05%
2019-04-29
Apply same 'track filter' cuts to all four tracks (ghost prob, track chi2, pT, IP chi2)
Replaced StdTightPhi2KK with StdLooseDetachedPhi2KK which reduces enormous background from prompt phi->K+K-
Remove m(KK) window and just take m(KK)<1.1 GeV from StdLooseDetachedPhi2KK
Loosen track pT cut to 250 MeV
Estimated retention using 100k events in 2016 data: 0.06%
'''

__author__ = ['Sean Benson','Adam Morris','Laurence Carson', 'Emmy Pauline Maria Gabriel', 'Markus Roehrken']
__date__ = '29/05/2019'
__version__ = '4.0'

__all__ = ( 'BsPhiRhoConf',
            'mkDiTrackList',
            'mkKKTrackList',
            'mkBs2PRKKhh' )

default_config = {
    'NAME'        : 'BsPhiRho',
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'BsPhiRhoConf',
    'CONFIG'      : {'PRPrescale'      : 1.,
                     'PRResMinPT'      : 900.,
                     'PRResMinP'       : 1.,
                     'PRResMinMass'    : 0.,
                     'PRResMaxMass'    : 4000.,
                     'PRResVtxChiDOF'  : 9.,
                     'PRBMinM'         : 4800.,
                     'PRBMaxM'         : 5600.,
                     'PRBVtxChi2DOF'   : 9.,
                     'PRIPCHI2'        : 20.,
                     'PRTrackChi2DOF'  : 4.,
                     'PRTrackGhostProb': 0.4,
                     'PRTrackMinPT'    : 250, # from StdLooseKaons
                     'PRTrackIPCHI2'   : 4., # from StdLooseKaons and StdLooseDetachedPhi2KK
                     },
    'STREAMS'     : ['BhadronCompleteEvent']
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions

name = "BsPhiRho"

class BsPhiRhoConf(LineBuilder) :
    """
    Builder for BsPhiRho
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        self.name = name
        LineBuilder.__init__(self, name, config)

        self.hlt1Filter = "( HLT_PASS_RE('Hlt1(Two)?TrackMVADecision') )"
        self.hlt2Filter = "( HLT_PASS_RE('Hlt2Topo[234]BodyDecision') | HLT_PASS_RE('Hlt2(Phi)?IncPhiDecision') )"
        _trkFilter = " & ".join(["(TRGHOSTPROB < {0})".format(config['PRTrackGhostProb']),
                                 "(TRCHI2DOF < {0})".format(config['PRTrackChi2DOF']),
                                 "(PT > {0}*MeV)".format(config['PRTrackMinPT']),
                                 "(MIPCHI2DV(PRIMARY) > {0})".format(config['PRTrackIPCHI2'])
                                ])

        self.TrackListhh = Selection( 'TrackList' + self.name,
                                       Algorithm = FilterDesktop(Code = _trkFilter),
                                       RequiredSelections = [StdNoPIDsPions])

        self.DiTrackList = mkDiTrackList( name="DiTracksForCharmlessB" + self.name,
                                          trkList=self.TrackListhh,
                                          MinPTCut = config['PRResMinPT'],
                                          MinPCut = config['PRResMinP'],
                                          MinMassCut = config['PRResMinMass'],
                                          MaxMassCut = config['PRResMaxMass'],
                                          VtxChi2DOFCut = config['PRResVtxChiDOF'] )

        self.KKTrackList = mkKKTrackList( name="KKTracksForCharmlessB"+ self.name,
                                          trkFilter = _trkFilter,
                                          MinPTCut = config['PRResMinPT'],
                                          MinPCut = config['PRResMinP'] )

        self.B2CharmlessPRKKhh = mkBs2PRKKhh( self.name,
                                              diTrkList=self.DiTrackList,
                                              diTrkListKK=self.KKTrackList,
                                              MinMassCut = config['PRBMinM'],
                                              MaxMassCut = config['PRBMaxM'],
                                              VtxChi2DOFCut = config['PRBVtxChi2DOF'],
                                              BIPchi2Cut = config['PRIPCHI2'])

        self.PRKKhhLine = StrippingLine( self.name+"Line",
                                         prescale = config['PRPrescale'],
                                         HLT1 = self.hlt1Filter,
                                         HLT2 = self.hlt2Filter,
                                         selection = self.B2CharmlessPRKKhh, EnableFlavourTagging = True,
                                         RelatedInfoTools = [ { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 0.8,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar08' },
                                                              { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 1.0,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar10' },
                                                              { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 1.3,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar13' },
                                                              { "Type"         : "RelInfoConeVariables",
                                                                "ConeAngle"    : 1.7,
                                                                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection" : self.TrackListhh,
                                                                "Location"     : 'P2ConeVar17' },
                                                              { "Type"         : "RelInfoVertexIsolation",
                                                                "Location"     : "VertexIsoInfo" } ] )

        self.registerLine(self.PRKKhhLine)

def mkDiTrackList( name,
                   trkList,
                   MinPTCut,
                   MinPCut,
                   MinMassCut,
                   MaxMassCut,
                   VtxChi2DOFCut ) :
    """
    Di-track selection
    """
    _diTrackPreVertexCuts = "(APT> %(MinPTCut)s *MeV) & (AP> %(MinPCut)s *GeV) & in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" % locals()
    _diTrackPostVertexCuts = "(VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s )"% locals()

    _combineDiTrack = CombineParticles( DecayDescriptor="rho(770)0 -> pi+ pi-",
                                        CombinationCut = _diTrackPreVertexCuts,
                                        MotherCut = _diTrackPostVertexCuts )

    return Selection(name,
                     Algorithm = _combineDiTrack,
                     RequiredSelections = [ trkList ] )

def mkKKTrackList( name,
                   trkFilter,
                   MinPTCut,
                   MinPCut ) :
    """
    KK selection
    """
    _code = "(PT> %(MinPTCut)s *MeV) & (P> %(MinPCut)s *GeV)" % locals()
    _code += " & CHILDCUT(%(trkFilter)s, 'phi(1020) -> ^K+ K-')" % locals()
    _code += " & CHILDCUT(%(trkFilter)s, 'phi(1020) -> K+ ^K-')" % locals()
    _stdPhi = DataOnDemand(Location="Phys/StdLooseDetachedPhi2KK/Particles")
    _phiFilter = FilterDesktop(Code = _code)

    return Selection( name,
                      Algorithm = _phiFilter,
                      RequiredSelections = [_stdPhi])

def mkBs2PRKKhh( name,
                 diTrkList,
                 diTrkListKK,
                 MinMassCut,
                 MaxMassCut,
                 VtxChi2DOFCut,
                 BIPchi2Cut) :
    """
    Bs to KKhh selection
    """
    _B2PRPreVertexCuts = "in_range( %(MinMassCut)s ,AM, %(MaxMassCut)s )" % locals()
    _B2PRPostVertexCuts = "(BPVDIRA > 0.9999) & (MIPCHI2DV(PRIMARY) < %(BIPchi2Cut)s) & (VFASPF(VCHI2/VDOF) < %(VtxChi2DOFCut)s )" % locals()

    _combineB2PR = CombineParticles( DecayDescriptor="B0 -> phi(1020) rho(770)0",
                                     MotherCut = _B2PRPostVertexCuts,
                                     CombinationCut = _B2PRPreVertexCuts )


    return Selection(name,
                     Algorithm = _combineB2PR,
                     RequiredSelections = [ diTrkList, diTrkListKK ])
