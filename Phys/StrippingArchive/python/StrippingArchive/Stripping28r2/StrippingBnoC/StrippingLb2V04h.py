###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Lb->V04h stripping Selections and StrippingLines.
Provides functions to build Lambda0->DD, Lambda0->LL, and Lambda0->LD selections.
Stripping20 with an inclusive approach for Lb->Lambda 4h modes.
Provides class Lb2V04hConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Selections based on previous version of the V0hh line by Thomas Latham, etc.
Exported symbols (use python help!):
   - Lb2V04hConf
"""

__author__ = ["Xiaokang Zhou", "Wenbin Qian"]
__date__ = '28/05/2019'
__version__ = 'Stripping28r2'
__all__ = 'Lb2V04hConf'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdNoPIDsPions as Pions

default_config = {
    'NAME': 'Lb2V04h',
    'WGs': ['BnoC'],
    'BUILDERTYPE': 'Lb2V04hConf',
    'CONFIG': {
        'Trk_Chi2':
        3.0,
        'Trk_GhostProb':
        0.5,
        'Lambda_DD_MassWindow':
        25.0,
        'Lambda_DD_VtxChi2':
        15.0,
        'Lambda_DD_FDChi2':
        50.0,
        'Lambda_DD_FD':
        300.0,
        'Lambda_DD_Pmin':
        2000.0,
        'Lambda_LL_MassWindow':
        20.0,
        'Lambda_LL_VtxChi2':
        15.0,
        'Lambda_LL_FDChi2':
        80.0,
        'Lambda_LD_MassWindow':
        25.0,
        'Lambda_LD_VtxChi2':
        20.0,
        'Lambda_LD_FDChi2':
        50.0,
        'Lambda_LD_FD':
        300.0,
        'Lambda_LD_Pmin':
        5000.0,
        'Lb_Mlow':
        1319.0,
        'Lb_Mhigh':
        600.0,
        'Lb_2bodyMlow':
        800.0,
        'Lb_2bodyMhigh':
        800.0,
        'Lb_APTmin':
        250.0,
        'Lb_PTmin':
        250.0,
        'LbDaug_MedPT_PT':
        800.0,
        'LbDaug_MaxPT_IP':
        0.05,
        'LbDaug_DD_maxDocaChi2':
        10.0,
        'LbDaug_LL_maxDocaChi2':
        10.0,
        'LbDaug_LD_maxDocaChi2':
        10.0,
        'LbDaug_DD_PTsum':
        1500.0,
        'LbDaug_LL_PTsum':
        1500.0,
        'LbDaug_LD_PTsum':
        1500.0,
        'Lbh_DD_PTMin':
        500.0,
        'Lbh_LL_PTMin':
        500.0,
        'Lbh_LD_PTMin':
        500.0,
        'Lb_VtxChi2':
        12.0,
        'Lb_DD_Dira':
        0.995,
        'Lb_LL_Dira':
        0.995,
        'Lb_LD_Dira':
        0.995,
        'Lb_DD_IPCHI2wrtPV':
        20.0,
        'Lb_LL_IPCHI2wrtPV':
        20.0,
        'Lb_LD_IPCHI2wrtPV':
        20.0,
        'Lb_FDwrtPV':
        1.0,
        'Lb_DD_FDChi2':
        30.0,
        'Lb_LL_FDChi2':
        30.0,
        'Lb_LD_FDChi2':
        30.0,
        'GEC_MaxTracks':
        250,
        # 2012 Triggers
        #'HLT1Dec'                  : 'Hlt1TrackAllL0Decision',
        #'HLT2Dec'                  : 'Hlt2Topo[234]Body.*Decision',
        # 2015 Triggers
        'HLT1Dec':
        'Hlt1(Two)?TrackMVADecision',
        'HLT2Dec':
        'Hlt2Topo[234]BodyDecision',
        'Prescale':
        1.0,
        'Postscale':
        1.0,
        'RelatedInfoTools':
        [{
            "Type": "RelInfoConeVariables",
            "ConeAngle": 1.7,
            "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
            "Location": 'ConeVar17'
        },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.5,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar15'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 1.0,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar10'
         },
         {
             "Type": "RelInfoConeVariables",
             "ConeAngle": 0.8,
             "Variables": ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
             "Location": 'ConeVar08'
         }, {
             "Type": "RelInfoVertexIsolation",
             "Location": "VtxIsolationVar"
         }]
    },
    'STREAMS': ['Bhadron']
}


class Lb2V04hConf(LineBuilder):
    """
    Builder of Lb->V0h(h) stripping Selection and StrippingLine.
    Constructs Lb -> V0 h+ h- h+ h- Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> lb2v04hConf = LB2V04hConf('Lb2V04hTest',config)
    >>> lb2v04hLines = lb2v04hConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selLambda2DD           : Lambda0 -> Down Down Selection object
    selLambda2LL           : Lambda0 -> Long Long Selection object
    selLambda2LD           : Lambda0 -> Long Down Selection object

    selLb2V0DD4h           : Lb -> Lambda0(DD) h+ h- h+ h- Selection object
    selLb2V0LL4h           : Lb -> Lambda0(LL) h+ h- h+ h- Selection object
    selLb2V0LD4h           : Lb -> Lambda0(LD) h+ h- h+ h- Selection object

    Lb_dd_line             : StrippingLine made out of selLb2V0DD4h
    Lb_ll_line             : StrippingLine made out of selLb2V0LL4h
    Lb_ld_line             : StrippingLine made out of selLb2V0LD4h

    lines                  : List of lines, [Lb_dd_line, Lb_ll_line, Lb_ld_line]

    Exports as class data member:
    Lb2V04hConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)

        GECCode = {
            'Code':
            "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" %
            config['GEC_MaxTracks'],
            'Preambulo': ["from LoKiTracks.decorators import *"]
        }

        self.hlt1Filter = {
            'Code': "HLT_PASS_RE('%s')" % config['HLT1Dec'],
            'Preambulo': ["from LoKiCore.functions import *"]
        }
        self.hlt2Filter = {
            'Code': "HLT_PASS_RE('%s')" % config['HLT2Dec'],
            'Preambulo': ["from LoKiCore.functions import *"]
        }

        self.pions = Pions

        self.makeLambda2DD('Lambda0DDLb2V04hLines', config)
        self.makeLambda2LL('Lambda0LLLb2V04hLines', config)
        self.makeLambda2LD('Lambda0LDLb2V04hLines', config)

        namesSelections = [
            (name + 'DD', self.makeLb2V0DD4h(name + 'DD', config)),
            (name + 'LL', self.makeLb2V0LL4h(name + 'LL', config)),
            (name + 'LD', self.makeLb2V0LD4h(name + 'LD', config)),
            (name + 'DDSS', self.makeLb2V0DD4h(name + 'DDSS', config)),
            (name + 'LLSS', self.makeLb2V0LL4h(name + 'LLSS', config)),
            (name + 'LDSS', self.makeLb2V0LD4h(name + 'LDSS', config)),
        ]

        # make lines

        for selName, sel in namesSelections:

            extra = {}

            if 'SS' in selName:
                extra['HLT1'] = self.hlt1Filter
                extra['HLT2'] = self.hlt2Filter

            line = StrippingLine(
                selName + 'Line',
                selection=sel,
                prescale=config['Prescale'],
                postscale=config['Postscale'],
                RelatedInfoTools=config['RelatedInfoTools'],
                FILTER=GECCode,
                **extra)

            self.registerLine(line)

    def makeLambda2DD(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_DD_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)   " % config['Lambda_DD_VtxChi2']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lambda_DD_FDChi2']
        _momCut = "(P>%s*MeV)" % config['Lambda_DD_Pmin']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config[
            'Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config[
            'Trk_GhostProb']

        _allCuts = _momCut
        _allCuts += '&' + _massCut
        _allCuts += '&' + _vtxCut
        _allCuts += '&' + _fdChi2Cut
        #_allCuts += '&'+_trkGhostProbCut1
        #_allCuts += '&'+_trkGhostProbCut2

        # get the Lambda0's to filter
        _stdLambdaDD = DataOnDemand(Location="Phys/StdLooseLambdaDD/Particles")

        # make the filter
        _filterLambdaDD = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2DD = Selection(
            name, Algorithm=_filterLambdaDD, RequiredSelections=[_stdLambdaDD])

        return self.selLambda2DD

    def makeLambda2LL(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_LL_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)" % config['Lambda_LL_VtxChi2']
        _trkChi2Cut1 = "(CHILDCUT((TRCHI2DOF<%s),1))" % config['Trk_Chi2']
        _trkChi2Cut2 = "(CHILDCUT((TRCHI2DOF<%s),2))" % config['Trk_Chi2']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config[
            'Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config[
            'Trk_GhostProb']

        _allCuts = _massCut
        _allCuts += '&' + _trkChi2Cut1
        _allCuts += '&' + _trkChi2Cut2
        _allCuts += '&' + _vtxCut
        _allCuts += '&' + _trkGhostProbCut1
        _allCuts += '&' + _trkGhostProbCut2

        # get the Lambda's to filter
        _stdLambdaLL = DataOnDemand(
            Location="Phys/StdVeryLooseLambdaLL/Particles")

        # make the filter
        _filterLambdaLL = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2LL = Selection(
            name, Algorithm=_filterLambdaLL, RequiredSelections=[_stdLambdaLL])

        return self.selLambda2LL

    def makeLambda2LD(self, name, config):
        # define all the cuts
        _massCut = "(ADMASS('Lambda0')<%s*MeV)" % config['Lambda_DD_MassWindow']
        _vtxCut = "(VFASPF(VCHI2)<%s)   " % config['Lambda_DD_VtxChi2']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lambda_DD_FDChi2']
        _momCut = "(P>%s*MeV)" % config['Lambda_DD_Pmin']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config[
            'Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config[
            'Trk_GhostProb']

        _allCuts = _momCut
        _allCuts += '&' + _massCut
        _allCuts += '&' + _vtxCut
        _allCuts += '&' + _fdChi2Cut
        #_allCuts += '&'+_trkGhostProbCut1
        #_allCuts += '&'+_trkGhostProbCut2

        # get the Lambda0's to filter
        _stdLambdaLD = DataOnDemand(Location="Phys/StdLooseLambdaLD/Particles")

        # make the filter
        _filterLambdaLD = FilterDesktop(Code=_allCuts)

        # make and store the Selection object
        self.selLambda2LD = Selection(
            name, Algorithm=_filterLambdaLD, RequiredSelections=[_stdLambdaLD])

        return self.selLambda2LD

    def makeLb2V0DD4h(self, name, config):
        """
        Create and store a Lb ->Lambda0(DD) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow = "(AM>(5619-%s)*MeV)" % config['Lb_Mlow']
        _massCutHigh = "(AM<(5619+%s)*MeV)" % config['Lb_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['Lb_APTmin']
        _daugMedPtCut = "(ANUM(PT>%s*MeV)>=2)" % config['LbDaug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config[
            'LbDaug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config[
            'LbDaug_DD_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3+APT4)>%s*MeV)" % config[
            'LbDaug_DD_PTsum']

        _combCuts = _aptCut
        #_combCuts += '&'+_daugPtSumCut
        _combCuts += '&' + _daugMedPtCut
        _combCuts += '&' + _massCutLow
        _combCuts += '&' + _massCutHigh
        #_combCuts += '&'+_daugMaxPtIPCut
        _combCuts += '&' + _maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['Lb_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['Lb_DD_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['Lb_DD_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lb_DD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&' + _vtxChi2Cut
        _motherCuts += '&' + _diraCut
        _motherCuts += '&' + _ipChi2Cut
        _motherCuts += '&' + _fdChi2Cut

        _Lb = CombineParticles()

        if 'SS' in name:  # Same sign

            _Lb.DecayDescriptors = [
                "Lambda_b0 -> pi+ pi+ pi+ pi+ Lambda0",
                "Lambda_b~0 -> pi+ pi+ pi+ pi+ Lambda~0",
                "Lambda_b0 -> pi+ pi- pi- pi- Lambda0",
                "Lambda_b~0 -> pi+ pi- pi- pi- Lambda~0",
                "Lambda_b0 -> pi+ pi+ pi+ pi- Lambda0",
                "Lambda_b~0 -> pi+ pi+ pi+ pi- Lambda~0",
                "Lambda_b0 -> pi- pi- pi- pi- Lambda0",
                "Lambda_b~0 -> pi- pi- pi- pi- Lambda~0"
            ]

        else:

            _Lb.DecayDescriptors = [
                "Lambda_b0 -> pi+ pi- pi+ pi- Lambda0",
                "Lambda_b~0 -> pi+ pi- pi+ pi- Lambda~0"
            ]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _Lb.DaughtersCuts = {"pi+": _daughtersCuts}
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True

        _LbConf = _Lb.configurable(name + '_combined')

        if 'SS' in name:  # Same sign
            self.selLb2V0DD4hSS = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2DD, self.pions])
            return self.selLb2V0DD4hSS
        else:
            self.selLb2V0DD4h = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2DD, self.pions])
            return self.selLb2V0DD4h

    def makeLb2V0LL4h(self, name, config):
        """
        Create and store a Lb -> Lambda0(LL) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow = "(AM>(5619-%s)*MeV)" % config['Lb_Mlow']
        _massCutHigh = "(AM<(5619+%s)*MeV)" % config['Lb_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['Lb_APTmin']
        _daugMedPtCut = "(ANUM(PT>%s*MeV)>=2)" % config['LbDaug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config[
            'LbDaug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config[
            'LbDaug_LL_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3+APT4)>%s*MeV)" % config[
            'LbDaug_LL_PTsum']

        _combCuts = _aptCut
        #_combCuts += '&'+_daugPtSumCut
        _combCuts += '&' + _daugMedPtCut
        _combCuts += '&' + _massCutLow
        _combCuts += '&' + _massCutHigh
        #_combCuts += '&'+_daugMaxPtIPCut
        _combCuts += '&' + _maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['Lb_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['Lb_LL_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['Lb_LL_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lb_LL_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&' + _vtxChi2Cut
        _motherCuts += '&' + _diraCut
        _motherCuts += '&' + _ipChi2Cut
        _motherCuts += '&' + _fdChi2Cut
        _motherCuts += '&' + _fdCut

        _Lb = CombineParticles()

        if 'SS' in name:  # Same sign

            _Lb.DecayDescriptors = [
                "Lambda_b0 -> pi+ pi+ pi+ pi+ Lambda0",
                "Lambda_b~0 -> pi+ pi+ pi+ pi+ Lambda~0",
                "Lambda_b0 -> pi+ pi- pi- pi- Lambda0",
                "Lambda_b~0 -> pi+ pi- pi- pi- Lambda~0",
                "Lambda_b0 -> pi+ pi+ pi+ pi- Lambda0",
                "Lambda_b~0 -> pi+ pi+ pi+ pi- Lambda~0",
                "Lambda_b0 -> pi- pi- pi- pi- Lambda0",
                "Lambda_b~0 -> pi- pi- pi- pi- Lambda~0"
            ]

        else:

            _Lb.DecayDescriptors = [
                "Lambda_b0 -> pi+ pi- pi+ pi- Lambda0",
                "Lambda_b~0 -> pi+ pi- pi+ pi- Lambda~0"
            ]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _Lb.DaughtersCuts = {"pi+": _daughtersCuts}
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True

        _LbConf = _Lb.configurable(name + '_combined')

        if 'SS' in name:  # Same sign
            self.selLb2V0LL4hSS = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2LL, self.pions])
            return self.selLb2V0LL4hSS
        else:
            self.selLb2V0LL4h = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2LL, self.pions])
            return self.selLb2V0LL4h

    def makeLb2V0LD4h(self, name, config):
        """
        Create and store a Lb ->Lambda0(LD) p+ h- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow = "(AM>(5619-%s)*MeV)" % config['Lb_Mlow']
        _massCutHigh = "(AM<(5619+%s)*MeV)" % config['Lb_Mhigh']
        _aptCut = "(APT>%s*MeV)" % config['Lb_APTmin']
        _daugMedPtCut = "(ANUM(PT>%s*MeV)>=2)" % config['LbDaug_MedPT_PT']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config[
            'LbDaug_MaxPT_IP']
        _maxDocaChi2Cut = "(ACUTDOCACHI2(%s,''))" % config[
            'LbDaug_LD_maxDocaChi2']
        _daugPtSumCut = "((APT1+APT2+APT3+APT4)>%s*MeV)" % config[
            'LbDaug_LD_PTsum']

        _combCuts = _aptCut
        #_combCuts += '&'+_daugPtSumCut
        _combCuts += '&' + _daugMedPtCut
        _combCuts += '&' + _massCutLow
        _combCuts += '&' + _massCutHigh
        #_combCuts += '&'+_daugMaxPtIPCut
        _combCuts += '&' + _maxDocaChi2Cut

        _ptCut = "(PT>%s*MeV)" % config['Lb_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)" % config['Lb_VtxChi2']
        _diraCut = "(BPVDIRA>%s)" % config['Lb_LD_Dira']
        _ipChi2Cut = "(MIPCHI2DV(PRIMARY)<%s)" % config['Lb_LD_IPCHI2wrtPV']
        _fdCut = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['Lb_FDwrtPV']
        _fdChi2Cut = "(BPVVDCHI2>%s)" % config['Lb_LD_FDChi2']

        _motherCuts = _ptCut
        _motherCuts += '&' + _vtxChi2Cut
        _motherCuts += '&' + _diraCut
        _motherCuts += '&' + _ipChi2Cut
        _motherCuts += '&' + _fdChi2Cut

        _Lb = CombineParticles()

        if 'SS' in name:  # Same sign

            _Lb.DecayDescriptors = [
                "Lambda_b0 -> pi+ pi+ pi+ pi+ Lambda0",
                "Lambda_b~0 -> pi+ pi+ pi+ pi+ Lambda~0",
                "Lambda_b0 -> pi+ pi- pi- pi- Lambda0",
                "Lambda_b~0 -> pi+ pi- pi- pi- Lambda~0",
                "Lambda_b0 -> pi+ pi+ pi+ pi- Lambda0",
                "Lambda_b~0 -> pi+ pi+ pi+ pi- Lambda~0",
                "Lambda_b0 -> pi- pi- pi- pi- Lambda0",
                "Lambda_b~0 -> pi- pi- pi- pi- Lambda~0"
            ]

        else:

            _Lb.DecayDescriptors = [
                "Lambda_b0 -> pi+ pi- pi+ pi- Lambda0",
                "Lambda_b~0 -> pi+ pi- pi+ pi- Lambda~0"
            ]

        _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']
        _trkChi2Cut = "(TRCHI2DOF<%s)" % config['Trk_Chi2']

        _daughtersCuts = _trkChi2Cut + '&' + _trkGhostProbCut

        _Lb.DaughtersCuts = {"pi+": _daughtersCuts}
        _Lb.CombinationCut = _combCuts
        _Lb.MotherCut = _motherCuts
        _Lb.ReFitPVs = True

        _LbConf = _Lb.configurable(name + '_combined')

        if 'SS' in name:  # Same sign
            self.selLb2V0LD4hSS = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2LD, self.pions])
            return self.selLb2V0LD4hSS
        else:
            self.selLb2V0LD4h = Selection(
                name,
                Algorithm=_LbConf,
                RequiredSelections=[self.selLambda2LD, self.pions])
            return self.selLb2V0LD4h
