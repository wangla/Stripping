###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting converted photons
'''

__author__ = ['Tom Boettcher']
__date__ = ''
__version__ = '$Revision: 1.0 $'

__all__ = (
    'HeavyIonConvertedPhotonConf',
    'default_config'
    )

from Gaudi.Configuration import *
from CommonParticles.Utils import *
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from Configurables import ( DiElectronMaker,
                            ProtoParticleCALOFilter,
                            ParticleTransporter,
                            LoKi__VertexFitter,
                            )
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from PhysSelPython.Wrappers import Selection
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME' : 'HeavyIonConvertedPhoton',
    'WGs' : ['IFT'],
    'STREAMS' : ['IFT'],
    'BUILDERTYPE' : 'HeavyIonConvertedPhotonConf',
    'CONFIG' : {
        'odin' : ['NoBeam', 'Beam1', 'Beam2', 'BeamCrossing'],

        'LLNoBrem' : {
            'Hlt1Filter' : None,
            #'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision')",
            'Hlt2Filter' : None,
            'Prescale' : 1.0,
            'TrackType' : 'Long',
            'AddBrem' : False,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 80.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0#,
            #'MaxPointing' : 0.03
            },

        'DDNoBrem' : {
            'Hlt1Filter' : None,
            #'Hlt2Filter' : "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')",
            'Hlt2Filter' : None,
            'Prescale' : 1.0,
            'TrackType' : 'Downstream',
            'AddBrem' : False,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0#,
            #'MaxPointing' : 0.03
            }
        }
    }

class HeavyIonConvertedPhotonConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])

        #================================
        # Lines with analysis level cuts

        # No Brem
        self.LLNoBremLine = StrippingLine(
            name = 'ConvPhotonLLNoBrem',
            prescale = self.config['LLNoBrem']['Prescale'],
            HLT1 = self.config['LLNoBrem']['Hlt1Filter'],
            HLT2 = self.config['LLNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonLLNoBrem',config['LLNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.LLNoBremLine)

        self.DDNoBremLine = StrippingLine(
            name = 'ConvPhotonDDNoBrem',
            prescale = self.config['DDNoBrem']['Prescale'],
            HLT1 = self.config['DDNoBrem']['Hlt1Filter'],
            HLT2 = self.config['DDNoBrem']['Hlt2Filter'],
            selection = self.makeConvertedPhoton('SelConvPhotonDDNoBrem',config['DDNoBrem']),
            ODIN = odin,
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
            )
        self.registerLine(self.DDNoBremLine)            

    #==============================
    # Make selections for each line
    def makeConvertedPhoton(self, name, config):
        alg = DiElectronMaker(name+'Maker')
        alg.DecayDescriptor = 'gamma -> e+ e-'
        selector = trackSelector(alg, trackTypes=[config['TrackType']])
        alg.addTool(ProtoParticleCALOFilter, name='Electron')
        alg.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'%s'"%config['MinPIDe']]
        alg.DeltaY = 3.0
        alg.DeltaYmax = 200.0*mm
        alg.DiElectronMassMax = config['MaxMass']
        alg.DiElectronPtMin = 200.0*MeV
        alg.AddBrem = config['AddBrem']
        
        # Extra setup for DD pairs
        if config['TrackType'] == 'Downstream':
            alg.ParticleCombiners.update({"" : "LoKi::VertexFitter"})
            alg.addTool(LoKi__VertexFitter)
            alg.LoKi__VertexFitter.addTool(ParticleTransporter, name='Transporter')
            alg.LoKi__VertexFitter.Transporter.TrackExtrapolator = "TrackRungeKuttaExtrapolator"
            alg.LoKi__VertexFitter.DeltaDistance = 100*mm

        code = ("(BPVVDZ > 0)"
                " & (BPVIPCHI2() < %s)"
                " & (PT > %s)"
                " & (PT < %s)"
                %(config['MaxIPChi2'], config['MinPt'], config['MaxPt']))
        print code
        preSel = Selection('Pre'+name, Algorithm=alg)
        sel = Selection(name,
                        Algorithm=FilterDesktop(Code=code),
                        RequiredSelections=[preSel])
        
        return sel
