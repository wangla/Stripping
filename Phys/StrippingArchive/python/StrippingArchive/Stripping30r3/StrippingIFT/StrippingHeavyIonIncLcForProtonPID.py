###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Select a Lambda_c -> pKpi signal without a PID
requirement on the proton.
Following PIDCalib Lc from B line and PIDCalib Inc Lc, with relaxed selections
"""
__author__ = ['IFT']
__date__ = '29/03/2018'
__version__ = '$Revision: 0.0 $'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdLooseProtons, StdNoPIDsProtons

__all__ = ('HeavyIonIncLcForProtonPIDConf',
           'default_config')

default_config = {
    'NAME'        : 'HeavyIonIncLcForProtonPID',
    'WGs'         : ['IFT'],
    "STREAMS"     :["IFT"],
    'BUILDERTYPE' : 'HeavyIonIncLcForProtonPIDConf',
    'CONFIG'      : {
        "prescale"              : 1 # adimensional
        ,"MAXGHOSTPROB"         : 0.35 # adimensional
        ,"Pi_PIDKMax"           : -2 # adimensional
        ,"K_PIDKMin"            : 8 # adimensional
        ,"H_PT"                 : 300 # MeV
        ,"proton_P"             : 6000 # MeV
        ,"proton_MinIPCHI2"     : 9 #adimensional
        ,"LambdaC_MASSWIN"      : 80 # MeV
        ,"LambdaC_PTSUM"        : 2000 # MeV
        ,"LambdaC_DOCACHI2_MAX" : 6 # adimensional
        ,"LambdaC_VCHI2NDF"     : 4 # adimensional
        ,"LambdaC_FDCHI2"       : 20 # adimensional
        ,"LambdaC_DIRA"         : 0.9999 # adimensional
        },
}
        
class HeavyIonIncLcForProtonPIDConf(LineBuilder) :
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    __confdict__={}
        
    def __init__(self, _name, config) :

        LineBuilder.__init__(self, _name, config)
        self.__confdict__=config

        #### define the cuts for the Lambda_c daughter tracks
       #   'K+'    : '( PT>250*MeV ) & ( P>2*GeV ) & ( TRPCHI2>0.0001 ) & ( MIPCHI2DV(PRIMARY)>8. ) ' 
        self.PionCuts   = "(TRGHOSTPROB < %(MAXGHOSTPROB)s) & (PIDK < %(Pi_PIDKMax)s) & (PT > %(H_PT)s *MeV)" % self.__confdict__ 
        self.KaonCuts   = "(TRGHOSTPROB < %(MAXGHOSTPROB)s) & (PIDK > %(K_PIDKMin)s)  & (PT > %(H_PT)s *MeV)" % self.__confdict__ 
        self.ProtonCuts = "(P > %(proton_P)s *MeV) & (PT > %(H_PT)s *MeV)  & (MIPCHI2DV(PRIMARY)>%(proton_MinIPCHI2)s)" % self.__confdict__   # put ghost rejection requirement offline

        #### Define the combination cuts 
        self.LambdaC_CombinationCut = "(ADAMASS('Lambda_c+') < %(LambdaC_MASSWIN)s *MeV)"\
                              "& (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3) > %(LambdaC_PTSUM)s *MeV)"\
                              "& (ADOCACHI2CUT(%(LambdaC_DOCACHI2_MAX)s, ''))" % self.__confdict__

        self.LambdaC_MotherCut = "(ADMASS('Lambda_c+') < %(LambdaC_MASSWIN)s *MeV)"\
                                 "& (VFASPF(VCHI2/VDOF) < %(LambdaC_VCHI2NDF)s) " \
                                 "& (BPVVDCHI2 > %(LambdaC_FDCHI2)s)"\
                                 "& ( MIPCHI2DV(PRIMARY)<16.)  "\
                                 "& (BPVDIRA> %(LambdaC_DIRA)s)"\
                                 "& ( (WM( 'K-' , 'pi+' , 'pi+' )>1.89*GeV) | (WM( 'K-' , 'pi+' , 'pi+' )<1.80*GeV) )"% self.__confdict__ 

        #### make the Lambda_c -> p K pi 
        
        self.comb_Lc2PKPi = CombineParticles(DecayDescriptors = ['[Lambda_c+ -> K- p+ pi+]cc'],
                                             DaughtersCuts = { "pi+" : self.PionCuts,
                                                               "K-"  : self.KaonCuts,
                                                               "p+"  : self.ProtonCuts},
                                             CombinationCut = self.LambdaC_CombinationCut,
                                             MotherCut = self.LambdaC_MotherCut)
        
        self.sel_Lc2PKPi = Selection(name="SelLc2PKPiFor"+_name,
                                     Algorithm = self.comb_Lc2PKPi,
                                     RequiredSelections = [StdLoosePions,StdLooseKaons,StdNoPIDsProtons])

        #### make the line
        self.StrippingLine = StrippingLine(_name+'Line',
                                           prescale = config['prescale'],
                                           selection = self.sel_Lc2PKPi,
                                           RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"]
                                           )
        self.registerLine(self.StrippingLine)

## the end
