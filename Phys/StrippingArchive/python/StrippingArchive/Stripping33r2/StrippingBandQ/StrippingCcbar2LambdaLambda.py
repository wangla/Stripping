###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Ccbar->LambdaLambda detached line, with loose PT, PID cuts. 
'''

__author__=['Andrii Usachov']
__date__ = '27/01/2019'

__all__ = (
    'Ccbar2LambdaLambdaConf'
    )

default_config = {
    'NAME'        : 'Ccbar2LambdaLambda', 
    'BUILDERTYPE' : 'Ccbar2LambdaLambdaConf',
    'CONFIG' : {
        'TRCHI2DOF'        :  5.   ,
        'TRIPCHI2'         :  4.   , 
        'ProtonProbNNp'    :  0.3  ,
        'ProtonP'          :  5000 ,
        'ProtonPTSec'      :  250. , # MeV
        'PionProbNNpi'     :  0.1  ,
        'PionPTSec'        :  250. , # MeV
        'LambdaMassW'      :  30 ,
        'LambdaVCHI2DOF'   :  9 ,
        'CombMaxMass'      :  15100., # MeV, before Vtx fit
        'CombMinMass'      :  2700., # MeV, before Vtx fit
        'MaxMass'          :  15000., # MeV, after Vtx fit
        'MinMass'          :  2750. # MeV, after Vtx fit
        },
    'STREAMS' : [ 'Charm'],
    'WGs'     : [ 'BandQ']
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


from StandardParticles import StdNoPIDsProtons
from StandardParticles import StdLoosePions, StdNoPIDsDownPions
from StandardParticles import StdAllLoosePions


class Ccbar2LambdaLambdaConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        """
        Lambda 
        """
        self.LambdaLLForJpsi = self.createSubSel( OutputList = "LambdaLLFor" + self.name,
                                                  InputList =  DataOnDemand( Location = 'Phys/StdLooseLambdaLL/Particles' ), 
                                                  Cuts = "(VFASPF(VCHI2/VDOF)< %(LambdaVCHI2DOF)s)"\
                                                  " & (ADMASS('Lambda0')<%(LambdaMassW)s *MeV)"\
                                                  " & (INTREE( ('pi+'==ABSID) & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNpi> %(PionProbNNpi)s) & (PT> %(PionPTSec)s*MeV)))"\
                                                  " & (INTREE( ('p+'==ABSID)  & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s) & (PT> %(ProtonPTSec)s*MeV)))"\
                                                  " & (ADWM( 'KS0' , WM( 'pi+' , 'pi-') ) > 20*MeV ) & (BPVDLS>5)" % self.config )
        
        self.LambdaDDForJpsi = self.createSubSel( OutputList = "LambdaDDFor" + self.name,
                                                  InputList =  DataOnDemand( Location = 'Phys/StdLooseLambdaDD/Particles' ), 
                                                  Cuts = "(VFASPF(VCHI2/VDOF)< %(LambdaVCHI2DOF)s)"\
                                                  " & (ADMASS('Lambda0')< %(LambdaMassW)s *MeV)"\
                                                  " & (INTREE( ('pi+'==ABSID) & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNpi> %(PionProbNNpi)s) & (PT> %(PionPTSec)s*MeV)))"\
                                                  " & (INTREE( ('p+'==ABSID)  & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s) & (PT> %(ProtonPTSec)s*MeV)))"\
                                                  " & (ADWM( 'KS0' , WM( 'pi+' , 'pi-') ) > 20*MeV ) & (BPVDLS>5)" % self.config )
        
        self.LambdaLDForJpsi = self.createSubSel( OutputList = "LambdaLDFor" + self.name,
                                                  InputList =  DataOnDemand( Location = 'Phys/StdLooseLambdaLD/Particles' ),
                                                  Cuts = "(VFASPF(VCHI2/VDOF)< %(LambdaVCHI2DOF)s)"\
                                                  " & (ADMASS('Lambda0')< %(LambdaMassW)s *MeV)"\
                                                  " & (INTREE( ('pi+'==ABSID) & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNpi> %(PionProbNNpi)s) & (PT> %(PionPTSec)s*MeV)))"\
                                                  " & (INTREE( ('p+'==ABSID)  & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s) & (PT> %(ProtonPTSec)s*MeV)))"\
                                                  " & (ADWM( 'KS0' , WM( 'pi+' , 'pi-') ) > 20*MeV ) & (BPVDLS>5)" % self.config )

        self.LambdaForJpsiList = MergedSelection("MergedLambdaForJpsi" + self.name,
                                                 RequiredSelections =  [ self.LambdaLLForJpsi,
                                                                         self.LambdaDDForJpsi, 
									 self.LambdaLDForJpsi
                                                                       ])


        self.makeJpsi2LambdaLambda()

        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "AALL",
                              PostVertexCuts = "ALL",
                              reFitPVs = True) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = reFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
        
    def makeJpsi2LambdaLambda(self):
        Jpsi2LambdaLambda = self.createCombinationSel( OutputList = "Jpsi2LambdaLambda" + self.name,
                                                       DecayDescriptor = "J/psi(1S) -> Lambda0 Lambda~0",
                                                       DaughterLists = [ self.LambdaForJpsiList],
                                                       PreVertexCuts = "(in_range( %(CombMinMass)s *MeV, AM, %(CombMaxMass)s *MeV))" % self.config,
                                                       PostVertexCuts = "(in_range( %(MinMass)s *MeV, MM, %(MaxMass)s *MeV)) & (VFASPF(VCHI2PDOF) < 9 )" %self.config )
                                                        
        Jpsi2LambdaLambdaLine = StrippingLine( self.name + "Line",
                                                 algos = [ Jpsi2LambdaLambda ] )
        self.registerLine(Jpsi2LambdaLambdaLine)




