###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Ccbar->LambdaLambda detached line, with loose PT, PID cuts. 
********
Updated for run2 re-stripping
A new line named 'Jpsi2LooseLambdaLambdaLine' 
which is made up by Lambda0 anti-Lambda0 from basic StdAllLoose and StdNoPIDsDown pion/proton combination 
Apply very loose P/PT cuts on proton and pions
'''

__author__=['Andrii Usachov', 'Jinlin Fu', 'Ziyi Wang']
__date__ = '20/06/2021'
__version__ = '$Revision: 0.1 $'

__all__ = (
    'Ccbar2LambdaLambdaConf'
           ,'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond


from StandardParticles import StdNoPIDsProtons
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdNoPIDsDownProtons
from StandardParticles import StdAllLoosePions,  StdAllLooseProtons



default_name='Ccbar2LambdaLambda'
default_config = {
    'NAME'        : 'Ccbar2LambdaLambda', 
    'BUILDERTYPE' : 'Ccbar2LambdaLambdaConf',
    'CONFIG' : {
          'TRCHI2DOF'               :     5.
        , 'TRIPCHI2'                :     4.
        , 'ProtonProbNNp'           :     0.3
        , 'ProtonPTSec'             :   250.  # MeV
        , 'PionProbNNpi'            :     0.1 
        , 'PionPTSec'               :   250.  # MeV
        , 'LambdaMassW'             :    30   
        , 'LambdaVCHI2DOF'          :     9   
        , 'CombMaxMass'             : 15100.  # MeV, before Vtx fit
        , 'CombMinMass'             :  2700.  # MeV, before Vtx fit
        , 'MaxMass'                 : 15000.  # MeV, after Vtx fit
        , 'MinMass'                 :  2750.  # MeV, after Vtx fit
        , 'Lambda0_MassWindowLarge' :   180   #MeV
        , 'Lambda0_APT'             :   700   #MeV
        , 'Lambda0_ENDVERTEXCHI2'   :    10
        , 'LambdaBPVDLSMAX'         :    20
        , 'TRCHI2DOFMax'            :     3.0
        , 'TrGhostProbMax'          :     0.5
        , 'MINIPCHI2'               :     4.0
        , 'PionP'                   :     2.0*GeV
        , 'PionPT'                  :   100.0*MeV
        , 'PionPIDK'                :    10.0
        , 'ProtonP'                 :     2.0*GeV
        , 'ProtonPT'                :   100.0*MeV
        , 'Proton_PIDpPIDpi_Min'    :     5.0
        , 'Proton_PIDpPIDK_Min'     :     0.0
        , 'Jpsi_ENDVERTEX_Z'        :   200*mm
        , 'Jpsi_BPVIPCHI2'          :    25.0
        },
    'STREAMS' : [ 'Charm'],
    'WGs'     : [ 'BandQ']
    }

class Ccbar2LambdaLambdaConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        """
        Lambda 
        """
        self.LambdaLLForJpsi = self.createSubSel( OutputList = "LambdaLLFor" + self.name,
                InputList =  DataOnDemand( Location = 'Phys/StdLooseLambdaLL/Particles' ), 
                Cuts = "(VFASPF(VCHI2/VDOF)< %(LambdaVCHI2DOF)s)"\
                        " & (ADMASS('Lambda0')<%(LambdaMassW)s *MeV)"\
                        " & (INTREE( ('pi+'==ABSID) & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNpi> %(PionProbNNpi)s) & (PT> %(PionPTSec)s*MeV)))"\
                        " & (INTREE( ('p+'==ABSID)  & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s) & (PT> %(ProtonPTSec)s*MeV)))"\
                        " & (ADWM( 'KS0' , WM( 'pi+' , 'pi-') ) > 20*MeV ) "\
                        " & (BPVDLS>5)" % self.config )

        self.LambdaDDForJpsi = self.createSubSel( OutputList = "LambdaDDFor" + self.name,
                InputList =  DataOnDemand( Location = 'Phys/StdLooseLambdaDD/Particles' ), 
                Cuts = "(VFASPF(VCHI2/VDOF)< %(LambdaVCHI2DOF)s)"\
                        " & (ADMASS('Lambda0')< %(LambdaMassW)s *MeV)"\
                        " & (INTREE( ('pi+'==ABSID) & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNpi> %(PionProbNNpi)s) & (PT> %(PionPTSec)s*MeV)))"\
                        " & (INTREE( ('p+'==ABSID)  & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s) & (PT> %(ProtonPTSec)s*MeV)))"\
                        " & (ADWM( 'KS0' , WM( 'pi+' , 'pi-') ) > 20*MeV )"\
                        " & (BPVDLS>5)" % self.config )

        self.LambdaLDForJpsi = self.createSubSel( OutputList = "LambdaLDFor" + self.name,
                InputList =  DataOnDemand( Location = 'Phys/StdLooseLambdaLD/Particles' ),
                Cuts = "(VFASPF(VCHI2/VDOF)< %(LambdaVCHI2DOF)s)"\
                        " & (ADMASS('Lambda0')< %(LambdaMassW)s *MeV)"\
                        " & (INTREE( ('pi+'==ABSID) & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNpi> %(PionProbNNpi)s) & (PT> %(PionPTSec)s*MeV)))"\
                        " & (INTREE( ('p+'==ABSID)  & (MIPCHI2DV(PRIMARY) > %(TRIPCHI2)s) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s) & (PT> %(ProtonPTSec)s*MeV)))"\
                        " & (ADWM( 'KS0' , WM( 'pi+' , 'pi-') ) > 20*MeV )"\
                        " & (BPVDLS>5)" % self.config )

        """
        Make basic Lambda from StdAllLoose and StdNoPIDsDown Pion/Proton
        """ 
        self.selLongPion = Selection( "SelLongPionfor" + name,
                Algorithm = self._longpionFilter("LongPionfor"+name),
                RequiredSelections = [StdAllLoosePions])

        self.selLongProton = Selection( "SelLongProtonfor" + name,
                Algorithm = self._longprotonFilter("LongProtonfor"+name),
                RequiredSelections = [StdAllLooseProtons])

        self.selDownPion = Selection( "SelDownPionfor" + name,
                Algorithm = self._downpionFilter("DownPionfor"+name),
                RequiredSelections = [StdNoPIDsDownPions])

        self.selDownProton = Selection( "SelDownProtonfor" + name,
                Algorithm = self._downprotonFilter("DownProtonfor"+name),
                RequiredSelections = [StdNoPIDsDownProtons])

        self.LooseLambdaLL = self.createCombinationSel( OutputList = "LooseLambdaLL"+ self.name,
                DecayDescriptor = "[Lambda0 -> p+ pi-]cc",
                DaughterLists   = [self.selLongProton, self.selLongPion],
                PreVertexCuts   = "( (ADAMASS('Lambda0') < %(Lambda0_MassWindowLarge)s *MeV)" \
                        "& (APT>%(Lambda0_APT)s*MeV) )" % self.config,
                PostVertexCuts  = "(VFASPF(VCHI2/VDOF)<%(Lambda0_ENDVERTEXCHI2)s)" \
                        "& (ADMASS('Lambda0') < %(LambdaMassW)s *MeV)" % self.config )

        self.LooseLambdaDD = self.createCombinationSel( OutputList = "LooseLambdaDD"+ self.name,
                DecayDescriptor = "[Lambda0 -> p+ pi-]cc",
                DaughterLists   = [self.selDownProton, self.selDownPion],
                PreVertexCuts   = "( (ADAMASS('Lambda0') < %(Lambda0_MassWindowLarge)s *MeV )" \
                        "& (APT>%(Lambda0_APT)s*MeV) )" % self.config,
                PostVertexCuts  = "(VFASPF(VCHI2/VDOF)<%(Lambda0_ENDVERTEXCHI2)s)" \
                        "& (ADMASS('Lambda0') < %(LambdaMassW)s *MeV)" % self.config )

        """
        MakeLambdaList
        """
        self.LambdaForJpsiList = MergedSelection("MergedLambdaForJpsi" + self.name,
                                                 RequiredSelections =  [ self.LambdaLLForJpsi,
                                                                         self.LambdaDDForJpsi, 
									                                     self.LambdaLDForJpsi ])
        self.LooseLambdaForJpsiList = MergedSelection("MergedLooseLambdaForJpsi" + self.name,
                                                 RequiredSelections =  [ self.LooseLambdaLL,
                                                                         self.LooseLambdaDD])

        self.makeJpsi2LambdaLambda()
        self.makeJpsi2LooseLambdaLambda()

        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "AALL",
                              PostVertexCuts = "ALL",
                              reFitPVs = True) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = reFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
        
    def makeJpsi2LambdaLambda(self):
        Jpsi2LambdaLambda = self.createCombinationSel( OutputList = "Jpsi2LambdaLambda" + self.name,
                DecayDescriptor = "J/psi(1S) -> Lambda0 Lambda~0",
                DaughterLists = [ self.LambdaForJpsiList],
                PreVertexCuts = "(in_range( %(CombMinMass)s *MeV, AM, %(CombMaxMass)s *MeV))" % self.config,
                PostVertexCuts = "(in_range( %(MinMass)s *MeV, MM, %(MaxMass)s *MeV)) & (VFASPF(VCHI2PDOF) < 9 )" %self.config )
        Jpsi2LambdaLambdaLine = StrippingLine( self.name + "Jpsi2LambdaLambdaLine",
                algos = [ Jpsi2LambdaLambda ] )
        self.registerLine(Jpsi2LambdaLambdaLine)

    def makeJpsi2LooseLambdaLambda(self):
        Jpsi2LooseLambdaLambda = self.createCombinationSel( OutputList = "Jpsi2LooseLambdaLambda" + self.name,
                DecayDescriptor = "J/psi(1S) -> Lambda0 Lambda~0",
                DaughterLists = [ self.LooseLambdaForJpsiList],
                PreVertexCuts = "(in_range( %(CombMinMass)s *MeV, AM, %(CombMaxMass)s *MeV))" % self.config,
                PostVertexCuts = "(in_range( %(MinMass)s *MeV, MM, %(MaxMass)s *MeV))"\
                        "& (VFASPF(VCHI2PDOF) < 9 )"\
                        "& (BPVIPCHI2() < %(Jpsi_BPVIPCHI2)s)"\
                        "& (abs(VFASPF(VZ)) < %(Jpsi_ENDVERTEX_Z)s )" %self.config )
        Jpsi2LooseLambdaLambdaLine = StrippingLine( self.name + "Jpsi2LooseLambdaLambdaLine",
                algos = [ Jpsi2LooseLambdaLambda ] )
        self.registerLine(Jpsi2LooseLambdaLambdaLine)

    def _longpionFilter( self , _name):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(PionP)s)"\
                "& (PT > %(PionPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK< %(PionPIDK)s)" % self.config
        _pi = FilterDesktop(Code = _code )
        return _pi

    def _downpionFilter( self , _name):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(PionP)s)"\
                "& (PT > %(PionPT)s)"\
                "& (PIDK< %(PionPIDK)s)" % self.config
        _pi = FilterDesktop(Code = _code )
        return _pi

    def _longprotonFilter( self, _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (PT > %(ProtonPT)s)"\
                "& (P>%(ProtonP)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (HASRICH)&(PIDp-PIDpi>%(Proton_PIDpPIDpi_Min)s)"\
                "& (HASRICH)&(PIDp-PIDK>%(Proton_PIDpPIDK_Min)s)"% self.config
        _pr = FilterDesktop(Code = _code)
        return _pr

    def _downprotonFilter( self, _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (PT > %(ProtonPT)s)"\
                "& (P>%(ProtonP)s)"\
                "& (HASRICH)&(PIDp-PIDpi>%(Proton_PIDpPIDpi_Min)s)"\
                "& (HASRICH)&(PIDp-PIDK>%(Proton_PIDpPIDK_Min)s)"% self.config
        _pr = FilterDesktop(Code = _code)
        return _pr
