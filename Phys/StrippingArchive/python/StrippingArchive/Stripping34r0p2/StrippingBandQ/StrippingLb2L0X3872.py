###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
StrippingLine for Lambda_b0 -> Lambda0 X(3872) and Lambda_b0 -> Lambda0 Psi(2S)
'''
__author__  = 'Xijun Wang'
__date__    = '2021-3-18'
__version__ = 'Stripping28'

__all__ = (
            'Lb2L0X3872Conf'            ,
            'default_config'            
            )

default_config = {
        'NAME'              :   'Lb2L0X3872',
        'BUILDERTYPE'       :   'Lb2L0X3872Conf',
        'CONFIG'    : {
            'PionCuts_X'    :   "(TRCHI2DOF < 4.0) & (PT > 10.*MeV) & (PROBNNpi > 0.2) & ( MIPCHI2DV(PRIMARY) >4)",
            'LooseJpsiCuts' :   "(MINTREE('mu+'==ABSID,PT) > 500.0 *MeV) & (MM > 2996.916) & (MM < 3196.916) & ((BPVDLS>2.5) | (BPVDLS<-2.5)) & (MINTREE('mu+'==ABSID,PIDmu) > 0.0)  & (VFASPF(VCHI2) < 25.)",
            'JpsiComCuts'   :   "(in_range(2.5*GeV, AM, 4.0*GeV))",
            'JpsiMomCuts'   :   "(VFASPF(VCHI2) < 25.)",
            'XComCuts'      :   "((AM > 3200*MeV) & (AM < 5000.*MeV))",
            'XMomCuts'      :   "(VFASPF(VCHI2) < 100.)",
            'LbComCuts'     :   "((AM > 5000.*MeV) & (AM < 6200.*MeV))",
            'LbMomCuts'     :   "(VFASPF(VCHI2) < 25.)",
            'L0Cuts'	    :	"(MAXTREE('p+'==ABSID, PT) > 10.*MeV) & (MAXTREE('pi-'==ABSID, PT) > 10.*MeV) & (ADMASS('Lambda0') < 50.*MeV) & (VFASPF(VCHI2) < 25 )",
            'Prescale'      :   1.0 ,
            },
        'STREAMS'           :   ['Dimuon'],
        'WGs'               :   ['BandQ']
        }
from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


class Lb2L0X3872Conf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__( self, name, config ):

        LineBuilder.__init__( self, name, config )
        self.name = name
        self.config = config


        self.SelPions_X = self.createSubSel( OutputList = self.name + "SelPions_X",
                                             InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles' ),
                                             Cuts = config['PionCuts_X']
                                             )


        """
        J/psi -> mu+ mu-
        """

        self.SelJpsi2Mumu = self.createSubSel( OutputList = self.name + "SelJpsiMumu",
                                               InputList = DataOnDemand(Location = 'Phys/StdLooseJpsi2MuMu/Particles'),
                                               Cuts = config['LooseJpsiCuts'])

        """
        Psi(2S) -> J/psi(1S) pi+ pi-
        """
        self.SelX2JpsiPiPi = self.createCombinationSel( OutputList = self.name + "SelX2JpsiPiPi",
                                                        DecayDescriptor = "psi(2S) -> J/psi(1S) pi+ pi-",
                                                        DaughterLists = [ self.SelPions_X, self.SelJpsi2Mumu ],
                                                        PreVertexCuts = config['XComCuts'],
                                                        PostVertexCuts = config['XMomCuts']
                                                        )
        """
        Lambda0 -> p+ pi-
        """
	from PhysSelPython.Wrappers import MergedSelection
	self.InputL0 = MergedSelection( self.name + "InputL0",
           			        RequiredSelections = [DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles"),
                                                              DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")]  )
	self.SelL0 = self.createSubSel( OutputList = self.name + "SelL0",
				        InputList = self.InputL0,
				        Cuts = config['L0Cuts']
				        )
	

        """
        Lambda_b0 -> Lambda0 Psi(2S)
        """
        self.SelLb2L0X3872 = self.createCombinationSel( OutputList = self.name + "SelLb2L0X3872",
                                                    DecayDescriptor = "[Lambda_b0 -> Lambda0 psi(2S)]cc",
                                                    DaughterLists = [ self.SelL0, self.SelX2JpsiPiPi ],
                                                    PreVertexCuts = config['LbComCuts'],
                                                    PostVertexCuts = config['LbMomCuts']
                                                    )

        self.Lb2L0X3872Line = StrippingLine( self.name + 'Line',
                                     	     prescale = config['Prescale'],
	                                     algos    = [ self.SelLb2L0X3872 ],
                	                     MDSTFlag = False,
					     )

        self.registerLine( self.Lb2L0X3872Line )
        
    def createSubSel( self, OutputList, InputList, Cuts ) : 
        filters = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filters,
                          RequiredSelections = [ InputList ] )
    

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughtersCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughtersCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists )


