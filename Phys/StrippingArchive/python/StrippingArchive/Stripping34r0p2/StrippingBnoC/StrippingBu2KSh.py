###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Bu->KSh stripping Selections and StrippingLines.
Provides functions to build KS->DD, KS->LL, Bu selections.
Provides class Bu2KShConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Selections based on previous version of the line by Jussara Miranda.
Exported symbols (use python help!):
   - Bu2KShConf
"""

__author__ = ["Aurelien Martens","Ganrong Wang"]
__date__ = '16/12/2020'
__version__ = 'Stripping28'
__all__ = 'Bu2KShConf'

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdNoPIDsPions as Pions

default_config = {
    'NAME' : 'Bu2KSh',
    'WGs'  : ['BnoC'],
    'BUILDERTYPE' : 'Bu2KShConf',
    'CONFIG' : {'Trk_Chi2'                : 3.0,
                'Trk_GhostProb'           : 0.5,
                'KS_DD_MassWindow'        : 30.0,
                'KS_DD_VtxChi2'           : 10.0,
                'KS_DD_FDChi2'            : 50.0,
                'KS_DD_Pmin'              : 8000.0,
                'KS_DD_Ptmin'             : 1000.0,
                'KS_LL_MassWindow'        : 15.0,
                'KS_LL_VtxChi2'           : 10.0,
                'KS_LL_FDChi2'            : 100.0,
                'Bach_Ptmin'              : 1000.0,
                'B_Mlow'                  : 500.0,
                'B_Mhigh'                 : 1500.0,
                'B_Pmin'                  : 25000.0,
                'BDaug_MaxPT_IP'          : 0.05,
                'BDaug_DD_PTsum'          : 4000.0,
                'BDaug_LL_PTsum'          : 4000.0,
                'B_VtxChi2'               : 6.0,
                'B_Dira'                  : 0.9995,
                'B_DD_IPCHI2wrtPV'        : 10.0,
                'B_LL_IPCHI2wrtPV'        : 10.0,
                'B_FDwrtPV'               : 1.0,
                'B_DD_FDChi2'             : 50.0,
                'B_LL_FDChi2'             : 50.0,
                'GEC_MaxTracks'           : 250,
                # 2012 Trigger
                #'HLT1Dec'                  : 'Hlt1TrackAllL0Decision',
                #'HLT2Dec'                  : 'Hlt2Topo[234]Body.*Decision',
                # 2015 Triggers
                'HLT1Dec'                  : 'Hlt1(Two)?TrackMVADecision',
                'HLT2Dec'                  : 'Hlt2Topo[234]BodyDecision',
                'Prescale'                 : 1.0,
                'Postscale'                : 1.0,
                'RelatedInfoTools' : [    { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.7, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar17'
                                            }, 
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.5, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar15'
                                            }, 
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 1.0, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar10'
                                            },
                                          { "Type" : "RelInfoConeVariables", 
                                            "ConeAngle" : 0.8, 
                                            "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                                            "Location"  : 'ConeVar08'
                                            },
                                          { "Type" : "RelInfoVertexIsolation",
                                            "Location" : "VtxIsolationVar"
                                            }
                                          ]
                },
    'STREAMS' : ['Bhadron']
    }

class Bu2KShConf(LineBuilder) :
    """
    Builder of Bu->KSh stripping Selection and StrippingLine.
    Constructs B+ -> KS h+ Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> bu2kshConf = Bu2KShConf('Bu2KShTest',config)
    >>> bu2kshLines = bu2kshConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selKS2DD           : KS -> Down Down Selection object
    selKS2LL           : KS -> Long Long Selection object
 
    selBu2KSDDh            : B -> KS(DD) h- Selection object
    selBu2KSLLh            : B -> KS(LL) h- Selection object        

    B_dd_line             : StrippingLine made out of selBu2KSDDh
    B_ll_line             : StrippingLine made out of selBu2KSLLh

    lines                  : List of lines, [B_dd_line, B_ll_line]

    Exports as class data member:
    Bu2KShConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        GECCode = {'Code' : "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %s)" % config['GEC_MaxTracks'],
                   'Preambulo' : ["from LoKiTracks.decorators import *"]}

        self.hlt1Filter = {'Code' : "HLT_PASS_RE('%s')" % config['HLT1Dec'],
                           'Preambulo' : ["from LoKiCore.functions import *"]}
        self.hlt2Filter = {'Code' : "HLT_PASS_RE('%s')" % config['HLT2Dec'],
                           'Preambulo' : ["from LoKiCore.functions import *"]}

        self.pions   = Pions

        self.makeKS2DD( 'KSforDDBLines', config )
        self.makeKS2LL( 'KSforLLBLines', config )

        self.makeH( 'HforBLines', config )

        namesSelections = [ (name + 'DD', self.makeBu2KSDDh(name + 'DD', config)),
                            (name + 'LL', self.makeBu2KSLLh(name + 'LL', config)),
                          ]

        # make lines

        for selName, sel in namesSelections:

            extra = {}

#            extra['HLT1'] = self.hlt1Filter
#            extra['HLT2'] = self.hlt2Filter

            line = StrippingLine(selName + 'Line',
                                 selection = sel,
                                 prescale = config['Prescale'],
                                 postscale = config['Postscale'],
                                 RelatedInfoTools = config['RelatedInfoTools'], 
                                 FILTER = GECCode,
                                 **extra) 

            self.registerLine(line)

    def makeKS2DD( self, name, config ) :
        # define all the cuts
        _massCut          = "(ADMASS('KS0')<%s*MeV)"      % config['KS_DD_MassWindow']
        _vtxCut           = "(VFASPF(VCHI2)<%s)   "           % config['KS_DD_VtxChi2']
        _fdChi2Cut        = "(BPVVDCHI2>%s)"                  % config['KS_DD_FDChi2']
        _momCut           = "(P>%s*MeV)"                      % config['KS_DD_Pmin']
        _ptCut            = "(PT>%s*MeV)"                     % config['KS_DD_Ptmin']

        _allCuts = _momCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_fdChi2Cut
        _allCuts += '&'+_ptCut

        # get the KS's to filter
        _stdKSDD = DataOnDemand(Location = "Phys/StdLooseKsDD/Particles")
        
        # make the filter
        _filterKSDD = FilterDesktop( Code = _allCuts )

        # make and store the Selection object
        self.selKS2DD = Selection( name, Algorithm = _filterKSDD, RequiredSelections = [_stdKSDD] )

        return self.selKS2DD

    def makeKS2LL( self, name, config ) : 
        # define all the cuts
        _massCut    = "(ADMASS('KS0')<%s*MeV)"               % config['KS_LL_MassWindow']
        _vtxCut     = "(VFASPF(VCHI2)<%s)"                   % config['KS_LL_VtxChi2']
        _fdCut      = "(BPVVDCHI2>%s)"                       % config['KS_LL_FDChi2']
        _momCut     = "(P>%s*MeV)"                           % config['KS_DD_Pmin']
        _ptCut      = "(PT>%s*MeV)"                          % config['KS_DD_Ptmin']
        _trkChi2Cut1 = "(CHILDCUT((TRCHI2DOF<%s),1))"        % config['Trk_Chi2']
        _trkChi2Cut2 = "(CHILDCUT((TRCHI2DOF<%s),2))"        % config['Trk_Chi2']
        _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config['Trk_GhostProb']
        _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config['Trk_GhostProb']

        _allCuts = _momCut
        _allCuts += '&'+_trkGhostProbCut1
        _allCuts += '&'+_trkGhostProbCut2
        _allCuts += '&'+_ptCut
        _allCuts += '&'+_massCut
        _allCuts += '&'+_trkChi2Cut1
        _allCuts += '&'+_trkChi2Cut2
        _allCuts += '&'+_vtxCut
        _allCuts += '&'+_fdCut
		
        # get the KS's to filter
        _stdKSLL = DataOnDemand(Location = "Phys/StdVeryLooseKsLL/Particles")

        # make the filter
        _filterKSLL = FilterDesktop( Code = _allCuts )
        
        # make and store the Selection object
        self.selKS2LL = Selection( name, Algorithm = _filterKSLL, RequiredSelections = [_stdKSLL] )

        return self.selKS2LL

    def makeH( self, name, config ) :
        # define all the cuts
        _bachPtCut          = "(PT>%s*MeV)"              % config['Bach_Ptmin']
        _trkChi2Cut         = "(TRCHI2DOF<%s)"           % config['Trk_Chi2']
        _trkGPCut           = "(TRGHOSTPROB<%s)"         % config['Trk_GhostProb']

        _allCuts = _bachPtCut
        _allCuts += '&'+_trkChi2Cut
        _allCuts += '&'+_trkGPCut

        
        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the Selection object
        self.selH = Selection( name, Algorithm = _filterH, RequiredSelections = [self.pions] )

        return self.selH

    def makeBu2KSDDh( self, name, config ) :
        """
        Create and store a Bu ->KS(DD) h+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5279-%s)*MeV)"               % config['B_Mlow']
        _massCutHigh    = "(AM<(5279+%s)*MeV)"               % config['B_Mhigh']
        _daugMaxPtIPCut = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)" % config['BDaug_MaxPT_IP']
        _daugPtSumCut   = "((APT1+APT2)>%s*MeV)"             % config['BDaug_DD_PTsum']

        _combCuts = _massCutLow
        _combCuts += '&'+_massCutHigh
        _combCuts += '&'+_daugPtSumCut
        #_combCuts += '&'+_daugMaxPtIPCut # does not work properly

        _pCut      = "(P>%s*MeV)"                      % config['B_Pmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['B_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['B_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_DD_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['B_DD_FDChi2']

        _motherCuts = _pCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut
        _motherCuts += '&'+_fdChi2Cut

        _B = CombineParticles()
        _B.DecayDescriptors = [ "B+ -> pi+ KS0", "B- -> pi- KS0"]
        _B.DaughtersCuts = { "pi+" : "TRCHI2DOF<%s"% config['Trk_Chi2'] }
        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts
        _B.ReFitPVs = True
        
        self.selBu2KSDDh = Selection (name, Algorithm = _B, RequiredSelections = [self.selKS2DD, self.selH])

        return self.selBu2KSDDh

    def makeBu2KSLLh( self, name, config ) :
        """
        Create and store a Bu -> KS(LL) h+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(5279-%s)*MeV)"               % config['B_Mlow']
        _massCutHigh    = "(AM<(5279+%s)*MeV)"               % config['B_Mhigh']
#        _daugMaxPtIPCut   = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)"             % config['BDaug_MaxPT_IP']
        _daugPtSumCut   = "(APT1+APT2>%s*MeV)"               % config['BDaug_LL_PTsum']

        _combCuts = _daugPtSumCut
#        _combCuts += '&'+_daugMaxPtIPCut # does not work properly
        _combCuts += '&'+_massCutLow
        _combCuts += '&'+_massCutHigh

        _pCut      = "(P>%s*MeV)"                      % config['B_Pmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['B_VtxChi2']
        _diraCut    = "(BPVDIRA>%s)"                   % config['B_Dira']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_LL_IPCHI2wrtPV']
        _fdCut      = "(VFASPF(VMINVDDV(PRIMARY))>%s)" % config['B_FDwrtPV']
        _fdChi2Cut  = "(BPVVDCHI2>%s)"                 % config['B_LL_FDChi2']

        _motherCuts = _pCut
        _motherCuts += '&'+_vtxChi2Cut
        _motherCuts += '&'+_diraCut
        _motherCuts += '&'+_ipChi2Cut
        _motherCuts += '&'+_fdCut
        _motherCuts += '&'+_fdChi2Cut

        _B = CombineParticles()
        _B.DecayDescriptors = [ "B+ -> pi+ KS0", "B- -> pi- KS0" ]
        _B.DaughtersCuts = { "pi+" : "TRCHI2DOF<%s"% config['Trk_Chi2'] }
        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts
        _B.ReFitPVs = True
        
        self.selBu2KSLLh = Selection (name, Algorithm = _B, RequiredSelections = [self.selKS2LL, self.selH])

        return self.selBu2KSLLh

