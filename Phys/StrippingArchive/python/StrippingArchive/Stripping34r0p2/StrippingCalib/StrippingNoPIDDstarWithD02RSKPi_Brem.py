###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__  = 'Vitalii Lisovskyi'
__date__    = '15/03/2021'
__version__ = '$Revision: 0.0 $'
__all__ =('NoPIDDstarWithD02RSKPi_bremConf','default_config')

'''
Configurable for the RICH calibration using D*+ -> pi+ D0( K- pi+) for hadron->electron misID with bremsstrahlung photons added.
'''


from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from GaudiKernel.SystemOfUnits import mm, cm , MeV, GeV
## from Configurables import FilterDesktop, CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsKaons, StdAllNoPIDsPions, StdAllNoPIDsElectrons

from Gaudi.Configuration   import *
from Configurables         import NoPIDsParticleMaker, CombinedParticleMaker
from CommonParticles.Utils import trackSelector, updateDoD

default_config =  {
    'NAME'       : 'noPIDDstar_brem',
    'WGs'        : ['Calib'],
    'BUILDERTYPE': 'NoPIDDstarWithD02RSKPi_bremConf',
    'CONFIG'     :{

      'DaugPt'           : 250 * MeV      ## GeV
    , 'DaugP'            : 2.0 * GeV      ## GeV
    , 'DaugIPChi2'       : 16             ## unitless
    , 'DaugTrkChi2'      : 4 # 5              ## unitless
    , 'D0MassWin'        : 500 * MeV       ## 75 MeV
    , 'D0Pt'             : 1.6 * GeV # 1.5 * GeV      ## GeV
    , 'D0VtxChi2Ndof'    : 10 # 13             ## unitless
    , 'D0FDChi2'         : 49             ## unitless
    , 'D0BPVDira'        : 0.9999         ## unitless
    , 'D0IPChi2'         : 30             ## unit
    , 'SlowPiPt'         : 150 * MeV      ## MeV
    , 'SlowPiTrkChi2'    : 4              ## unitless
    , 'DstarPt'          : 2.2 * GeV      ## GeV
    , 'DstarVtxChi2Ndof' : 13             ## unitless
    , 'DeltaM_Min'       : 130 * MeV      ## MeV
    , 'DeltaM_Max'       : 155 * MeV      ## MeV
    ##
    , 'DCS_WrongMass'    : 25 * MeV       ## MeV (3 sigma veto)
    , 'KK_WrongMass'     : 25 * MeV       ## MeV (3 sigma veto)
    , 'PiPi_WrongMass'   : 25 * MeV       ## MeV (3 sigma veto)
    ##
    , 'Prescale'         : 1 #0.800          ## unitless
    , 'Postscale'        : 1.00           ## unitless
    ##
    , 'Monitor'          : False          ## Activate the monitoring?
      },
    'STREAMS'     : ['Charm']#['PID']

}

class NoPIDDstarWithD02RSKPi_bremConf(LineBuilder) :
    """
    Definition of prompt D*+ -> D0( K- pi+) pi+ stripping lines.
    The the main user method is Lines(), which returns a list of the
    StrippingLine objects defined and configured by this configurable.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        self.selbrempion_PMaker = brempion_PMaker(name+"_brempion_PMaker",
                            config
                            )

        self.selbremkaon_PMaker = bremkaon_PMaker(name+"_bremkaon_PMaker",
                            config
                            )

        # self.selbrempion_asE = brempion_CombMaker(name+"_brempion_asE",
        #                     config
        #                     )

        # 1 brem builders

        self.selD02KPi_BremPi = D0_pi(name+"_BremPi", brem=1,
                            config=config, pions=self.selbrempion_PMaker,
                            Monitor=config['Monitor']
                            )

        self.selD02KPi_BremPiasE = D0_piasE(name+"_BremPiasE", brem=1,
                            config=config, #pions=self.selbrempion_asE,
                            Monitor=config['Monitor']
                            )

        self.selD02KPi_BremK = D0_K(name+"_BremK", brem=1,
                            config=config, kaons=self.selbremkaon_PMaker,
                            Monitor=config['Monitor']
                            )

        self.selD02KPi_BremKasE = D0_KasE(name+"_BremKasE", brem=1,
                            config=config, #pions=self.selbrempion_asE,
                            Monitor=config['Monitor']
                            )

        # 0 brem builders

        self.selD02KPi_noBremPi = D0_pi(name+"_noBremPi", brem=0,
                            config=config, pions=self.selbrempion_PMaker,
                            Monitor=config['Monitor']
                            )

        self.selD02KPi_noBremPiasE = D0_piasE(name+"_noBremPiasE", brem=0,
                            config=config, #pions=self.selbrempion_asE,
                            Monitor=config['Monitor']
                            )

        self.selD02KPi_noBremK = D0_K(name+"_noBremK", brem=0,
                            config=config, kaons=self.selbremkaon_PMaker,
                            Monitor=config['Monitor']
                            )

        self.selD02KPi_noBremKasE = D0_KasE(name+"_noBremKasE", brem=0,
                            config=config, #pions=self.selbrempion_asE,
                            Monitor=config['Monitor']
                            )

        # 1 brem Dstar
        self.selDstar2D0Pi_BremPi = Dstar(name+"_BremPi",
                                   self.selD02KPi_BremPi,
                                   1, 0,
                                   config,
                                   Monitor=config['Monitor']
                                   )

        self.selDstar2D0Pi_BremPiasE = Dstar(name+"_BremPiasE",
                                   self.selD02KPi_BremPiasE,
                                   1, 1,
                                   config,
                                   Monitor=config['Monitor']
                                   )

        self.selDstar2D0Pi_BremK = Dstar(name+"_BremK",
                                   self.selD02KPi_BremK,
                                   1, 0,
                                   config,
                                   Monitor=config['Monitor']
                                   )

        self.selDstar2D0Pi_BremKasE = Dstar(name+"_BremKasE",
                                   self.selD02KPi_BremKasE,
                                   1, 1,
                                   config,
                                   Monitor=config['Monitor']
                                   )

        # 0 brem Dstar
        self.selDstar2D0Pi_noBremPi = Dstar(name+"_noBremPi",
                                   self.selD02KPi_noBremPi,
                                   0, 0,
                                   config,
                                   Monitor=config['Monitor']
                                   )

        self.selDstar2D0Pi_noBremPiasE = Dstar(name+"_noBremPiasE",
                                   self.selD02KPi_noBremPiasE,
                                   0, 1,
                                   config,
                                   Monitor=config['Monitor']
                                   )

        self.selDstar2D0Pi_noBremK = Dstar(name+"_noBremK",
                                   self.selD02KPi_noBremK,
                                   0, 0,
                                   config,
                                   Monitor=config['Monitor']
                                   )

        self.selDstar2D0Pi_noBremKasE = Dstar(name+"_noBremKasE",
                                   self.selD02KPi_noBremKasE,
                                   0, 1,
                                   config,
                                   Monitor=config['Monitor']
                                   )


        # define stripping lines

        self.Dstar2D0Pi_BremPi_line = StrippingLine(name+'Pion1BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_BremPi,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> K- pi+ ) pi+]CC')  ]
                                             )

        self.Dstar2D0Pi_BremPiasE_line = StrippingLine(name+'PionasE1BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_BremPiasE,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> K- e+ ) pi+]CC')  ]
                                             )

        self.Dstar2D0Pi_BremKasE_line = StrippingLine(name+'KaonasE1BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_BremKasE,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> e- pi+ ) pi+]CC')  ]
                                             )


        self.Dstar2D0Pi_BremK_line = StrippingLine(name+'Kaon1BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_BremK,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> K- pi+ ) pi+]CC')  ]
                                             )
#
        self.Dstar2D0Pi_noBremPi_line = StrippingLine(name+'Pion0BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_noBremPi,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> K- pi+ ) pi+]CC')  ]
                                             )

        self.Dstar2D0Pi_noBremPiasE_line = StrippingLine(name+'PionasE0BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_noBremPiasE,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> K- e+ ) pi+]CC')  ]
                                             )

        self.Dstar2D0Pi_noBremKasE_line = StrippingLine(name+'KaonasE0BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_noBremKasE,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> e- pi+ ) pi+]CC')  ]
                                             )


        self.Dstar2D0Pi_noBremK_line = StrippingLine(name+'Kaon0BremLine',
                                             prescale = config['Prescale'],
                                             postscale = config['Postscale'],
                                             selection = self.selDstar2D0Pi_noBremK,
                                             RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                             RelatedInfoTools = [  addRelInfoMuonIsolation('[D*(2010)+ ->  ( D0 -> K- pi+ ) pi+]CC')  ]
                                             )

        self.registerLine(self.Dstar2D0Pi_BremPi_line)
        self.registerLine(self.Dstar2D0Pi_BremPiasE_line)
        self.registerLine(self.Dstar2D0Pi_BremK_line)
        self.registerLine(self.Dstar2D0Pi_BremKasE_line)
        self.registerLine(self.Dstar2D0Pi_noBremPi_line)
        self.registerLine(self.Dstar2D0Pi_noBremPiasE_line)
        self.registerLine(self.Dstar2D0Pi_noBremK_line)
        self.registerLine(self.Dstar2D0Pi_noBremKasE_line)


def D0_pi ( name,
         pions,
         brem,
         config,
         Monitor
         ) :
    """
    Selection for D0 with brem added to the pion
    """
    #first, build pions with brem


    ## create the algorithm
    # algorithm_pi =  NoPIDsParticleMaker ( 'StdAllNoPIDsBremPions' ,
    #                                    Particle = 'pion'  )
    # algorithm_pi.AddBremPhotonTo = ['pi+']
    # # configure the track selector
    # selector_pi = trackSelector ( algorithm_pi )
    # ## configure Data-On-Demand service
    # updateDoD ( algorithm_pi )
    # ## finally: define the symbol
    # StdAllNoPIDsBremPions = AutomaticData(Location = 'Phys/StdAllNoPIDsBremPions/Particles')  #algorithm_pi


    _D0 = CombineParticles()
    _D0.DecayDescriptor = "[D0 -> K- pi+]cc"
    dauCutStr = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s)" %locals()['config']
    if brem:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)> 0)" %locals()['config']
    else:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)== 0)" %locals()['config']
    #brem1 = "& (PINFO(LHCb::Particle::HasBremAdded,0.)> 0)"
    _D0.DaughtersCuts = { "K+" : dauCutStr,
                          "pi+" : dauCutStr_brem1
                          }
    _D0.CombinationCut = "(ADAMASS('D0')<550 * MeV)" %locals()['config']
    if not brem:
        _D0.CombinationCut = "(ADAMASS('D0')<100 * MeV)"
    mothercut = """
    (PT>%(D0Pt)s)
    & (VFASPF(VCHI2PDOF)<%(D0VtxChi2Ndof)s)
    & (BPVVDCHI2>%(D0FDChi2)s)
    & (BPVDIRA>%(D0BPVDira)s)
    & (BPVIPCHI2()<%(D0IPChi2)s)
    & (ADMASS('D0') < %(D0MassWin)s )
    """
    #    & ( ADWM( 'D0' , WM( 'pi-' , 'K+') ) > %(DCS_WrongMass)s)
# take these out as they shape the sidebands
   # & ( ADWM( 'D0' , WM( 'K-' , 'K+') ) > %(KK_WrongMass)s)
   # & ( ADWM( 'D0' , WM( 'pi-' , 'pi+') ) > %(PiPi_WrongMass)s)
   # """
    _D0.MotherCut = mothercut %locals()['config']

    # if Monitor != None :
    #     _D0.Preambulo    = [
    #         ## define historam type (shortcut)
    #         "Histo  = Gaudi.Histo1DDef"  ,
    #         ## monitor LL-case
    #         "mass     = monitor ( M / GeV , Histo ( 'D0' , 1.79 , 1.94 , 100 ) , 'M'     ) " ,
    #         "pT       = monitor ( PT / GeV , Histo ( 'D0' , 0.00 , 20.0 , 100 ) , 'pT'   ) " ,
    #         "y        = monitor ( Y , Histo ( 'D0' , 1.60 , 5.40 , 100 ) , 'y'           ) "
    #         ]
    #     _D0.Monitor       = True
    #     _D0.HistoProduce  = True
    #     _D0.MotherMonitor = """ process ( mass )
    #     >> process ( pT )
    #     >> process ( y )
    #     >> EMPTY
    #         """

    D0 = Selection("SelD02RSKPiFor"+name,
                   Algorithm = _D0,
                   RequiredSelections = [StdAllNoPIDsKaons, pions])
    return D0


def D0_piasE ( name,
         #pions,
         brem,
         config,
         Monitor
         ) :
    """
    Selection for D0 with brem added to the pion
    """

    _D0 = CombineParticles()
    _D0.DecayDescriptor = "[D0 -> K- e+]cc"
    dauCutStr = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s)" %locals()['config']
    if brem:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)> 0)" %locals()['config']
    else:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)== 0)" %locals()['config']
    #brem1 = "& (PINFO(LHCb::Particle::HasBremAdded,0.)> 0)"
    _D0.DaughtersCuts = { "K+" : dauCutStr,
                          "e+" : dauCutStr_brem1
                          }
    _D0.CombinationCut = "(APT>0)" %locals()['config']
    # if not brem:
    #     _D0.CombinationCut = "(ADAMASS('D0')<100 * MeV)"
    mothercut = """
    (PT>%(D0Pt)s)
    & (VFASPF(VCHI2PDOF)<%(D0VtxChi2Ndof)s)
    & (BPVVDCHI2>%(D0FDChi2)s)
    & (BPVDIRA>%(D0BPVDira)s)
    & (BPVIPCHI2()<%(D0IPChi2)s)
    & (ADWM( 'D0' , WM( 'K-' , 'pi+') )<500 * MeV)
    """
#    & ( ADWM( 'D0' , WM( 'pi-' , 'K+') ) > %(DCS_WrongMass)s)
# take these out as they shape the sidebands
   # & ( ADWM( 'D0' , WM( 'K-' , 'K+') ) > %(KK_WrongMass)s)
   # & ( ADWM( 'D0' , WM( 'pi-' , 'pi+') ) > %(PiPi_WrongMass)s)
   # """
    _D0.MotherCut = mothercut %locals()['config']
    if not brem:
        _D0.MotherCut += " & (ADWM( 'D0' , WM( 'K-' , 'pi+') )<100 * MeV)"
    # if Monitor != None :
    #     _D0.Preambulo    = [
    #         ## define historam type (shortcut)
    #         "Histo  = Gaudi.Histo1DDef"  ,
    #         ## monitor LL-case
    #         "mass     = monitor ( M / GeV , Histo ( 'D0' , 1.79 , 1.94 , 100 ) , 'M'     ) " ,
    #         "pT       = monitor ( PT / GeV , Histo ( 'D0' , 0.00 , 20.0 , 100 ) , 'pT'   ) " ,
    #         "y        = monitor ( Y , Histo ( 'D0' , 1.60 , 5.40 , 100 ) , 'y'           ) "
    #         ]
    #     _D0.Monitor       = True
    #     _D0.HistoProduce  = True
    #     _D0.MotherMonitor = """ process ( mass )
    #     >> process ( pT )
    #     >> process ( y )
    #     >> EMPTY
    #         """

    D0 = Selection("SelD02RSKPiFor"+name,
                   Algorithm = _D0,
                   RequiredSelections = [StdAllNoPIDsKaons, StdAllNoPIDsElectrons])
    return D0

def D0_K ( name,
         kaons,
         brem,
         config,
         Monitor
         ) :
    """
    Selection for D0 with brem added to the kaon
    """
    #first, build pions with brem

    ## create the algorithm
    # algorithm_K =  NoPIDsParticleMaker ( 'StdAllNoPIDsBremKaons' ,
    #                                    Particle = 'kaon'  )
    # algorithm_K.AddBremPhotonTo = ['K+']
    # # configure the track selector
    # selector_K = trackSelector ( algorithm_K )
    # ## configure Data-On-Demand service
    # updateDoD ( algorithm_K )
    # ## finally: define the symbol
    # StdAllNoPIDsBremKaons = AutomaticData(Location = 'Phys/StdAllNoPIDsBremKaons/Particles')   #StdAllNoPIDsBremKaons = algorithm_K


    _D0 = CombineParticles()
    _D0.DecayDescriptor = "[D0 -> K- pi+]cc"
    dauCutStr = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s)" %locals()['config']
    if brem:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)> 0)" %locals()['config']
    else:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)== 0)" %locals()['config']
    #brem1 = "& (PINFO(LHCb::Particle::HasBremAdded,0.)> 0)"
    _D0.DaughtersCuts = { "K+" : dauCutStr_brem1,
                          "pi+" : dauCutStr
                          }
    _D0.CombinationCut = "(ADAMASS('D0')<550 * MeV)" %locals()['config']
    if not brem:
        _D0.CombinationCut = "(ADAMASS('D0')<100 * MeV)"
    mothercut = """
    (PT>%(D0Pt)s)
    & (VFASPF(VCHI2PDOF)<%(D0VtxChi2Ndof)s)
    & (BPVVDCHI2>%(D0FDChi2)s)
    & (BPVDIRA>%(D0BPVDira)s)
    & (BPVIPCHI2()<%(D0IPChi2)s)
    & (ADMASS('D0') < %(D0MassWin)s )
    """
#    & ( ADWM( 'D0' , WM( 'pi-' , 'K+') ) > %(DCS_WrongMass)s)
# take these out as they shape the sidebands
   # & ( ADWM( 'D0' , WM( 'K-' , 'K+') ) > %(KK_WrongMass)s)
   # & ( ADWM( 'D0' , WM( 'pi-' , 'pi+') ) > %(PiPi_WrongMass)s)
   # """
    _D0.MotherCut = mothercut %locals()['config']
    # if not brem:
    #     _D0.MotherCut += " & (ADWM( 'D0' , WM( 'K-' , 'pi+') )<100 * MeV)"
    # if Monitor != None :
    #     _D0.Preambulo    = [
    #         ## define historam type (shortcut)
    #         "Histo  = Gaudi.Histo1DDef"  ,
    #         ## monitor LL-case
    #         "mass     = monitor ( M / GeV , Histo ( 'D0' , 1.79 , 1.94 , 100 ) , 'M'     ) " ,
    #         "pT       = monitor ( PT / GeV , Histo ( 'D0' , 0.00 , 20.0 , 100 ) , 'pT'   ) " ,
    #         "y        = monitor ( Y , Histo ( 'D0' , 1.60 , 5.40 , 100 ) , 'y'           ) "
    #         ]
    #     _D0.Monitor       = True
    #     _D0.HistoProduce  = True
    #     _D0.MotherMonitor = """ process ( mass )
    #     >> process ( pT )
    #     >> process ( y )
    #     >> EMPTY
    #         """

    D0 = Selection("SelD02RSKPiFor"+name,
                   Algorithm = _D0,
                   RequiredSelections = [kaons, StdAllNoPIDsPions])
    return D0

def D0_KasE ( name,
         #pions,
         brem,
         config,
         Monitor
         ) :
    """
    Selection for D0 with brem added to the kaon
    """

    _D0 = CombineParticles()
    _D0.DecayDescriptor = "[D0 -> e- pi+]cc"
    dauCutStr = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s)" %locals()['config']
    if brem:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)> 0)" %locals()['config']
    else:
        dauCutStr_brem1 = "(PT > %(DaugPt)s) & (P > %(DaugP)s) & (TRCHI2DOF < %(DaugTrkChi2)s) & (MIPCHI2DV(PRIMARY) > %(DaugIPChi2)s) & (PINFO(LHCb.Particle.HasBremAdded,0.)== 0)" %locals()['config']
    #brem1 = "& (PINFO(LHCb::Particle::HasBremAdded,0.)> 0)"
    _D0.DaughtersCuts = { "pi+" : dauCutStr,
                          "e+" : dauCutStr_brem1
                          }
    _D0.CombinationCut = "(APT>0)" %locals()['config']

    mothercut = """
    (PT>%(D0Pt)s)
    & (VFASPF(VCHI2PDOF)<%(D0VtxChi2Ndof)s)
    & (BPVVDCHI2>%(D0FDChi2)s)
    & (BPVDIRA>%(D0BPVDira)s)
    & (BPVIPCHI2()<%(D0IPChi2)s)
    & (ADWM( 'D0' , WM( 'K-' , 'pi+') )<550 * MeV)
    """
#    & ( ADWM( 'D0' , WM( 'pi-' , 'K+') ) > %(DCS_WrongMass)s)
# take these out as they shape the sidebands
   # & ( ADWM( 'D0' , WM( 'K-' , 'K+') ) > %(KK_WrongMass)s)
   # & ( ADWM( 'D0' , WM( 'pi-' , 'pi+') ) > %(PiPi_WrongMass)s)
   # """
    _D0.MotherCut = mothercut %locals()['config']
    if not brem:
        _D0.MotherCut += " & (ADWM( 'D0' , WM( 'K-' , 'pi+') )<100 * MeV)"
    # if Monitor != None :
    #     _D0.Preambulo    = [
    #         ## define historam type (shortcut)
    #         "Histo  = Gaudi.Histo1DDef"  ,
    #         ## monitor LL-case
    #         "mass     = monitor ( M / GeV , Histo ( 'D0' , 1.79 , 1.94 , 100 ) , 'M'     ) " ,
    #         "pT       = monitor ( PT / GeV , Histo ( 'D0' , 0.00 , 20.0 , 100 ) , 'pT'   ) " ,
    #         "y        = monitor ( Y , Histo ( 'D0' , 1.60 , 5.40 , 100 ) , 'y'           ) "
    #         ]
    #     _D0.Monitor       = True
    #     _D0.HistoProduce  = True
    #     _D0.MotherMonitor = """ process ( mass )
    #     >> process ( pT )
    #     >> process ( y )
    #     >> EMPTY
    #         """

    D0 = Selection("SelD02RSKPiFor"+name,
                   Algorithm = _D0,
                   RequiredSelections = [StdAllNoPIDsPions, StdAllNoPIDsElectrons])
    return D0



def Dstar ( name,
            D0Sel,
            brem,
            asE,
            config,
            Monitor) :
    """
    Selection for D*
    """

    _DSt = CombineParticles()
    _DSt.DecayDescriptor = "[D*(2010)+ -> D0 pi+]cc"
    slowPiCuts = "(PT>%(SlowPiPt)s) & (TRCHI2DOF < %(SlowPiTrkChi2)s)" %locals()['config']
    d0Cuts = "ALL"
    _DSt.DaughtersCuts   = { 'pi+' : slowPiCuts, 'D0' : d0Cuts }
    combcut = """
    (APT>%(DstarPt)s)
    & (ADAMASS('D*(2010)+')<500)
    & (AM - AM1 < 200 * MeV)
    """
    #was: 75, 165 MeV
    _DSt.CombinationCut = combcut %locals()['config']
    if (not brem) and (not asE):
        _DSt.CombinationCut += " & (ADAMASS('D*(2010)+')<75 * MeV) & (AM - AM1 < 200 * MeV)"
    if (not brem) and asE:
        _DSt.CombinationCut += " & (ADAMASS('D*(2010)+')<500 * MeV) & (AM - AM1 < 225 * MeV)"
    mothercut = """
    (VFASPF(VCHI2PDOF)<%(DstarVtxChi2Ndof)s)
    & (M-MAXTREE('D0'==ABSID,M)<%(DeltaM_Max)s)
    & (M-MAXTREE('D0'==ABSID,M)>%(DeltaM_Min)s)
    """
    _DSt.MotherCut =  mothercut %locals()['config']

    # if Monitor != None :
    #     _DSt.Preambulo    = [
    #         ## define historam type (shortcut)
    #         "Histo  = Gaudi.Histo1DDef"  ,
    #         ## monitor LL-case
    #         "mass     = monitor ( M / GeV ,    Histo ( 'DSt' , 1.93 , 2.09 , 100 ) , 'M'     ) " ,
    #         "deltaM   = monitor ( M - M1 /MeV, Histo ( 'DSt' , 135  , 160  , 50  ) , 'DeltaM') " ,
    #         "pT       = monitor ( PT / GeV ,   Histo ( 'DSt' , 0.00 , 20.0 , 100 ) , 'pT'    ) " ,
    #         "y        = monitor ( Y ,          Histo ( 'DSt' , 1.60 , 5.40 , 100 ) , 'y'     ) "
    #         ]
    #     _DSt.Monitor       = True
    #     _DSt.HistoProduce  = True
    #     _DSt.MotherMonitor = """ process ( mass )
    #     >> process ( deltaM )
    #     >> process ( pT )
    #     >> process ( y )
    #     >> EMPTY
    #     """

    DSt = Selection("SelDSt2D0PiFor"+name,
                    Algorithm = _DSt,
                    RequiredSelections = [StdAllNoPIDsPions,
                                          D0Sel])

    return DSt



def brempion_PMaker ( name,
         config
         ) :
    """
    Selection for pions with brem adder via NoPIDsParticleMaker
    """
    #first, build pions with brem


    ## create the algorithm
    algorithm_pi =  NoPIDsParticleMaker ( 'StdAllNoPIDsBremPions' ,
                                       Particle = 'pion'  )
    algorithm_pi.AddBremPhotonTo = ['pi+']
    # configure the track selector
    selector_pi = trackSelector ( algorithm_pi )
    ## configure Data-On-Demand service
    updateDoD ( algorithm_pi )
    ## finally: define the symbol
    StdAllNoPIDsBremPions = AutomaticData(Location = 'Phys/StdAllNoPIDsBremPions/Particles')  #algorithm_pi

    return StdAllNoPIDsBremPions
#
# def brempion_CombMaker ( name,
#          config
#          ) :
#     """
#     Selection for pions reconstructed in the same manner as Std Electrons
#     """
#     #first, build pions with brem
#
#
#     ## create the algorithm
#     algorithm_pi =  CombinedParticleMaker ( 'StdAllNoPIDsBremPionsAsE' ,
#                                        Particle = 'electron'  )
#     #algorithm_pi.AddBremPhotonTo = ['pi+']
#     # configure the track selector
#     selector_pi = trackSelector ( algorithm_pi )
#     ## configure Data-On-Demand service
#     updateDoD ( algorithm_pi )
#     ## finally: define the symbol
#     StdAllNoPIDsBremPions = AutomaticData(Location = 'Phys/StdAllNoPIDsBremPionsAsE/Particles')  #algorithm_pi
#
#     return StdAllNoPIDsBremPions


def bremkaon_PMaker ( name,
         config
         ) :
    """
    Selection for D0 with brem added to the kaon
    """
    #first, build pions with brem

    ## create the algorithm
    algorithm_K =  NoPIDsParticleMaker ( 'StdAllNoPIDsBremKaons' ,
                                       Particle = 'kaon'  )
    algorithm_K.AddBremPhotonTo = ['K+']
    # configure the track selector
    selector_K = trackSelector ( algorithm_K )
    ## configure Data-On-Demand service
    updateDoD ( algorithm_K )
    ## finally: define the symbol
    StdAllNoPIDsBremKaons = AutomaticData(Location = 'Phys/StdAllNoPIDsBremKaons/Particles')   #StdAllNoPIDsBremKaons = algorithm_K

    return StdAllNoPIDsBremKaons


def addRelInfoMuonIsolation( decdes ):
    import re
    _DauLoc={}
    _daughters = re.match(r'(.*)->([ |\[]*)([^\]]+)(.*)', decdes)
    if _daughters:
        _particles = _daughters.group(3).split()
        _ip=1
        _gp=1
        for _p in _particles:
            if re.match(r'(pi|p|K|e|mu)[\+|-]',_p):
                _key= _daughters.group(1)+"->"+_daughters.group(2)
                _jp=1
                for _p2 in _particles:
                    _key+=" "
                    if _jp==_ip: _key+="^"
                    _key+=_p2
                    _jp=_jp+1
                _key+=_daughters.group(4).replace("cc","CC")
                _DauLoc[_key] = "MudetIso"+str(_gp)
                _gp=_gp+1
            _ip=_ip+1
    else:
        return {}
    return {  "Type" : "RelInfoMuonIsolation", "DaughterLocations" : _DauLoc}
