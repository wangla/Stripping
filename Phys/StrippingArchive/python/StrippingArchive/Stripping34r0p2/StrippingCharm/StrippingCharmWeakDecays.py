###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Charmed Weak Decays Stripping Selections and StrippingLines.
Provides functions to build D0, Ds1 selections.
Provides class Bc2Ds1GammaConf, which constructs the Selections and
StrippingLines given a configuration dictionary.
Exported selection makers: 'makeD0', 'makeDs1', 'makeDs1WS',

"""

__author__ = ['Marco Pappagallo']
__date__ = '24/01/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ('CharmWeakDecaysConf',
           'makeD0st',
           'makeDs1',
           'makeDs1WS',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME'              : 'CharmWeakDecays',
    'BUILDERTYPE'       : 'CharmWeakDecaysConf',
    'CONFIG'    : {
                    #D0 and D0*
                    'MinTrackPT'         : 800.      # MeV
                    ,'DaugPtMax'         : 1500.     # MeV
                    ,'TrGhostProb'       : 0.3       # Dimensionless
                    ,'MinTrackIPchi2'    : 9         # Dimensionless
                    ,'KaonPID'        : 7.        # Dimensionless
                    ,'PionPID'        : 0.        # Dimensionless
                    ,'MinD0stMass'       : 1750      # MeV
                    ,'MaxD0stMass'       : 2120      # MeV
                    ,'MinD0stPT'         : 2000      # MeV
                    ,'MaxD0stVertChi2DOF': 16        # Dimensionless 
                    #Ds1
                    ,'MinKaonPT'         : 250       # MeV
                    ,'Ds1PT'             : 4000      # MeV 
                    ,'QValueDs1Decay'    : 80        # MeV
                    ,'MaxDs1VertChi2DOF' : 9         # Dimensionless 
                    # Pre- and postscales
                    ,'CharmWeakDecaysPreScale'     : 1.0
                    ,'CharmWeakDecaysPostScale'    : 1.0
                    },
    'STREAMS' : [ 'Charm' ],
    'WGs'    : [ 'Charm' ]
    }


class CharmWeakDecaysConf(LineBuilder):
    """
    Definition of Ds1 -> D(*)0 K stripping
    
    Constructs Bc -> Ds1 Gamma Selections and StrippingLines from
    a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> gammaConf = StrippingB2XGammaConf('StrippingB2XGammaTest',config)
    >>> gammaLines = gammaConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selD0                        : nominal D0 -> K pi Selection object
    selDs1                       : nominal Ds1 -> D0 K Selection object 
    selDs1WS                     : nominal Ds1 -> D~0 K Selection object
    lines                  : List of lines

    Exports as class data member:
    StrippingB2XGammaConf.__configuration_keys__ : List of required configuration parameters.    
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        # if name not set outside, set it to empty
        #if name == None:
        #    name = ""   

        # Selection of D0 and Ds1

        self.selD0st  = makeD0st('D0stFor%s' % name,
                                 config['KaonPID'],
                                 config['PionPID'],
                                 config['TrGhostProb'],
                                 config['MinTrackPT'],
                                 config['MinTrackIPchi2'],
                                 config['MinD0stMass'],
                                 config['MaxD0stMass'],
                                 config['MinD0stPT'],
                                 config['MaxD0stVertChi2DOF'],
                                 config['DaugPtMax']
                                 )

        self.selDs1 = makeDs1('Ds1For%s' % name,
                              self.selD0st,
                              '[D_s1(2536)+ -> D*(2007)0 K+]cc', #DecayDescriptor
                              config['KaonPID'],
                              config['TrGhostProb'],
                              config['MinKaonPT'],
                              config['MinTrackIPchi2'],
                              config['Ds1PT'],
                              config['QValueDs1Decay'],
                              config['MaxDs1VertChi2DOF'])

        self.selDs1WS = makeDs1('Ds1WSFor%s' % name,
                                self.selD0st,
                                '[D_s1(2536)+ -> D*(2007)~0 K+]cc', #DecayDescriptor
                                config['KaonPID'],
                                config['TrGhostProb'],
                                config['MinKaonPT'],
                                config['MinTrackIPchi2'],
                                config['Ds1PT'],
                                config['QValueDs1Decay'],
                                config['MaxDs1VertChi2DOF'])

        # Create and register stripping lines

        self.Ds12D0stKLine = StrippingLine("%sDs12D0stKLine" % name,
                                           prescale =config['CharmWeakDecaysPreScale'],
                                           postscale=config['CharmWeakDecaysPostScale'],
                                           selection=self.selDs1)
        self.registerLine(self.Ds12D0stKLine)

        self.Ds12D0stKWSLine = StrippingLine("%sDs12D0stKWSLine" % name,
                                           prescale =config['CharmWeakDecaysPreScale'],
                                           postscale=config['CharmWeakDecaysPostScale'],
                                           selection=self.selDs1WS)
        self.registerLine(self.Ds12D0stKWSLine)
        
def makeD0st(name, KaonPID, PionPID, TrGhostProb, MinTrackPT, MinTrackIPchi2, MinD0stMass, MaxD0stMass, MinD0stPT, MaxD0stVertChi2DOF, DaugPtMax) :
    """
    Create and return a D(*)0->Kpi Selection object
    
    @arg name: name of the Selection.
    @arg KaonPID: PID of Kaon
    @arg PionPID: PID of pion
    @arg TrGhostProb: Ghost probability of pion/kaon
    @return: Selection object
    
    """

    _daughterCutK  = "(TRGHOSTPROB<%(TrGhostProb)s) & (PT > %(MinTrackPT)s) & (MIPCHI2DV(PRIMARY) < %(MinTrackIPchi2)s) & (PIDK  > %(KaonPID)s)" % locals()
    _daughterCutpi = "(TRGHOSTPROB<%(TrGhostProb)s) & (PT > %(MinTrackPT)s) & (MIPCHI2DV(PRIMARY) < %(MinTrackIPchi2)s) & (PIDK  < %(PionPID)s)" % locals()
    
    _combinationCut = "(in_range( (%(MinD0stMass)s*MeV-100*MeV), AM, (%(MaxD0stMass)s*MeV+100*MeV))) & (APT > (%(MinD0stPT)s*MeV-500*MeV)) & (AHASCHILD( PT > %(DaugPtMax)s* MeV))" % locals()
    _motherCut = "(in_range( %(MinD0stMass)s*MeV, M, %(MaxD0stMass)s*MeV)) & (PT > %(MinD0stPT)s*MeV) & (VFASPF(VCHI2/VDOF) < %(MaxD0stVertChi2DOF)s)" % locals()
    _D0st = CombineParticles(DecayDescriptor = "[D*(2007)0 -> K- pi+]cc",
                             DaughtersCuts   = {"K+"  : _daughterCutK,
                                              "pi+" : _daughterCutpi},
                             CombinationCut  = _combinationCut,
                             MotherCut       =_motherCut)
    
    _stdANNKaon = DataOnDemand(Location="Phys/StdAllLooseANNKaons/Particles")
    _stdANNPion = DataOnDemand(Location="Phys/StdAllLooseANNPions/Particles")
    
    return Selection(name, Algorithm=_D0st, RequiredSelections=[_stdANNKaon, _stdANNPion])


def makeDs1(name, D0stSel, DecDescr, KaonPID, TrGhostProb, MinKaonPT, MinTrackIPchi2, Ds1PT, QValueDs1Decay, MaxDs1VertChi2DOF):
    """
    Create and return a Ds1+ -> D0/D~0 K+ Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg D0stSel: D0 selection.
    @arg DecDescr: Decay descriptor
    @arg KaonPID: PID of Kaon
    @arg TrGhostProb: Ghost probability of kaon
    @arg Ds1DeltaMassWin: Mass difference m(D0K) - m(D0)
    @arg MaxDs1VertChi2DOF: chi2/NDOF
    @return: Selection object
    """

    _daughterCutK  = "(TRGHOSTPROB<%(TrGhostProb)s) & (PT > %(MinKaonPT)s) & (MIPCHI2DV(PRIMARY) < %(MinTrackIPchi2)s) & (PIDK  > %(KaonPID)s)" % locals()
    _combinationCut = "((AM-AM1-AM2) < (1.5*%(QValueDs1Decay)s*MeV)) & (APT > (0.75*%(Ds1PT)s*MeV))" % locals()
    _motherCut = "((M-M1-M2) < %(QValueDs1Decay)s*MeV) & (VFASPF(VCHI2/VDOF) < %(MaxDs1VertChi2DOF)s) & (PT > %(Ds1PT)s*MeV)" % locals()
    _Ds1 = CombineParticles(DecayDescriptor = DecDescr,
                            DaughtersCuts   = {"K+" : _daughterCutK},
                            CombinationCut  = _combinationCut,
                            MotherCut       = _motherCut)
    
    _stdANNKaon = DataOnDemand(Location="Phys/StdAllLooseANNKaons/Particles")
    return Selection(name, Algorithm=_Ds1, RequiredSelections=[D0stSel, _stdANNKaon])
                                
# EOF
