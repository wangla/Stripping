###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of LmbdacForNeutronPID Stripping Selections and StrippingLines.
Provides functions to build Lc, Sigmac selections.
Provides class LambdacForNeutronPIDConf, which constructs the Selections and
StrippingLines given a configuration dictionary.
Exported selection makers: 'makeLc', 'makeSigmac', 'makeSigmacWS',

"""

__author__  = ['Marco Pappagallo']
__date__    = '17/03/2021'
__version__ = '$Revision: 1.0 $'

__all__ = ('LambdacForNeutronPIDConf',
           'makeLc',
           'makeSigmac',
           'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.PhysicalConstants import c_light

default_config = {
    'NAME'              : 'LambdacForNeutronPID',
    'BUILDERTYPE'       : 'LambdacForNeutronPIDConf',
    'CONFIG'    : {
                        #Lc*
                        'MinTrackPT'         : 400.      # MeV
                        ,'MinNeutronPT'      : 400.      # MeV
                        ,'TrGhostProb'       : 0.3       # Dimensionless
                        ,'MinTrackIPchi2'    : 9         # Dimensionless
                        ,'KaonPID'           : 5.        # Dimensionless
                        ,'PionPID'           : 0.        # Dimensionless
                        ,'MinLcMass'         : 1400      # MeV
                        ,'MaxLcMass'         : 2200      # MeV
                        ,'ctau'              : 0.1       # mm
                        ,'MinLcPT'           : 4000      # MeV
                        ,'MaxLcVertChi2DOF'  : 16        # Dimensionless 
                        #Sigmac
                        ,'MaxTrackIPchi2'       : 9         # Dimensionless
                        ,'MinPionPT'            : 250       # MeV
                        ,'SigmacPT'             : 0         # MeV 
                        ,'QValueSigmacDecay'    : 120        # MeV
                        ,'MaxSigmacVertChi2DOF' : 16         # Dimensionless 
                        # Pre- and postscales
                        ,'LambdacForNeutronPIDPreScale'     : 1.0
                        ,'LambdacForNeutronPIDPostScale'    : 1.0
                        },
    'STREAMS' : [ 'Charm' ],
    'WGs'    : [ 'Charm' ]
    }


class LambdacForNeutronPIDConf(LineBuilder):
    """
    Definition of Sigmac -> Lambdac pi+-, Lambdac -> n K- pi+ pi+ stripping
    
    Constructs Sigmac -> Lambdac pi+- Selections and StrippingLines from
    a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> gammaConf = StrippingB2XGammaConf('StrippingB2XGammaTest',config)
    >>> gammaLines = gammaConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selLc                           : nominal Lc -> K pi Selection object
    selSigmac                       : nominal Sigmac -> Lc pi- Selection object 
    selSigmacWS                     : nominal Sigmac -> Lc pi+ Selection object
    lines                  : List of lines

    Exports as class data member:
    StrippingB2XGammaConf.__configuration_keys__ : List of required configuration parameters.    
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        # if name not set outside, set it to empty
        #if name == None:
        #    name = ""   

        # Selection of Lc and Sigmac

        self.selLc  = makeLc('LcFor%s' % name,
                             config['KaonPID'],
                             config['PionPID'],
                             config['TrGhostProb'],
                             config['MinTrackPT'],
                             config['MinNeutronPT'],
                             config['MinTrackIPchi2'],
                             config['MinLcMass'],
                             config['MaxLcMass'],
                             config['MinLcPT'],
                             config['MaxLcVertChi2DOF'],
                             config['ctau']
                             )

        self.selSigmac = makeSigmac('Sigmac02LcpiFor%s' % name,
                                    self.selLc,
                                    '[Sigma_c0 -> Lambda_c+ pi-]cc', #DecayDescriptor
                                    config['TrGhostProb'],
                                    config['MinPionPT'],
                                    config['MaxTrackIPchi2'],
                                    config['SigmacPT'],
                                    config['QValueSigmacDecay'],
                                    config['MaxSigmacVertChi2DOF'],
                                    config['PionPID'])
        
        self.selSigmacWS = makeSigmac('Sigmacpp2LcpiFor%s' % name,
                                      self.selLc,
                                      '[Sigma_c++ -> Lambda_c+ pi+]cc', #DecayDescriptor
                                      config['TrGhostProb'],
                                      config['MinPionPT'],
                                      config['MaxTrackIPchi2'],
                                      config['SigmacPT'],
                                      config['QValueSigmacDecay'],
                                      config['MaxSigmacVertChi2DOF'],
                                      config['PionPID'])

        # Create and register stripping lines
        ## Rate is too high
        #        self.Lc2nKpipiLine = StrippingLine("%sLc2nKpipiLine" % name,
        #                                             prescale =config['LambdacForNeutronPIDPreScale'],
        #                                             postscale=config['LambdacForNeutronPIDPostScale'],
        #                                             selection=self.selLc)
        #        self.registerLine(self.Lc2nKpipiLine)
        
        self.Sigmac2LcpiLine = StrippingLine("%sSigmac02LcpiLine" % name,
                                             prescale =config['LambdacForNeutronPIDPreScale'],
                                             postscale=config['LambdacForNeutronPIDPostScale'],
                                             selection=self.selSigmac)
        self.registerLine(self.Sigmac2LcpiLine)

        self.SigmacWS2LcpiLine = StrippingLine("%sSigmacpp2LcpiLine" % name,
                                               prescale =config['LambdacForNeutronPIDPreScale'],
                                               postscale=config['LambdacForNeutronPIDPostScale'],
                                               selection=self.selSigmacWS)
        self.registerLine(self.SigmacWS2LcpiLine)

        
def makeLc(name, KaonPID, PionPID, TrGhostProb, MinTrackPT, MinNeutronPT, MinTrackIPchi2, MinLcMass, MaxLcMass, MinLcPT, MaxLcVertChi2DOF, ctau) :
    """
    Create and return a Lc -> n K- pi+ pi+ Selection object
    
    @arg name: name of the Selection.
    @arg KaonPID: PID of Kaon
    @arg PionPID: PID of pion
    @arg TrGhostProb: Ghost probability of pion/kaon
    @arg MinTrackPT: PT of the tracks
    @arg MinNeutronPT: PT of photon (used instead of neutron)
    @arg MinTrackIPchi2: IPchi2 of tracks
    @arg MinLcPT: Lc PT
    @arg MaxLcVertChi2DOF: vertex chi2
    @arg ctau: Lc ctau
    @return: Selection object
    
    """

    _daughterCutK  = "(TRGHOSTPROB<%(TrGhostProb)s) & (PT > %(MinTrackPT)s) & (MIPCHI2DV(PRIMARY) > %(MinTrackIPchi2)s) & (PIDK  > %(KaonPID)s)" % locals()
    _daughterCutpi = "(TRGHOSTPROB<%(TrGhostProb)s) & (PT > %(MinTrackPT)s) & (MIPCHI2DV(PRIMARY) > %(MinTrackIPchi2)s) & (PIDK  < %(PionPID)s)" % locals()
    _daughterCutn  = "(PT > %(MinNeutronPT)s)" % locals()
    
    _combinationCut = "(in_range( (%(MinLcMass)s*MeV-50*MeV), AM, (%(MaxLcMass)s*MeV+50*MeV))) & (APT > (%(MinLcPT)s*MeV-300*MeV))" % locals()
    _motherCut      = "(in_range( (%(MinLcMass)s*MeV), M, (%(MaxLcMass)s*MeV))) & ((BPVLTIME(9)*c_light) >  %(ctau)s*mm) & (PT > %(MinLcPT)s*MeV) & (VFASPF(VCHI2/VDOF) < %(MaxLcVertChi2DOF)s)" % locals()
    _Lc = CombineParticles(DecayDescriptor = "[Lambda_c+ ->  gamma K- pi+ pi+]cc",
                             DaughtersCuts   = {"K+"    : _daughterCutK,
                                                "pi+"   : _daughterCutpi,
                                                "gamma" : _daughterCutn},
                             CombinationCut  = _combinationCut,
                             MotherCut       = _motherCut)
    
    _stdKaon = DataOnDemand(Location="Phys/StdAllLooseKaons/Particles")
    _stdPion = DataOnDemand(Location="Phys/StdAllLoosePions/Particles")
    _stdNeutron = DataOnDemand(Location="Phys/StdLooseAllPhotons/Particles")
    
    return Selection(name, Algorithm=_Lc, RequiredSelections=[_stdKaon, _stdPion, _stdNeutron])

def makeSigmac(name, LcSel, DecDescr, TrGhostProb, MinPionPT, MaxTrackIPchi2, SigmacPT, QValueSigmacDecay, MaxSigmacVertChi2DOF, PionPID):
    """
    Create and return a Sigmac -> Lambda_c+ pi+- Selection object, starting with the daughters' selections.

    @arg name: name of the Selection.
    @arg LcSel: Lc selection.
    @arg DecDescr: Decay descriptor
    @arg TrGhostProb: Ghost probability of kaon
    @arg MinPionPT: PT of soft pionn
    @arg MaxTrackIPchi2: IPCHI2 soft pion
    @arg SigmacPT: PT Sigmac
    @arg QValueSigmacDecay: Mass difference m(Lcpi) - m(Lc) - m(pi)
    @arg MaxSigmacVertChi2DOF: chi2/NDOF
    @arg PionPID: Pion PID
    @return: Selection object
    """

    #["[Sigma_c0 -> Lambda_c+ pi-]cc", "[Sigma_c++ -> Lambda_c+ pi+]cc"],

    _daughterCutpi  = "(TRGHOSTPROB<%(TrGhostProb)s) & (PT > %(MinPionPT)s) & (MIPCHI2DV(PRIMARY) < %(MaxTrackIPchi2)s) & (PIDK  < %(PionPID)s)" % locals()
    _combinationCut = "((AM-AM1-AM2) < (1.5*%(QValueSigmacDecay)s*MeV)) & (APT > (0.75*%(SigmacPT)s*MeV))" % locals()
    _motherCut = "((M-M1-M2) < %(QValueSigmacDecay)s*MeV) & (VFASPF(VCHI2/VDOF) < %(MaxSigmacVertChi2DOF)s) & (PT > %(SigmacPT)s*MeV)" % locals()
    _Sigmac = CombineParticles(DecayDescriptor  = DecDescr, 
                               DaughtersCuts    = {"pi+" : _daughterCutpi},
                               CombinationCut   = _combinationCut,
                               MotherCut        = _motherCut)
    
    _stdPion = DataOnDemand(Location="Phys/StdAllLoosePions/Particles")
    return Selection(name, Algorithm=_Sigmac, RequiredSelections=[LcSel, _stdPion])
                                
# EOF
