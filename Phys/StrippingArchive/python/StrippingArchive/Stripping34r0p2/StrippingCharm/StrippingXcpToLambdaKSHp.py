###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Exclusive lines for Xc+ -> Lambda0 KS0 h+
"""
__author__ = ["Marian Stahl"]

__all__ = ("XcpToLambdaKSHpConf", "default_config")

moduleName = "XcpToLambdaKSHp"

# Import Packages
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import AutomaticData
from PhysConf.Selections import FilterSelection, Combine3BodySelection, MergedSelection
from Configurables import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

# Default configuration dictionary
default_config = {
  "NAME": "XcpToLambdaKSHp",
  "BUILDERTYPE": "XcpToLambdaKSHpConf",
  "CONFIG": {
    "descriptor_pion" : ["[Xi_c+ -> Lambda0 KS0 pi+]cc"],
    "descriptor_kaon" : ["[Xi_c+ -> Lambda0 KS0 K+]cc"],
    "bach_pion" : {
      "tes"    : "Phys/StdAllNoPIDsPions/Particles",
      "filter" : "(P>2*GeV) & (PT>150*MeV) & (MIPCHI2DV(PRIMARY)>1.5) & (PROBNNpi>0.05)"
    },
    "bach_kaon" : {
      "tes"    : "Phys/StdAllNoPIDsKaons/Particles",
      "filter" : "(P>4.5*GeV) & (PT>300*MeV) & (MIPCHI2DV(PRIMARY)>1) & (PROBNNk>0.05)"
    },
    "kshort_ll" : {
      "tes"    : "Phys/StdVeryLooseKsLL/Particles",
      "filter" : """(ADMASS('KS0')<25*MeV) & (PT>300*MeV) & (CHI2VXNDF<24) & (BPVVDZ>4*mm) & (BPVVDCHI2>12) & (CHILDIP(1)<0.8*mm) &
                    (CHILDIP(2)<0.8*mm) & (DOCA(1,2)<0.8*mm) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>12) & (MAXTREE('pi+'==ABSID,PT)>100*MeV)"""
    },
    "kshort_dd" : {
      "tes"    : "Phys/StdLooseKsDD/Particles",
      "filter" : "(ADMASS('KS0')<25*MeV) & (PT>400*MeV) & (CHI2VXNDF<24) & (CHILDIP(1)<3*mm) & (CHILDIP(2)<3*mm) & (MAXTREE('pi+'==ABSID,PT)>150*MeV)"
    },
    "lambda_ll" : {
      "tes"    : "Phys/StdVeryLooseLambdaLL/Particles",
      "filter" : """(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>800*MeV) & (BPVVDZ>8*mm) & (BPVVDCHI2>32) &
                    (DOCA(1,2)<0.6*mm) & (DOCACHI2(1,2)<16) & (MAXTREE('p+'==ABSID,PT)>600*MeV) & (MAXTREE('p+'==ABSID,P)>7.5*GeV) &
                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>16) & (MAXTREE('pi+'==ABSID,PT)>100*MeV) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>16)"""
    },
    "lambda_dd" : {
      "tes"    : "Phys/StdLooseLambdaDD/Particles",
      "filter" : """(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>1*GeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV) &
                    (MAXTREE('p+'==ABSID,PT)>800*MeV) & (MAXTREE('pi+'==ABSID,PT)>150*MeV)"""
    },
    "xcp_ll" : {
      "comb12_cut"      : "(ACHI2DOCA(1,2)<12) & (ADOCA(1,2)<0.4*mm) & (AMASS(1,2)<2470*MeV)",
      "comb_cut"        : "(ADOCA(1,3)<0.25*mm) & (ADOCA(2,3)<0.3*mm) & (ACHI2DOCA(1,3)<12) & (ACHI2DOCA(1,3)<12) & (ASUM(PT)>2*GeV) & (in_range(2070*MeV,AMASS(),2610*MeV))",
      "mother_cut"      : """(P>24*GeV) & (PT>1.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0*mm) & (in_range(2120*MeV,M,2560*MeV)) &
                             (CHILDIP(1)<0.25*mm) & (CHILDIP(2)<0.4*mm) & (CHILDIP(3)<0.2*mm) & (CHILDIPCHI2(1)<12) & (CHILDIPCHI2(2)<12) & (CHILDIPCHI2(3)<8) &
                             ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>8*mm) & ((CHILD(VFASPF(VZ),2) - VFASPF(VZ))>4*mm)"""
    },
    "xcp_ld" : {
      "comb12_cut"      : "(ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<24) & (AMASS(1,2)<2470*MeV)",
      "comb_cut"        : "(ADOCA(1,3)<0.3*mm) & (ADOCA(2,3)<1.6*mm) & (ACHI2DOCA(1,3)<12) & (ACHI2DOCA(2,3)<16) & (ASUM(PT)>2.1*GeV) & (in_range(2070*MeV,AMASS(),2610*MeV))",
      "mother_cut"      : """(P>24*GeV) & (PT>1.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0*mm) & (in_range(2120*MeV,M,2560*MeV)) & (CHILDIP(1)<0.25*mm) & (CHILDIP(2)<3*mm) &
                             (CHILDIP(3)<0.2*mm) & (CHILDIPCHI2(1)<14) & (CHILDIPCHI2(2)<16) & (CHILDIPCHI2(3)<8) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>8*mm)"""
    },
    "xcp_dl" : {
      "comb12_cut"      : "(ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<20) & (AMASS(1,2)<2470*MeV)",
      "comb_cut"        : "(ADOCA(1,3)<1.4*mm) & (ADOCA(2,3)<0.3*mm) & (ACHI2DOCA(1,3)<16) & (ACHI2DOCA(2,3)<12) & (ASUM(PT)>2.2*GeV) & (in_range(2070*MeV,AMASS(),2610*MeV))",
      "mother_cut"      : """(P>24*GeV) & (PT>1.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0*mm) & (in_range(2120*MeV,M,2560*MeV)) & (CHILDIP(1)<2*mm) & (CHILDIP(2)<0.4*mm) &
                             (CHILDIP(3)<0.2*mm) & (CHILDIPCHI2(1)<16) & (CHILDIPCHI2(2)<14) & (CHILDIPCHI2(3)<8) & ((CHILD(VFASPF(VZ),2) - VFASPF(VZ))>4*mm)"""
    },
  },
  "STREAMS" : {
      "Charm" : ["StrippingXcpToLambdaKSHp_PiLine","StrippingXcpToLambdaKSHp_KLine"]
  },
  "WGs": ["Charm"]
}

class XcpToLambdaKSHpConf(LineBuilder):

  __configuration_keys__ = default_config["CONFIG"].keys()

  def __init__(self, moduleName, config):
    LineBuilder.__init__(self, moduleName, config)

    bach_pion = FilterSelection(moduleName+"_bach_pion", [AutomaticData(config["bach_pion"]["tes"])], Code=config["bach_pion"]["filter"])
    bach_kaon = FilterSelection(moduleName+"_bach_kaon", [AutomaticData(config["bach_kaon"]["tes"])], Code=config["bach_kaon"]["filter"])
    lambda_ll = FilterSelection(moduleName+"_lambda_ll", [AutomaticData(config["lambda_ll"]["tes"])], Code=config["lambda_ll"]["filter"])
    lambda_dd = FilterSelection(moduleName+"_lambda_dd", [AutomaticData(config["lambda_dd"]["tes"])], Code=config["lambda_dd"]["filter"])
    kshort_ll = FilterSelection(moduleName+"_kshort_ll", [AutomaticData(config["kshort_ll"]["tes"])], Code=config["kshort_ll"]["filter"])
    kshort_dd = FilterSelection(moduleName+"_kshort_dd", [AutomaticData(config["kshort_dd"]["tes"])], Code=config["kshort_dd"]["filter"])

    xcp_lkspi_ll = Combine3BodySelection(moduleName+"_xcp_lkspi_ll", [lambda_ll, kshort_ll, bach_pion], DecayDescriptors=config["descriptor_pion"],
                                         Combination12Cut=config["xcp_ll"]["comb12_cut"], CombinationCut=config["xcp_ll"]["comb_cut"],
                                         MotherCut=config["xcp_ll"]["mother_cut"])
    xcp_lkspi_ld = Combine3BodySelection(moduleName+"_xcp_lkspi_ld", [lambda_ll, kshort_dd, bach_pion], DecayDescriptors=config["descriptor_pion"],
                                         Combination12Cut=config["xcp_ll"]["comb12_cut"], CombinationCut=config["xcp_ll"]["comb_cut"],
                                         MotherCut=config["xcp_ll"]["mother_cut"])
    xcp_lkspi_dl = Combine3BodySelection(moduleName+"_xcp_lkspi_dl", [lambda_dd, kshort_ll, bach_pion], DecayDescriptors=config["descriptor_pion"],
                                         Combination12Cut=config["xcp_ll"]["comb12_cut"], CombinationCut=config["xcp_ll"]["comb_cut"],
                                         MotherCut=config["xcp_ll"]["mother_cut"])

    xcp_lksk_ll = Combine3BodySelection(moduleName+"_xcp_lksk_ll", [lambda_ll, kshort_ll, bach_kaon], DecayDescriptors=config["descriptor_kaon"],
                                        Combination12Cut=config["xcp_ll"]["comb12_cut"], CombinationCut=config["xcp_ll"]["comb_cut"],
                                        MotherCut=config["xcp_ll"]["mother_cut"])
    xcp_lksk_ld = Combine3BodySelection(moduleName+"_xcp_lksk_ld", [lambda_ll, kshort_dd, bach_kaon], DecayDescriptors=config["descriptor_kaon"],
                                        Combination12Cut=config["xcp_ll"]["comb12_cut"], CombinationCut=config["xcp_ll"]["comb_cut"],
                                        MotherCut=config["xcp_ll"]["mother_cut"])
    xcp_lksk_dl = Combine3BodySelection(moduleName+"_xcp_lksk_dl", [lambda_dd, kshort_ll, bach_kaon], DecayDescriptors=config["descriptor_kaon"],
                                        Combination12Cut=config["xcp_ll"]["comb12_cut"], CombinationCut=config["xcp_ll"]["comb_cut"],
                                        MotherCut=config["xcp_ll"]["mother_cut"])

    xcp_lkspi = MergedSelection(moduleName+"_lkspi_comb", RequiredSelections=[xcp_lkspi_ll, xcp_lkspi_ld, xcp_lkspi_dl])
    xcp_lksk  = MergedSelection(moduleName+"_lksk_comb",  RequiredSelections=[xcp_lksk_ll,  xcp_lksk_ld,  xcp_lksk_dl])

    # Create the stripping lines
    self.registerLine(StrippingLine(moduleName+"_PiLine", algos=[xcp_lkspi]))
    self.registerLine(StrippingLine(moduleName+"_KLine", algos=[xcp_lksk]))
