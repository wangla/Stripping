###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of HeL->ppppi Stripping Selections and StrippingLines.
Provides class HeL2ppppiConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Exported symbols (use python help!):
   - HeL2ppppiConf
"""

__author__  = ['Hendrik Jage', 'Gediminas Sarpis']
__date__    = '19/03/2021'
__version__ = 'v1r0'
__all__     = {'HeL2ppppiConf',
               'default_config'}

from Gaudi.Configuration                   import *
from PhysSelPython.Wrappers                import Selection
from StrippingConf.StrippingLine           import StrippingLine
from StrippingUtils.Utils                  import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, DaVinci__N4BodyDecays

from StandardParticles                     import StdLooseANNPions   as Pions
from StandardParticles                     import StdLooseANNProtons as Protons

default_config = {
    'NAME'        : 'HeL2ppppi',
    'WGs'         : ['IFT'],
    'BUILDERTYPE' : 'HeL2ppppiConf',
    'CONFIG'      : {'Trk_MaxChi2Ndof'    : 3.0,
                     'Trk_MaxGhostProb'   : 0.4,
                     'Trk_MinIPChi2'      : 9.0,

                     'Proton_MinP'        : 3000.0,
                     'Proton_MinProbNNp'  : 0.10,
                     'Pion_MinP'          : 1500.0,
                     'Pion_MinProbNNpi'   : 0.05,

                     'HeL_MaxM_4body'     : 5200.0,
                     'HeL_MaxM'           : 5000.0,
                     'HeL_MaxDOCAChi2'    : 20.0,
                     'HeL_MaxVtxChi2Ndof' : 10.0,
                     'HeL_MinFDChi2'      : 100.0,
                     'HeL_MaxIPChi2'      : 25.0,
                     'HeL_MinDira'        : 0.9995,
                     'Prescale'          : 1.0,
                     'Postscale'         : 1.0
                     },
    'STREAMS'     : ['BhadronCompleteEvent']
    }

class HeL2ppppiConf(LineBuilder) :
    """
    Builder of HeL -> ppppi Stripping Selection and StrippingLine.
    Constructs HeL -> ppppi Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config              = { .... }
    >>> HeL2ppppiConf  = HeL2ppppiConf('HeL2ppppiTest', config)
    >>> HeL2ppppiLines = HeL2ppppiConf.lines
    >>> for line in HeL2ppppiLines :
    >>>   print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selHeL2ppppi   : HeL -> ppppi
    lineHeL2ppppi  : StrippingLine made out of selHeL2ppppi
    lines        : List of lines, [lineHeL2ppppi]

    Exports as class data member:
    HeL2ppppiConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        self.pion      = Pions
        self.proton    = Protons

        trkFilterP     = FilterDesktop(Code = '(MIPCHI2DV(PRIMARY) > %(Trk_MinIPChi2)s)    & \
                                               (TRGHOSTPROB        < %(Trk_MaxGhostProb)s) & \
                                               (TRCHI2DOF          < %(Trk_MaxChi2Ndof)s)  & \
                                               (P                  > %(Proton_MinP)s *MeV)    & \
                                               (PROBNNp            > %(Proton_MinProbNNp)s)' % config )

        trkFilterPi    = FilterDesktop(Code = '(MIPCHI2DV(PRIMARY) > %(Trk_MinIPChi2)s)    & \
                                               (TRGHOSTPROB        < %(Trk_MaxGhostProb)s) & \
                                               (TRCHI2DOF          < %(Trk_MaxChi2Ndof)s)  & \
                                               (P                  > %(Pion_MinP)s *MeV)    & \
                                               (PROBNNpi           > %(Pion_MinProbNNpi)s)' % config )

        self.myPions   = Selection( 'PionsFor'+name,
                                    Algorithm = trkFilterPi,
                                    RequiredSelections = [self.pion] )

        self.myProtons = Selection( 'ProtonsFor'+name,
                                    Algorithm = trkFilterP,
                                    RequiredSelections = [self.proton] )

        self.makeHeL2ppppi(name+'_4body', config)

        self.lineHeL2ppppi = StrippingLine('HeL2ppppiLine',
                                         prescale         = config['Prescale'],
                                         postscale        = config['Postscale'],
                                         selection        = self.selHeL2ppppi,
                                         RequiredRawEvents = ["Rich","Velo","Tracker","Calo"],
                                         )

        self.registerLine(self.lineHeL2ppppi)

    def makeHeL2ppppi(self, name, config) :
        # Define all the cuts
        _mass12CutPreVtx          = '(AM < (%s - 1000)*MeV)'                                  % config['HeL_MaxM_4body']
        _doca12chi2CutPreVtx      = '(ACHI2DOCA(1,2) < %s)'                                   % config['HeL_MaxDOCAChi2']
        _combCuts12               = _mass12CutPreVtx
        _combCuts12              += ' & '+_doca12chi2CutPreVtx

        _mass123CutPreVtx         = '(AM < (%s - 100)*MeV)'                                   % config['HeL_MaxM_4body']
        _doca123chi2CutPreVtx13   = '(ACHI2DOCA(1,3) < %s)'                                   % config['HeL_MaxDOCAChi2']
        _doca123chi2CutPreVtx23   = '(ACHI2DOCA(2,3) < %s)'                                   % config['HeL_MaxDOCAChi2']
        _combCuts123              = _mass123CutPreVtx
        _combCuts123             += ' & '+_doca123chi2CutPreVtx13
        _combCuts123             += ' & '+_doca123chi2CutPreVtx23

        _massCutPreVtx            = '(AM             < %s*MeV)'                               % config['HeL_MaxM_4body']
        _docachi2CutPreVtx14      = '(ACHI2DOCA(1,4) < %s)'                                   % config['HeL_MaxDOCAChi2']
        _docachi2CutPreVtx24      = '(ACHI2DOCA(2,4) < %s)'                                   % config['HeL_MaxDOCAChi2']
        _docachi2CutPreVtx34      = '(ACHI2DOCA(3,4) < %s)'                                   % config['HeL_MaxDOCAChi2']

        _combCuts                 = _massCutPreVtx
        _combCuts                += ' & '+_docachi2CutPreVtx14
        _combCuts                += ' & '+_docachi2CutPreVtx24
        _combCuts                += ' & '+_docachi2CutPreVtx34

        _massCutVtx               = '(M                   < %s*MeV)'                           % config['HeL_MaxM']
        _vtxChi2CutPostVtx        = '(VFASPF(VCHI2/VDOF)  < %s)'                               % config['HeL_MaxVtxChi2Ndof']
        _fdChi2CutPostVtx         = '(BPVVDCHI2           > %s)'                               % config['HeL_MinFDChi2']
        _diraCutPostVtx           = '(BPVDIRA             > %s)'                               % config['HeL_MinDira']
        _ipChi2CutPostVtx         = '(BPVIPCHI2()         < %s)'                               % config['HeL_MaxIPChi2']
        _motherCuts               = _massCutVtx
        _motherCuts              += ' & '+_vtxChi2CutPostVtx
        _motherCuts              += ' & '+_fdChi2CutPostVtx
        _motherCuts              += ' & '+_diraCutPostVtx 
        _motherCuts              += ' & '+_ipChi2CutPostVtx

        _HeL                       = DaVinci__N4BodyDecays()
        _HeL.Combination12Cut      = _combCuts12
        _HeL.Combination123Cut     = _combCuts123
        _HeL.CombinationCut        = _combCuts
        _HeL.MotherCut             = _motherCuts
        _HeL.DecayDescriptor       = "[Lambda_b0 -> p+ p+ p+ pi-]cc"
        _HeL.ReFitPVs              = True

        self.selHeL2ppppi           = Selection(name, Algorithm = _HeL, RequiredSelections = [self.myProtons, self.myPions])
