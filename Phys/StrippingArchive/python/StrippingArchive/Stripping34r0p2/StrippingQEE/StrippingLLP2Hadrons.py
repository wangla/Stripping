#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Stripping lines selecting scalar long-lived particles decaying to hadrons
"""

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, SimpleSelection, MergedSelection, AutomaticData
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine


__author__  = 'Andrii Usachov'
__date__    = '2021-15-06'
__version__ = 1.0
__all__     = 'LLP2HadronsConf', 'default_config'

default_config = {
  'NAME'        : 'LLP2Hadrons',
  'BUILDERTYPE' : 'LLP2HadronsConf',
  'WGs'         : [ 'QEE' ],
  'STREAMS'     : {
    'BhadronCompleteEvent': [
      'StrippingLLP2Hadrons_KstKst_LongLine',
      'StrippingLLP2Hadrons_KsKpi_LL_LongLine',
      'StrippingLLP2Hadrons_KsKpi_DD_LongLine',
      'StrippingLLP2Hadrons_DpDm_LongLine',
      'StrippingLLP2Hadrons_DpDz_LongLine',
      'StrippingLLP2Hadrons_DpDp_LongLine',
      'StrippingLLP2Hadrons_DzDz_LongLine',
      'StrippingLLP2Hadrons_DzDzbar_LongLine',
      'StrippingLLP2Hadrons_EtaPiPi_LongLine',
      'StrippingLLP2Hadrons_KstKst_DownLine',
      'StrippingLLP2Hadrons_KsKpi_DD_DownLine',
      'StrippingLLP2Hadrons_DpDm_DownLine',
      'StrippingLLP2Hadrons_DpDz_DownLine',
      'StrippingLLP2Hadrons_DpDp_DownLine',
      'StrippingLLP2Hadrons_DzDz_DownLine',
      'StrippingLLP2Hadrons_DzDzbar_DownLine',
      'StrippingLLP2Hadrons_EtaPiPi_DownLine',
    ],
  },
  'CONFIG'      : {
    'Common': {
      'CommonTrackCuts': "(TRGHOSTPROB<0.3) & (P>3.*GeV) & (BPVIPCHI2() > 9.)",
    },
    'Prescales': {
      'KstKst'        : 1.0,
      'KsKpi'         : 1.0,
      'DD'            : 1.0,
      'EtaPiPi'       : 1.0,
    },
    'KstKst': {
      'Long':{
          'TrackCuts'     : "(PT>500*MeV)",
          'K_id'          : "(PROBNNk  > 0.4)",
          'Pi_id'         : "(PROBNNpi > 0.4)",
          'KstCombCuts'   : "(ADAMASS('K*(892)0')<300*MeV)",
          'KstMomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))",
          'CombCuts'      : "AALL",
          'MomCuts'       : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 10) & (PT>2.*GeV) & (BPVIPCHI2() < 9.)"
      },
     'Down':{
          'TrackCuts'     : "(PT>500*MeV)",
          'KstCombCuts'   : "(ADAMASS('K*(892)0')<300*MeV)",
          'KstMomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (in_range( PDGM('K*(892)0') - 120*MeV , M , PDGM('K*(892)0') + 120*MeV ))",
          'CombCuts'      : "AALL",
          'MomCuts'       : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS > 5)  & (VFASPF(VZ)>200*mm) & (PT>2.*GeV)"
      },
    },
    'KsKpi_LL': {
      'Long':{
          'TrackCuts'    : "(PT > 700*MeV)", 
          'K_id'         : "(PROBNNk  > 0.4)",
          'Pi_id'        : "(PROBNNpi > 0.4)",
          'Ks_PiCuts'    : "(MINTREE('pi-'==ABSID, PROBNNpi) > 0.4) & (MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'Ks_MomCuts'   : "(VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.) & (BPVDLS>5) & (ADMASS('KS0') < 30.*MeV)",
          'CombCuts'     : "in_range(1.0*GeV, AM, 12.2*GeV)",
          'MomCuts'      : "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF)<9.) & (BPVDLS>10) & (PT>2.*GeV) & (BPVIPCHI2()<9.) & (BPVLTIME()>1.5*picosecond)"
      }
    },
    'KsKpi_DD': {
        'Long':{
          'TrackCuts'  : "(PT > 700*MeV)", 
          'K_id'       : "(PROBNNk  > 0.4)",
          'Pi_id'      : "(PROBNNpi > 0.4)",
          'Ks_PiCuts'  : "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, MIPCHI2DV(PRIMARY)) > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'Ks_MomCuts' : "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 30.*MeV) & (BPVIPCHI2() > 9.)",
          'CombCuts'   : "in_range(1.0*GeV, AM, 12.2*GeV)",
          'MomCuts'    : "in_range(1.0*GeV, M, 12.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (PT>2.*GeV) & (BPVIPCHI2() < 9.) & (BPVLTIME()>1.*picosecond)"
        },
        'Down':{
          'TrackCuts'  : "(PT>700*MeV)", 
          'Ks_PiCuts'  : "(MAXTREE('pi-'==ABSID, TRGHOSTPROB) < 0.3) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'Ks_MomCuts' : "(VFASPF(VCHI2PDOF) < 9.) & (ADMASS('KS0') < 50.*MeV) & (BPVIPCHI2() > 9.) & (MINTREE('pi-'==ABSID, PT) > 500*MeV)",
          'CombCuts'   : "AM>1.*GeV",
          'MomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5)  & (VFASPF(VZ)>200*mm) & (BPVIPCHI2() < 9.)"
        },      
    },
    'DD': {
        'Long':{
          'TrackCuts'  : "(PT > 500*MeV)", 
          'K_id'       : "(PROBNNk  > 0.2)",
          'Pi_id'      : "(PROBNNpi > 0.2)",
          'DCombCuts'  : "in_range(1.0*GeV, AM, 3.*GeV)",
          'DMomCuts'   : "(ADMASS('D+') < 80.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 4.)",
          'CombCuts'   : "(AM>3.0*GeV)",
          'MomCuts'    : "(M>3.0*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (PT>1.*GeV) & (BPVDLS>5)"
        },
        'Down':{
          'TrackCuts'  : "(PT>500*MeV)", 
          'DCombCuts'  : "in_range(1.0*GeV, AM, 3.*GeV)",
          'DMomCuts'   : "(ADMASS('D+') < 150.*MeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVIPCHI2() > 9.)",
          'CombCuts'   : "(AM>3.0*GeV)",
          'MomCuts'    : "(M>3.0*GeV) & (PT>1.*GeV) & (VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>3) & (VFASPF(VZ)>200*mm)"        
        },
    },
    'EtaPiPi': {
        'Long':{
          'TrackCuts'  : "(PT>500*MeV)",
          'Pi_id'      : "(PROBNNpi > 0.4)",
          'Pi0_cuts'   : "(PT>600*MeV)",
          'EtaCombCuts': "(AM<1*GeV)",
          'EtaMomCuts' : "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)",
          'CombCuts'   : "AALL",
          'MomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (PT>2.*GeV) & (BPVDLS>5) & (BPVIPCHI2() < 9.) & (M<2.5*GeV) & (BPVLTIME()>1.5*picosecond)"
        },
        'Down':{
          'TrackCuts'  : "(PT>500*MeV)",
          'Pi0_cuts'   : "(PT > 600*MeV)",
          'EtaCombCuts': "AALL",
          'EtaMomCuts' : "(ADMASS('eta') < 100.*MeV) & (VFASPF(VCHI2PDOF) < 9.)",
          'CombCuts'   : "AALL",
          'MomCuts'    : "(VFASPF(VCHI2PDOF) < 9.) & (BPVDLS>5) & (VFASPF(VZ)>200*mm) & (M<2.5*GeV)"
        },
    },
  },
}


#===============================================================================

class LLP2HadronsConf(LineBuilder):

  __configuration_keys__ = default_config['CONFIG'].keys()  # Legacy field

  def __init__(self, name, config):
    LineBuilder.__init__(self, name, config)

    ## Inject 'Common' into all other confs
    d = config.pop('Common')

    prescales = config['Prescales']
    decays = ['KsKpi_LL','KsKpi_DD','DD','EtaPiPi','KstKst']
    for key in decays:
      for tr_type in config[key]:
        config[key][tr_type].update(d)

    ## Register lines
    self.EtaPiPi(confs=config['EtaPiPi'])
    self.KstKst(confs=config['KstKst'])
    self.KsKpi(confs={"LL": config['KsKpi_LL'], "DD": config['KsKpi_DD']})
    self.DoubleCharm(confs=config['DD']) 

    self.LLP2EtaPiPi_LongLine = StrippingLine( name+'_EtaPiPi_LongLine', 
                                          selection = self.SelLLP2EtaPiPi_3pi_Long,
                                          prescale  = prescales['EtaPiPi'])
    self.registerLine(self.LLP2EtaPiPi_LongLine)


    self.LLP2KstKst_LongLine  = StrippingLine( name+'_KstKst_LongLine', 
                                          selection = self.SelLLP2KstKst_Long,
                                          prescale  = prescales['KstKst'])
    self.registerLine(self.LLP2KstKst_LongLine)


    self.LLP2KsKpi_DD_LongLine = StrippingLine( name+'_KsKpi_DD_LongLine', 
                                          selection = self.SelLLP2KsKpi_DD_Long,
                                          prescale  = prescales['KsKpi'])
    self.registerLine( self.LLP2KsKpi_DD_LongLine)


    self.LLP2DD_DpDm_LongLine  = StrippingLine( name+'_DpDm_LongLine',
                                          selection = self.SelLLP2DD_DpDm_Long,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DpDm_LongLine)


    self.LLP2DD_DpDp_LongLine  = StrippingLine( name+'_DpDp_LongLine',
                                          selection = self.SelLLP2DD_DpDp_Long,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DpDp_LongLine)

    self.LLP2DD_DzDz_LongLine  = StrippingLine( name+'_DzDz_LongLine',
                                          selection = self.SelLLP2DD_DzDz_Long,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DzDz_LongLine)

    self.LLP2DD_DzDzbar_LongLine  = StrippingLine( name+'_DzDzbar_LongLine',
                                          selection = self.SelLLP2DD_DzDzbar_Long,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DzDzbar_LongLine)

    self.LLP2DD_DpDz_LongLine  = StrippingLine( name+'_DpDz_LongLine',
                                          selection = self.SelLLP2DD_DpDz_Long,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DpDz_LongLine)


    # ==========================================================================================
    self.LLP2KsKpi_LL_LongLine = StrippingLine( name+'_KsKpi_LL_LongLine',
                                          selection = self.SelLLP2KsKpi_LL_Long,
                                          prescale  = prescales['KsKpi'])
    self.registerLine(self.LLP2KsKpi_LL_LongLine)
    # ==========================================================================================

      
    self.LLP2EtaPiPi_DownLine = StrippingLine( name+'_EtaPiPi_DownLine', 
                                          selection = self.SelLLP2EtaPiPi_3pi_Down,
                                          prescale  = prescales['EtaPiPi'])
    self.registerLine(self.LLP2EtaPiPi_DownLine)


    self.LLP2KstKst_DownLine  = StrippingLine( name+'_KstKst_DownLine', 
                                          selection = self.SelLLP2KstKst_Down,
                                          prescale  = prescales['KstKst'])
    self.registerLine(self.LLP2KstKst_DownLine)


    self.LLP2KsKpi_DD_DownLine = StrippingLine( name+'_KsKpi_DD_DownLine', 
                                          selection = self.SelLLP2KsKpi_DD_Down,
                                          prescale  = prescales['KsKpi'])
    self.registerLine( self.LLP2KsKpi_DD_DownLine)


    self.LLP2DD_DpDm_DownLine  = StrippingLine( name+'_DpDm_DownLine',
                                          selection = self.SelLLP2DD_DpDm_Down,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DpDm_DownLine)


    self.LLP2DD_DpDp_DownLine  = StrippingLine( name+'_DpDp_DownLine',
                                          selection = self.SelLLP2DD_DpDp_Down,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DpDp_DownLine)

    self.LLP2DD_DzDz_DownLine  = StrippingLine( name+'_DzDz_DownLine',
                                          selection = self.SelLLP2DD_DzDz_Down,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DzDz_DownLine)

    self.LLP2DD_DzDzbar_DownLine  = StrippingLine( name+'_DzDzbar_DownLine',
                                          selection = self.SelLLP2DD_DzDzbar_Down,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DzDzbar_DownLine)

    self.LLP2DD_DpDz_DownLine  = StrippingLine( name+'_DpDz_DownLine',
                                          selection = self.SelLLP2DD_DpDz_Down,
                                          prescale  = prescales['DD'])
    self.registerLine(self.LLP2DD_DpDz_DownLine)



  def createSubSel(self, OutputList, InputList, Cuts):
    '''create a selection using a FilterDesktop'''
    filt = FilterDesktop(Code=Cuts)
    return Selection(
        OutputList, Algorithm=filt, RequiredSelections=[InputList])

  def createCombinationSel( self,
                            OutputList,
                            DecayDescriptor,
                            DaughterLists,
                            DaughterCuts={},
                            PreVertexCuts="ALL",
                            PostVertexCuts="ALL",
                            ReFitPVs=False):
    '''create a selection using a ParticleCombiner with a single decay descriptor'''
    combiner = CombineParticles(
        DecayDescriptor=DecayDescriptor,
        DaughtersCuts=DaughterCuts,
        MotherCut=PostVertexCuts,
        CombinationCut=PreVertexCuts,
        ReFitPVs=ReFitPVs)
    return Selection(OutputList, Algorithm=combiner, RequiredSelections=DaughterLists)

  def sel_Long_kaons(self, conf, name=""):
    return self.createSubSel(
                OutputList=name,
                InputList=AutomaticData(Location='Phys/StdAllNoPIDsKaons/Particles'),
                Cuts=conf['CommonTrackCuts']+" & "+conf['TrackCuts']+" & "+conf['K_id'])

  def sel_Down_kaons(self, conf, name=""):
    return self.createSubSel(
          OutputList=name,
          InputList=AutomaticData(Location='Phys/StdNoPIDsDownKaons/Particles'),
          Cuts=conf['CommonTrackCuts']+" & "+conf['TrackCuts'])

  def sel_Long_pions(self, conf, name=""):
    return self.createSubSel(
                OutputList=name,
                InputList=AutomaticData(Location='Phys/StdAllNoPIDsPions/Particles'),
                Cuts=conf['CommonTrackCuts']+" & "+conf['TrackCuts']+" & "+conf['Pi_id'])

  def sel_Down_pions(self, conf, name=""):
    return self.createSubSel(
                OutputList=name,
                InputList=AutomaticData(Location='Phys/StdNoPIDsDownPions/Particles'),
                Cuts=conf['CommonTrackCuts']+" & "+conf['TrackCuts'])


  def KstKst( self, confs ):

    self.SelKaons4KstKst_Long = self.sel_Long_kaons(conf=confs['Long'], name="SelKaons_KstKst_Long")
    self.SelPions4KstKst_Long = self.sel_Long_pions(conf=confs['Long'], name="SelPions_KstKst_Long")

    self.SelKst_Long = self.createCombinationSel(
          OutputList="SelKst_Long",
          DecayDescriptor="[K*(892)0 -> K+ pi-]cc",
          DaughterLists=[self.SelKaons4KstKst_Long,self.SelPions4KstKst_Long],
          PreVertexCuts=confs['Long']['KstCombCuts'],
          PostVertexCuts=confs['Long']['KstMomCuts'])

    self.SelLLP2KstKst_Long = self.createCombinationSel(
          OutputList="SelLLP2KstKst_Long",
          DecayDescriptor="B0 -> K*(892)0 K*(892)~0",
          DaughterLists=[self.SelKst_Long],
          PreVertexCuts=confs['Long']['CombCuts'],
          PostVertexCuts=confs['Long']['MomCuts']) 

    self.SelKaons4KstKst_Down = self.sel_Down_kaons(conf=confs['Down'], name="SelKaons_KstKst_Down")
    self.SelPions4KstKst_Down = self.sel_Down_pions(conf=confs['Down'], name="SelPions_KstKst_Down")

    self.SelKst_Down = self.createCombinationSel(
          OutputList="SelKst_Down",
          DecayDescriptor="[K*(892)0 -> K+ pi-]cc",
          DaughterLists=[self.SelKaons4KstKst_Down,self.SelPions4KstKst_Down],
          PreVertexCuts=confs['Down']['KstCombCuts'],
          PostVertexCuts=confs['Down']['KstMomCuts'])

    self.SelLLP2KstKst_Down = self.createCombinationSel(
          OutputList="SelLLP2KstKst_Down",
          DecayDescriptor="B0 -> K*(892)0 K*(892)~0",
          DaughterLists=[self.SelKst_Down],
          PreVertexCuts=confs['Down']['CombCuts'],
          PostVertexCuts=confs['Down']['MomCuts'])

  def EtaPiPi(self, confs ):

    self.MergedPiz4EtaPiPi = MergedSelection(
            "MergedPiz4EtaPiPi",
            RequiredSelections=[
                AutomaticData(Location="Phys/StdLooseResolvedPi0/Particles"),
                AutomaticData(Location="Phys/StdLooseMergedPi0/Particles")
            ])

    self.SelPions4EtaPiPi_Long = self.sel_Long_pions(conf=confs['Long'], name="SelPions_EtaPiPi_Long")
    self.SelPiz4EtaPiPi_Long = self.createSubSel(
            OutputList="SelPiz_EtaPiPi_Long",
            InputList=self.MergedPiz4EtaPiPi,
            Cuts=confs['Long']['Pi0_cuts'])
    self.SelEta_3pi_Long = self.createCombinationSel(
          OutputList="SelEta_Long",
          DecayDescriptor="eta -> pi+ pi- pi0",
          DaughterLists=[self.SelPiz4EtaPiPi_Long, self.SelPions4EtaPiPi_Long],
          PreVertexCuts=confs['Long']['EtaCombCuts'],
          PostVertexCuts=confs['Long']['EtaMomCuts'])
    self.SelLLP2EtaPiPi_3pi_Long = self.createCombinationSel(
          OutputList="SelLLP2EtaPiPi_3pi_Long",
          DecayDescriptor="B0 -> eta pi+ pi-",
          DaughterLists=[self.SelEta_3pi_Long, self.SelPions4EtaPiPi_Long],
          PreVertexCuts=confs['Long']['CombCuts'],
          PostVertexCuts=confs['Long']['MomCuts'])


    self.SelPions4EtaPiPi_Down = self.sel_Down_pions(conf=confs['Down'], name="SelPions_EtaPiPi_Down")
    self.SelPiz4EtaPiPi_Down = self.createSubSel(
            OutputList="SelPiz_EtaPiPi_Down",
            InputList=self.MergedPiz4EtaPiPi,
            Cuts=confs['Down']['Pi0_cuts'])
    self.SelEta_3pi_Down = self.createCombinationSel(
          OutputList="SelEta_Down",
          DecayDescriptor="eta -> pi+ pi- pi0",
          DaughterLists=[self.SelPiz4EtaPiPi_Down, self.SelPions4EtaPiPi_Down],
          PreVertexCuts=confs['Down']['EtaCombCuts'],
          PostVertexCuts=confs['Down']['EtaMomCuts'])
    self.SelLLP2EtaPiPi_3pi_Down = self.createCombinationSel(
          OutputList="SelLLP2EtaPiPi_3pi_Down",
          DecayDescriptor="B0 -> eta pi+ pi-",
          DaughterLists=[self.SelEta_3pi_Down, self.SelPions4EtaPiPi_Down],
          PreVertexCuts=confs['Down']['CombCuts'],
          PostVertexCuts=confs['Down']['MomCuts'])


  def KsKpi( self, confs ):

    self.SelKaons4KsKpi_LL_Long = self.sel_Long_kaons(conf=confs['LL']['Long'], name="SelKaons_KsKpi_LL_Long")
    self.SelPions4KsKpi_LL_Long = self.sel_Long_pions(conf=confs['LL']['Long'], name="SelPions_KsKpi_LL_Long")

    self.SelKs4KsKpi_LL_Long = self.createSubSel(
        OutputList="SelKs_LL_Long",
        InputList=AutomaticData(Location="Phys/StdVeryLooseKsLL/Particles"),
        Cuts=confs['LL']['Long']['Ks_PiCuts']+" & "+confs['LL']['Long']['Ks_MomCuts'])

    self.SelLLP2KsKpi_LL_Long = self.createCombinationSel(
        OutputList="SelLLP2KsKpi_LL_Long",
        DecayDescriptor="[B0 -> KS0 K+ pi-]cc",
        DaughterLists=[self.SelKs4KsKpi_LL_Long, self.SelKaons4KsKpi_LL_Long, self.SelPions4KsKpi_LL_Long],
        PreVertexCuts=confs['LL']['Long']['CombCuts'],
        PostVertexCuts=confs['LL']['Long']['MomCuts'])


    self.SelKaons4KsKpi_DD_Long = self.sel_Down_kaons(conf=confs['DD']['Long'], name="SelKaons_KsKpi_DD_Long")
    self.SelPions4KsKpi_DD_Long = self.sel_Down_pions(conf=confs['DD']['Long'], name="SelPions_KsKpi_DD_Long")

    self.SelKs4KsKpi_DD_Long = self.createSubSel(
        OutputList="SelKs_DD_Long",
        InputList=AutomaticData(Location="Phys/StdLooseKsDD/Particles"),
        Cuts=confs['DD']['Long']['Ks_PiCuts']+" & "+confs['DD']['Long']['Ks_MomCuts'])

    self.SelLLP2KsKpi_DD_Long = self.createCombinationSel(
        OutputList="SelLLP2KsKpi_DD_Long",
        DecayDescriptor="[B0 -> KS0 K+ pi-]cc",
        DaughterLists=[self.SelKs4KsKpi_DD_Long, self.SelKaons4KsKpi_DD_Long, self.SelPions4KsKpi_DD_Long],
        PreVertexCuts=confs['DD']['Long']['CombCuts'],
        PostVertexCuts=confs['DD']['Long']['MomCuts'])


    self.SelKaons4KsKpi_DD_Down = self.sel_Down_kaons(conf=confs['DD']['Down'], name="SelKaons_KsKpi_DD_Down")
    self.SelPions4KsKpi_DD_Down = self.sel_Down_pions(conf=confs['DD']['Down'], name="SelPions_KsKpi_DD_Down")

    self.SelKs4KsKpi_DD_Down = self.createSubSel(
        OutputList="SelKs_DD_Down",
        InputList=AutomaticData(Location="Phys/StdLooseKsDD/Particles"),
        Cuts=confs['DD']['Down']['Ks_PiCuts']+" & "+confs['DD']['Down']['Ks_MomCuts'])

    self.SelLLP2KsKpi_DD_Down = self.createCombinationSel(
        OutputList="SelLLP2KsKpi_DD_Down",
        DecayDescriptor="[B0 -> KS0 K+ pi-]cc",
        DaughterLists=[self.SelKs4KsKpi_DD_Down, self.SelKaons4KsKpi_DD_Down, self.SelPions4KsKpi_DD_Down],
        PreVertexCuts=confs['DD']['Down']['CombCuts'],
        PostVertexCuts=confs['DD']['Down']['MomCuts'])



  def DoubleCharm( self, confs ):

    self.SelKaons4DD_Long = self.sel_Long_kaons(conf=confs['Long'], name="SelKaons_DoubleD_Long")
    self.SelPions4DD_Long = self.sel_Long_pions(conf=confs['Long'], name="SelPions_DoubleD_Long")

    self.SelDp_Long = self.createCombinationSel(
          OutputList="SelDp_Long",
          DecayDescriptor="[D+ -> K- pi+ pi+]cc",
          DaughterLists=[self.SelKaons4DD_Long, self.SelPions4DD_Long],
          PreVertexCuts=confs['Long']['DCombCuts'],
          PostVertexCuts=confs['Long']['DMomCuts'])

    self.SelDz_Long = self.createCombinationSel(
          OutputList="SelDz_Long",
          DecayDescriptor="[D0 -> K- pi+]cc",
          DaughterLists=[self.SelKaons4DD_Long, self.SelPions4DD_Long],
          PreVertexCuts=confs['Long']['DCombCuts'],
          PostVertexCuts=confs['Long']['DMomCuts'])

    self.SelKaons4DD_Down = self.sel_Down_kaons(conf=confs['Down'], name="SelKaons_DoubleD_Down")
    self.SelPions4DD_Down = self.sel_Down_pions(conf=confs['Down'], name="SelPions_DoubleD_Down")

    self.SelDp_Down = self.createCombinationSel(
          OutputList="SelDp_Down",
          DecayDescriptor="[D+ -> K- pi+ pi+]cc",
          DaughterLists=[self.SelKaons4DD_Down, self.SelPions4DD_Down],
          PreVertexCuts=confs['Down']['DCombCuts'],
          PostVertexCuts=confs['Down']['DMomCuts'])

    self.SelDz_Down = self.createCombinationSel(
          OutputList="SelDz_Down",
          DecayDescriptor="[D0 -> K- pi+]cc",
          DaughterLists=[self.SelKaons4DD_Down, self.SelPions4DD_Down],
          PreVertexCuts=confs['Down']['DCombCuts'],
          PostVertexCuts=confs['Down']['DMomCuts'])


    descriptors = {
      "DpDm": "Upsilon(1S) -> D+ D-",
      "DpDp": "[Upsilon(1S) -> D+ D+]cc",
      "DpDz": "[Upsilon(1S) -> D+ D0]cc",
      "DzDzbar": "Upsilon(1S) -> D0 D~0",
      "DzDz": "[Upsilon(1S) -> D0 D0]cc"
    }

    self.SelLLP2DD_DpDm_Long = self.createCombinationSel(
        OutputList="SelLLP2DD_DpDm_Long",
        DecayDescriptor=descriptors["DpDm"],
        DaughterLists=[self.SelDp_Long],
        PreVertexCuts=confs['Long']['CombCuts'],
        PostVertexCuts=confs['Long']['MomCuts'])

    self.SelLLP2DD_DpDp_Long = self.createCombinationSel(
        OutputList="SelLLP2DD_DpDp_Long",
        DecayDescriptor=descriptors["DpDp"],
        DaughterLists=[self.SelDp_Long],
        PreVertexCuts=confs['Long']['CombCuts'],
        PostVertexCuts=confs['Long']['MomCuts'])

    self.SelLLP2DD_DpDz_Long = self.createCombinationSel(
        OutputList="SelLLP2DD_DpDz_Long",
        DecayDescriptor=descriptors["DpDz"],
        DaughterLists=[self.SelDz_Long, self.SelDp_Long],
        PreVertexCuts=confs['Long']['CombCuts'],
        PostVertexCuts=confs['Long']['MomCuts'])

    self.SelLLP2DD_DzDz_Long = self.createCombinationSel(
        OutputList="SelLLP2DD_DzDz_Long",
        DecayDescriptor=descriptors["DzDz"],
        DaughterLists=[self.SelDz_Long],
        PreVertexCuts=confs['Long']['CombCuts'],
        PostVertexCuts=confs['Long']['MomCuts'])

    self.SelLLP2DD_DzDzbar_Long = self.createCombinationSel(
        OutputList="SelLLP2DD_DzDzbar_Long",
        DecayDescriptor=descriptors["DzDzbar"],
        DaughterLists=[self.SelDz_Long],
        PreVertexCuts=confs['Long']['CombCuts'],
        PostVertexCuts=confs['Long']['MomCuts'])

    # ============================================================================
      
    self.SelLLP2DD_DpDm_Down = self.createCombinationSel(
        OutputList="SelLLP2DD_DpDm_Down",
        DecayDescriptor=descriptors["DpDm"],
        DaughterLists=[self.SelDp_Down],
        PreVertexCuts=confs['Down']['CombCuts'],
        PostVertexCuts=confs['Down']['MomCuts'])

    self.SelLLP2DD_DpDp_Down = self.createCombinationSel(
        OutputList="SelLLP2DD_DpDp_Down",
        DecayDescriptor=descriptors["DpDp"],
        DaughterLists=[self.SelDp_Down],
        PreVertexCuts=confs['Down']['CombCuts'],
        PostVertexCuts=confs['Down']['MomCuts'])

    self.SelLLP2DD_DpDz_Down = self.createCombinationSel(
        OutputList="SelLLP2DD_DpDz_Down",
        DecayDescriptor=descriptors["DpDz"],
        DaughterLists=[self.SelDz_Down, self.SelDp_Down],
        PreVertexCuts=confs['Down']['CombCuts'],
        PostVertexCuts=confs['Down']['MomCuts'])

    self.SelLLP2DD_DzDz_Down = self.createCombinationSel(
        OutputList="SelLLP2DD_DzDz_Down",
        DecayDescriptor=descriptors["DzDz"],
        DaughterLists=[self.SelDz_Down],
        PreVertexCuts=confs['Down']['CombCuts'],
        PostVertexCuts=confs['Down']['MomCuts'])

    self.SelLLP2DD_DzDzbar_Down = self.createCombinationSel(
        OutputList="SelLLP2DD_DzDzbar_Down",
        DecayDescriptor=descriptors["DzDzbar"],
        DaughterLists=[self.SelDz_Down],
        PreVertexCuts=confs['Down']['CombCuts'],
        PostVertexCuts=confs['Down']['MomCuts'])




    

    
