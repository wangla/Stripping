###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Bu/Bc->Majorana(KS0) mu+ stripping Selections and StrippingLines.
Provides functions to build KS->DD, KS->LL, Bu/Bc selections.
"""

__author__ = ['Valeriia Lukashenko']
__date__ = '19/03/21'
__version__ = 'V0.2'
__all__ = ('Beauty2MajoLepConf',
	   'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import *

from StandardParticles import StdAllNoPIDsMuons as Muons
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllNoPIDsElectrons as Electrons
from StandardParticles import StdAllNoPIDsKaons as Kaons

from StandardParticles import StdNoPIDsDownPions as DownPions
from StandardParticles import StdNoPIDsDownElectrons as DownElectrons
from StandardParticles import StdLooseDownMuons as DownMuons

default_config = {
    'NAME' : 'Beauty2MajoLep',
    'WGs' : ['RD'],
    'STREAMS' : ['BhadronCompleteEvent'],
    'BUILDERTYPE' : 'Beauty2MajoLepConf',
    'CONFIG'      : {
                     'Lep_IPChi2min'             : 25.0,
                     'Lep_PTmin'                 : 300.,
                     'Trk_GhostProb'             : 0.5,  
                     'Majo_AMhigh'               : 7000.0,
                     'Majo_AMlow'                : 200.,
                     'Majo_PTmin'                : 700.0,
                     'Majo_Bach_PTmin'           : 500.0,
                     'Majo_DocaChi2'             : 25.0, 
                     'Majo_VtxChi2'              : 10.0,
                     'AM_Mlow'                   : 0.,
                     'AM_Mhigh'                  : 7200.0, 
                     'B_Mlow'                    : 4800.0,
                     'B_Mhigh'                   : 6800.0,
                     'B_IPCHI2wrtPV'             : 16.0,
                     'BVtxChi2'                  : 10.0, 
                     'B_DLS'                     : 5.0,
                     'Bach_PTmin'                : 700.0,
                     'Jpsi_pdg'                  : 100.,
                     'D0_pdg'                    : 100., 
                     'Ks_pdg_DD'                 : 50.,
                     'B_pdg_Ks_DD'               : 200.0,           
                     'Ks_Bach_PT_DD'             : 800.0,
                     'B_Bach_PT_DD'              : 1000.0,
                     'Ks_pdg'                    : 100.,
                     'B_pdg'                     : 300.0,
                     'B_electron_pdg'            : 500.0, 
                     'Prescale1'                 : 0.05,
                     'Prescale2'                 : 0.1,
                     'Prescale3'                 : 0.2, 
                     'Postscale'                 : 1.0, 
                     'RequiredRawEvents'         : ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"]
                     },
    }


name="Beauty2MajoLep"

class Beauty2MajoLepConf(LineBuilder) :
    """
    Builder of Bu->KS mu+ stripping Selection and StrippingLine.
    Constructs B+ -> KS mu+ Selections and StrippingLines from a configuration dictionary.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        dd_name = name+'DD'
        ll_name = name+'LL'
        
        Beauty2MajoMuDDName = dd_name.replace("Beauty2MajoLep","Beauty2MajoMu")
        Beauty2MajoMuLLName = ll_name.replace("Beauty2MajoLep","Beauty2MajoMu")

        Beauty2MajoEDDName = dd_name.replace("Beauty2MajoLep","Beauty2MajoE")
        Beauty2MajoELLName = ll_name.replace("Beauty2MajoLep","Beauty2MajoE")

        B2KsPiPiDDName = "Bu2KsPiPiDD"
        B2KsPiPiLLName = "Bu2KsPiPiLL"
 
        B2D0PiName = "Bu2piD02Kpi"
 
        B2JpsiKName = "Bu2JpsiKplus"


        self.muons = Muons
        self.makeMu( 'Mufor'+name, config )

        self.pions = Pions
        self.makePi( 'Pifor'+name, config )

        self.electrons = Electrons
        self.makeE( 'Efor'+name, config )

        self.downpions = DownPions
        self.makeDownPi( 'DownPifor'+name, config )

        self.downelectrons = DownElectrons
        self.makeDownE( 'DownEfor'+name, config )

        self.downmuons = DownMuons
        self.makeDownMu( 'DownMufor'+name, config )

        self.kaons = Kaons
        self.makeKaon('KaonforD0', config) 

        

        ####################NORMALLINES#################################################
        self.selMajo2EPiLL = self.makeMajo2EPiLL( 'Majofor'+Beauty2MajoMuLLName, config )

        self.selMajo2MuPiDD = self.makeMajo2MuPiDD( 'Majofor'+Beauty2MajoEDDName, config )
        self.selMajo2MuPiLL = self.makeMajo2MuPiLL( 'Majofor'+Beauty2MajoELLName, config )

        self.selMajo2EPiDDOS = self.makeMajo2EPiDDOS( 'Majofor'+Beauty2MajoMuDDName+"OS", config )
        self.selMajo2EPiDDSS = self.makeMajo2EPiDDSS( 'Majofor'+Beauty2MajoMuDDName+"SS", config )
     
        self.Beauty2MajoMuLL = self.makeBeauty2MajoMu2LL( Beauty2MajoMuLLName, config )

        self.Beauty2MajoEDD = self.makeBeauty2MajoE2DD( Beauty2MajoEDDName, config )
        self.Beauty2MajoELL = self.makeBeauty2MajoE2LL( Beauty2MajoELLName, config )
   
        self.Beauty2MajoMuDDOS = self.makeBeauty2MajoMu2DDOS( Beauty2MajoMuDDName+"OS", config )
        self.Beauty2MajoMuDDSS = self.makeBeauty2MajoMu2DDSS( Beauty2MajoMuDDName+"SS", config )
 
        self.B2MajoMuDDOSLine = StrippingLine(Beauty2MajoMuDDName+"OSLine",
                                           prescale = 1.0,
                                           postscale = 1.0,
                                           selection = self.Beauty2MajoMuDDOS,
                                           RequiredRawEvents = config['RequiredRawEvents'],
                                           checkPV = True
                                           )

        self.B2MajoMuDDSSLine = StrippingLine(Beauty2MajoMuDDName+"SSLine",
                                           prescale = 1.0,
                                           postscale = 1.0,
                                           selection = self.Beauty2MajoMuDDSS,
                                           RequiredRawEvents = config['RequiredRawEvents'],
                                           checkPV = True
                                           )


        self.B2MajoEDDLine = StrippingLine(Beauty2MajoEDDName+"Line",
                                           prescale = 1.0,
                                           postscale = 1.0,
                                           selection = self.Beauty2MajoEDD,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True
                                           )


        self.B2MajoMuLLLine = StrippingLine(Beauty2MajoMuLLName+"Line",
                                           prescale = 1.0,
                                           postscale = 1.0,
                                           selection =  self.Beauty2MajoMuLL,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True
                                           )

        self.B2MajoELLLine = StrippingLine(Beauty2MajoELLName+"Line",
                                           prescale = 1.0,
                                           postscale = 1.0,
                                           selection =  self.Beauty2MajoELL,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True
                                           )


        #######################PRESCALED LINES#############################################
        

        self.selMajo2EPiLLPr = self.makeMajo2EPiLL( 'Majofor'+Beauty2MajoMuLLName+'Prescale', config, True )

        self.selMajo2MuPiDDPr = self.makeMajo2MuPiDD( 'Majofor'+Beauty2MajoEDDName+'Prescale', config, True )
        self.selMajo2MuPiLLPr = self.makeMajo2MuPiLL( 'Majofor'+Beauty2MajoELLName+'Prescale', config, True )

        self.selMajo2EPiDDOSPr = self.makeMajo2EPiDDOS( 'Majofor'+Beauty2MajoMuDDName+"OS"+"Prescale", config, True )
        self.selMajo2EPiDDSSPr = self.makeMajo2EPiDDSS( 'Majofor'+Beauty2MajoMuDDName+"SS"+"Prescale", config, True )
        
 
        self.Beauty2MajoMuLLPr = self.makeBeauty2MajoMu2LL( Beauty2MajoMuLLName+'Prescale', config, True )

        self.Beauty2MajoEDDPr = self.makeBeauty2MajoE2DD( Beauty2MajoEDDName+'Prescale', config, True )
        self.Beauty2MajoELLPr = self.makeBeauty2MajoE2LL( Beauty2MajoELLName+'Prescale', config, True )

        self.Beauty2MajoMuDDOSPr = self.makeBeauty2MajoMu2DDOS( Beauty2MajoMuDDName+"OS"+"Prescale", config, True )
        self.Beauty2MajoMuDDSSPr = self.makeBeauty2MajoMu2DDSS( Beauty2MajoMuDDName+"SS"+"prescale", config, True )
 

        self.B2MajoMuDDLineOSPr = StrippingLine(Beauty2MajoMuDDName+"OS"+"Line"+'Prescale',
                                           prescale = config['Prescale1'],
                                           postscale = config['Postscale'],
                                           selection = self.Beauty2MajoMuDDOSPr,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True, 
                                           )

        self.B2MajoMuDDLineSSPr = StrippingLine(Beauty2MajoMuDDName+"SS"+"Line"+'Prescale',
                                           prescale = config['Prescale1'],
                                           postscale = config['Postscale'],
                                           selection = self.Beauty2MajoMuDDSSPr,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True, 
                                           )


        self.B2MajoEDDLinePr = StrippingLine(Beauty2MajoEDDName+"Line"+'Prescale',
                                           prescale = config['Prescale2'],
                                           postscale = config['Postscale'],
                                           selection = self.Beauty2MajoEDDPr,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True,
                                           )


        self.B2MajoMuLLLinePr = StrippingLine(Beauty2MajoMuLLName+"Line"+'Prescale',
                                           prescale = config['Prescale3'],
                                           postscale = config['Postscale'],
                                           selection =  self.Beauty2MajoMuLLPr,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True,
                                           )

        self.B2MajoELLLinePr = StrippingLine(Beauty2MajoELLName+"Line"+'Prescale',
                                           prescale = config['Prescale1'],
                                           postscale = config['Postscale'],
                                           selection =  self.Beauty2MajoELLPr,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True, 
                                           )


       #################NORMALISATION CHANNEL##########################################

        self.selKs2PiPiDD = self.makeKs2PiPiDD( 'Ksfor'+B2KsPiPiDDName, config)
        self.selKs2PiPiLL = self.makeKs2PiPiLL( 'Ksfor'+B2KsPiPiLLName, config)

        self.B2KsPiDD = self.makeBeauty2KsPiDD( B2KsPiPiDDName, config)
        self.B2KsPiLL = self.makeBeauty2KsPiLL( B2KsPiPiLLName, config)

        self.B2KsPiDDLine = StrippingLine(B2KsPiPiDDName+"Line",
                                           prescale = 1.0,
                                           postscale = config['Postscale'],
                                           selection = self.B2KsPiDD,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True, 
                                           )

        self.B2KsPiLLLine = StrippingLine(B2KsPiPiLLName+"Line",
                                           prescale = 1.0,
                                           postscale = config['Postscale'],
                                           selection = self.B2KsPiLL,
                                           RequiredRawEvents = config['RequiredRawEvents'], 
                                           checkPV = True,
                                           )

        self.selD02PiKLL = self.makeD02PiKLL( 'D0for'+B2D0PiName, config )

        self.B2D0PiLL = self.makeBeauty2D0PiLL( B2D0PiName, config )

        self.B2D0PiLLLine = StrippingLine(B2D0PiName+"Line",
                                           prescale = 1.0,
                                           postscale = config['Postscale'],
                                           selection = self.B2D0PiLL,
                                           RequiredRawEvents = config['RequiredRawEvents'],
                                           checkPV = True, 
                                           )
  
        self.selJpsi2MuMu = self.makeJpsi2MuMu( 'Jpsifor'+B2JpsiKName+"mumu" , config )

        self.B2JpsiKplusMuMu = self.makeBeauty2JpsiKmumu( B2JpsiKName+"mumu" , config )

        self.B2JpsiKplusmumuLine = StrippingLine(B2JpsiKName +"mumu"+"Line",
                                           prescale = 1.0,
                                           postscale = config['Postscale'],
                                           selection = self.B2JpsiKplusMuMu,
                                           RequiredRawEvents = config['RequiredRawEvents'],
                                           checkPV = True, 
                                           )
  
        self.selJpsi2EE = self.makeJpsi2EE( 'Jpsifor'+B2JpsiKName+"ee" , config )

        self.B2JpsiKplusEE = self.makeBeauty2JpsiKee( B2JpsiKName+"ee" , config )

        self.B2JpsiKpluseeLine = StrippingLine(B2JpsiKName +"ee"+"Line",
                                           prescale = 1.0,
                                           postscale = config['Postscale'],
                                           selection = self.B2JpsiKplusEE,
                                           RequiredRawEvents = config['RequiredRawEvents'],
                                           checkPV = True, 
                                           )
 

        self.registerLine(self.B2MajoMuDDLineOSPr)
        self.registerLine(self.B2MajoMuDDLineSSPr)
 
        self.registerLine(self.B2MajoMuLLLinePr)

        self.registerLine(self.B2MajoEDDLinePr)
        self.registerLine(self.B2MajoELLLinePr)

        self.registerLine(self.B2MajoMuDDOSLine)
        self.registerLine(self.B2MajoMuDDSSLine)
 

        self.registerLine(self.B2MajoMuLLLine)

        self.registerLine(self.B2MajoEDDLine)
        self.registerLine(self.B2MajoELLLine)


        self.registerLine(self.B2KsPiDDLine)
        self.registerLine(self.B2KsPiLLLine)


        self.registerLine(self.B2D0PiLLLine)
        self.registerLine(self.B2JpsiKplusmumuLine)
        self.registerLine(self.B2JpsiKpluseeLine)



    def makeDownE( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Lep_PTmin']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']
        _allCuts = _trkGPCut + '&' + _bachPtCut 

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selDownE = Selection( name, Algorithm = _filterH, RequiredSelections = [self.downelectrons] )


    def makeDownPi( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Lep_PTmin']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']
         
        _allCuts = _trkGPCut + '&' + _bachPtCut 

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selDownPi = Selection( name, Algorithm = _filterH, RequiredSelections = [self.downpions] )

    def makeDownMu( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Lep_PTmin']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']

        _allCuts = _trkGPCut + '&' + _bachPtCut 

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selDownMu = Selection( name, Algorithm = _filterH, RequiredSelections = [self.downmuons] )


    def makeE( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Lep_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['Lep_IPChi2min']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']
        
        _allCuts = _trkGPCut + '&' + _bachPtCut + '&' + _bachIPChi2Cut 

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selE = Selection( name, Algorithm = _filterH, RequiredSelections = [self.electrons] )


    def makePi( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Lep_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['Lep_IPChi2min']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']
       
 
        _allCuts = _trkGPCut + '&' + _bachPtCut + '&' + _bachIPChi2Cut 

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selPi = Selection( name, Algorithm = _filterH, RequiredSelections = [self.pions] )


    def makeMu( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Lep_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['Lep_IPChi2min']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']
        _pidMuon = '(ISMUON)' 

        _allCuts = _trkGPCut + '&' + _bachPtCut + '&' + _pidMuon

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selMu = Selection( name, Algorithm = _filterH, RequiredSelections = [self.muons] )

    def makeKaon( self, name, config ) :
        # define all the cuts
        _bachPtCut      = "(PT>%s*MeV)"                  % config['Lep_PTmin']
        _bachIPChi2Cut  = "(MIPCHI2DV(PRIMARY) > %s)"    % config['Lep_IPChi2min']
        _trkGPCut       = "(TRGHOSTPROB<%s)"             % config['Trk_GhostProb']

        _allCuts = _trkGPCut + '&' + _bachPtCut + '&' + _bachIPChi2Cut 

        # make the filter
        _filterH = FilterDesktop( Code = _allCuts )

        # make and store the selection object
        self.selKaon = Selection( name, Algorithm = _filterH, RequiredSelections = [self.kaons] )


################################SIGNALLINES#################################
    #For DD
    def makeMajo2EPiDDSS( self, name, config, prescale=False ) :
        """
        Create and store a Bu -> epi DD Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
            
        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           %  config['Majo_DocaChi2']
        _massCutLow     = "(AM>(%s*MeV))" % config['Majo_AMlow']
        

 
        _combCuts = _massCutHigh +'&'+_massCutLow+'&'+_docaCut
       
        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"  %    config['Majo_VtxChi2']

        _motherCuts = _ptCut+'&'+_vtxChi2Cut       
       
        _pidCut_electron = "(PIDe>3.0)&((PIDe-PIDmu)>3.0)"
        _pidCut_pion = "((PIDe)<0.0)" 
 
              
        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "[Lambda0 -> pi+ e-]cc" ]

        _daughterCuts =  { "pi+": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "e-": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}
 
        if not prescale:
             _daughterCuts =  { "pi+" : _pidCut_pion +"&"+ "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "e-": _pidCut_electron+"&"+"(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}
             _Majo.DaughtersCuts =  _daughterCuts 

        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts
    
        return Selection(name, Algorithm = _Majo, RequiredSelections = [ self.selDownPi, self.selDownE ])

    def makeMajo2EPiDDOS( self, name, config, prescale=False ) :
        """ 
        Create and store a Bu -> epi DD Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
            
        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           %  config['Majo_DocaChi2']
        _massCutLow     = "(AM>(%s*MeV))" % config['Majo_AMlow']
        

 
        _combCuts = _massCutHigh +'&'+_massCutLow+'&'+_docaCut
       
        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"  %    config['Majo_VtxChi2']

        _motherCuts = _ptCut+'&'+_vtxChi2Cut
        _pidCut_electron = "(PIDe>3.0)&((PIDe-PIDmu)>3.0)"
        _pidCut_pion = "((PIDe)<0.0)" 
        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "[Lambda0 -> pi+ e-]cc" ]

        _daughterCuts =  { "pi+": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "e-": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}
 
        if not prescale:
             _daughterCuts =  { "pi+" :  _pidCut_pion +"&"+"(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "e-": _pidCut_electron+"&"+"(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}
             _Majo.DaughtersCuts =  _daughterCuts 

        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts
    
        return Selection(name, Algorithm = _Majo, RequiredSelections = [ self.selDownPi, self.selDownE ])
    

    def makeMajo2MuPiDD( self, name, config, prescale=False ) :
        """
        Create and store a HNL -> mu pi DD Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _massCutLow     = "(AM>(%s*MeV))" % config['Majo_AMlow']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DocaChi2']

        _combCuts = _massCutHigh + '&' + _massCutLow+'&'+_docaCut

        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_VtxChi2']

        _motherCuts = _ptCut+'&'+_vtxChi2Cut 

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "KS0 -> pi+ mu-", "KS0 -> pi- mu+" ]

        _daughterCuts =  {  "mu-" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "pi+" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}
        _Majo.DaughtersCuts = _daughterCuts
 
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts
        return Selection(name, Algorithm = _Majo, RequiredSelections = [ self.selDownPi, self.selDownMu ])
    # For LL

    
    def makeMajo2EPiLL( self, name, config, prescale=False ) :
        """
        Create and store a HNL -> epi LL Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DocaChi2']
        _massCutLow     = "(AM>(%s*MeV))" % config['Majo_AMlow']
        _combCuts = _massCutHigh + '&' + _massCutLow+'&'+_docaCut

        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_VtxChi2']

        _motherCuts = _ptCut+'&'+_vtxChi2Cut

        _pidCut_electron = "(PIDe>0.)"
        
        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "KS0 -> pi+ e-", "KS0 -> pi- e+" ]
 
        if not prescale:
             _daughterCuts =  { "pi+": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "e-": _pidCut_electron+"&"+"(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}
        else:
             _daughterCuts =  { "pi+": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "e-": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}

        _Majo.DaughtersCuts =  _daughterCuts 
        
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts

        return Selection (name, Algorithm = _Majo, RequiredSelections = [ self.selPi, self.selE ])

    def makeMajo2MuPiLL( self, name, config, prescale=False ) :
        """
        Create and store a HNL -> mu pi LL Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DocaChi2']
        _massCutLow = "(AM>(%s*MeV))" % config['Majo_AMlow']
        _combCuts = _massCutHigh+'&'+_massCutLow+'&'+_docaCut

        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_VtxChi2']
        _motherCuts = _ptCut+'&'+_vtxChi2Cut
        
        _pidCut_muon = "(PIDmu>0.)"
        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "KS0 -> pi+ mu-", "KS0 -> pi- mu+"  ]

        if not prescale:
             _daughterCuts =  { "pi+": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "mu-": _pidCut_muon+"&"+"(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}
        else:
             _daughterCuts =  { "pi+": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "mu-": "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]}

        _Majo.DaughtersCuts =  _daughterCuts 

        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts

        return Selection (name, Algorithm = _Majo, RequiredSelections = [ self.selPi, self.selMu ])

#for LL
    def makeBeauty2MajoMu2LL( self, name, config, prescale=False ) :
        """
        Create and store a Bu -> KS(LL) mu+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(%s*MeV))" % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))" % config['AM_Mhigh']
      
        _combCuts = _massCutLow+'&'+_massCutHigh

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']
        _massCutLow2     = "(M>(%s*MeV))"               % config['B_Mlow']
        _massCutHigh2    = "(M<(%s*MeV))"               % config['B_Mhigh']
 
        _motherCuts = _vtxChi2Cut+'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCutLow2+'&'+_massCutHigh2
 
        _B = CombineParticles()
        _pidCut_muons = "(PIDmu>0.)"

        _B.DecayDescriptors = [ "B+ -> mu+ KS0", "B- -> mu- KS0"]
        if not prescale:
            _daughterCuts =  { "mu+" : "(PT>%s)"% (config['Bach_PTmin'])+"&"+_pidCut_muons }
        else: 
            _daughterCuts =  { "mu+" : "(PT>%s)"% (config['Bach_PTmin']) }
        _B.DaughtersCuts = _daughterCuts
            
        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts
        if not prescale:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2EPiLL ])
        else:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2EPiLLPr ])

    def makeBeauty2MajoMu2DDOS( self, name, config, prescale=False ) :
        """
        Create and store a Bu -> KS(DD) mu+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']

        _combCuts = _massCutLow+'&'+_massCutHigh 

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS'] 
        _massCutLow2     = "(M>(%s*MeV))"               % config['B_Mlow']
        _massCutHigh2    = "(M<(%s*MeV))"               % config['B_Mhigh']
        _pidCut_muons = "((PIDmu-PIDe)>1.0)&(PIDmu>1.0)"
       

        _motherCuts = _vtxChi2Cut+'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCutLow2+'&'+_massCutHigh2
        
        
        _B = CombineParticles()

        _B.DecayDescriptors = [ "[B+ -> mu+ Lambda0]cc"]
        if not prescale:
            _daughterCuts = { "mu+" : _pidCut_muons+"&"+"(PT>%s*MeV)" % (config['Bach_PTmin'])}
        else:
            _daughterCuts = { "mu+" : "(PT>%s)" % (config['Bach_PTmin'])}

        _B.DaughtersCuts = _daughterCuts

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts
        if not prescale:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2EPiDDOS ])
        else:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2EPiDDOSPr ])
 
    def makeBeauty2MajoMu2DDSS( self, name, config, prescale=False ) :
        """
        Create and store a Bu -> KS(DD) mu+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']

        _combCuts = _massCutLow+'&'+_massCutHigh 

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']
        _massCutLow2     = "(M>(%s*MeV))"               % config['B_Mlow']
        _massCutHigh2    = "(M<(%s*MeV))"               % config['B_Mhigh']
        _pidCut_muons = "((PIDmu-PIDe)>1.0)&(PIDmu>1.0)"
        _motherCuts = _vtxChi2Cut+'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCutLow2+'&'+_massCutHigh2
  
              
        _B = CombineParticles()

        _B.DecayDescriptors = [ "[B- -> mu- Lambda0]cc"]
        if not prescale:
            _daughterCuts = { "mu+" : _pidCut_muons+"&"+"(PT>%s*MeV)" % (config['Bach_PTmin'])}
        else:
            _daughterCuts = { "mu+" : "(PT>%s)" % (config['Bach_PTmin'])}

        _B.DaughtersCuts = _daughterCuts

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts
        if not prescale:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2EPiDDSS ])
        else:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selMu, self.selMajo2EPiDDSSPr ])
 

    def makeBeauty2MajoE2LL( self, name, config, prescale=False ) :
        """
        Create and store a Bu -> KS(LL) e+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutLow     = "(AM>(%s*MeV))" % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))" % config['AM_Mhigh']

        _combCuts = _massCutLow+'&'+_massCutHigh 

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']
        _massCutLow2     = "(M>(%s*MeV))"               % config['B_Mlow']
        _massCutHigh2    = "(M<(%s*MeV))"               % config['B_Mhigh']

        _pidCut_electron = '(PIDe>0.)'
        _motherCuts = _vtxChi2Cut+'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCutLow2+'&'+_massCutHigh2


        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> e+ KS0", "B- -> e- KS0"]

        if not prescale:
            _daughterCuts = { "e+" : "(PT>%s) & %s"% (config['Bach_PTmin'], _pidCut_electron)} 
        else:
            _daughterCuts = { "e+" : "(PT>%s)"% (config['Bach_PTmin'])}
 
        _B.DaughtersCuts = _daughterCuts

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        if not prescale: 
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selE, self.selMajo2MuPiLL ])
        else:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selE, self.selMajo2MuPiLLPr ])



# For DD

    def makeBeauty2MajoE2DD( self, name, config, prescale=False ) :
        """
        Create and store a Bu -> KS(DD) e+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']
     
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']
        _massCutLow2     = "(M>(%s*MeV))"               % config['B_Mlow']
        _massCutHigh2    = "(M<(%s*MeV))"               % config['B_Mhigh']
        _pidCut_electron = '(PIDe>0.)'

        _motherCuts = _vtxChi2Cut+'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCutLow2+'&'+_massCutHigh2



        _combCuts = _massCutLow+'&'+_massCutHigh  
        if not prescale:
            _daughterCuts = { "e+" : "(PT>%s)"% (config['Bach_PTmin'])+ '&' +_pidCut_electron} 
        else:
            _daughterCuts = { "e+" : "(PT>%s)"% (config['Bach_PTmin'])}

        
        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> e+ KS0", "B- -> e- KS0" ]

        _B.CombinationCut = _combCuts
        _B.DaughtersCuts = _daughterCuts
        _B.MotherCut = _motherCuts

        if not prescale:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selE, self.selMajo2MuPiDD ])
        else:
            return Selection (name, Algorithm = _B, RequiredSelections = [ self.selE, self.selMajo2MuPiDDPr ])
 
##########################NORMALISATIONLINES###############################################

    def makeKs2PiPiDD( self, name, config) :
        """
        Create and store a KS->pi+pi- DD Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
            
        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           %  config['Majo_DocaChi2']
        _massCutLow     = "(AM>(%s*MeV))" % config['Majo_AMlow']
        
        _combCuts = _massCutHigh +'&'+_docaCut

        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_VtxChi2']
        _massKsCut1 = "(ADMASS('KS0') < %s)" % config['Ks_pdg_DD']


        _motherCuts = _ptCut+'&'+_vtxChi2Cut+'&'+_massKsCut1

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "KS0 -> pi- pi+" ]

        
        _daughterCuts =  { "pi+" : "(PT>%s*MeV)" % config['Ks_Bach_PT_DD'], "pi-" : "(PT>%s*MeV)" % config['Ks_Bach_PT_DD']} 
   
        _Majo.DaughtersCuts = _daughterCuts
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts

        return Selection(name, Algorithm = _Majo, RequiredSelections = [ self.selDownPi, self.selDownPi ])

    def makeKs2PiPiLL( self, name, config ) :
        """
        Create and store KS->pi+pi- LL Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """

        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DocaChi2']
        _massCutLow     = "(AM>(%s*MeV))" % config['Majo_AMlow']
        _combCuts = _massCutHigh + '&'+_docaCut

        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_VtxChi2']
        _massKsCut1 = "(ADMASS('KS0') < %s)" % config['Ks_pdg']

        
        _motherCuts = _ptCut+'&'+_vtxChi2Cut+'&'+_massKsCut1

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = ["KS0 -> pi- pi+" ]
        _daughterCuts =  { "pi+" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "pi-" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]} 
      
        _Majo.DaughtersCuts = _daughterCuts
        
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts

        return Selection (name, Algorithm = _Majo, RequiredSelections = [ self.selPi, self.selPi ])

    def makeBeauty2KsPiDD( self, name, config) :
        """
        Create and store a Bu -> KS(DD) pi+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['Majo_VtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']
        _massCut = "(ADMASS('B+') < %s)" % config['B_pdg_Ks_DD']
 
        _combCuts = _massCutLow +'&'+_massCutHigh       
        _motherCuts = _vtxChi2Cut +'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCut 
        
       
        _daughterCuts = { "pi+" : "(PT>%s)"% config['B_Bach_PT_DD']}
      
        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> pi+ KS0", "B- -> pi- KS0" ]
        _B.DaughtersCuts = _daughterCuts

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        return Selection (name, Algorithm = _B, RequiredSelections = [ self.selPi, self.selKs2PiPiDD ])


    def makeBeauty2KsPiLL( self, name, config) :
        """
        Create and store a Bu -> KS(DD) pi+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']
        _massCut = "(ADMASS('B+') < %s)" % config['B_pdg']
        
        _combCuts = _massCutLow+'&'+_massCutHigh 
        _motherCuts = _vtxChi2Cut +'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCut
        
        _daughterCuts = { "pi+" : "(PT>%s)"% (config['Majo_Bach_PTmin'])}
     
        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> pi+ KS0", "B- -> pi- KS0" ]
        _B.CombinationCut = _combCuts
        _B.DaughtersCuts = _daughterCuts
        _B.MotherCut = _motherCuts

        return Selection (name, Algorithm = _B, RequiredSelections = [ self.selPi, self.selKs2PiPiLL ])



    def makeD02PiKLL( self, name, config) :
        """
        Create and store a Bu -> D0 pi+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
            
        _massCutHigh    = "(AM<(%s*MeV))"                     % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"            %  config['Majo_DocaChi2']
        _massCutLow     = "(AM>(%s*MeV))"                     % config['Majo_AMlow']
        
        _combCuts = _massCutLow + '&'+ _massCutHigh +'&'+_docaCut

        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
 
        _massCut1 = "(ADMASS('D0') < %s)" % config['D0_pdg']
      

        _motherCuts = _ptCut+'&'+_vtxChi2Cut+'&'+_massCut1

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "[D0 -> K- pi+]cc" ]

        
        _daughterCuts =  { "pi+" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"],  "K-" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]} 
 
        _Majo.DaughtersCuts = _daughterCuts
 
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts
        #_Majo.Preambulo = preambulo


        return Selection(name, Algorithm = _Majo, RequiredSelections = [ self.selPi, self.selKaon ])

    def makeBeauty2D0PiLL( self, name, config) :
        """
        Create and store a Bu -> D0pi+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']

        _combCuts = _massCutLow+'&'+_massCutHigh 

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']
        _massCut = "(ADMASS('B+') < %s)" % config['B_pdg']
        



        _motherCuts = _vtxChi2Cut +'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCut
        
        
        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> pi+ D~0", "B- -> pi- D0" ]
        _daughterCuts = { "pi+" : "(PT>%s)"% (config['Majo_Bach_PTmin'])}
        _B.DaughtersCuts = _daughterCuts

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        return Selection (name, Algorithm = _B, RequiredSelections = [ self.selPi, self.selD02PiKLL ])


    def makeJpsi2MuMu( self, name, config) :
        """
        Create and store a Jpsi ->mu+mu- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
        _massCut     = "(AM<%s*MeV)"               % config['Majo_AMhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DocaChi2']

   
        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _massCutJpsi = "(ADMASS('J/psi(1S)') < %s)" % config['Jpsi_pdg']
        
        _motherCuts = _ptCut+'&'+_massCutJpsi
        _combCuts = _massCut + '&' + _docaCut

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "J/psi(1S) -> mu+ mu-" ]

        
      
        _daughterCuts =  { "mu-" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "mu+" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]} 
 
        _Majo.DaughtersCuts = _daughterCuts
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts

        return Selection(name, Algorithm = _Majo, RequiredSelections = [ self.selMu, self.selMu ])

    def makeJpsi2EE( self, name, config) :
        """
        Create and store a Jpsi ->e+e- Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DocaChi2']
        _massCut     = "(AM<%s*MeV)"               % config['Majo_AMhigh']
 
   
        _ptCut      = "(PT>%s*MeV)"                    % config['Majo_PTmin']
        _massCutJpsi = "(ADMASS('J/psi(1S)') < %s)" % config['Jpsi_pdg']
      

        _motherCuts = _ptCut+'&'+_massCutJpsi
        _combCuts = _massCut + '&' + _docaCut

        _Majo = CombineParticles()

        _Majo.DecayDescriptors = [ "J/psi(1S) -> e+ e-" ]

             
        _daughterCuts =  { "e-" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"], "e+" : "(PT>%s*MeV)" % config["Majo_Bach_PTmin"]} 
 
        _Majo.DaughtersCuts = _daughterCuts
        _Majo.CombinationCut = _combCuts
        _Majo.MotherCut = _motherCuts
      
        return Selection(name, Algorithm = _Majo, RequiredSelections = [ self.selE, self.selE ])

 
    def makeBeauty2JpsiKmumu( self, name, config) :
        """
        Create and store a Bu -> JpsiK+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']
        _docaCut        = "(ADOCACHI2CUT(%s, ''))"           % config['Majo_DocaChi2']


        _combCuts = _massCutLow+'&'+_massCutHigh 

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']       
        _massCut = "(ADMASS('B+') < %s)" % config['B_electron_pdg']
 


        _motherCuts = _vtxChi2Cut +'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCut
        
        
        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> K+ J/psi(1S)", "B- -> K- J/psi(1S)" ]
        _daughterCuts = { "K+" : "(PT>%s)"% (config['Majo_Bach_PTmin'])}
        _B.DaughtersCuts = _daughterCuts

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        return Selection (name, Algorithm = _B, RequiredSelections = [ self.selKaon, self.selJpsi2MuMu ])

    def makeBeauty2JpsiKee( self, name, config) :
        """
        Create and store a Bu -> JpsiK+ Selection object.
        Arguments:
        name             : name of the Selection.
        config           : config dictionary
        """


        _massCutLow     = "(AM>(%s*MeV))"               % config['AM_Mlow']
        _massCutHigh    = "(AM<(%s*MeV))"               % config['AM_Mhigh']

        _combCuts = _massCutLow+'&'+_massCutHigh 

        _vtxChi2Cut = "(VFASPF(VCHI2)<%s)"             % config['BVtxChi2']
        _ipChi2Cut  = "(MIPCHI2DV(PRIMARY)<%s)"        % config['B_IPCHI2wrtPV']
        _fdChi2Cut  = "(BPVDLS>%s)"                 % config['B_DLS']       
        _massCut = "(ADMASS('B+') < %s)" % config['B_electron_pdg']
 

        _motherCuts = _vtxChi2Cut +'&'+_ipChi2Cut+'&'+_fdChi2Cut+'&'+_massCut
        
        
        _B = CombineParticles()

        _B.DecayDescriptors = [ "B+ -> K+ J/psi(1S)", "B- -> K- J/psi(1S)" ]
        _daughterCuts = { "K+" : "(PT>%s)"% (config['Majo_Bach_PTmin'])}
        _B.DaughtersCuts = _daughterCuts

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        return Selection (name, Algorithm = _B, RequiredSelections = [ self.selKaon, self.selJpsi2EE ])


