###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for construction of tau -->MuMuMu stripping selections and lines

Exported symbols (use python help!):
   -
'''

__author__ = ['Jon Harrison', 'Paul Seyfert', 'Marcin Chrzaszcz', 'Giulia Frau', 'Vitalii Lisovskyi']
__date__ = '12/03/2021'
__version__ = '$Revision: 3.1$'

__all__ = ('Tau23MuLinesConf',
           'default_config',
           'makeTau23Mu',
           'makeDs23PiTIS',
           'makeDs23Pi',
           'makeDsPhiPi',
           'makeTau25Mu',
           'makeTau2PMuMu'
           )

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays as Combine5Particles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.PhysicalConstants import c_light

default_config = {
    'NAME'        : 'Tau23Mu',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'Tau23MuLinesConf',
    'CONFIG'      : {
       'TauPrescale'         :1.,
       'TauPostscale'        :1.,
       'Ds23PiTISPrescale'   :0.0,
       'Ds23PiPrescale'      :0.0,
       'Ds2PhiPiPrescale'    :1.,
       'Tau25Prescale'       :1.,
       'Tau2PMuMuPrescale'   :1.,
       'TrackGhostProb'      :0.45
       },
    'STREAMS'     : { 'Leptonic' : ['StrippingTau23MuTau23MuLine','StrippingTau23MuDs2PhiPiLine','StrippingTau23MuTau2PMuMuLine','StrippingTau23MuDs23PiLine','StrippingTau23MuTau25MuLine'],
                      'Dimuon' : ['StrippingTau23MuTau23Mu_DSTLine', 'StrippingTau23MuTau23Mu_DST_3muLine', 'StrippingTau23MuDs2PhiPi_DSTLine']}
    }



class Tau23MuLinesConf(LineBuilder) :
    """
    Builder
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        self.name = name
        self.__confdict__ = config


        LineBuilder.__init__(self, name, config)
        #checkConfig(Bs2MuMuLinesConf.__configuration_keys__,config)

        tau_name=name+'Tau23Mu'
        tau_name_DST=name+'Tau23Mu_DST'
        tau_name_DST_3mu=name+'Tau23Mu_DST_3mu'
        ds23PiTIS_name = name+'Ds23PiTIS'
        ds23Pi_name=name+'Ds23Pi'
        ds2PhiPi_name=name+'Ds2PhiPi'
        ds2PhiPi_name_DST=name+'Ds2PhiPi_DST'
        tau25_name=name+'Tau25Mu'
        tau2pmm_name=name+'Tau2PMuMu'



        self.selTau23Mu = makeTau23Mu(tau_name,config)
        self.selTau23MuDST = makeTau23MufullDST(tau_name_DST,config)
        self.selTau23MuDST3mu = makeTau23MufullDST3mu(tau_name_DST_3mu,config)
        #self.selDs23PiTIS = makeDs23PiTIS(self,ds23PiTIS_name)
        self.selDs23Pi = makeDs23Pi(ds23Pi_name,config)
        self.selDs2PhiPi = makeDs2PhiPi(ds2PhiPi_name,config)
        self.selDs2PhiPi_DST = makeDs2PhiPifullDST(ds2PhiPi_name_DST,config)
        self.selTau25Mu = makeTau25Mu(tau25_name,config)
        self.selTau2PMuMu = makeTau2pmm(tau2pmm_name,config)


        self.tau23MuLine = StrippingLine(tau_name+"Line",
                                     prescale = config['TauPrescale'],
                                     postscale = config['TauPostscale'],
                                     MDSTFlag = False,
                                     RequiredRawEvents = ["Muon"],
                                     algos = [ self.selTau23Mu ],
                                     RelatedInfoTools = [{ 'Type' : 'RelInfoConeVariables', 'ConeAngle' : 1.,
                                     'Variables' : ['CONEANGLE', 'CONEMULT', 'CONEPT', 'CONEPTASYM'],
                                     'Location':'ConeIsoInfo',
                                     'DaughterLocations':{'[tau+ -> ^mu+ mu+ mu-]CC' : 'MuonConeVarInfo1', '[tau+ -> mu+ ^mu+ mu-]CC' : 'MuonConeVarInfo2', '[tau+ -> mu+ mu+ ^mu-]CC' : 'MuonConeVarInfo3'}
                                     },
                                     {'Type': 'RelInfoVertexIsolation',
                                     'Location':'VtxIsoInfo' },
                                     { 'Type': 'RelInfoTrackIsolationBDT',
                                     'Variables' : 0,
                                     'Location':'TrackIsoInfo',
                                     'DaughterLocations':{'[tau+ -> ^mu+ mu+ mu-]CC' : 'MuonTrackIsoBDTInfo1', '[tau+ -> mu+ ^mu+ mu-]CC' : 'MuonTrackIsoBDTInfo2', '[tau+ -> mu+ mu+ ^mu-]CC' : 'MuonTrackIsoBDTInfo3'}
                                     },
                                     { "Type" : "RelInfoTrackIsolationBDT2",
                                     "Location" : "TrackIsolationBDT2_01",
                                     "Particles" : [0,1]
                                     },
                                     { "Type" : "RelInfoTrackIsolationBDT2",
                                     "Location" : "TrackIsolationBDT2_02",
                                     "Particles" : [0,2]
                                     },
                                     { "Type" : "RelInfoTrackIsolationBDT2",
                                     "Location" : "TrackIsolationBDT2_12",
                                     "Particles" : [1,2]
                                     },
                                     { "Type" : "RelInfoMuonIDPlus",
                                     "Variables" : ["MU_BDT"],
                                     'DaughterLocations':{'[tau+ -> ^mu+ mu+ mu-]CC' : 'Muon1BDT', '[tau+ -> mu+ ^mu+ mu-]CC' : 'Muon2BDT', '[tau+ -> mu+ mu+ ^mu-]CC' : 'Muon3BDT'}
                                     }
                                     ]
                                     )

        self.tau23MuLineDST = StrippingLine(tau_name_DST+"Line",
                                     prescale = config['TauPrescale'],
                                     postscale = config['TauPostscale'],
                                     MDSTFlag = False,
                                     RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X+ ->  ^mu+ mu+ mu- ]CC" : "Muon1BDT",
                                         "[ X+ ->  mu+ ^mu+ mu- ]CC" : "Muon2BDT",
                                         "[ X+ ->  mu+ mu+ ^mu- ]CC" : "Muon3BDT",
                                         }
                                         },
                                     ],
                                     algos = [ self.selTau23MuDST ],
                                     )

        self.tau23MuLineDST3mu = StrippingLine(tau_name_DST_3mu+"Line",
                                     prescale = config['TauPrescale'],
                                     postscale = config['TauPostscale'],
                                     MDSTFlag = False,
                                     RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X+ ->  ^mu+ mu+ mu- ]CC" : "Muon1BDT",
                                         "[ X+ ->  mu+ ^mu+ mu- ]CC" : "Muon2BDT",
                                         "[ X+ ->  mu+ mu+ ^mu- ]CC" : "Muon3BDT",
                                         }
                                         },
                                     ],
                                     algos = [ self.selTau23MuDST3mu ],
                                     )

        #self.ds23PiTISLine = StrippingLine(ds23PiTIS_name+"Line",
        #                              prescale = config['Ds23PiTISPrescale'],
        #                              postscale = config['TauPostscale'],
        #                              algos = [ self.selDs23PiTIS ]
        #                              )

        self.ds23PiLine = StrippingLine(ds23Pi_name+"Line",
                                      prescale = config['Ds23PiPrescale'],
                                      postscale = config['TauPostscale'],
                                      MDSTFlag = False,
                                      #RequiredRawEvents = [ ],
                                      algos = [ self.selDs23Pi ]
                                      )

        self.ds2PhiPiLine = StrippingLine(ds2PhiPi_name+"Line",
                                          prescale = config['Ds2PhiPiPrescale'],
                                          postscale = config['TauPostscale'],
                                          MDSTFlag = False,
                                          #RequiredRawEvents = ["Calo"],
                                          algos = [ self.selDs2PhiPi ],
                                          RelatedInfoTools = [{ 'Type' : 'RelInfoConeVariables', 'ConeAngle' : 1.,
                                          'Variables' : ['CONEANGLE', 'CONEMULT', 'CONEPT', 'CONEPTASYM'],
                                          'Location':'ConeIsoInfo',
                                          'DaughterLocations':{'[D_s+  -> ^pi+  mu+ mu-]CC' : 'PionConeVarInfo', '[D_s+  -> pi+  ^mu+ mu-]CC' : 'MuonConeVarInfo1', '[D_s+  -> pi+  mu+ ^mu-]CC' : 'MuonConeVarInfo2'}
                                          },
                                          {'Type': 'RelInfoVertexIsolation',
                                          'Location':'VtxIsoInfo' },
                                          { 'Type': 'RelInfoTrackIsolationBDT',
                                          'Variables' : 0,
                                          'Location':'TrackIsoInfo',
                                          'DaughterLocations':{'[D_s+  -> ^pi+  mu+ mu-]CC' : 'PionTrackIsoBDTInfo', '[D_s+  -> pi+  ^mu+ mu-]CC' : 'MuonTrackIsoBDTInfo1', '[D_s+  -> pi+  mu+ ^mu-]CC' : 'MuonTrackIsoBDTInfo2'}
                                          },
                                          { "Type" : "RelInfoTrackIsolationBDT2",
                                          "Location" : "TrackIsolationBDT2_01",
                                          "Particles" : [0,1]
                                          },
                                          { "Type" : "RelInfoTrackIsolationBDT2",
                                          "Location" : "TrackIsolationBDT2_02",
                                          "Particles" : [0,2]
                                          },
                                          { "Type" : "RelInfoTrackIsolationBDT2",
                                          "Location" : "TrackIsolationBDT2_12",
                                          "Particles" : [1,2]
                                          },
                                          { "Type" : "RelInfoMuonIDPlus",
                                          "Variables" : ["MU_BDT"],
                                          "DaughterLocations"  : {
                                          "[ X ->  ^mu+ mu- X ]CC" : "Muon1BDT",
                                          "[ X ->  mu+ ^mu- X ]CC" : "Muon2BDT",
                                          }
                                          },
                                          ]
                                      )

        self.ds2PhiPiLineDST = StrippingLine(ds2PhiPi_name_DST+"Line",
                                          prescale = config['Ds2PhiPiPrescale'],
                                          postscale = config['TauPostscale'],
                                          MDSTFlag = False,
                                          RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                          #RequiredRawEvents = ["Calo"],
                                          RelatedInfoTools = [
                                             { "Type" : "RelInfoMuonIDPlus",
                                             "Variables" : ["MU_BDT"],
                                             "DaughterLocations"  : {
                                             "[ X ->  ^mu+ mu- X ]CC" : "Muon1BDT",
                                             "[ X ->  mu+ ^mu- X ]CC" : "Muon2BDT",
                                             }
                                             },
                                          ],
                                          algos = [ self.selDs2PhiPi_DST ],
                                          )

        self.tau25MuLine = StrippingLine(tau25_name+"Line",
                                         prescale = config['Tau25Prescale'],
                                         postscale = config['TauPostscale'],
                                         MDSTFlag = False,
                                         #RequiredRawEvents = [ ],
                                         RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X ->  ^mu+ mu+ mu+ mu- mu- ]CC" : "Muon1BDT",
                                         "[ X ->  mu+ ^mu+ mu+ mu- mu- ]CC" : "Muon2BDT",
                                         "[ X ->  mu+ mu+ ^mu+ mu- mu- ]CC" : "Muon3BDT",
                                         "[ X ->  mu+ mu+ mu+ ^mu- mu- ]CC" : "Muon4BDT",
                                         "[ X ->  mu+ mu+ mu+ mu- ^mu- ]CC" : "Muon5BDT",
                                         }
                                         },
                                         ],
                                         algos = [ self.selTau25Mu ]
                                         )

        self.tau2PMuMuLine = StrippingLine(tau2pmm_name+"Line",
                                           prescale = config['Tau2PMuMuPrescale'],
                                           postscale = config['TauPostscale'],
                                           MDSTFlag = False,
                                           #RequiredRawEvents = ["Calo"],
                                           algos = [ self.selTau2PMuMu ] ,
                                           RelatedInfoTools = [{ 'Type' : 'RelInfoConeVariables', 'ConeAngle' : 1.,
                                           'Variables' : ['CONEANGLE', 'CONEMULT', 'CONEPT', 'CONEPTASYM'],
                                           'Location':'ConeIsoInfo',
                                           'DaughterLocations':{"[[ tau+ -> ^p+ mu+ mu- ]CC, [ tau+ -> ^p~- mu+ mu+ ]CC, [ Lambda_c+ -> ^p+ mu+ mu- ]CC, [ Lambda_c+ -> ^p~- mu+ mu+ ]CC]" : 'ProtonConeVarInfo', "[[ tau+ -> p+ ^mu+ mu- ]CC, [ tau+ -> p~- ^mu+ mu+ ]CC, [ Lambda_c+ -> p+ ^mu+ mu- ]CC, [ Lambda_c+ -> p~- ^mu+ mu+ ]CC]" : 'MuonConeVarInfo1', "[[ tau+ -> p+ mu+ ^mu- ]CC, [ tau+ -> p~- mu+ ^mu+ ]CC, [ Lambda_c+ -> p+ mu+ ^mu- ]CC, [ Lambda_c+ -> p~- mu+ ^mu+ ]CC]" : 'MuonConeVarInfo2'}
                                           },

                                           {'Type': 'RelInfoVertexIsolation',
                                           'Location':'VtxIsoInfo' },
                                           { 'Type': 'RelInfoTrackIsolationBDT',
                                           'Variables' : 0,
                                           'Location':'TrackIsoInfo',
                                           'DaughterLocations':{"[[ tau+ -> ^p+ mu+ mu- ]CC, [ tau+ -> ^p~- mu+ mu+ ]CC, [ Lambda_c+ -> ^p+ mu+ mu- ]CC, [ Lambda_c+ -> ^p~- mu+ mu+ ]CC]" : 'ProtonTrackIsoBDTInfo', "[[ tau+ -> p+ ^mu+ mu- ]CC, [ tau+ -> p~- ^mu+ mu+ ]CC, [ Lambda_c+ -> p+ ^mu+ mu- ]CC, [ Lambda_c+ -> p~- ^mu+ mu+ ]CC]" : 'MuonTrackIsoBDTInfo1', "[[ tau+ -> p+ mu+ ^mu- ]CC, [ tau+ -> p~- mu+ ^mu+ ]CC, [ Lambda_c+ -> p+ mu+ ^mu- ]CC, [ Lambda_c+ -> p~- mu+ ^mu+ ]CC]" : 'MuonTrackIsoBDTInfo2'}
                                           },
                                           { "Type" : "RelInfoTrackIsolationBDT2",
                                           "Location" : "TrackIsolationBDT2",
                                           "Particles" : [1,2]
                                           }
                                           ]
                                           )

        self.registerLine(self.tau23MuLine)
        self.registerLine(self.tau23MuLineDST)
        self.registerLine(self.tau23MuLineDST3mu)
        #self.registerLine(self.ds23PiTISLine)
        self.registerLine(self.ds23PiLine)
        self.registerLine(self.ds2PhiPiLine)
        self.registerLine(self.ds2PhiPiLineDST)
        self.registerLine(self.tau25MuLine)
        self.registerLine(self.tau2PMuMuLine)


def makeTau23Mu(name, config):
    """
    Please contact Marcin Chrzaszcz if you think of prescaling this line!

    Arguments:
    name        : name of the Selection.
    """

    Tau2MuMuMu = Combine3Particles(\
               DecayDescriptor = " [ tau+ -> mu+ mu+ mu- ]cc",
               DaughtersCuts = { "mu+" : " ( PT > 300 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) "\
                                 "& ( BPVIPCHI2 () >  9 ) " % config},
               Combination12Cut = "(AM< 2000*MeV) & (AM >240*MeV) ", #"(ADAMASS('tau+')<400*MeV)" -> 1778+400  and then -100 for muon rest mass
               CombinationCut = "(ADAMASS('tau+')<200*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 1.5  )", # requiring 2 IsMuon
               MotherCut = """
            ( VFASPF(VCHI2) < 15 ) &
            ( (BPVLTIME () * c_light)   > 100 * micrometer ) &
            ( BPVIPCHI2() < 225 )
            """
            )

    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

    return Selection (name,
                      Algorithm = Tau2MuMuMu,
                      RequiredSelections = [ _stdNoPIDLooseMuons ])


def makeTau23MufullDST(name, config):
    """
    Tightened by Vitalii Lisovskyi to squeeze it to the DST limits. This is a clone of the MDST line, not a replacement.
    """

    Tau2MuMuMuDST = Combine3Particles(\
               DecayDescriptor = " [ tau+ -> mu+ mu+ mu- ]cc",
               DaughtersCuts = { "mu+" : " ( PT > 300 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) "\
                                 "& ( BPVIPCHI2 () >  10 ) " % config},
               Combination12Cut = "(AM< 2000*MeV) & (AM >240*MeV) & (ACUTDOCACHI2(9,''))", #"(ADAMASS('tau+')<400*MeV)" -> 1778+400  and then -100 for muon rest mass
               #CombinationCut = "(ADAMASS('tau+')<200*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 1.5  ) & ( ANUM ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 1.5  )", # requiring at least 2 IsMuon and 1 ProbNNmu>0.1
               CombinationCut = "(ADAMASS('tau+')<200*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 1.5  ) & ( ANUM ( ( 'mu-' == ABSID )  & (PROBNNmu*(1-PROBNNpi)*(1-PROBNNk)>0.11) )> 1.5  )", # requiring at least 2 IsMuon and 1 ProbNNmu>0.1 #& ( TrMATCHCHI2<20 )

               #CombinationCut = "( ANUM (( 'mu-' == ABSID ) & (MUONCHI2CORRELATED<500))> 1.5 )", # (ADAMASS('tau+')<200*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 1.5  ) &
               MotherCut = """
            ( VFASPF(VCHI2) < 9 ) &
            ( (BPVLTIME () * c_light)   > 100 * micrometer ) &
            ( BPVIPCHI2() < 225 ) &
            ( BPVDIRA > 0.9995 )
            """
            )

    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

    return Selection (name,
                      Algorithm = Tau2MuMuMuDST,
                      RequiredSelections = [ _stdNoPIDLooseMuons ])

def makeTau23MufullDST3mu(name, config):
    """
    Changed by Vitalii Lisovskyi to squeeze it to the DST limits. This is a clone of the MDST line, not a replacement.
    """

    Tau2MuMuMuDST = Combine3Particles(\
               DecayDescriptor = " [ tau+ -> mu+ mu+ mu- ]cc",
               DaughtersCuts = { "mu+" : " ( PT > 290 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ISMUON "\
                                 "& ( BPVIPCHI2 () >  9 ) " % config},
               Combination12Cut = "(AM< 2000*MeV) & (AM >240*MeV) & ( ANUM ( PT>350*MeV )> 0.5  )  ", #"(ADAMASS('tau+')<400*MeV)" -> 1778+400  and then -100 for muon rest mass
               #CombinationCut = "(ADAMASS('tau+')<200*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 1.5  ) & ( ANUM ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 1.5  )", # requiring at least 2 IsMuon and 1 ProbNNmu>0.1
               CombinationCut = "(ADAMASS('tau+')<200*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )==3  )  ", # requiring at least 2 IsMuon and 1 ProbNNmu>0.1 #& ( TrMATCHCHI2<20 )

               #CombinationCut = "( ANUM (( 'mu-' == ABSID ) & (MUONCHI2CORRELATED<500))> 1.5 )", # (ADAMASS('tau+')<200*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 1.5  ) &
               MotherCut = """
            ( VFASPF(VCHI2) < 12 ) &
            ( (BPVLTIME () * c_light)   > 100 * micrometer ) &
            ( BPVIPCHI2() < 500 ) &
            ( BPVDIRA > 0.995 )
            """
            )

    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

    return Selection (name,
                      Algorithm = Tau2MuMuMuDST,
                      RequiredSelections = [ _stdNoPIDLooseMuons ])

def makeDs23Pi(name, config):
    """
    Please contact Marcin Chrzaszcz if you think of prescaling this line!

    Arguments:
    name        : name of the Selection.
    """

    Ds2PiPiPi = Combine3Particles(\
              DecayDescriptor = " [ D_s+  -> pi+ pi+ pi- ]cc " ,
              DaughtersCuts = { "pi+" : " ( PT > 300 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config},
              Combination12Cut = "AM('D_s+')<1920*MeV)", #"(ADAMASS('D_s+')<80*MeV)" -> 1970+80 = 2050 and then minus 130 for pion rest mass
              CombinationCut = "(ADAMASS('D_s+')<80*MeV)",
              MotherCut = """
            ( VFASPF(VCHI2) < 15 ) &
            ( (BPVLTIME () * c_light)   > 100 * micrometer ) &
            ( BPVIPCHI2() < 225 )
            """
              )

    _stdLoosePions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")

    return Selection (name,
                      Algorithm = Ds2PiPiPi,
                      RequiredSelections = [ _stdLoosePions ])

def makeDs23PiTIS(self, name, config):
    """
    Please contact Marcin Chrzaszcz if you think of prescaling this line!

    Arguments:
    name        : name of the Selection.
    """
    def makeTISTOS( name, _input, _trigger ) :
            from Configurables import TisTosParticleTagger
            _tisTosFilter = TisTosParticleTagger( name + "Tagger" )
            _tisTosFilter.TisTosSpecs = { _trigger : 0 }
            #_tisTosFilter.ProjectTracksToCalo = False
            #_tisTosFilter.CaloClustForCharged = False
            #_tisTosFilter.CaloClustForNeutral = False
            #_tisTosFilter.TOSFrac = { 4:0.0, 5:0.0 }
            return Selection( name
                              , Algorithm = _tisTosFilter
                              , RequiredSelections = [ _input ]
                              )

    self.combDs2pipipi=makeDs23Pi(name, config)

    self.selDs23PiHlt1TIS = makeTISTOS( self.name() + "Ds23PiHlt1TIS"
                                        , self.combDs2pipipi#makeDs23Pi#self.combPiPiPi
                                        , "Hlt1.*Decision%TIS"
                                        )
    self.selDs23PiHlt2TIS = makeTISTOS( self.name() + "Ds23PiHlt2TIS"
                                        , self.selDs23PiHlt1TIS
                                        , "Hlt2.*Decision%TIS"
                                        )

    return self.selDs23PiHlt2TIS

#    return Selection (name,
#                      Algorithm = Ds2PiPiPiTIS,
#                      RequiredSelections = [ Ds2PiPiPi ])


def makeDs2PhiPi(name, config):
    """
    Please contact Marcin Chrzaszcz if you think of prescaling this line!

    Arguments:
    name        : name of the Selection.
    """

    Ds2PhiPi = Combine3Particles(\
             DecayDescriptor =   " [ D_s+  -> mu+  mu- pi+ ]cc ",
             DaughtersCuts = { "pi+" : " ( PT > 300 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config,
                               "mu+" : " ( PT > 300 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config},
             Combination12Cut = " in_range ( 970 * MeV , AM , 1070 * MeV ) &  (0.5 < ANUM ( ( 'mu-' == ABSID ) & ISMUON )) ",
             CombinationCut = "(ADAMASS('D_s+')<250*MeV)", # & in_range ( 970 * MeV , AM12 , 1070 * MeV )"
             MotherCut = """
            ( VFASPF(VCHI2) < 15 ) &
            ( (BPVLTIME () * c_light)   >100 * micrometer ) &
            ( BPVIPCHI2() < 225 )
            """
            )

    _stdLoosePions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
    _stdLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")



    return Selection (name,
                      Algorithm = Ds2PhiPi,
                      RequiredSelections = [ _stdLooseMuons, _stdLoosePions ])

def makeDs2PhiPifullDST(name, config):
    """
    Clone of the MDST line which will go to the DST. Changed a bit to improve consistency with the tau->3mu DST line.
    """

    Ds2PhiPi = Combine3Particles(\
             DecayDescriptor =   " [ D_s+  -> mu+  mu- pi+ ]cc ",
             DaughtersCuts = { "pi+" : " ( PT > 300 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config,
                               "mu+" : " ( PT > 300 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config},
             Combination12Cut = " in_range ( 970 * MeV , AM , 1070 * MeV ) &  (0.5 < ANUM ( ( 'mu-' == ABSID ) & ISMUON & (PROBNNmu > 0.1) )) ",
             CombinationCut = "(ADAMASS('D_s+')<250*MeV)", # & in_range ( 970 * MeV , AM12 , 1070 * MeV )"
             MotherCut = """
            ( VFASPF(VCHI2) < 16 ) &
            ( (BPVLTIME () * c_light)   >100 * micrometer ) &
            ( BPVIPCHI2() < 225 ) &
            ( BPVDIRA > 0.995 )
            """
            )

    _stdLoosePions = DataOnDemand(Location = "Phys/StdLoosePions/Particles")
    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")



    return Selection (name,
                      Algorithm = Ds2PhiPi,
                      RequiredSelections = [ _stdLooseMuons, _stdLoosePions ])


def makeTau25Mu(name, config):
    """
    Edited by Vitalii Lisovskyi to loosen the cuts. The original version survives in the old stripping campaigns.

    Arguments:
    name        : name of the Selection.
    """

    Tau2MuMuMuMuMu = Combine5Particles(\
                   DecayDescriptor = " [ tau+ -> mu+ mu+ mu+ mu- mu-]cc",
                   DaughtersCuts = { "mu+" : " ( PT > 150 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  4 ) " % config }, # was 300 MeV, ipchi2 > 9
                   Combination12Cut = "AM<1878*MeV",   # 1778 + 400 - 3*100
                   Combination123Cut = "(AM<1978*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 0.5)",   # 1778 + 400 - 2*100
                   Combination1234Cut = "(AM<2078*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & ISMUON )> 1.5)",   # 1778 + 400 - 1*100  
                   CombinationCut = "(ADAMASS('tau+')<400*MeV) & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) & (PROBNNmu>0.1) )> 2.5  )",
                   MotherCut = "( VFASPF(VCHI2/VDOF) < 30 ) & ( (BPVLTIME() * c_light)  > 36 * micrometer ) & (BPVDIRA > 0.99)"
            #        """
            # ( VFASPF(VCHI2) < 30 ) &
            # ( (BPVLTIME () * c_light)   > 100 * micrometer ) &
            # ( BPVIPCHI2() < 225 )
            # """
                   )

    #_stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

    return Selection (name,
                      Algorithm = Tau2MuMuMuMuMu,
                      RequiredSelections = [ _stdNoPIDLooseMuons ])


def makeTau2pmm(name, config):
    """
    Please contact Marcin Chrzaszcz if you think of prescaling this line!

    Arguments:
    name        : name of the Selection.
    """

    Tau2PMuMu = Combine3Particles(\
              DecayDescriptors = [" [ tau+ -> p+ mu+ mu- ]cc"," [ tau+ -> p~- mu+ mu+ ]cc",
                                  " [ Lambda_c+ -> p+ mu+ mu- ]cc"," [ Lambda_c+ -> p~- mu+ mu+ ]cc" ],
              DaughtersCuts = { "mu+" : " ( PT > 300 * MeV ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) "\
                                  "& ( PIDmu > -5 ) & ( (PIDmu - PIDK) > 0 ) & ( TRGHOSTPROB < %(TrackGhostProb)s )"% config,
                                  "p+" :  " ( PT > 300 * MeV ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) "\
                                  "& (PIDp>10) & ( TRGHOSTPROB < %(TrackGhostProb)s )" % config},
              Combination12Cut = "AM<2340*MeV", # lambda_c mass + 150 - muon mass = 2290 + 150 -100 =
              CombinationCut = "( (ADAMASS('tau+')<150*MeV) | (ADAMASS('Lambda_c+')<150*MeV) )",
              MotherCut = """
            ( VFASPF(VCHI2) < 15 ) &
            ( (BPVLTIME () * c_light)   > 100 * micrometer ) &
            ( BPVIPCHI2() < 225 )
            """
              )

    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdLooseProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles")

    return Selection (name,
                      Algorithm = Tau2PMuMu,
                      RequiredSelections = [ _stdLooseMuons, _stdLooseProtons ])
