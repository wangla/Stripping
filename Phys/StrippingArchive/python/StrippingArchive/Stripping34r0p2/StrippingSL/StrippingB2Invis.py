###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Mark Smith']
__date__ = '13/03/2021'
__version__ = '$Revision: 0.0 $'
"""
These are some lines for measuring the BF of Lc->pKpi and Ds->KKpi
"""

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdAllLoosePions

__all__ = ('B2InvisAllLinesConf',
           'default_config')

default_config = {
    'NAME'        : 'B2Invis',
    'WGs'         : ['Semileptonic'],
    'BUILDERTYPE' : 'B2InvisAllLinesConf',
    'CONFIG'      : {
          "GEC_nSPDHits"        : 600.   ,#adimensional
          #Proton Cuts
          "TRGHOSTPROB"         : 0.3,
          "ProtonTRCHI2"        : 3.     ,#adimensional
          "ProtonP"             : 15000. ,#MeV
          "ProtonPT"            : 800.  , 
          "ProtonPIDK"          : 4.     ,#adimensional 
          "ProtonPIDp"          : 4.     ,#adimensional
          "ProtonMINIPCHI2"     : 16.    ,#adminensional
          #Pion Cuts 
          "PionTRCHI2"        : 3.     ,#adimensional
          "PionP"             : 3000. ,#MeV
          "PionPT"            : 500.  , 
          "PionPIDK"          : 0.     ,#adimensional 
          "PionMINIPCHI2"     : 16.    ,#adminensional
          #Xu Cuts
          "XuPT"                : 1800. ,#MeV
          "XuVCHI2DOF"           : 3.     ,#adminensional
          "XuDIRA"               : 0.9995  ,#adminensional
          "XuFDCHI2HIGH"         : 150.   ,#adimensional
          "XuMassLower"        : 1230.  ,#MeV
          "XuMassUpper"         : 2800, #MeV
          #B Cuts
          "BVCHI2DOF"           : 4.     ,#adminensional
          "BDIRA"               : 0.9995  ,#adminensional
          "BFDCHI2HIGH"         : 150.   ,#adimensional
          "XuPiPT"               : 2500.,  #MeV
          "XuPiP"               : 40000.,   #MeV
          "XuPiMassLower"        : 1800.  ,#MeV
          "XuPiMassUpper"       : 2900  ,#MeV
          "massDiff"            : 400,
          "XuPiConstrainMLower" : 2300, #MeV
          "XuPiConstrainMUpper" : 2700, #MeV
            },
    'STREAMS'     : ['Semileptonic'],
}

class B2InvisAllLinesConf(LineBuilder) :
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    __confdict__={}
        
    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.__confdict__=config

        self.GECs = { "Code":"( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(GEC_nSPDHits)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}      

        self._protonSel=None
        self._protonFilter()

        self._fakeProtonSel=None
        self._fakeProtonFilter()


        self._pionSel=None
        self._pionFilter()

        self._picSel=None
        self._picFilter()

        self._XuSel = None
        self._XuFilter()

        self._fakeP_XuSel = None
        self._fakeP_XuFilter()


        self._Definitions()

        self.registerLine(self._Bplus_line())
        self.registerLine(self._Bplus_SS_line())
        self.registerLine(self._Bplus_fakeP_line())


    def _Definitions(self):
        return [ 
            "from LoKiPhys.decorators import *",
                           ]

    #Define selection strings for the decay products
    def _NominalPSelection( self ):
        return "(TRCHI2DOF < %(ProtonTRCHI2)s ) &  (P> %(ProtonP)s *MeV) &  (PT> %(ProtonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDp-PIDpi > %(ProtonPIDp)s )& (PIDp-PIDK > %(ProtonPIDK)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(ProtonMINIPCHI2)s )"\
               "& (switch(ISMUON,1,0) < 1)"

    #Define selection strings for the decay products
    def _FakePSelection( self ):
        return "(TRCHI2DOF < %(ProtonTRCHI2)s ) &  (P> %(ProtonP)s *MeV) &  (PT> %(ProtonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDp-PIDpi < %(ProtonPIDp)s )& (PIDp-PIDK < %(ProtonPIDK)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(ProtonMINIPCHI2)s )"\
               "& (switch(ISMUON,1,0) < 1)"


    def _NominalPiSelection( self ):
        return "(TRCHI2DOF < %(ProtonTRCHI2)s ) &  (P> %(PionP)s *MeV) &  (PT> %(PionPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"\
               "& (switch(ISMUON,1,0) < 1)"

    def _PicSelection( self ):
        return "(TRCHI2DOF < %(ProtonTRCHI2)s ) &  (P> %(PionP)s *MeV) &  (PT> %(ProtonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"\
               "& (switch(ISMUON,1,0) < 1)"

    #Define selection string for the decay products of the B+ that are not from the sigma++
    def _XuSelection( self ):
        return "(VFASPF(VCHI2/VDOF)< %(XuVCHI2DOF)s) & (BPVDIRA> %(XuDIRA)s)"\
                     " & (BPVVDCHI2 >%(XuFDCHI2HIGH)s)"\
                     " & (PT > %(XuPT)s)"\
                     " & (M < %(XuMassUpper)s) & (M > %(XuMassLower)s)"
    #Define the selection for the Bplus candidate
    def _BPlusSelection( self ):
        return "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"\
                     "& (PT > %(XuPiPT)s) & (P > %(XuPiP)s)"\
                     "& (BPVVDCHI2 >%(BFDCHI2HIGH)s)"\
                     " & (abs(M-MAXTREE('Delta++'==ABSID,M)-169.) < %(massDiff)s )" \
                     " & (BPVMASSMOMCONSTR('Lambda_c+', 2, True) < %(XuPiConstrainMUpper)s) & (BPVMASSMOMCONSTR('Lambda_c+', 2, True) > %(XuPiConstrainMLower)s) "

    ###### Pion Filter ######
    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _pi = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _piSel=Selection("pi_for"+self._name,
                         Algorithm=_pi,
                         RequiredSelections = [StdLoosePions])
        
        self._pionSel=_piSel
        return _piSel

    ###### Pion from Sigma_c Filter ######
    def _picFilter( self ):
        if self._picSel is not None:
            return self._picSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _pic = FilterDesktop( Code = self._PicSelection() % self._config )
        _picSel=Selection("pic_for"+self._name,
                         Algorithm=_pic,
                         RequiredSelections = [StdLoosePions])
        
        self._picSel=_picSel
        return _picSel

    ###### Proton Filter ######
    def _protonFilter( self ):
        if self._protonSel is not None:
            return self._protonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseProtons
        
        _pr = FilterDesktop( Code = self._NominalPSelection() % self._config )
        _prSel=Selection("p_for"+self._name,
                         Algorithm=_pr,
                         RequiredSelections = [StdLooseProtons])
        
        self._protonSel=_prSel
        
        return _prSel

    ###### Proton Filter ######
    def _fakeProtonFilter( self ):
        if self._fakeProtonSel is not None:
            return self._fakeProtonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsProtons
        
        _pr = FilterDesktop( Code = self._FakePSelection() % self._config )
        _prSel=Selection("fake_p_for"+self._name,
                         Algorithm=_pr,
                         RequiredSelections = [StdNoPIDsProtons])
        
        self._fakeProtonSel=_prSel
        
        return _prSel

    ##### Xu Filter #####
    def _XuFilter( self ):
        if self._XuSel  is not None:
            return self._XuSel
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        Xu = CombineParticles(DecayDescriptors = ["[Delta++ -> p+ pi+ pi+]cc"], ReFitPVs = True)
        Xu.Preambulo = self._Definitions()
        Xu.MotherCut = self._XuSelection() % self._config
        Xu.ReFitPVs = True
            
        XuSel=Selection("Xu_B_for"+self._name,
                         Algorithm=Xu,
                         RequiredSelections = [self._protonFilter(), self._pionFilter()])
        self._XuSel = XuSel 
        return XuSel

    ##### Xu Filter #####
    def _fakeP_XuFilter( self ):
        if self._fakeP_XuSel  is not None:
            return self._fakeP_XuSel
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        Xu = CombineParticles(DecayDescriptors = ["[Delta++ -> p+ pi+ pi+]cc"], ReFitPVs = True)
        Xu.Preambulo = self._Definitions()
        Xu.MotherCut = self._XuSelection() % self._config
        Xu.ReFitPVs = True
            
        XuSel=Selection("fakeP_Xu_B_for"+self._name,
                         Algorithm=Xu,
                         RequiredSelections = [self._fakeProtonFilter(), self._pionFilter()])
        self._fakeP_XuSel = XuSel 
        return XuSel

    ##### B+ Filter #####
    def _BpFilter( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection,MergedSelection
        
        _XuPi = CombineParticles(DecayDescriptors = ["[B+ -> Delta++ pi-]cc"], ReFitPVs = True)
        _XuPi.Preambulo = self._Definitions()
        _XuPi.CombinationCut = "(AM>%(XuPiMassLower)s*MeV) & (AM<%(XuPiMassUpper)s)" % self._config
        _XuPi.MotherCut = self._BPlusSelection() % self._config
        _XuPi.ReFitPVs = True
            
        _XuPiSel=Selection("XuPi_B_for"+self._name,
                         Algorithm=_XuPi,
                         RequiredSelections = [self._picFilter(), self._XuFilter()])
        return _XuPiSel

    ##### B+ same-sign Filter #####
    def _Bp_SSFilter( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection,MergedSelection
        
        _XuPi = CombineParticles(DecayDescriptors = ["[B+ -> Delta++ pi+]cc"], ReFitPVs = True)
        _XuPi.Preambulo = self._Definitions()
        _XuPi.CombinationCut = "(AM>%(XuPiMassLower)s*MeV) & (AM<%(XuPiMassUpper)s)" % self._config
        _XuPi.MotherCut = self._BPlusSelection() % self._config
        _XuPi.ReFitPVs = True
            
        _XuPiSel=Selection("XuPiSS_B_for"+self._name,
                         Algorithm=_XuPi,
                         RequiredSelections = [self._picFilter(), self._XuFilter()])
        return _XuPiSel

    ##### B+ fake proton Filter #####
    def _Bp_fakeP_Filter( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection,MergedSelection
        
        _XuPi = CombineParticles(DecayDescriptors = ["[B+ -> Delta++ pi-]cc"], ReFitPVs = True)
        _XuPi.Preambulo = self._Definitions()
        _XuPi.CombinationCut = "(AM>%(XuPiMassLower)s*MeV) & (AM<%(XuPiMassUpper)s)" % self._config
        _XuPi.MotherCut = self._BPlusSelection() % self._config
        _XuPi.ReFitPVs = True
            
        _XuPiSel=Selection("XuPi_fakeP_B_for"+self._name,
                         Algorithm=_XuPi,
                         RequiredSelections = [self._picFilter(), self._fakeP_XuFilter()])
        return _XuPiSel

    ##### Signal line #####
    def _Bplus_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = ""
        ldu = ''
        return StrippingLine(self._name+'Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._BpFilter()], HLT2 = hlt, L0DU = ldu)

    ##### same-sign line #####
    def _Bplus_SS_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = ""
        ldu = ''
        return StrippingLine(self._name+'SSLine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bp_SSFilter()], HLT2 = hlt, L0DU = ldu)

    ##### fake proton line #####
    def _Bplus_fakeP_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = ""
        ldu = ''
        return StrippingLine(self._name+'fakePLine', prescale = 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bp_fakeP_Filter()], HLT2 = hlt, L0DU = ldu)        
