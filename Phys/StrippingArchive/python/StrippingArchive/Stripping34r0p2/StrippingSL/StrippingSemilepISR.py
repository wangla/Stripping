###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Vitalii Lisovskyi; heavily based on B2DMuNuX lines by their corresponding authors']
__date__ = '07/03/2021'
__version__ = '$Revision: 0.0 $'

'''
Stripping lines for semileptonic decays with emission of a dilepton via initial-state-radiation.
'''

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdLooseProtons, StdLooseElectrons
from StandardParticles import StdNoPIDsPions, StdNoPIDsKaons,StdNoPIDsProtons,StdNoPIDsMuons
#from StrippingSemilepISRUtils import *
from GaudiKernel.SystemOfUnits import MeV, GeV, cm, mm
from CommonParticles.Utils import *

__all__ = ('SemilepISRAllLinesConf',
           'default_config')

default_config = {
    'SemilepISR' : {
        'WGs'         : ['Semileptonic'],
        'BUILDERTYPE' : 'SemilepISRAllLinesConf',
        'STREAMS'     : ['Semileptonic'],
        'CONFIG'      : {
            "prescaleFakes": 0.05 #0.02
            ,"prescales": 1 #{'StrippingSemilepISR_D0_Electron':1.0}
            ,"GEC_nLongTrk" : 250
            ,"TTSpecs"      : {}
            ,"HLT1"   : "HLT_PASS_RE('Hlt1.*Decision')"
            ,"HLT2"   : "HLT_PASS_RE('Hlt2.*Decision')"
            ,"Monitor"      : False
            ,"UseNoPIDsInputs":False
            ,"TRGHOSTPROB"   : 0.35
            ,"TRCHI2"        : 3.0
            ,"MuonPIDmu"     : 0.0
            ,"MuonPT"        : 1000*MeV
            ,"MuonIPCHI2"    : 9.00
            ,"MuonP"         : 6.0*GeV
            ,"HadronPT"      : 250.0*MeV
            ,"HadronP"       : 2.0*GeV
            ,"HadronIPCHI2"  : 4.0
            ,"ProtonPIDp"    : 0.0
            ,"ProtonPIDpK"   : 0.0
            ,"ProtonP"       : 8.0*GeV
            ,"KaonPIDK"      : -2.0
            ,"KaonP"         : 2.0*GeV
            ,"PionPIDK"      : 10.0
            ,"ElectronPIDe"  : 3.0
            ,"ElectronPT"    : 300*MeV
            ,"D_BPVDIRA"     : 0.99
            ,"D_FDCHI2"      : 25.0
            ,"D_MassMax"     : 1700. * MeV
            ,"D_MassMin"     : 700. * MeV
            ,"D_AMassWin"    : 90.*MeV ## this should be 10 MeV wider than the widest D_MassWin
            ,"D_MassWin"     : {"default":80*MeV,
                                "Xic0": 60*MeV,
                                "Omegac": 60*MeV}
            ,"D_VCHI2DOF"    : 6.0
            ,"D_DocaChi2Max" : 20
            ,"B_DIRA"         : 0.99 # was 0.999
            ,"B_VCHI2DOF"      : 9.0
            ,"B_D_DZ"         : -2.0*mm
            ,"B_MassMin"       : 2.2*GeV
            ,"B_MassMax"       : 8.0*GeV
            ,"B_DocaChi2Max"  : 10
            },

    }
}

class SemilepISRAllLinesConf(LineBuilder) :

    __configuration_keys__ = default_config['SemilepISR']['CONFIG'].keys()

    __confdict__={}

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.__confdict__=config

        ####################### BASIC FINAL STATE PARTICLE SELECTIONS ##########################

        self.HadronCuts = "(P>%(HadronP)s) & (PT > %(HadronPT)s )"\
            "& (TRCHI2DOF < %(TRCHI2)s)"\
            "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
            "& (MIPCHI2DV(PRIMARY)> %(HadronIPCHI2)s)" % config

        self.MuonTrackCuts = "(PT > %(MuonPT)s ) & (P> %(MuonP)s)"\
            "& (TRCHI2DOF < %(TRCHI2)s)"\
            "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
            "& (MIPCHI2DV(PRIMARY)> %(MuonIPCHI2)s)" %config

        inputs = {"muons":StdLooseMuons,
                  "pions":StdLoosePions,
                  "kaons":StdLooseKaons,
                  "protons":StdLooseProtons,
                  "electrons":StdLooseElectrons,
                  "fakemuons":StdNoPIDsMuons}

        if config["UseNoPIDsInputs"] == True:
            inputs["muons"] = StdNoPIDsMuons
            inputs["pions"] = StdNoPIDsPions
            inputs["kaons"] = StdNoPIDsKaons
            inputs["protons"] = StdNoPIDsProtons

        self.cuts = {"muons":self.MuonTrackCuts + " & (PIDmu > %(MuonPIDmu)s)" % config,
                     "fakemuons":self.MuonTrackCuts + " & (INMUON) & (PIDmu < %(MuonPIDmu)s)" % config,
                     "kaons":self.HadronCuts + " & (P>%(KaonP)s) & (PIDK> %(KaonPIDK)s)" % config,
                     "pions":self.HadronCuts + " & (PIDK< %(PionPIDK)s)" % config,
                     "protons":self.HadronCuts + "& (P>%(ProtonP)s)"\
                         "& (PIDp > %(ProtonPIDp)s) & (PIDp-PIDK > %(ProtonPIDpK)s)" % config,
                     "electrons":self.MuonTrackCuts + " & (PT> %(ElectronPT)s) & (PIDe > %(ElectronPIDe)s)" % config
                     }

        self.selMuon = Selection("Mufor"+name,
                                 Algorithm=FilterDesktop(Code=self.cuts["muons"]),
                                 RequiredSelections = [inputs["muons"]])

        self.selKaon = Selection( "Kfor" + name,
                                  Algorithm = FilterDesktop(Code=self.cuts["kaons"]),
                                  RequiredSelections = [inputs["kaons"]])

        self.selPion = Selection( "Pifor" + name,
                                  Algorithm = FilterDesktop(Code=self.cuts["pions"]),
                                  RequiredSelections = [inputs["pions"]])

        self.selProton = Selection( "ProtonsFor" + name,
                                    Algorithm = FilterDesktop(Code=self.cuts["protons"]),
                                    RequiredSelections = [inputs["protons"]])

        self.selElectron = Selection("efor"+name,
                                     Algorithm=FilterDesktop(Code=self.cuts["electrons"]),
                                     RequiredSelections = [inputs["electrons"]])


        self.selMuonFakes = Selection( "FakeMuonsFor" + name,
                                       Algorithm = FilterDesktop(Code = self.cuts["fakemuons"]),
                                       RequiredSelections = [inputs["fakemuons"]])

        ### Dictionary containing special cuts for each charm mode
        ### at this stage they are just the mass windows.
        ### All other cuts are the same for all charm species
        ### and will be specified later in the line building function
        CharmCuts = {}
        for mode in ["D0","D0_partreco","Dp","Ds","Lc","Xic","Xic0","Omegac"]:
            CharmCuts[mode] = config.copy()

        # the Dp (->Kpipi) and Ds (->KKpi) are special
        # in that they each cover both the Ds and D+ mass peaks with the default sideband on either side.
        CharmCuts["Dp"]["CharmComboCuts"] = "(DAMASS('D_s+') < %s )"\
            "& (DAMASS('D+')> -%s )" %(config["D_AMassWin"],config["D_AMassWin"])
        CharmCuts["Dp"]["CharmMotherCuts"] = "(DMASS('D_s+') < %s )"\
            "& (DMASS('D+')> -%s )" %(config["D_MassWin"]["default"],config["D_MassWin"]["default"])
        # and Ds and Dp are the same in this regard
        CharmCuts["Ds"] = CharmCuts["Dp"].copy()

        CharmCuts['D0']["CharmComboCuts"]  = "(ADAMASS('D0') < %s)" % config["D_AMassWin"]
        CharmCuts['D0']["CharmMotherCuts"] = "(ADMASS('D0')  < %s)" % config["D_MassWin"]["default"]

        CharmCuts['D0_partreco']["CharmComboCuts"] = "((AM < %s) & (AM > %s))" % (config["D_MassMax"], config["D_MassMin"])
        CharmCuts['D0_partreco']["CharmMotherCuts"] = "((MM < %s) & (MM > %s))" % (config["D_MassMax"], config["D_MassMin"])

        CharmCuts["Lc"]["CharmComboCuts"] = "(ADAMASS('Lambda_c+') < %s )" %config["D_AMassWin"]
        CharmCuts["Lc"]["CharmMotherCuts"] = "(ADMASS('Lambda_c+') < %s )" %config["D_MassWin"]["default"]

        CharmCuts["Xic"]["CharmComboCuts"] = "(ADAMASS('Xi_c+') < %s )"  %config["D_AMassWin"]
        CharmCuts["Xic"]["CharmMotherCuts"] = "(ADMASS('Xi_c+') < %s )"  %config["D_MassWin"]["default"]

        CharmCuts["Xic0"]["CharmComboCuts"] = "(ADAMASS('Xi_c0') < %s )" %config["D_AMassWin"]
        CharmCuts["Xic0"]["CharmMotherCuts"] = "(ADMASS('Xi_c0') < %s )" %config["D_MassWin"]["Xic0"]

        CharmCuts["Omegac"]["CharmComboCuts"] = "(ADAMASS('Omega_c0') < %s )" %config["D_AMassWin"]
        CharmCuts["Omegac"]["CharmMotherCuts"] = "(ADMASS('Omega_c0') < %s )" %config["D_MassWin"]["Omegac"]

        self.b2D0MuXLine = BtoDlnuLine(name,
                                       'D0',
                                       ['[B- -> D0 mu- rho(770)0]cc','[B+ -> D0 mu+ rho(770)0]cc'],
                                       ['[D0 -> K- pi+]cc'],
                                       CharmCuts["D0"],
                                       [self.selKaon, self.selPion],self.selMuon,self.selMuonFakes)

        self.b2D0Mu2eXLine = BtoDlnuLine(name,
                                       'D02e',
                                       ['[B- -> D0 mu- omega(782)]cc','[B+ -> D0 mu+ omega(782)]cc'],
                                       ['[D0 -> K- pi+]cc'],
                                       CharmCuts["D0"],
                                       [self.selKaon, self.selPion],self.selMuon,self.selMuonFakes)


        # self.b2D0MuXK3PiLine = BtoDlnuLine(name,
        #                                'D0_K3Pi',
        #                                ['[B- -> D0 mu-]cc','[B+ -> D0 mu+]cc'],
        #                                ['[D0 -> K- pi+ pi- pi+]cc'],
        #                                CharmCuts["D0"],
        #                                [self.selKaon, self.selPion],self.selMuon,self.selMuonFakes)

        # self.b2D0MuXKMuLine = BtoDlnuLine(name,
        #                                'D0_KMuNu',
        #                                ['[B- -> D0 mu-]cc', '[B+ -> D0 mu+]cc'],
        #                                ['[D0 -> K- mu+]cc'],
        #                                CharmCuts["D0_partreco"],
        #                                [self.selKaon, self.selMuon], self.selMuon, self.selMuonFakes)

        # self.b2D0MuXKFakeMuLine = BtoDlnuLine(name,
        #                                 'D0_KFakeMuNu',
        #                                 ['[B- -> D0 mu-]cc', '[B+ -> D0 mu+]cc'],
        #                                 ['[D0 -> K- mu+]cc'],
        #                                 CharmCuts["D0_partreco"],
        #                                 [self.selKaon, self.selMuonFakes], self.selMuon, self.selMuonFakes)

        self.b2D0eXLine = BtoDlnuLine(name,
                                      'D0_Electron',
                                      ['[B- -> D0 e- rho(770)0]cc', '[B+ -> D0 e+ rho(770)0]cc'],
                                      ['[D0 -> K- pi+]cc'],
                                      CharmCuts["D0"],
                                      [self.selKaon, self.selPion],self.selElectron)


        self.b2DpMuXLine = BtoDlnuLine(name,
                                       'Dp',
                                       [ '[B0 -> D- mu+ rho(770)0]cc', '[B0 -> D- mu- rho(770)0]cc' ],
                                       [ '[D+ -> K- pi+ pi+]cc' ],
                                       CharmCuts["Dp"],
                                       [self.selKaon, self.selPion],self.selMuon,self.selMuonFakes)

        self.b2DpMu2eXLine = BtoDlnuLine(name,
                                       'Dp2e',
                                       [ '[B0 -> D- mu+ omega(782)]cc', '[B0 -> D- mu- omega(782)]cc' ],
                                       [ '[D+ -> K- pi+ pi+]cc' ],
                                       CharmCuts["Dp"],
                                       [self.selKaon, self.selPion],self.selMuon,self.selMuonFakes)

        self.b2DpeXLine = BtoDlnuLine(name,
                                       'Dp_Electron',
                                       [ '[B0 -> D- e+ rho(770)0]cc', '[B0 -> D- e- rho(770)0]cc' ],
                                       [ '[D+ -> K- pi+ pi+]cc' ],
                                       CharmCuts["Dp"],
                                       [self.selKaon, self.selPion],self.selElectron)

        self.b2DsMuXLine = BtoDlnuLine(name,
                                       'Ds',
                                       [ '[B0 -> D- mu+ rho(770)0]cc', '[B0 -> D- mu- rho(770)0]cc' ],
                                       [ '[D+ -> K+ K- pi+]cc' ],
                                       CharmCuts["Ds"],
                                       [self.selKaon, self.selPion],self.selMuon,self.selMuonFakes)

        self.b2DsMu2eXLine = BtoDlnuLine(name,
                                       'Ds2e',
                                       [ '[B0 -> D- mu+ omega(782)]cc', '[B0 -> D- mu- omega(782)]cc' ],
                                       [ '[D+ -> K+ K- pi+]cc' ],
                                       CharmCuts["Ds"],
                                       [self.selKaon, self.selPion],self.selMuon,self.selMuonFakes)

        self.b2DseXLine = BtoDlnuLine(name,
                                       'Ds_Electron',
                                       [ '[B0 -> D- e+ rho(770)0]cc', '[B0 -> D- e- rho(770)0]cc' ],
                                       [ '[D+ -> K+ K- pi+]cc' ],
                                       CharmCuts["Ds"],
                                       [self.selKaon, self.selPion],self.selElectron)

        self.lb2LcMuXLine = BtoDlnuLine(name,
                                        "Lc",
                                        [ '[Lambda_b0 -> Lambda_c+ mu- rho(770)0]cc', '[Lambda_b0 -> Lambda_c+ mu+ rho(770)0]cc'],
                                        [ '[Lambda_c+ -> K- p+ pi+]cc' ],
                                        CharmCuts["Lc"],
                                        [self.selProton,self.selKaon,self.selPion],self.selMuon,self.selMuonFakes)

        self.lb2LcMu2eXLine = BtoDlnuLine(name,
                                        "Lc2e",
                                        [ '[Lambda_b0 -> Lambda_c+ mu- omega(782)]cc', '[Lambda_b0 -> Lambda_c+ mu+ omega(782)]cc'],
                                        [ '[Lambda_c+ -> K- p+ pi+]cc' ],
                                        CharmCuts["Lc"],
                                        [self.selProton,self.selKaon,self.selPion],self.selMuon,self.selMuonFakes)

        self.lb2LceXLine = BtoDlnuLine(name,
                                        "Lc_Electron",
                                        [ '[Lambda_b0 -> Lambda_c+ e- rho(770)0]cc', '[Lambda_b0 -> Lambda_c+ e+ rho(770)0]cc'],
                                        [ '[Lambda_c+ -> K- p+ pi+]cc' ],
                                        CharmCuts["Lc"],
                                        [self.selProton,self.selKaon,self.selPion],self.selElectron)

        # self.Xic_Line = BtoDlnuLine(name,
        #                             "Xic",
        #                             [ '[Xi_b0 -> Xi_c+ mu-]cc', '[Xi_b0 -> Xi_c+ mu+]cc'],
        #                             [ '[Xi_c+ -> K- p+ pi+]cc' ],
        #                             CharmCuts["Xic"],
        #                             [self.selProton,self.selKaon,self.selPion],self.selMuon,self.selMuonFakes)
        #
        # self.Xic0_Line = BtoDlnuLine(name,
        #                              "Xic0",
        #                              [ '[Xi_b- -> Xi_c0 mu-]cc', '[Xi_b- -> Xi_c0 mu+]cc'],
        #                              [ '[Xi_c0 -> p+ K- K- pi+]cc'],
        #                              CharmCuts["Xic0"],
        #                              [self.selProton,self.selKaon,self.selPion],self.selMuon,self.selMuonFakes)
        #
        # self.Omegac_Line = BtoDlnuLine(name,
        #                                "Omegac",
        #                                [ '[Omega_b- -> Omega_c0 mu-]cc', '[Omega_b- -> Omega_c0 mu+]cc'],
        #                                [ '[Omega_c0 -> p+ K- K- pi+]cc' ],
        #                                CharmCuts["Omegac"],
        #                                [self.selProton,self.selKaon,self.selPion],self.selMuon,self.selMuonFakes)

        ##### line registration
        self.registerLine(self.b2D0eXLine)
        self.registerLine(self.b2DpeXLine)
        self.registerLine(self.b2DseXLine)
        self.registerLine(self.lb2LceXLine)
        #self.registerLine(self.b2D0MuXKKLine)
        #self.registerLine(self.b2D0MuXPiPiLine)
        for Mu in ["RealMuon","FakeMuon"]:
            self.registerLine(self.b2D0MuXLine[Mu])
            self.registerLine(self.b2D0Mu2eXLine[Mu])
            #self.registerLine(self.b2D0MuXK3PiLine[Mu])
            #self.registerLine(self.b2D0MuXKMuLine[Mu])
            #self.registerLine(self.b2D0MuXKFakeMuLine[Mu])
            self.registerLine(self.b2DpMuXLine[Mu])
            self.registerLine(self.b2DsMuXLine[Mu])
            self.registerLine(self.lb2LcMuXLine[Mu])
            self.registerLine(self.b2DpMu2eXLine[Mu])
            self.registerLine(self.b2DsMu2eXLine[Mu])
            self.registerLine(self.lb2LcMu2eXLine[Mu])
            #self.registerLine(self.Omegac_Line[Mu])
            #self.registerLine(self.Xic_Line[Mu])
            #self.registerLine(self.Xic0_Line[Mu])

######################################
def BtoDlnuLine(module_name,
                name,
                BDecays,
                DDecays,
                CONFIG,
                CHARM_DAUGHTERS,
                MUON,
                FAKE_MUON = None):

    #DEFAULT_GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" %CONFIG,
    #                 "Preambulo": ["from LoKiTracks.decorators import *"]}

    CHARM_DaugCuts = {}
    CHARM_ComboCuts = CONFIG["CharmComboCuts"]
    CHARM_MotherCuts = CONFIG["CharmMotherCuts"]
    CHARM_ComboCuts += " & (ADOCACHI2CUT( %(D_DocaChi2Max)s, ''))" % CONFIG
    CHARM_MotherCuts += "& (VFASPF(VCHI2/VDOF) < %(D_VCHI2DOF)s) " \
        "& (BPVVDCHI2 > %(D_FDCHI2)s) &  (BPVDIRA> %(D_BPVDIRA)s)"  % CONFIG

    if "CharmDaugCuts" in CONFIG.keys():
        CHARM_DaugCuts = CONFIG["CharmDaugCuts"]
    if "CharmExtraComboCuts" in CONFIG.keys():
        CHARM_ComboCuts += CONFIG["CharmExtraComboCuts"]
    if "CharmExtraMotherCuts" in CONFIG.keys():
        CHARM_MotherCuts += CONFIG["CharmExtraMotherCuts"]

    CHARM = Selection("CharmSelFor"+name+module_name,
                      Algorithm=CombineParticles(DecayDescriptors = DDecays,
                                                 DaughtersCuts = CHARM_DaugCuts,
                                                 CombinationCut = CHARM_ComboCuts,
                                                 MotherCut = CHARM_MotherCuts),
                      RequiredSelections = CHARM_DAUGHTERS)

    USED_CHARM = CHARM
    if "D*" in BDecays:
        DST = makeDstar("CharmSelDstFor"+name+module_name,CHARM,CONFIG)
        USED_CHARM = DST

    if "2e" in name:
        SOFTDILEPTON = _makeDiElectron("SoftDiElectronFor"+name+module_name,CONFIG)
    else:
        SOFTDILEPTON = _makeDiMuonDetached("SoftDimuonFor"+name+module_name,CONFIG)

    B_combinationCut = "(AM > %(B_MassMin)s) & (AM < %(B_MassMax)s) & (ADOCACHI2CUT( %(B_DocaChi2Max)s, ''))" %CONFIG
    B_motherCut = " (MM>%(B_MassMin)s) & (MM<%(B_MassMax)s)"\
        "&(VFASPF(VCHI2/VDOF)< %(B_VCHI2DOF)s) & (BPVDIRA> %(B_DIRA)s)"\
        "&(MINTREE(((ABSID=='D+')|(ABSID=='D0')|(ABSID=='Lambda_c+')|(ABSID=='Omega_c0')|(ABSID=='Xi_c+')|(ABSID=='Xi_c0'))"\
        ", VFASPF(VZ))-VFASPF(VZ) > %(B_D_DZ)s  ) " %CONFIG
    if "ExtraComboCuts" in CONFIG.keys():
        B_combinationCut += CONFIG["ExtraComboCuts"]
    if "ExtraMotherCuts" in CONFIG.keys():
        B_motherCut += CONFIG["ExtraMotherCuts"]

    B_DaugCuts = {}
    if "ExtraMuonCuts" in CONFIG.keys():
        B_DaugCuts = {"mu+":CONFIG["ExtraMuonCuts"]}
    if "ExtraElectronCuts" in CONFIG.keys():
        B_DaugCuts = {"e+":CONFIG["ExtraElectronCuts"]}
    _B = CombineParticles(DecayDescriptors = BDecays,
                          DaughtersCuts = B_DaugCuts,
                          CombinationCut = B_combinationCut,
                          MotherCut = B_motherCut)

    if CONFIG["Monitor"] == True :
        _B.Preambulo    = [
            "hdm1 = Gaudi.Histo1DDef ( 'D_M' , 1700 , 2800  , 200 )"
            ]
        _B.Monitor      = True
        _B.MotherMonitor  = "process ( monitor ( CHILD(M,1) , hdm1 , 'D_M' ) )  >> ~EMPTY """

    BSel = Selection ("BSelFor"+name+module_name,
                      Algorithm = _B,
                      RequiredSelections = [MUON,USED_CHARM,SOFTDILEPTON])


    BSelTOS = TOSFilter( "BSelFor"+name+module_name+"TOS"
                         ,BSel
                         ,CONFIG["TTSpecs"])

    LINE_NAME = module_name + "_"+name
    _prescale = 1.0
    #if LINE_NAME in CONFIG["prescales"].keys():
    #    _prescale = CONFIG["prescales"][LINE_NAME]

    if "Fake" in LINE_NAME:
        main_line = StrippingLine(LINE_NAME,
                    selection=BSelTOS,
                    HLT1=CONFIG["HLT1"],
                    HLT2=CONFIG["HLT2"],
                    RelatedInfoTools = [
                    { "Type" : "RelInfoMuonIDPlus",
                    "Variables" : ["MU_BDT"],
                    "DaughterLocations"  : {
                    "[ Beauty -> X ^l+ (X -> l+ l-) ]CC" : "Muon0BDT",
                    "[ Beauty -> X l+ (X -> ^l+ l-) ]CC" : "Muon1BDT",
                    "[ Beauty -> X l+ (X -> l+ ^l-) ]CC" : "Muon2BDT",}
                    },
                    ],
                    #FILTER=DEFAULT_GECs,
                    prescale=CONFIG["prescaleFakes"])

    else:
        main_line = StrippingLine(LINE_NAME,
                                  selection = BSelTOS,
                                  HLT1 = CONFIG["HLT1"],
                                  HLT2 = CONFIG["HLT2"],
                                  RelatedInfoTools = [
                                  { "Type" : "RelInfoMuonIDPlus",
                                  "Variables" : ["MU_BDT"],
                                  "DaughterLocations"  : {
                                  "[ Beauty -> X ^l+ (X -> l+ l-) ]CC" : "Muon0BDT",
                                  "[ Beauty -> X l+ (X -> ^l+ l-) ]CC" : "Muon1BDT",
                                  "[ Beauty -> X l+ (X -> l+ ^l-) ]CC" : "Muon2BDT",}
                                  },
                                  ],

                                  #FILTER=DEFAULT_GECs,
                                  prescale = _prescale)

    if FAKE_MUON == None:
        return main_line
    else:
        BSelFake = Selection ("BSelFakeFor"+name+module_name,
                              Algorithm = _B,
                              RequiredSelections = [FAKE_MUON,USED_CHARM,SOFTDILEPTON])

        BSelFakeTOS = TOSFilter( "BSelFakeFor"+name+module_name+"TOS"
                                 ,BSelFake
                                 ,CONFIG["TTSpecs"])
        return {"RealMuon":main_line,
                "FakeMuon":StrippingLine(LINE_NAME + '_FakeMuon',
                                         selection = BSelFakeTOS,
                                         HLT1 = CONFIG["HLT1"],
                                         HLT2 = CONFIG["HLT2"],
                                         RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> X ^l+ (X -> l+ l-) ]CC" : "Muon0BDT",
                                         "[ Beauty -> X l+ (X -> ^l+ l-) ]CC" : "Muon1BDT",
                                         "[ Beauty -> X l+ (X -> l+ ^l-) ]CC" : "Muon2BDT",}
                                         },
                                         ],
                                         #FILTER=DEFAULT_GECs,
                                         prescale = CONFIG["prescaleFakes"])
                }


########### HELP WITH MAKING A DSTAR ########################
def makeDstar(_name, inputD0,CONFIG) :
    _softPi = DataOnDemand(Location = 'Phys/StdAllLoosePions/Particles')
    _inputD0_conj = Selection("SelConjugateD0For"+_name,
                             Algorithm = ConjugateNeutralPID('ConjugateD0For'+_name),
                             RequiredSelections = [inputD0])
    _cutsSoftPi = '( PT > %(Dstar_SoftPion_PT)s  )' % CONFIG
    _cutsDstarComb = '(AM - ACHILD(M,1) + 5 > %(Dstar_wideDMCutLower)s ) & (AM - ACHILD(M,1) - 5 < %(Dstar_wideDMCutUpper)s )' % CONFIG
    _cutsDstarMoth_base = '(VFASPF(VCHI2/VDOF) < %(Dstar_Chi2)s )' % CONFIG
    _cutsDstarMoth_DM = '(M - CHILD(M,1) > %(Dstar_wideDMCutLower)s ) & (M - CHILD(M,1) < %(Dstar_wideDMCutUpper)s )' % CONFIG
    _cutsDstarMoth = '(' + _cutsDstarMoth_base + ' & ' + _cutsDstarMoth_DM + ')'
    _Dstar = CombineParticles( DecayDescriptor = "[D*(2010)+ -> D0 pi+]cc",
                               DaughtersCuts = { "pi+" : _cutsSoftPi },
                               CombinationCut = _cutsDstarComb,
                               MotherCut = _cutsDstarMoth)
    return Selection (name = "Sel"+_name,Algorithm = _Dstar,RequiredSelections = [inputD0,_inputD0_conj] + [_softPi])

########## TISTOS FILTERING ##################################
def TOSFilter( name = None, sel = None, trigger = None ):
    if len(trigger) == 0:
        return sel
    from Configurables import TisTosParticleTagger
    _filter = TisTosParticleTagger(name+"_TriggerTos")
    _filter.TisTosSpecs = trigger
    _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = [ sel ], Algorithm = _filter )
    return _sel

#####################################################
def _makeDiMuonDetached(  name, params ) :
    """
    Make a dimuon
    rho(770)0 is just a proxy to get the two-body combination
    """

    _Decays = "rho(770)0 -> mu+ mu-"

     # define all the cuts
    _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<0.75*mm)" % params

    _MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (BPVVDCHI2 > 9) " % params
    _daughtersCutsmu = "(ISMUON) & (TRCHI2DOF < %(TRCHI2)s) & (TRGHOSTPROB < %(TRGHOSTPROB)s) " % params

    _Combine = CombineParticles()

    _Combine.DecayDescriptor = _Decays

    _Combine.DaughtersCuts = {
        "mu+"  : _daughtersCutsmu }

    _Combine.CombinationCut   = _CombCuts
    _Combine.MotherCut        = _MotherCuts

    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
    # make and store the Selection object
    return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons ] )

#####################################################
def _makeDiElectron( name, params ) :
    """
    Make a dielectron
    omega(782) is just a proxy to get the two-body combination
    """

    from Configurables import DiElectronMaker, ProtoParticleCALOFilter

    dieLL = DiElectronMaker('MyDiElectronFromTracks')
    dieLL.Particle = "omega(782)"
    selector = trackSelector ( dieLL , trackTypes = ["Long"] )
    dieLL.addTool( ProtoParticleCALOFilter, name='Electron' )
    dieLL.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'2.0'"]
    dieLL.DiElectronMassMax = 5000.*MeV
    dieLL.DiElectronMassMin = 0.*MeV
    dieLL.DiElectronPtMin = 200.*MeV

    _sele = Selection( name+"_singleele", Algorithm = dieLL )

    _Filter = FilterDesktop( Code = "(VFASPF(VCHI2/VDOF) < 9) & (BPVVDCHI2 > 9)" )

    return Selection(name, Algorithm = _Filter, RequiredSelections = [ _sele ] )

#
