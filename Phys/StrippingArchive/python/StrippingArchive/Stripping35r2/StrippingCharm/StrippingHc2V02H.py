###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for selection of general decay topologies
    [Xi_c0 -> (V0 -> h+ h-) h+ h-]CC
In this file the stripping line for this decay is build 
    [Xi_c0 -> (Lambda0 -> p+ pi-) K+ K-]CC
    [Xi_c0 -> (Lambda0 -> p+ pi-) K+ pi-]CC
Throughout this file, 'Bachelor' refers to the children of the Xi_c0 which is
not part of the V0 decay.
"""

__author__ = ['Louis Henry']
__date__ = '13/10/2017'

__all__ = (
    'default_config',
    'StrippingHc2V02HConf'
)

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, mrad
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays
from StandardParticles import StdNoPIDsPions as InputPions
from StandardParticles import StdNoPIDsKaons as InputKaons
from StandardParticles import StdLooseLambdaLL as InputLambdasLL
from StandardParticles import StdLooseLambdaDD as InputLambdasDD

from PhysSelPython.Wrappers import Selection, MergedSelection, DataOnDemand
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME': 'Hc2V02H',
    'WGs': ['Charm'],
    'BUILDERTYPE': 'StrippingHc2V02HConf',
    'STREAMS': ['CharmCompleteEvent'],
    'CONFIG': {
        # Minimum Xic0 bachelor momentum
        'Bach_P_MIN': 2.0*GeV,
        # PID of the Lambda proton
        "ProbNNpMin_LL"   : 0.10,
        "ProbNNpMin_DD"   : 0.10,
        # Minimum L0 momentum
        'Lambda0_P_MIN': 2000*MeV,
        # Minimum L0 transverse momentum
        'Lambda0_PT_MIN': 250*MeV,
        # Minimum flight distance chi^2 of L0 from the primary vertex
        'Lambda0_FDCHI2_MIN_DD': 256,
        'Lambda0_FDCHI2_MIN_LL': 256,
        # Minimum flight distance of L0 from the primary vertex
        "LambdaMinFD_LL"  : 25.*mm,
        "LambdaMinFD_DD"  : 0. *mm,
        # Maximum L0 vertex chi^2 per vertex fit DoF
        'Lambda0_VCHI2VDOF_MAX': 12.0,
        # Xic0 mass window around the nominal Xic0 mass before the vertex fit
        'Comb_ADAMASS_WIN': 120.0*MeV,
        # Xic0 mass window around the nominal Xic0 mass after the vertex fit
        'Xic_ADMASS_WIN'    : 90.0*MeV,
        # Maximum distance of closest approach of Xic0 children
        'Comb_ADOCAMAX_MAX': 0.3*mm,
        # Maximum Xic0 vertex chi^2 per vertex fit DoF
        'Xic_VCHI2VDOF_MAX_LL'    : 3.0,
        'Xic_VCHI2VDOF_MAX_DD'    : 3.0,
        # Maximum angle between Xic0 momentum and Xic0 direction of flight
        'Xic_acosBPVDIRA_MAX_LL':  31.6*mrad,
        'Xic_acosBPVDIRA_MAX_DD':  31.6*mrad,
        # Primary vertex displacement requirement, either that the Xic0 is some
        # sigma away from the PV, or it has a minimum flight time
        'Xic_PVDispCut_LL': '(BPVVDCHI2 > 49.0)',
        'Xic_PVDispCut_DD': '(BPVVDCHI2 > 49.0)',
        # HLT filters, only process events firing triggers matching the RegEx
        'Hlt1Filter': None,
        'Hlt2Filter': None,
        # Fraction of candidates to randomly throw away before stripping
        'PrescaleXic2LambdaPiPiLL': 1.0,
        'PrescaleXic2LambdaPiPiDD': 1.0,
        'PrescaleXic2LambdaKPiLL': 1.0,
        'PrescaleXic2LambdaKPiDD': 1.0,
        # Fraction of candidates to randomly throw away after stripping
        'PostscaleXic2LambdaPiPiLL': 1.0,
        'PostscaleXic2LambdaPiPiDD': 1.0,
        'PostscaleXic2LambdaKPiLL': 1.0,
        'PostscaleXic2LambdaKPiDD': 1.0,
    }
}


class StrippingHc2V02HConf(LineBuilder):
    """Creates LineBuilder object containing the stripping lines."""
    # Allowed configuration keys
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        """Initialise this LineBuilder instance."""
        self.name = name
        self.config = config
        LineBuilder.__init__(self, name, config)
        
        # Decay descriptors
        self.Xic2LambdaPiPi  = ['[Xi_c0 -> Lambda0 pi+ pi-]cc']
        self.Xic2LambdaKPi   = ['[Xi_c0 -> Lambda0 K-  pi+]cc' ]
        
        # Line names
        # 'LL' and 'DD' will be appended to these names for the LL and DD
        # Selection and StrippingLine instances
        self.Xic2LambdaPiPi_name  = '{0}_Xic2LambdaPiPi' .format(name)
        self.Xic2LambdaKPi_name   = '{0}_Xic2LambdaKPi'.format(name)
        
        # Build bachelor pion and kaon cut strings
        # Cuts MIPCHI2DV(PRIMARY)>4 & PT>250*MeV already present in the InputsParticles
        childCuts = (
            '(P > {0[Bach_P_MIN]})'
        ).format(self.config)
        
        kineticCuts = '{0}'.format(childCuts)

        # Build Lambda0 cut strings
        lambda0Cuts = (
            '(P > {0[Lambda0_P_MIN]})'
            '& (PT > {0[Lambda0_PT_MIN]})'
            '& (VFASPF(VCHI2/VDOF) < {0[Lambda0_VCHI2VDOF_MAX]})'
        ).format(self.config)
        # Define any additional cuts on LL/DD difference
        lambda0LLCuts = lambda0Cuts
        lambda0DDCuts = lambda0Cuts
        
        # Filter Input particles
        self.Pions = Selection(
            'PionsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                 Code = kineticCuts
            ),
            RequiredSelections=[InputPions]
        )

        self.Kaons = Selection(
            'KaonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                  Code = kineticCuts
            ),
            RequiredSelections=[InputKaons]
        )
        
        # Filter Input Lambdas
        self.LambdaListLooseDD = MergedSelection("StdLooseDDLambdaFor" + self.name,
                                                 RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles")])
        
        self.LambdaListLooseLL = MergedSelection("StdLooseLLLambdaFor" + self.name,
                                                 RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")])
        
        self.LambdaListLL =  self.createSubSel(OutputList = "LambdaLLFor" + self.name,
                                               InputList = self.LambdaListLooseLL ,
                                               Cuts = "(MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMin_LL)s ) "
                                               '& (BPVVDCHI2 > %(Lambda0_FDCHI2_MIN_LL)s )'                                               
                                               %self.config
                                               )
        
        self.LambdaListDD =  self.createSubSel(OutputList = "LambdaDDFor" + self.name,
                                               InputList = self.LambdaListLooseDD ,
                                               Cuts = "(MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMin_DD)s ) "
                                               '& (BPVVDCHI2 > %(Lambda0_FDCHI2_MIN_DD)s )'
                                               %self.config
                                               )
        
        self.Lambda0LL = Selection(
            'Lambda0LLFor{0}'.format(name),
            Algorithm=FilterDesktop(
                Code=lambda0LLCuts
            ),
            RequiredSelections=[self.LambdaListLL]
        )
        self.Lambda0DD = Selection(
            'Lambda0DDFor{0}'.format(name),
            Algorithm=FilterDesktop(
                Code=lambda0DDCuts
            ),
            RequiredSelections=[self.LambdaListDD]
        )

        # Build selection for Xic -> L pi pi
        self.selXic2LambdaPiPi = self.makeHc2V02H(
            name=self.Xic2LambdaPiPi_name,
            inputSelLL=[self.Lambda0LL, self.Pions],
            inputSelDD=[self.Lambda0DD, self.Pions],
            decDescriptors=self.Xic2LambdaPiPi
        )

        # Build selection for Xic -> L K pi
        self.selXic2LambdaKPi = self.makeHc2V02H(
            name=self.Xic2LambdaKPi_name,
            inputSelLL=[self.Lambda0LL, self.Kaons, self.Pions],
            inputSelDD=[self.Lambda0DD, self.Kaons, self.Pions],
            decDescriptors=self.Xic2LambdaKPi
        )

        # Make line for Xic -> L pi pi
        self.line_Xic2LambdaPiPiLL = self.make_line(
            name='{0}LLLine'.format(self.Xic2LambdaPiPi_name),
            selection=self.selXic2LambdaPiPi[0],
            prescale=config['PrescaleXic2LambdaPiPiLL'],
            postscale=config['PostscaleXic2LambdaPiPiLL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )
        self.line_Xic2LambdaPiPiDD = self.make_line(
            name='{0}DDLine'.format(self.Xic2LambdaPiPi_name),
            selection=self.selXic2LambdaPiPi[1],
            prescale=config['PrescaleXic2LambdaPiPiDD'],
            postscale=config['PostscaleXic2LambdaPiPiDD'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        # Make line for Xic -> L K pi
        self.line_Xic2LambdaKPiLL = self.make_line(
            name='{0}LLLine'.format(self.Xic2LambdaKPi_name),
            selection=self.selXic2LambdaKPi[0],
            prescale=config['PrescaleXic2LambdaKPiLL'],
            postscale=config['PostscaleXic2LambdaKPiLL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )
        self.line_Xic2LambdaKPiDD = self.make_line(
            name='{0}DDLine'.format(self.Xic2LambdaKPi_name),
            selection=self.selXic2LambdaKPi[1],
            prescale=config['PrescaleXic2LambdaKPiDD'],
            postscale=config['PostscaleXic2LambdaKPiDD'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

    def make_line(self, name, selection, prescale, postscale, **kwargs):
        """Create the stripping line defined by the selection.

        Keyword arguments:
        name -- Base name for the Line
        selection -- Selection instance
        prescale -- Fraction of candidates to randomly drop before stripping
        postscale -- Fraction of candidates to randomly drop after stripping
        **kwargs -- Keyword arguments passed to StrippingLine constructor
        """
        # Only create the line with positive pre- and postscales
        # You can disable each line by setting either to a negative value
        if prescale > 0 and postscale > 0:
            line = StrippingLine(
                name,
                selection=selection,
                prescale=prescale,
                postscale=postscale,
                **kwargs
            )
            self.registerLine(line)
            return line
        else:
            return False

    def makeHc2V02H(self, name, inputSelLL, inputSelDD, decDescriptors):
        """Return two Selection instances for a Xi_c0 -> V0 h+ h+ h- decay.

        The return value is a two-tuple of Selection instances as
            (LL Selection, DD Selection)
        where LL and DD are the method of reconstruction for the V0.
        Keyword arguments:
        name -- Name to give the Selection instance
        inputSelLL -- List of inputs passed to Selection.RequiredSelections
                      for the LL Selection
        inputSelDD -- List of inputs passed to Selection.RequiredSelections
                      for the DD Selection
        decDescriptors -- List of decay descriptors for CombineParticles
        """
        lclPreambulo = [
            'from math import cos'
        ]

        combCuts = (
            "(ADAMASS('Xi_c0') < {0[Comb_ADAMASS_WIN]})"
            "& (ADOCA(1,2) < {0[Comb_ADOCAMAX_MAX]})"
            "& (ADOCA(1,3) < {0[Comb_ADOCAMAX_MAX]})"
        ).format(self.config)

        xicCuts_LL = (
            "(VFASPF(VCHI2/VDOF) < {0[Xic_VCHI2VDOF_MAX_LL]})"
            "& ({0[Xic_PVDispCut_LL]})"
            "& (BPVDIRA > cos({0[Xic_acosBPVDIRA_MAX_LL]}))"
            "& (ADMASS('Xi_c0') < {0[Xic_ADMASS_WIN]})"
            "& ((CHILD(VFASPF(VZ),1) - VFASPF(VZ)) > {0[LambdaMinFD_LL]})"
        ).format(self.config)

        xicCuts_DD = (
            "(VFASPF(VCHI2/VDOF) < {0[Xic_VCHI2VDOF_MAX_DD]})"
            "& ({0[Xic_PVDispCut_DD]})"
            "& (BPVDIRA > cos({0[Xic_acosBPVDIRA_MAX_DD]}))"
            "& (ADMASS('Xi_c0') < {0[Xic_ADMASS_WIN]})"
            "& ((CHILD(VFASPF(VZ),1) - VFASPF(VZ)) > {0[LambdaMinFD_DD]})"
        ).format(self.config)

        comb12Cuts = (
            "(DAMASS('Xi_c0') < {0[Comb_ADAMASS_WIN]})"
            "& (ADOCA(1,2) < {0[Comb_ADOCAMAX_MAX]})"
            ).format(self.config)

        _Xic_LL = DaVinci__N3BodyDecays(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=xicCuts_LL,
            Combination12Cut=comb12Cuts,
            )

        _Xic_DD = DaVinci__N3BodyDecays(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=xicCuts_DD,
            Combination12Cut=comb12Cuts,
            )

        selLL = Selection(
            '{0}LL'.format(name),
            Algorithm=_Xic_LL,
            RequiredSelections=inputSelLL
        )
        selDD = Selection(
            '{0}DD'.format(name),
            Algorithm=_Xic_DD,
            RequiredSelections=inputSelDD
        )
        return selLL, selDD

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] ) 
