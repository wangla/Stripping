###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = ['Phillip Urquijo, Alessandra Borgia', 'Michel De Cian']
__date__ = '11/01/2019'
__version__ = '$Revision: 1.6 $'

'''
B->Xu e nu exclusive reconstruction in Xu=pi/rho/omega/phi/K/K*+ channels
'''
# =============================================================================
##
#  B->Xu mu nu exclusive reconstruction in Xu=pi/rho/omega/phi/K/K*+ channels
#
#  Essentially a copy of the B2XuMuNu module with electrons instead of muons
#
#  Stripping lines for charmless semileptonic B+/B0/Bs decays
#  to final states with hadron+electron, where the hadron is
#  either a charged track or a resonance
#  The charmless hadronic signal modes are:
#
#  B0 -> pi+ e- nu
#  B+ -> rho e+ nu
#  B+ -> omega e+ nu
#  Bs0 -> K+  e- nu, 
#  B+ -> phi e+ nu
#  Bs0 -> K*+ e- nu, (with K*+->KS0 Pi)
#
############
#  Also added are 2 new lines for Majorana Neutrino decays.
#  B- -> KS0 e- nu, with KS0 ->pi+ e- (SS = Same Sign)
#  B- -> KS0 e- nu, with KS0 ->pi- e+ (OS = Opposite Sign)
############
#
#  Stripping XX, with requirements that the
#  rate <0.05% and timing <1ms/evt.
## 

"""
# =============================================================================
##
#  B->Xu mu nu exclusive reconstruction in Xu=pi/rho/omega/phi/K/K*+ channels
#
#  Essentially a copy of the B2XuMuNu module with electrons instead of muons
#
#  Stripping lines for charmless semileptonic B+/B0/Bs decays
#  to final states with hadron+electron, where the hadron is
#  either a charged track or a resonance
#  The charmless hadronic signal modes are:
#
#  B0 -> pi+ e- nu
#  B+ -> rho e+ nu
#  B+ -> omega e+ nu
#  Bs0 -> K+  e- nu, 
#  B+ -> phi e+ nu
#  Bs0 -> K*+ e- nu, (with K*+->KS0 Pi)
#
############
#  Also added are 2 new lines for Majorana Neutrino decays.
#  B- -> KS0 e- nu, with KS0 ->pi+ e- (SS = Same Sign)
#  B- -> KS0 e- nu, with KS0 ->pi- e+ (OS = Opposite Sign)
############
#
#  Stripping XX, with requirements that the
#  rate <0.05% and timing <1ms/evt.
## 
Last modification $Date: 2018-January-10 $
               by $Author: decianm $
"""


default_config = {
  'NAME'        : 'B2XuENu',
  'WGs'         : ['Semileptonic'],
  'BUILDERTYPE' : 'B2XuENuBuilder',
  'CONFIG'      :  {
      "GEC_nLongTrk"        : 250.  , #adimensional
      "TRGHOSTPROB"         : 0.5    ,#adimensional
      #Electrons
      "ElectronGHOSTPROB"       : 0.35  ,#adimensional
      "ElectronTRCHI2"          : 4.    ,#adimensional
      "ElectronP"               : 3000. ,#MeV
      "ElectronPTight"          : 6000. ,#MeV
      "ElectronPT"              : 1000. ,#MeV
      "ElectronPTTight"         : 1500. ,#MeV
      "ElectronPIDK"            : 0.    ,#adimensional
      "ElectronPIDe"           : 3.    ,#adimensional
      "ElectronPIDp"            : 0.    ,#adimensional
      "ElectronMINIPCHI2"       : 25    ,#adminensional
      #Xu
      #K channel
      "KaonTRCHI2"          : 4.     ,#adimensional
      "KaonP"               : 3000.  ,#MeV
      "KaonPTight"          : 10000. ,#MeV
      "KaonPT"              : 400.   ,#MeV
      "KaonPTTight"         : 1000.   ,#MeV
      "KaonPIDK"            : 5.     ,#adimensional 
      "KaonPIDmu"           : 5.     ,#adimensional
      "KaonPIDp"            : 5.     ,#adimensional
      "KaonPIDK_phi"        : 2.     ,#adimensional 
      "KaonPIDmu_phi"       : -2.    ,#adimensional
      "KaonPIDp_phi"        : -2.    ,#adimensional
      "KaonMINIPCHI2"       : 36     ,#adminensional
      #Pion channel
      "PionTRCHI2"          : 4.     ,#adimensional
      "PionP"               : 3000.  ,#MeV
      "PionPTight"          : 10000. ,#MeV
      "PionPT"              : 300.   ,#MeV
      "PionPTTight"         : 800.   ,#MeV
      "PionPIDK"            : -2.    ,#adimensional 
      "PionMINIPCHI2"       : 36      ,#adminensional
      
      #phi channel
      "PhiUpperMass"        : 2200. ,#MeV
      "PhiVCHI2DOF"         : 6     ,#adimensional
      "PhiPT"               : 600.  ,#MeV
      "PhiMINIPCHI2"        : 9     ,#adimensional
      "PhiDIRA"             : 0.9   ,#adimensional

      #Rho channel
      "RhoMassWindow"       : 1500. ,#MeV
      "RhoMassWindowMin1SB" : 300.  ,#MeV
      "RhoMassWindowMax1SB" : 620.  ,#MeV
      "RhoMassWindowMin2SB" : 920.  ,#MeV
      "RhoMassWindowMax2SB" : 1200. ,#MeV
      "RhoVCHI2DOF"         : 4     ,#adimensional
      "RhoPT"               : 1000.  ,#MeV
      "RhoLeadingPionPT"    : 900.  ,#MeV
      "RhoLeadingPionP"     : 5000.  ,#MeV
      "RhoMINIPCHI2"        : 50    ,#adimensional
      "RhoChPionPT"         : 400.  ,#MeV
      "RhoChPionTRCHI2"     : 4.   ,#MeV
      "RhoChPionPIDK"       : -2.  ,#adminensional
                   #    "RhoFDCHI2"           : 100.  ,#adimensional
                   #    "RhoFD"               : 6     ,#mm
                   #    "RhoIPMin"            : 0.3   ,#mm    
      "RhoDIRA"             : 0.98   ,#adimensional
      "RhoChPionMINIPCHI2"  : 36.    ,#adimensional
      # Omega cuts
      "OmegaChPionPT"       : 300, #MeV
      "OmegaPi0PT"          : 500, #MeV
      "OmegaChPionMINIPCHI2": 25, #adimensional
      "OmegaChPionPIDK"     : 0, #adimensional
      "OmegaMassWindow"     : 150, #MeV
      "OmegaVCHI2DOF"       : 6, #MeV
      #Kshort Daughter Cuts
      "KS0DaugP"            : 2000. ,#MeV
      "KS0DaugPT"           : 100.  ,#MeV
      "KS0DaugTrackChi2"    : 4.    ,#adimensional
      "KS0DaugMIPChi2"      : 50.   ,#adimensional
      #Kshort cuts
      "KSMajoranaCutFDChi2" : 100.  ,#adimensional
      "KS0VertexChi2"       : 4.    ,#adimensional
      "KS0PT"               : 250.  ,#adimensional
      "KsLLMaxDz"           : 650.  ,#mm
      "KsLLCutFD"           : 20.   ,#mm
      "KSLLMassLow"         : 456.  ,#MeV
      "KSLLMassHigh"        : 536.  ,#MeV
      "KSLLCutFDChi2"       : 0.    ,#adimensional
      "KS0MIPChi2"          : 0.    ,#adimensional
      'KS0Z'                : 0.    ,#mm
      #Kstar Cuts
      "KstarMassWindow"     : 1200  ,#MeV
      "KstarPT"             : 800   ,#MeV
      #Kstar Daughter cuts
      "KstarChPionPT"       : 250.  ,#MeV
      "KstarChPionP"        : 3000. ,#MeV
      "KstarChPionTRCHI2"   : 4.    ,#adimensional
      "KstarChPionMINIPCHI2": 25.   ,#adimensional
      "KstarChPionPIDK"     : 2.    ,#adminensional
      #B Mother Cuts
      "BVCHI2DOF"           : 6.    ,#adminensional, for 3- or more-track vertices
      "BVCHI2DOFTight"      : 4.    ,#adminensional, ideally for 2-track vertices
      "BDIRA"               : 0.99  ,#adminensional
      "BDIRAMed"            : 0.994 ,#adminensional
      "BDIRATight"          : 0.999 ,#adminensional
      "BFDCHI2HIGH"         : 100.  ,#adimensional
      "BFDCHI2Tight"        : 120.  ,#adimensional
      "BFDCHI2ForPhi"       : 50.   ,#adimensional
      #B Mass Minima
      "KEMassLowTight"     : 1500. ,#MeV
      "PhiE_MCORR"         : 2500. ,#MeV
      "RhoEMassLowTight"   : 2000. ,#MeV
      "KS0EMassLowTight"   : 3000. ,#MeV
      "KstarEMassLowTight" : 2500. ,#MeV
      #B corrected mass limits
      "BCorrMLow"           : 2500. ,#MeV
      "BCorrMHigh"          : 7000. ,#MeV
      #
      "XEMassUpper"        : 5500. ,#MeV
      "XEMassUpperHigh"    : 6500. ,#MeV
      "Enu"                 : 1850. ,#MeV
      "EnuK"                : 2000. ,#MeV      
      # Prescales
      'NoPrescales'         : False,
      'PiENu_prescale'     : 0.2
      },
  'STREAMS'     : ['Semileptonic']    
}

from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder

import logging
def makeTOSFilter(name,specs, useCalo):
    from Configurables import TisTosParticleTagger
    tosFilter = TisTosParticleTagger(name+'TOSFilter')
    tosFilter.TisTosSpecs = specs
    tosFilter.ProjectTracksToCalo = useCalo
    tosFilter.CaloClustForCharged = useCalo
    tosFilter.CaloClustForNeutral = False
    tosFilter.TOSFrac = {4:0.0, 5:0.0}
    #tosFilter.PassOnAll = True
    return tosFilter

def tosSelection(sel,specs,useCalo=False):
    from PhysSelPython.Wrappers import Selection 
    '''Filters Selection sel to be TOS.'''
    tosFilter = makeTOSFilter(sel.name(),specs, useCalo)
    return Selection(sel.name()+'TOS', Algorithm=tosFilter,
                     RequiredSelections=[sel])

default_name="B2XuENu"

class B2XuENuBuilder(LineBuilder):
    """
    Definition of B->Xu e nu stripping module
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self,name,config):
        LineBuilder.__init__(self, name, config)
        self.config=config
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        self._stdVeryLooseKsLL = DataOnDemand("Phys/StdVeryLooseKsLL/Particles")
        self._stdLooseKsLD = DataOnDemand("Phys/StdLooseKsLD/Particles")
        self._stdLooseKsDD = DataOnDemand("Phys/StdLooseKsDD/Particles")

        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}

        ## Electrons
        self._electronSel=None
        self._electronFilter()
        
        self._electronSelNoPID=None
        self._electronFilterNoPID()

        self._electronSelTOS=None
        self._electronFilterTOS()

        self._electronSelTOSNoPID=None
        self._electronFilterTOSNoPID()
        
        self._electronSelTight=None
        self._electronFilterTight()

        self._electronSelTightNoPID=None
        self._electronFilterTightNoPID()

        self._MajoranaLineElectronSel=None
        self._MajoranaLineElectronFilter()

        ## Pions
        self._pionSel=None
        self._pionFilter()

        self._pionSelNoPID=None
        self._pionFilterNoPID()
        
        self._pionSelTight=None
        self._pionFilterTight()

        self._pionSelTightNoPID=None
        self._pionFilterTightNoPID()

        ## Kaons
        self._kaonSel=None
        self._kaonFilter()
        
        self._kaonForPhiSel=None
        self._kaonForPhiFilter()

        self._kaonSelNoPID=None
        self._kaonFilterNoPID()

        self._kaonForPhiSelNoPID=None
        self._kaonForPhiFilterNoPID()

        ## Rhos
        self._rho770Sel=None
        self._Rho02PiPiFilter()

        self._rho770WSSel=None
        self._Rho02PiPiWSFilter()

        self._rho770SBSel=None
        self._Rho02PiPiSBFilter()

        self._rho770SelNoPID=None
        self._Rho02PiPiFilterNoPID()

        ## Omega
        self._omega782Sel=None
        self._Omega2PiPiPi0Filter()
        
        self._omega782WSSel=None
        self._Omega2PiPiPi0WSFilter()
        
        self._omega782SelNoPID=None
        self._Omega2PiPiPi0FilterNoPID()

        ## Phi
        self._phi1020Sel=None
        self._Phi2KKFilter()
        
        self._phi1020SelWS=None
        self._Phi2KKFilterWS()

        self._phi1020SelNoPID=None
        self._Phi2KKFilterNoPID()
        
        ## KS for Majorana
        self._KshMajoranaSSESel=None
        self._KshMajoranaSSEFilter()

        self._KshMajoranaOSESel=None
        self._KshMajoranaOSEFilter()

        ## KS for other stuff
        self._KSSel=None
        self._KsFilter()

        self._KstarSel=None
        self._Kstar2KPiFilter()

        self._KstarSelNoPID=None
        self._Kstar2KPiFilterNoPID()

        ###########################################
        ##
        ## Register the stripping lines
        ##
        ###########################################
        # pi lines
        self.registerLine(self._Pi_line())
        self.registerLine(self._Pi_NoETopo_line())
        self.registerLine(self._Pi_NoPIDe_line())
        self.registerLine(self._Pi_NoPIDhad_line())
        self.registerLine(self._PiSS_line())
        # K lines
        self.registerLine(self._K_line())
        self.registerLine(self._K_NoETopo_line())
        self.registerLine(self._KSS_line())
        self.registerLine(self._K_NoPIDe_line())
        self.registerLine(self._KSS_NoPIDe_line())
        self.registerLine(self._K_NoPIDK_line())
        self.registerLine(self._KSS_NoPIDK_line())
        self.registerLine(self._K_NoPIDKe_line())
        self.registerLine(self._KSS_NoPIDKe_line())
        # phi lines
        self.registerLine(self._Phi_line())
        self.registerLine(self._Phi_NoETopo_line())
        self.registerLine(self._PhiWS_line())
        self.registerLine(self._Phi_NoPIDe_line())
        self.registerLine(self._Phi_NoPIDhad_line())
        self.registerLine(self._PhiBadVtx_line())
        # rho lines
        self.registerLine(self._Rho_line())
        self.registerLine(self._Rho_NoETopo_line())
        self.registerLine(self._Rho_NoPIDe_line())
        self.registerLine(self._Rho_NoPIDhad_line())
        self.registerLine(self._RhoWS_line())
        self.registerLine(self._RhoBadVtx_line())
        #self.registerLine(self._RhoSB_line()) With a large rho mass window, this is not needed anymore
        # omega lines
        self.registerLine(self._Omega_line())
        self.registerLine(self._Omega_NoETopo_line())
        self.registerLine(self._Omega_NoPIDe_line())
        self.registerLine(self._Omega_NoPIDhad_line())
        self.registerLine(self._OmegaWS_line())
        self.registerLine(self._OmegaBadVtx_line())
        # majorana lines
        self.registerLine(self._KshMajoranaSSE_line())
        self.registerLine(self._KshMajoranaOSE_line())
        # Kstar lines
        self.registerLine(self._Kstar_line())
        self.registerLine(self._Kstar_NoETopo_line())
        self.registerLine(self._Kstar_NoPIDe_line())
        self.registerLine(self._Kstar_NoPIDhad_line())
        self.registerLine(self._KstarSS_line())
        self.registerLine(self._KstarBadVtx_line())

    ### Electrons
    def _NominalElectronSelection( self ):
        return "(TRCHI2DOF < %(ElectronTRCHI2)s ) &  (P> %(ElectronP)s *MeV) &  (PT> %(ElectronPT)s* MeV)"\
               "& (TRGHOSTPROB < %(ElectronGHOSTPROB)s)"\
               "& (PIDe > %(ElectronPIDe)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(ElectronMINIPCHI2)s )"

    def _NominalElectronSelectionNoPID( self ):
        return "(TRCHI2DOF < %(ElectronTRCHI2)s ) &  (P> %(ElectronP)s *MeV) &  (PT> %(ElectronPT)s* MeV)"\
               "& (TRGHOSTPROB < %(ElectronGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(ElectronMINIPCHI2)s )"

    def _NominalElectronSelectionTight( self ):
        return "(TRCHI2DOF < %(ElectronTRCHI2)s ) &  (P> %(ElectronPTight)s *MeV) &  (PT> %(ElectronPTTight)s* MeV)"\
               "& (TRGHOSTPROB < %(ElectronGHOSTPROB)s)"\
               "& (PIDe > %(ElectronPIDe)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(ElectronMINIPCHI2)s )"
   
    def _NominalElectronSelectionTightNoPID( self ):
        return "(TRCHI2DOF < %(ElectronTRCHI2)s ) &  (P> %(ElectronPTight)s *MeV) &  (PT> %(ElectronPTTight)s* MeV)"\
               "& (TRGHOSTPROB < %(ElectronGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(ElectronMINIPCHI2)s )"


    def _MajoranaLineElectronSelection( self ):
        return "(P > %(KS0DaugP)s) & (PT > %(KS0DaugPT)s)"\
               "& (TRGHOSTPROB < %(ElectronGHOSTPROB)s)"\
               "&(TRCHI2DOF < %(KS0DaugTrackChi2)s ) & (PIDe-PIDpi> %(ElectronPIDe)s )& (PIDe-PIDp> %(ElectronPIDp)s )"\
               "&(PIDe-PIDK> %(ElectronPIDK)s )&(MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s)"\

    ### Kaons
    def _NominalKSelection( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonPTight)s *MeV) &  (PT> %(KaonPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK > %(KaonPIDK)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _NominalKSelectionNoPID( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonPTight)s *MeV) &  (PT> %(KaonPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _KforPhiSelection( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK> %(KaonPIDK_phi)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _KforPhiSelectionNoPID( self ):
        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    ### Pions
    def _NominalPiSelection( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionP)s *MeV) &  (PT> %(PionPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s )"\
               "& (PIDmu < 2 ) "\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"

    def _NominalPiSelectionNoPID( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionP)s *MeV) &  (PT> %(PionPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"

    def _NominalPiSelectionTight( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionPTight)s *MeV) &  (PT> %(PionPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"

    def _NominalPiSelectionTightNoPID( self ):
        return "(TRCHI2DOF < %(PionTRCHI2)s )&  (P> %(PionPTight)s *MeV) &  (PT> %(PionPTTight)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(PionMINIPCHI2)s )"
    
    ### K shorts
    def _NominalKsSelection( self ):
        return " (BPVVD >%(KsLLCutFD)s*mm)& (MM>%(KSLLMassLow)s*MeV)&(MM<%(KSLLMassHigh)s*MeV)  & (BPVVDCHI2> %(KSLLCutFDChi2)s ) & (PT > %(KS0PT)s*MeV) & (VFASPF(VCHI2PDOF) < %(KS0VertexChi2)s) & CHILDCUT((TRCHI2DOF < %(KS0DaugTrackChi2)s),1) & CHILDCUT((TRCHI2DOF < %(KS0DaugTrackChi2)s),2) & CHILDCUT((MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s),1) & CHILDCUT((MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s),2) & (MIPCHI2DV(PRIMARY) > %(KS0MIPChi2)s)"

    #####################################

    ##### B -> pi e nu lines
    def _Pi_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_Line', prescale = 1.0 if NoPS else self.config["PiENu_prescale"],
                             FILTER=self.GECs,
                             algos = [ self._B2PiENu()])
    def _Pi_NoETopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_NoETopoLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._B2PiENuNoETopo()])

    def _PiSS_line( self ):
        NoPS =  self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_SSLine', prescale =  1.0 if NoPS else self.config["PiENu_prescale"],
                             FILTER=self.GECs,
                             algos = [ self._B2PiENuSS()])
    
    def _Pi_NoPIDe_line( self ):
        NoPS =  self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_NoPIDELine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._B2PiENuNoPIDe()])
    
    def _Pi_NoPIDhad_line( self ):
        NoPS =  self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Pi_NoPIDPiLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._B2PiENuNoPIDhad()])

    ###### Bs -> K e nu lines
    def _K_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENu()])

    def _K_NoETopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoETopoLine', prescale = 1.0  if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENuNoETopo()])

    def _KSS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENuSS()])
   
    def _K_NoPIDe_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoPIDELine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENu_NoPIDe()])
    
    def _KSS_NoPIDe_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSNoPIDELine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENuSS_NoPIDe()])
    
    def _K_NoPIDK_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoPIDKLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENu_NoPIDK()])
    
    def _KSS_NoPIDK_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSNoPIDKLine', prescale = 1.0 if NoPS else .02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENuSS_NoPIDK()])
    
    def _K_NoPIDKe_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_NoPIDKELine', prescale = 1.0 if NoPS else .01,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENu_NoPIDKe()])
    
    def _KSS_NoPIDKe_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2K_SSNoPIDKELine', prescale = 1.0 if NoPS else .01,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KENuSS_NoPIDKe()])

    ##### Bs -> phi/KK e nu lines
    def _Phi_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiENu()])

    def _Phi_NoETopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_NoETopoLine', prescale =  1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiENuNoETopo()])
    
    def _PhiWS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_WSLine', prescale = 1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiENuWS()])
    
    def _Phi_NoPIDe_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_NoPIDELine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiENu_NoPIDe()])

    def _Phi_NoPIDhad_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_NoPIDKLine', prescale = 1.0 if NoPS else 0.01,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiENu_NoPIDhad()])
    
    def _PhiBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'B2Phi_BadVtxLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._B2PhiENuBadVtx()])


    ##### B -> rho/pipi e nu lines
    def _Rho_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoENu()])

    def _Rho_NoETopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_NoETopoLine', prescale =  1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoENuNoETopo()])
    
    def _Rho_NoPIDe_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_NoPIDELine', prescale = 1.0 if NoPS else 0.05,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoENu_NoPIDe()])

    def _Rho_NoPIDhad_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_NoPIDPiLine', prescale = 1.0 if NoPS else 0.05,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoENu_NoPIDhad()])
    
    def _RhoWS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_WSLine', prescale = 1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoENuWS()])
    
    def _RhoBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_BadVtxLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoENuBadVtx()])
    
    def _RhoSB_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Rho_SBLine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2RhoENuSB()])
    
    ##### B -> omega e nu lines
    def _Omega_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaENu()])

    def _Omega_NoETopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_NoETopoLine', prescale =  1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaENuNoETopo()])
    
    def _Omega_NoPIDe_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_NoPIDELine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaENu_NoPIDe()])
    
    def _Omega_NoPIDhad_line(self):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_NoPIDPiLine', prescale = 1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaENu_NoPIDhad()])

    def _OmegaWS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_WSLine', prescale = 1.0 if NoPS else 0.3,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaENuWS()])
    
    def _OmegaBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2Omega_BadVtxLine', prescale = 1.0 if NoPS else 0.2,
                             FILTER=self.GECs,
                             algos = [ self._Bu2OmegaENuBadVtx()])

    ##### Ks majorana lines
    def _KshMajoranaSSE_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2KshSSE_SSEminusLine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2KshSSENu_SSEminus()])
    
    def _KshMajoranaOSE_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bu2KshOSE_SSEplusLine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bu2KshOSENu_SSEplus()])

    ##### Bs -> K*/Kspi e nu lines
    def _Kstar_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarENu()])
    
    def _Kstar_NoETopo_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_NoETopoLine', prescale =  1.0 if NoPS else 0.1,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarENuNoETopo()])

    def _Kstar_NoPIDe_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_NoPIDELine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarENu_NoPIDe()])

    def _Kstar_NoPIDhad_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_NoPIDPiLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarENu_NoPIDhad()])
    
    def _KstarSS_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_SSLine', prescale = 1.0 if NoPS else 0.3,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarENuSS()])

    def _KstarBadVtx_line( self ):
        NoPS = self.config['NoPrescales']
        from StrippingConf.StrippingLine import StrippingLine
        return StrippingLine(self._name+'Bs2Kstar_BadVtxLine', prescale = 1.0 if NoPS else 0.02,
                             FILTER=self.GECs,
                             algos = [ self._Bs2KstarENuBadVtx()])

    ###############################
    ##
    ## Filters for particles
    ##
    ##############################

    ### Electrons
    def _electronFilter( self ):
        if self._electronSel is not None:
            return self._electronSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseElectrons
        _e = FilterDesktop( Code = self._NominalElectronSelection() % self._config )
        _eSel=Selection("E_for"+self._name,
                         Algorithm=_e,
                         RequiredSelections = [StdLooseElectrons])
        
        self._electronSel=_eSel
        
        return _eSel

    ######--######
    def _electronFilterNoPID( self ):
        if self._electronSelNoPID is not None:
            return self._electronSelNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsElectrons
        _eNoPID = FilterDesktop( Code = self._NominalElectronSelectionNoPID() % self._config )
        _eSelNoPID=Selection("ENoPID_for"+self._name,
                         Algorithm=_eNoPID,
                         RequiredSelections = [StdNoPIDsElectrons])
        
        self._electronSelNoPID=_eSelNoPID
        
        return _eSelNoPID

    
    ######--######
    def _electronFilterTight( self ):
        if self._electronSelTight is not None:
            return self._electronSelTight
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseElectrons
        _eTight = FilterDesktop( Code = self._NominalElectronSelectionTight() % self._config )
        _eTightSel=Selection("ETightCuts_for"+self._name,
                         Algorithm=_eTight,
                         RequiredSelections = [StdLooseElectrons])
        
        self._electronSelTight=_eTightSel
        
        return _eTightSel

    ######--######
    def _electronFilterTightNoPID( self ):
        if self._electronSelTightNoPID is not None:
            return self._electronSelTightNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsElectrons
        _eTightNoPID = FilterDesktop( Code = self._NominalElectronSelectionTightNoPID() % self._config )
        _eTightSelNoPID=Selection("ETightNoPIDCuts_for"+self._name,
                         Algorithm=_eTightNoPID,
                         RequiredSelections = [StdNoPIDsElectrons])
        
        self._electronSelTightNoPID=_eTightSelNoPID
        
        return _eTightSelNoPID

    ######--######
    def _electronFilterTOS( self ):
        if self._electronSelTOS is not None:
            return self._electronSelTOS
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseElectrons
        _eTOS = FilterDesktop( Code = self._NominalElectronSelectionTight() % self._config )
        _eSelTOS=Selection("EL0TOS_for"+self._name,
                            Algorithm=_eTOS,
                            RequiredSelections = [StdLooseElectrons])
        eSelTOS = tosSelection(_eSelTOS,{'L0.*Electron.*Decision%TOS':0}, True)

        self._electronSelTOS=_eSelTOS
        
        return _eSelTOS

    ######--######
    def _electronFilterTOSNoPID( self ):
        if self._electronSelTOSNoPID is not None:
            return self._electronSelTOSNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsElectrons
        _eTOSNoPID = FilterDesktop( Code = self._NominalElectronSelectionTightNoPID() % self._config )
        _eSelTOSNoPID = Selection("EL0TOSNoPID_for"+self._name,
                                   Algorithm=_eTOSNoPID,
                                   RequiredSelections = [StdNoPIDsElectrons])
        _eSelTOSNoPID = tosSelection(_eSelTOSNoPID,{'L0.*Electron.*Decision%TOS':0}, True)

        self._electronSelTOSNoPID=_eSelTOSNoPID
        
        return _eSelTOSNoPID

    ## Pions
    ######--######
    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _pi = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _piSel=Selection("Pi_for"+self._name,
                         Algorithm=_pi,
                         RequiredSelections = [StdLoosePions])
        self._pionSel=_piSel
        return _piSel

    ######--######
    def _pionFilterNoPID( self ):
        if self._pionSelNoPID is not None:
            return self._pionSelNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions
        
        _piNoPID = FilterDesktop( Code = self._NominalPiSelectionNoPID() % self._config )
        _piSelNoPID=Selection("PiNoPID_for"+self._name,
                         Algorithm=_piNoPID,
                         RequiredSelections = [StdNoPIDsPions])
        self._pionSelNoPID = _piSelNoPID
        return _piSelNoPID

    ######--######
    def _pionFilterTight( self ):
        if self._pionSelTight is not None:
            return self._pionSelTight
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _piTight = FilterDesktop( Code = self._NominalPiSelectionTight() % self._config )
        _piSelTight=Selection("PiTight_for"+self._name,
                         Algorithm=_piTight,
                         RequiredSelections = [StdLoosePions])
        self._pionSelTight=_piSelTight
        return _piSelTight
    
    ######--######
    def _pionFilterTightNoPID( self ):
        if self._pionSelTightNoPID is not None:
            return self._pionSelTightNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions
        
        _piNoPIDTight = FilterDesktop( Code = self._NominalPiSelectionTightNoPID() % self._config )
        _piSelNoPIDTight=Selection("PiTightNoPID_for"+self._name,
                         Algorithm=_piNoPIDTight,
                         RequiredSelections = [StdNoPIDsPions])
        self._pionSelTightNoPID=_piSelNoPIDTight
        return _piSelNoPIDTight

    ## Kaons
    
    ######Kaon Filter######
    def _kaonFilter( self ):
        if self._kaonSel is not None:
            return self._kaonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons
        
        _ka = FilterDesktop( Code = self._NominalKSelection() % self._config )
        _kaSel=Selection("K_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLooseKaons])
        self._kaonSel=_kaSel
        return _kaSel

    ######--######
    def _kaonFilterNoPID( self ):
        if self._kaonSelNoPID is not None:
            return self._kaonSelNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons
        
        _ka = FilterDesktop( Code = self._NominalKSelectionNoPID() % self._config )
        _kaSelNoPID=Selection("KNoPID_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLooseKaons])
        self._kaonSelNoPID=_kaSelNoPID
        return _kaSelNoPID

    ######--######
    def _kaonForPhiFilter( self ):
        if self._kaonForPhiSel is not None:
            return self._kaonForPhiSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons
        
        _ka = FilterDesktop( Code = self._KforPhiSelection() % self._config )
        _kaSel=Selection("K_Phi"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLooseKaons])
        self._kaonForPhiSel=_kaSel
        return _kaSel

    ######--######
    def _kaonForPhiFilterNoPID( self ):
        if self._kaonForPhiSelNoPID is not None:
            return self._kaonForPhiSelNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsKaons
        
        _kaNoPID = FilterDesktop( Code = self._KforPhiSelectionNoPID() % self._config )
        _kaSelNoPID=Selection("KNoPID_Phi"+self._name,
                         Algorithm=_kaNoPID,
                         RequiredSelections = [StdNoPIDsKaons])
        self._kaonForPhiSelNoPID = _kaSelNoPID
        return _kaSelNoPID

    ######Majorana Electron Filter######
    def _MajoranaLineElectronFilter( self ):
        if self._MajoranaLineElectronSel is not None:
            return self._MajoranaLineElectronSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseElectrons
        _emajorana = FilterDesktop( Code = self._MajoranaLineElectronSelection() % self._config )
        _emajoranaSel=Selection("EMajorana_for"+self._name,
                                 Algorithm=_emajorana,
                                 RequiredSelections = [StdLooseElectrons])
        
        self._MajoranaLineElectronSel=_emajoranaSel
        
        return _emajoranaSel
    
    #####
    ## Composites
    #####
    
    ######--######
    def _KsFilter( self ):
        if self._KSSel is not None:
           return self._KSSel
       
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        
        _code =  FilterDesktop( Code = self._NominalKsSelection() % self._config )
      
        _KsSel=Selection("KS02PiPi_for"+self._name,
                         Algorithm= _code,
                         RequiredSelections = [self._stdVeryLooseKsLL,self._stdLooseKsLD,self._stdLooseKsDD])
      
        self._KSSel = _KsSel
      
        return _KsSel
    
    ##### phi -> KK filter (generall di-kaon filter) #####
    def _Phi2KKFilter( self ):
        if self._phi1020Sel is not None:
            return self._phi1020Sel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _phi1020 = CombineParticles(
            DecayDescriptors = ["phi(1020) -> K- K+"] ,
            CombinationCut = "(AM< %(PhiUpperMass)s)" % self._config,
            MotherCut       = "(VFASPF(VCHI2/VDOF) < %(PhiVCHI2DOF)s ) & (PT > %(PhiPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(PhiMINIPCHI2)s ) & (BPVDIRA> %(PhiDIRA)s)" %self._config
            )
        _phi1020Sel=Selection("PhiKK_for"+self._name,
                             Algorithm=_phi1020,
                             RequiredSelections = [self._kaonForPhiFilter()])
        
        self._phi1020Sel=_phi1020Sel
        
        return _phi1020Sel

    ######--######
    def _Phi2KKFilterNoPID( self ):
        if self._phi1020SelNoPID is not None:
            return self._phi1020SelNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _phi1020NoPID = CombineParticles(
            DecayDescriptors = ["phi(1020) -> K- K+"] ,
            CombinationCut = "(AM< %(PhiUpperMass)s)" % self._config,
            MotherCut       = "(VFASPF(VCHI2/VDOF) < %(PhiVCHI2DOF)s ) & (PT > %(PhiPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(PhiMINIPCHI2)s ) & (BPVDIRA> %(PhiDIRA)s)" %self._config
            )
        _phi1020SelNoPID=Selection("PhiKKNoPID_for"+self._name,
                             Algorithm=_phi1020NoPID,
                             RequiredSelections = [self._kaonForPhiFilterNoPID()])
        
        self._phi1020SelNoPID=_phi1020SelNoPID
        
        return _phi1020SelNoPID

    ######--######
    def _Phi2KKFilterWS( self ):
        if self._phi1020SelWS is not None:
            return self._phi1020SelWS
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _phi1020WS = CombineParticles(
            DecayDescriptors = ["[phi(1020) -> K+ K+]cc"] ,
            CombinationCut = "(AM< %(PhiUpperMass)s)" % self._config,
            MotherCut       = "(VFASPF(VCHI2/VDOF) < %(PhiVCHI2DOF)s ) & (PT > %(PhiPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(PhiMINIPCHI2)s ) & (BPVDIRA> %(PhiDIRA)s)" %self._config
            )
        _phi1020SelWS=Selection("PhiKKWS_for"+self._name,
                                Algorithm=_phi1020WS,
                                RequiredSelections = [self._kaonForPhiFilter()])
        
        self._phi1020SelWS=_phi1020SelWS
        
        return _phi1020SelWS

    
    #####Make the Rho######
    def _Rho02PiPiFilter( self ):
        if self._rho770Sel is not None:
            return self._rho770Sel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _rho770 = CombineParticles(
            DecayDescriptors = ["rho(770)0 -> pi- pi+"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s )"\
                               "& (PIDK < %(RhoChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "(ADAMASS('rho(770)0')< %(RhoMassWindow)s)" % self._config,
            MotherCut       = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (MAXTREE('pi+'==ABSID,P )>%(RhoLeadingPionP)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"\
            "& (DMASS('rho(770)0')< %(RhoMassWindow)s) " %self._config
            )
        _rho770Sel=Selection("Rho02PiPi_for"+self._name,
                             Algorithm=_rho770,
                             RequiredSelections = [self._pionFilter()])
        
        self._rho770Sel=_rho770Sel
        
        return _rho770Sel

    ######--######
    def _Rho02PiPiFilterNoPID( self ):
        if self._rho770SelNoPID is not None:
            return self._rho770SelNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _rho770NoPID = CombineParticles(
            DecayDescriptors = ["rho(770)0 -> pi- pi+"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s )"\
                               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "(ADAMASS('rho(770)0')< %(RhoMassWindow)s)" % self._config,
            MotherCut       = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (MAXTREE('pi+'==ABSID,P )>%(RhoLeadingPionP)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"\
            "& (DMASS('rho(770)0')< %(RhoMassWindow)s) " %self._config
            )
        _rho770SelNoPID=Selection("Rho02PiPiNoPID_for"+self._name,
                                  Algorithm=_rho770NoPID,
                                  RequiredSelections = [self._pionFilterNoPID()])
        
        self._rho770SelNoPID=_rho770SelNoPID
        
        return _rho770SelNoPID

    ######--######
    def _Rho02PiPiWSFilter( self ):
        if self._rho770WSSel is not None:
            return self._rho770WSSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _rho770WS = CombineParticles(
            DecayDescriptors = ["[rho(770)0 -> pi+ pi+]cc"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s )"\
                               "& (PIDK < %(RhoChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "(ADAMASS('rho(770)0')< %(RhoMassWindow)s)" % self._config,
            MotherCut      = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"\
            "& (DMASS('rho(770)0')< %(RhoMassWindow)s) " %self._config
            )
        _rho770SelWS=Selection("Rho02PiPiWS_for"+self._name,
                             Algorithm=_rho770WS,
                             RequiredSelections = [self._pionFilter()])
        
        self._rho770WSSel=_rho770SelWS
        return _rho770SelWS
    
    ######--######
    def _Rho02PiPiSBFilter( self ):
        if self._rho770SBSel is not None:
            return self._rho770SBSel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _rho770SB = CombineParticles(
            DecayDescriptors = ["rho(770)0 -> pi+ pi-"] ,
            DaughtersCuts   = {"pi+":"(PT> %(RhoChPionPT)s *MeV) & (TRCHI2DOF < %(RhoChPionTRCHI2)s )"\
                               "& (MIPCHI2DV(PRIMARY)> %(RhoChPionMINIPCHI2)s ) & (PIDpi-PIDK> %(RhoChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config
                               },
            CombinationCut = "((AM > %(RhoMassWindowMin1SB)s) & (AM < %(RhoMassWindowMax1SB)s))"\
            " | ((AM > %(RhoMassWindowMin2SB)s) & (AM < %(RhoMassWindowMax2SB)s))" % self._config,
            
            MotherCut       = "(MAXTREE('pi+'==ABSID,PT )>%(RhoLeadingPionPT)s *MeV )"\
            "& (VFASPF(VCHI2/VDOF) < %(RhoVCHI2DOF)s ) & (PT > %(RhoPT)s *MeV) "\
            "& (MIPCHI2DV(PRIMARY)> %(RhoMINIPCHI2)s ) & (BPVDIRA> %(RhoDIRA)s)"%self._config
            )
        _rho770SelSB=Selection("Rho02PiPiSB_for"+self._name,
                               Algorithm=_rho770SB,
                               RequiredSelections = [self._pionFilter()])
        
        self._rho770SBSel=_rho770SelSB
        return _rho770SelSB

    ### Make the omega(782) ###
    def _Omega2PiPiPi0Filter( self ):
        if self._omega782Sel is not None:
            return self._omega782Sel
        
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0
        
        _omega782 = DaVinci__N3BodyDecays(
            DecayDescriptor  = "omega(782) -> pi+ pi- pi0" ,
            DaughtersCuts    = {"pi+":"(PT> %(OmegaChPionPT)s *MeV)"\
                                "& (MIPCHI2DV(PRIMARY)> %(OmegaChPionMINIPCHI2)s )"\
                                "& (PIDK < %(OmegaChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config,
                                "pi0":"(PT > %(OmegaPi0PT)s *MeV)" % self._config
                                },
            CombinationCut   = "(ADAMASS('omega(782)')< %(OmegaMassWindow)s)" % self._config,
            Combination12Cut = "(AM < 800*MeV)&(ACHI2DOCA(1,2) < 8)",
            MotherCut        = "(VFASPF(VCHI2/VDOF) < %(OmegaVCHI2DOF)s) & (DMASS('omega(782)')< %(OmegaMassWindow)s) " %self._config
            )
        _omega782Sel=Selection("Omega2PiPiPi0_for"+self._name,
                               Algorithm=_omega782,
                               RequiredSelections = [StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0])
        
        self._omega782Sel=_omega782Sel
        
        return _omega782Sel

    ######--######
    def _Omega2PiPiPi0FilterNoPID( self ):
        if self._omega782SelNoPID is not None:
            return self._omega782SelNoPID
        
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions, StdLooseResolvedPi0, StdLooseMergedPi0
        
        _omega782NoPID = DaVinci__N3BodyDecays(
            DecayDescriptor  = "omega(782) -> pi+ pi- pi0" ,
            DaughtersCuts    = {"pi+":"(PT> %(OmegaChPionPT)s *MeV)"\
                                "& (MIPCHI2DV(PRIMARY)> %(OmegaChPionMINIPCHI2)s )"\
                                "& (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config,
                                "pi0":"(PT > %(OmegaPi0PT)s *MeV)" % self._config
                                },
            CombinationCut   = "(ADAMASS('omega(782)')< %(OmegaMassWindow)s)" % self._config,
            Combination12Cut = "(AM < 800*MeV)&(ACHI2DOCA(1,2) < 8)",
            MotherCut        = "(VFASPF(VCHI2/VDOF) < %(OmegaVCHI2DOF)s) & (DMASS('omega(782)')< %(OmegaMassWindow)s) " %self._config
            )
        _omega782SelNoPID=Selection("Omega2PiPiPi0NoPID_for"+self._name,
                                    Algorithm=_omega782NoPID,
                                    RequiredSelections = [StdNoPIDsPions, StdLooseResolvedPi0, StdLooseMergedPi0])
        
        self._omega782SelNoPID=_omega782SelNoPID
        
        return _omega782SelNoPID

    ######--######
    def _Omega2PiPiPi0WSFilter( self ):
        if self._omega782WSSel is not None:
            return self._omega782WSSel
        
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0
        
        _omega782WS = DaVinci__N3BodyDecays(
            DecayDescriptor   = "[omega(782) -> pi+ pi+ pi0]cc" ,
            DaughtersCuts     = {"pi+":"(PT> %(OmegaChPionPT)s *MeV)"\
                                 "& (MIPCHI2DV(PRIMARY)> %(OmegaChPionMINIPCHI2)s )"\
                                 "& (PIDK < %(OmegaChPionPIDK)s ) & (TRGHOSTPROB < %(TRGHOSTPROB)s)" % self._config,
                                 "pi0":"(PT > %(OmegaPi0PT)s *MeV)" % self._config
                                 },
            CombinationCut    = "(ADAMASS('omega(782)')< %(OmegaMassWindow)s)" % self._config,
            Combination12Cut  = "(AM < 800*MeV)&(ACHI2DOCA(1,2) < 8)",
            MotherCut         = "(VFASPF(VCHI2/VDOF) < %(OmegaVCHI2DOF)s) & (DMASS('omega(782)')< %(OmegaMassWindow)s) " %self._config
            )
        _omega782WSSel=Selection("Omega2PiPiPi0WS_for"+self._name,
                               Algorithm=_omega782WS,
                               RequiredSelections = [StdLoosePions, StdLooseResolvedPi0, StdLooseMergedPi0])
        
        self._omega782WSSel=_omega782WSSel
        
        return _omega782WSSel
    
    ######Make the Kshort->EPi Same Sign for Majorana####
    def _KshMajoranaSSEFilter( self ):
        if self._KshMajoranaSSESel is not None:
            return self._KshMajoranaSSESel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseElectrons
     
        _Ksh = CombineParticles(
            DecayDescriptor = "KS0 -> e- pi+",
            DaughtersCuts   = {"pi+":"(P > %(KS0DaugP)s)& (PT > %(KS0DaugPT)s)"\
                               "& (TRCHI2DOF < %(KS0DaugTrackChi2)s)" \
                               "& (MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s)"   % self._config
                               },
            CombinationCut  = "(ADOCACHI2CUT(25, ''))"% self._config,
            
            MotherCut       = "(BPVVDCHI2> %(KSMajoranaCutFDChi2)s )& (VFASPF(VCHI2/VDOF) < %(KS0VertexChi2)s)&(PT > %(KS0PT)s*MeV)" % self._config 
            )
        
        #_Ksh.ReFitPVs = True        
        _KshMajoranaSSESel=Selection("KshMajoranaSSE_for"+self._name,
                                      Algorithm=_Ksh,
                                      RequiredSelections = [StdLoosePions, self._MajoranaLineElectronFilter()])
        self._KshMajoranaSSESel=_KshMajoranaSSESel
        
        return _KshMajoranaSSESel
    
    ######Make the Kshort->EPi Opposite Sign for Majorana####
    def _KshMajoranaOSEFilter( self ):
        if self._KshMajoranaOSESel is not None:
            return self._KshMajoranaOSESel
        
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions, StdLooseElectrons
        
        _Ksh = CombineParticles(
            DecayDescriptor = "KS0 -> e+ pi-",
            DaughtersCuts   = {"pi-":"(P > %(KS0DaugP)s)& (PT > %(KS0DaugPT)s)"\
                               "&(TRCHI2DOF < %(KS0DaugTrackChi2)s)"\
                               "&(MIPCHI2DV(PRIMARY) > %(KS0DaugMIPChi2)s)"   % self._config
                               },
            CombinationCut  = "(ADOCACHI2CUT(25, ''))"% self._config,
            
            MotherCut       = "(BPVVDCHI2> %(KSMajoranaCutFDChi2)s )& (VFASPF(VCHI2/VDOF) < %(KS0VertexChi2)s)&(PT > %(KS0PT)s*MeV)" % self._config 
            )
        # _Ksh.ReFitPVs = True
        _KshMajoranaOSESel=Selection("KshMajoranaOSE_for"+self._name,
                                      Algorithm=_Ksh,
                                      RequiredSelections = [StdLoosePions, self._MajoranaLineElectronFilter()])
        self._KshMajoranaOSESel=_KshMajoranaOSESel
        
        return _KshMajoranaOSESel

    ## Kstars
    
    # Make the K*+ out of KS and pi+
    def _Kstar2KPiFilter( self ):
        if self._KstarSel is not None:
            return self._KstarSel

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions

        _Kstar2KsPi = CombineParticles(
            DecayDescriptors = ["[K*(892)+ -> KS0 pi+]cc"],
            CombinationCut = "(ADAMASS('K*(892)+')< %(KstarMassWindow)s )" % self._config,
            DaughtersCuts   = {"pi+":"(PT> %(KstarChPionPT)s*MeV) &(P> %(KstarChPionP)s*MeV)"\
                               "&  (TRCHI2DOF < %(KstarChPionTRCHI2)s )& (MIPCHI2DV(PRIMARY)>%(KstarChPionMINIPCHI2)s)"\
                               "& (PIDK < %(KstarChPionPIDK)s)" % self._config
                               },
            MotherCut = "(PT > %(KstarPT)s)" % self._config,
            ReFitPVs = True
            )
        _KstarSel = Selection("Kstar_for"+self._name,
                              Algorithm = _Kstar2KsPi,
                              RequiredSelections = [StdLoosePions, self._KsFilter()])
        self._KstarSel = _KstarSel

        return _KstarSel

    ######--######
    def _Kstar2KPiFilterNoPID( self ):
        if self._KstarSelNoPID is not None:
            return self._KstarSelNoPID

        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdNoPIDsPions

        _Kstar2KsPiNoPID = CombineParticles(
            DecayDescriptors = ["[K*(892)+ -> KS0 pi+]cc"],
            CombinationCut = "(ADAMASS('K*(892)+')< %(KstarMassWindow)s )" % self._config,
            DaughtersCuts   = {"pi+":"(PT> %(KstarChPionPT)s*MeV) &(P> %(KstarChPionP)s*MeV)"\
                               "&  (TRCHI2DOF < %(KstarChPionTRCHI2)s )& (MIPCHI2DV(PRIMARY)>%(KstarChPionMINIPCHI2)s)" % self._config
                               },
            MotherCut = "(PT > %(KstarPT)s)" % self._config,
            ReFitPVs = True
            )
        _KstarSelNoPID = Selection("KstarNoPID_for"+self._name,
                                   Algorithm = _Kstar2KsPiNoPID,
                                   RequiredSelections = [StdNoPIDsPions, self._KsFilter()])
        self._KstarSelNoPID = _KstarSelNoPID

        return _KstarSelNoPID

    ################################
    ##
    ## B -> X e nu definitions
    ##
    ################################

    ### B+ -> pi+ e- nu
    def _B2PiENu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KE = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ e-]cc"],
            CombinationCut = "(AM>%(KEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _KESel=Selection("PiE_for"+self._name,
                         Algorithm=_KE,
                         RequiredSelections = [self._electronFilterTOS(), self._pionFilterTight()])
        _KESel = tosSelection(_KESel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESel

    def _B2PiENuNoETopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KENoETopo = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ e-]cc"],
            CombinationCut = "(AM>%(KEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _KESelNoETopo=Selection("PiENoETopo_for"+self._name,
                         Algorithm=_KENoETopo,
                         RequiredSelections = [self._electronFilterTOS(), self._pionFilterTight()])
        _KESelNoETopo = tosSelection(_KESelNoETopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelNoETopo


    def _B2PiENuNoPIDe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KENoPIDe = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ e-]cc"],
            CombinationCut = "(AM>%(KEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _KESelNoPIDe=Selection("PiENoPIDe_for"+self._name,
                                 Algorithm=_KENoPIDe,
                                 RequiredSelections = [self._electronFilterNoPID(), self._pionFilterTight()])
        _KESelNoPIDe = tosSelection(_KESelNoPIDe,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelNoPIDe

    def _B2PiENuNoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KENoPIDhad = CombineParticles(
            DecayDescriptors = ["[B0 -> pi+ e-]cc"],
            CombinationCut = "(AM>%(KEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _KESelNoPIDhad=Selection("PiENoPIDhad_for"+self._name,
                                  Algorithm=_KENoPIDhad,
                                  RequiredSelections = [self._electronFilterTOS(), self._pionFilterTightNoPID()])
        _KESelNoPIDhad = tosSelection(_KESelNoPIDhad,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelNoPIDhad
    
    def _B2PiENuSS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KESS = CombineParticles(
            DecayDescriptors = ["[B0 -> pi- e-]cc"],
            CombinationCut = "(AM>%(KEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = " (VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRATight)s) "\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
      
        _KESelSS=Selection("PiESS_for"+self._name,
                          Algorithm=_KESS,
                          RequiredSelections = [self._electronFilterTOS(), self._pionFilterTight()])
        _KESelSS = tosSelection(_KESelSS,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelSS


    ######Bs->KENu######
    def _Bs2KENu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KE = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )
        
        _KESel=Selection("KE_for"+self._name,
                         Algorithm=_KE,
                         RequiredSelections = [self._electronFilterTOS(), self._kaonFilter()])
        _KESel = tosSelection(_KESel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESel

    def _Bs2KENuNoETopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KENoETopo = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )
        
        _KESelNoETopo=Selection("KENoETopo_for"+self._name,
                         Algorithm=_KENoETopo,
                         RequiredSelections = [self._electronFilter(), self._kaonFilter()])
        _KESelNoETopo = tosSelection(_KESelNoETopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelNoETopo
    
    def _Bs2KENuSS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KESS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KESelSS=Selection("KESS_for"+self._name,
                          Algorithm=_KESS,
                          RequiredSelections = [self._electronFilterTOS(), self._kaonFilter()])
        _KESelSS = tosSelection(_KESelSS,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelSS

    ######Bs->KENu misid, no PID on e######
    def _Bs2KENu_NoPIDe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KE = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )
        
        _KESel=Selection("KE_NoPIDe_for"+self._name,
                         Algorithm=_KE,
                         RequiredSelections = [self._electronFilterNoPID(), self._kaonFilter()])
        _KESel = tosSelection(_KESel,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESel
    
    def _Bs2KENuSS_NoPIDe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KESS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KESelSS=Selection("KESS_NoPIDe_for"+self._name,
                          Algorithm=_KESS,
                          RequiredSelections = [self._electronFilterTightNoPID(), self._kaonFilter()])
        #_KESelSS = tosSelection(_KESelSS,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelSS

    ######Bs->KENu misid, no PID on K######
    def _Bs2KENu_NoPIDK( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KE = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )
        
        _KESel=Selection("KE_NoPIDK_for"+self._name,
                         Algorithm=_KE,
                         RequiredSelections = [self._electronFilterTOS(), self._kaonFilterNoPID()])
        _KESel = tosSelection(_KESel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESel
    
    def _Bs2KENuSS_NoPIDK( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KESS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KESelSS=Selection("KESS_NoPIDK_for"+self._name,
                          Algorithm=_KESS,
                          RequiredSelections = [self._electronFilterTOS(), self._kaonFilterNoPID()])
        _KESelSS = tosSelection(_KESelSS,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelSS

    ######Bs->KENu misid, no PID on e or K######
    def _Bs2KENu_NoPIDKe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KE = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )
        
        _KESel=Selection("KE_NoPIDKe_for"+self._name,
                         Algorithm=_KE,
                         RequiredSelections = [self._electronFilterNoPID(), self._kaonFilterNoPID()])
        _KESel = tosSelection(_KESel,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESel
    
    def _Bs2KENuSS_NoPIDKe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KESS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K- e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOFTight)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"
            % self._config,
            ReFitPVs = True
            )

        _KESelSS=Selection("KESS_NoPIDKe_for"+self._name,
                          Algorithm=_KESS,
                          RequiredSelections = [self._electronFilterNoPID(), self._kaonFilterNoPID()])
        _KESelSS = tosSelection(_KESelSS,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KESelSS

   ######Bu->PhiENu ######
    def _B2PhiENu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _PhiE = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) e+]cc"],
            CombinationCut = "(AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiE_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiESel=Selection("PhiE_for"+self._name,
                         Algorithm=_PhiE,
                         RequiredSelections = [self._electronFilterTOS(), self._Phi2KKFilter()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleElectron.*Decision%TOS':0})
        _PhiESel = tosSelection(_PhiESel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _PhiESel

    def _B2PhiENuNoETopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _PhiENoETopo = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) e+]cc"],
            CombinationCut = "(AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiE_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiESelNoETopo=Selection("PhiENoETopo_for"+self._name,
                         Algorithm=_PhiENoETopo,
                         RequiredSelections = [self._electronFilter(), self._Phi2KKFilter()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleElectron.*Decision%TOS':0})
        _PhiESelNoETopo = tosSelection(_PhiESelNoETopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _PhiESelNoETopo


    def _B2PhiENuWS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _PhiEWS = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) e+]cc"],
            CombinationCut = "(AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiE_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiESelWS=Selection("PhiEWS_for"+self._name,
                         Algorithm=_PhiEWS,
                         RequiredSelections = [self._electronFilterTOS(), self._Phi2KKFilterWS()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleElectron.*Decision%TOS':0})
        _PhiESelWS = tosSelection(_PhiESelWS,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _PhiESelWS
   
   ######Bu->PhiENu Fake E ######
   
    def _B2PhiENu_NoPIDe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _PhiE = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) e+]cc"],
            CombinationCut = "(AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiE_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiENoPIDeSel=Selection("PhiE_NoPIDme_for"+self._name,
                         Algorithm=_PhiE,
                         RequiredSelections = [self._electronFilterNoPID(), self._Phi2KKFilter()])
        _PhiENoPIDeSel = tosSelection(_PhiENoPIDeSel,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _PhiENoPIDeSel
    
    def _B2PhiENu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _PhiENoPIDHad = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) e+]cc"],
            CombinationCut = "(AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiE_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiENoPIDhadSel=Selection("PhiE_NoPIDHad_for"+self._name,
                                    Algorithm=_PhiENoPIDHad,
                                    RequiredSelections = [self._electronFilterTightNoPID(), self._Phi2KKFilterNoPID()])
        # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleElectron.*Decision%TOS':0})
        _PhiENoPIDhadSel = tosSelection(_PhiENoPIDhadSel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _PhiENoPIDhadSel

    def _B2PhiENuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _PhiEBadVtx = CombineParticles(
            DecayDescriptors = ["[B+ -> phi(1020) e+]cc"],
            CombinationCut = "(AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(BPVCORRM>%(PhiE_MCORR)s*MeV) & (VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRAMed)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2ForPhi)s)"
            % self._config,
            ReFitPVs = True
            )
        _PhiESelBadVtx=Selection("PhiEBadVtx_for"+self._name,
                         Algorithm=_PhiEBadVtx,
                         RequiredSelections = [self._electronFilterTOS(), self._Phi2KKFilter()])
            # _KMuSel = tosSelection(_KMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*SingleElectron.*Decision%TOS':0})
        _PhiESelBadVtx = tosSelection(_PhiESelBadVtx,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _PhiESelBadVtx


      
    ######Bu->RhoENu ######
    def _Bu2RhoENu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _RhoE = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoESel=Selection("RhoE_for"+self._name,
                            Algorithm=_RhoE,
                            RequiredSelections = [self._electronFilterTight(), self._Rho02PiPiFilter()])
        _RhoESel = tosSelection(_RhoESel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _RhoESel

    def _Bu2RhoENuNoETopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _RhoENoETopo = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoESelNoETopo=Selection("RhoENoETopo_for"+self._name,
                            Algorithm=_RhoENoETopo,
                            RequiredSelections = [self._electronFilterTight(), self._Rho02PiPiFilter()])
        _RhoESelNoETopo = tosSelection(_RhoESelNoETopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _RhoESelNoETopo

    def _Bu2RhoENu_NoPIDe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _RhoE_NoPIDe = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoESel_NoPIDe=Selection("RhoE_NoPIDe_for"+self._name,
                            Algorithm=_RhoE_NoPIDe,
                            RequiredSelections = [self._electronFilterTightNoPID(), self._Rho02PiPiFilter()])
        _RhoESel_NoPIDe = tosSelection(_RhoESel_NoPIDe,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _RhoESel_NoPIDe

    def _Bu2RhoENu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _RhoE_NoPIDhad = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _RhoESel_NoPIDhad=Selection("RhoE_NoPIDHad_for"+self._name,
                            Algorithm=_RhoE_NoPIDhad,
                            RequiredSelections = [self._electronFilterTight(), self._Rho02PiPiFilterNoPID()])
        _RhoESel_NoPIDhad = tosSelection(_RhoESel_NoPIDhad,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _RhoESel_NoPIDhad


    def _Bu2RhoENuWS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _RhoEWS = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _RhoESelWS=Selection("RhoEWS_for"+self._name,
                         Algorithm=_RhoEWS,
                         RequiredSelections = [self._electronFilterTight(), self._Rho02PiPiWSFilter()])
        _RhoESelWS = tosSelection(_RhoESelWS,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _RhoESelWS

    def _Bu2RhoENuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _RhoEBadVtx = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _RhoESelBadVtx=Selection("RhoEBadVtx_for"+self._name,
                                  Algorithm=_RhoEBadVtx,
                                  RequiredSelections = [self._electronFilterTight(), self._Rho02PiPiFilter()])
        _RhoESelBadVtx = tosSelection(_RhoESelBadVtx,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _RhoESelBadVtx
    
    ### This is not needed anymore, as long as the pi+pi- mass window is large enough.    
    def _Bu2RhoMuNuSB( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _RhoMu = CombineParticles(
            DecayDescriptors = ["[B+ -> rho(770)0 mu+]cc"],
            CombinationCut = "(AM>%(RhoMuMassLowTight)s*MeV) & (AM<%(XMuMassUpper)s*MeV)" % self._config,
            MotherCut ="(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s) & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (ratio >0.4)"% self._config,
            #" ( ((((5279.17*5279.17) -(MASS(1,2))*(MASS(1,2))))/(2*5279.17)) < %(Enu)s*MeV)"\

            ReFitPVs = True
            )
        
        _RhoMu.Preambulo = [ "from LoKiPhys.decorators import *",
                           "dx = (VFASPF(VX)-BPV(VX))",
                           "dy = (VFASPF(VY)-BPV(VY))",
                           "dz = (VFASPF(VZ)-BPV(VZ))",
                           "norm = (max(sqrt(dx*dx+dy*dy+dz*dz),0))",
                           "B_xdir  = ((dx)/norm)",
                           "B_ydir  = ((dy)/norm)",
                           "B_zdir  = ((dz)/norm)",
                           "Pxrhomu   = (CHILD(PX,1)+CHILD(PX,2))",
                           "Pyrhomu   = (CHILD(PY,1)+CHILD(PY,2))",
                           "Pzrhomu   = (CHILD(PZ,1)+CHILD(PZ,2))",
                           "Bflight = (B_xdir*Pxrhomu + B_ydir*Pyrhomu+ B_zdir*Pzrhomu)",
                           "mB  = 5279.25",
                           "M_2 = (mB*mB + M12*M12)",
                           "rhomu_E  = (CHILD(E,1)+CHILD(E,2))",
                           "quada = (Bflight*Bflight - rhomu_E*rhomu_E)",
                           "quadb = (M_2*Bflight)",
                           "quadc = (((M_2*M_2)/4) - (rhomu_E*rhomu_E*mB*mB))",
                           "Discriminant = ((quadb*quadb)-(4*quada*quadc))",
                           "solution_a = ((-quadb + sqrt(max(Discriminant,0)))/(2*quada))",
                           "solution_b = ((-quadb - sqrt(max(Discriminant,0)))/(2*quada))",
                           "ratio = (solution_a/solution_b)"
                           ]
        
        _RhoMuSel=Selection("RhoMuSB_for"+self._name,
                         Algorithm=_RhoMu,
                         RequiredSelections = [self._electronFilterTight(), self._Rho02PiPiSBFilter()])
        _RhoMuSel = tosSelection(_RhoMuSel,{'Hlt2.*TopoMu2Body.*Decision%TOS':0,'Hlt2.*TopoMu3Body.*Decision%TOS':0,'Hlt2.*SingleElectron.*Decision%TOS':0})
        return _RhoMuSel
    
    ######Bu->OmegaENu ######
    def _Bu2OmegaENu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _OmegaE = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaESel=Selection("OmegaE_for"+self._name,
                              Algorithm=_OmegaE,
                              RequiredSelections = [self._electronFilterTight(), self._Omega2PiPiPi0Filter()])
        _OmegaESel = tosSelection(_OmegaESel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _OmegaESel
    
    def _Bu2OmegaENuNoETopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _OmegaENoETopo = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaESelNoETopo=Selection("OmegaENoETopo_for"+self._name,
                              Algorithm=_OmegaENoETopo,
                              RequiredSelections = [self._electronFilterTight(), self._Omega2PiPiPi0Filter()])
        _OmegaESelNoETopo = tosSelection(_OmegaESelNoETopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _OmegaESelNoETopo

    def _Bu2OmegaENu_NoPIDe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _OmegaE_NoPIDe = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaESel_NoPIDe=Selection("OmegaE_NoPIDe_for"+self._name,
                                      Algorithm=_OmegaE_NoPIDe,
                                      RequiredSelections = [self._electronFilterTightNoPID(), self._Omega2PiPiPi0Filter()])
        _OmegaESel_NoPIDe = tosSelection(_OmegaESel_NoPIDe,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _OmegaESel_NoPIDe

    def _Bu2OmegaENu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _OmegaE_NoPIDhad = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )

        _OmegaESel_NoPIDhad=Selection("OmegaE_NoPIDhad_for"+self._name,
                                      Algorithm=_OmegaE_NoPIDhad,
                                      RequiredSelections = [self._electronFilterTight(), self._Omega2PiPiPi0FilterNoPID()])
        _OmegaESel_NoPIDhad = tosSelection(_OmegaESel_NoPIDhad,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _OmegaESel_NoPIDhad


    def _Bu2OmegaENuWS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _OmegaEWS = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            "& (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _OmegaESelWS=Selection("OmegaEWS_for"+self._name,
                                Algorithm=_OmegaEWS,
                                RequiredSelections = [self._electronFilterTight(), self._Omega2PiPiPi0WSFilter()])
        _OmegaESelWS = tosSelection(_OmegaESelWS,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _OmegaESelWS

    def _Bu2OmegaENuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _OmegaEBadVtx = CombineParticles(
            DecayDescriptors = ["[B+ -> omega(782) e+]cc"],
            CombinationCut = "(AM>%(RhoEMassLowTight)s*MeV) & (AM<%(XEMassUpper)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRATight)s)"\
            " & (BPVVDCHI2 >%(BFDCHI2Tight)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)"% self._config,
            ReFitPVs = True
            )
        
        _OmegaESelBadVtx=Selection("OmegaEBadVtx_for"+self._name,
                                    Algorithm=_OmegaEBadVtx,
                                    RequiredSelections = [self._electronFilterTight(), self._Omega2PiPiPi0Filter()])
        _OmegaESelBadVtx = tosSelection(_OmegaESelBadVtx,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single:*Electron.*Decision%TOS':0})
        return _OmegaESelBadVtx
    

    ######Bu->KshENu SS & OS######
    def _Bu2KshSSENu_SSEminus( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KshSSE_SSEminus = CombineParticles(
            DecayDescriptors = ["[B- -> KS0 e-]cc"],
            CombinationCut = "(AM>%(KS0EMassLowTight)s*MeV) & (AM<%(XEMassUpperHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"\
            "& (MINTREE( (ABSID=='KS0') , VFASPF(VZ))-VFASPF(VZ) > %(KS0Z)s *mm )" % self._config,
            ReFitPVs = True
            )
        
        _KshSSE_SSEminusSel=Selection("KshSSE_SSEminus_for"+self._name,
                              Algorithm=_KshSSE_SSEminus,
                              RequiredSelections = [self._electronFilter(), self._KshMajoranaSSEFilter()])
        return _KshSSE_SSEminusSel
    
    
    def _Bu2KshOSENu_SSEplus( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KshOSE_SSEplus = CombineParticles(
            DecayDescriptors = ["[B- -> KS0 e-]cc"],
            CombinationCut = "(AM>%(KS0EMassLowTight)s*MeV) & (AM<%(XEMassUpperHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"\
            "& (MINTREE( (ABSID=='KS0') , VFASPF(VZ))-VFASPF(VZ) > %(KS0Z)s *mm )" % self._config,
            ReFitPVs = True
            )
        
        _KshOSE_SSEplusSel=Selection("KshOSE_SSEplus_for"+self._name,
                              Algorithm=_KshOSE_SSEplus,
                              RequiredSelections = [self._electronFilter(), self._KshMajoranaOSEFilter()])
        return _KshOSE_SSEplusSel

#    ###Combine Bs->K*(892)+eNu, K*->KS0Pi###
    def _Bs2KstarENu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarE = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )
        
        _KstarESel=Selection("KstarE_for"+self._name,
                                Algorithm=_KstarE,
                                RequiredSelections = [self._electronFilter(), self._Kstar2KPiFilter()])
        _KstarESel = tosSelection(_KstarESel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KstarESel

    def _Bs2KstarENuNoETopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarENoETopo = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )
        
        _KstarESelNoETopo=Selection("KstarENoETopo_for"+self._name,
                                Algorithm=_KstarENoETopo,
                                RequiredSelections = [self._electronFilter(), self._Kstar2KPiFilter()])
        _KstarESelNoETopo = tosSelection(_KstarESelNoETopo,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KstarESelNoETopo

    def _Bs2KstarENu_NoPIDe( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarENoPIDe = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )
        
        _KstarESelNoPIDe=Selection("KstarENoPIDe_for"+self._name,
                                   Algorithm=_KstarENoPIDe,
                                   RequiredSelections = [self._electronFilterNoPID(), self._Kstar2KPiFilter()])
        _KstarESelNoPIDe = tosSelection(_KstarESelNoPIDe,{'Hlt2.*Topo2Body.*Decision%TOS':0,'Hlt2.*Topo3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KstarESelNoPIDe

    def _Bs2KstarENu_NoPIDhad( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection

        _KstarENoPIDhad = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )
        
        _KstarESelNoPIDhad=Selection("KstarENoPIDHad_for"+self._name,
                                      Algorithm=_KstarENoPIDhad,
                                      RequiredSelections = [self._electronFilter(), self._Kstar2KPiFilterNoPID()])
        _KstarESelNoPIDhad = tosSelection(_KstarESelNoPIDhad,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KstarESelNoPIDhad

    
    def _Bs2KstarENuSS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KstarESS = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)- e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )
        
        _KstarESSSel=Selection("KstarESS_for"+self._name,
                                  Algorithm=_KstarESS,
                                  RequiredSelections = [self._electronFilter(), self._Kstar2KPiFilter()])
        _KstarESSSel = tosSelection(_KstarESSSel,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KstarESSSel

    def _Bs2KstarENuBadVtx( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _KstarEBadVtx = CombineParticles(
            DecayDescriptors = ["[B_s~0 -> K*(892)+ e-]cc"],
            MotherCut = "(VFASPF(VCHI2/VDOF)> %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s) & (BPVCORRM > %(BCorrMLow)s *MeV) & (BPVCORRM < %(BCorrMHigh)s *MeV)" % self._config,
            ReFitPVs = True
            )
        
        _KstarESelBadVtx=Selection("KstarEBadVtx_for"+self._name,
                                Algorithm=_KstarEBadVtx,
                                RequiredSelections = [self._electronFilter(), self._Kstar2KPiFilter()])
        _KstarESelBadVtx = tosSelection(_KstarESelBadVtx,{'Hlt2.*TopoE2Body.*Decision%TOS':0,'Hlt2.*TopoE3Body.*Decision%TOS':0,'Hlt2.*Single.*Electron.*Decision%TOS':0})
        return _KstarESelBadVtx
