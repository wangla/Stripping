###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Gerwin Meier'
__date__ = '2019/08/12'

'''
Based on the work of the stripping line StrippingBd2JpsieeKSBd2JpsieeKS from Ramon Niet
Bd->JpsieeKS stripping selection

Exports the following stripping line
- Bd2JpsieeKSFullDSTBd2JpsieeKSFullDSTDetachedLine
'''

__all__ = (
    'Bd2JpsieeKSFullDSTConf',
    'default_config'
    )

default_config = {
    'NAME'              : 'Bd2JpsieeKSFullDST',
    'BUILDERTYPE'       : 'Bd2JpsieeKSFullDSTConf',
    'CONFIG'    : {
          'BPVLTIME'              :     0.2   # ps
        , 'B0_BPV_DIRA'           :   0.995   # adimensional
        , 'B0_BPV_IP'             :      1.   # mm
        , 'B0_DOCA_chi2'          :     13.   # adimensional
        , 'B0_DOCA'               :      3.   # adimensional
        , 'BM_DTF_Jpsi_Const_max' :   5900.   # MeV
        , 'BM_DTF_Jpsi_Const_min' :   4600.   # MeV
        , 'ElectronPT'            :   500.    # MeV
        , 'ElectronPID'           :     0.    # adimensional
        , 'ElectronTrackCHI2pDOF' :     5.    # adimensional
        , 'EProbNNe'              :     0.01  # adimensional
        , 'TRCHI2DOF'             :     3.    # adimensional
        , 'JpsiVertexCHI2pDOF'    :    13.    # adimensional
        , 'JpsiMassMin'           :  2300.    # MeV
        , 'JpsiMassMax'           :  3300.    # MeV
        , 'KSVCHI2'               :    20.    # adimensional
        , 'KSBPVDLS'              :     5.    # adimensional
        , 'KSTau'                 :     1.    # ps
        , 'KSMassMin'             :   460.    # MeV
        , 'KSMassMax'             :   535.    # MeV
        , 'BdVertexCHI2pDOF'      :     6.    # adimensional
        , 'BdMassMin'             :  4400.    # MeV
        , 'BdMassMax'             :  6000.    # MeV
        },
    'STREAMS' : { 'Dimuon'   : ['StrippingBd2JpsieeKSFullDSTBd2JpsieeKSFullDSTDetachedLine'] },
    'WGs'    : [ 'B2CC' ]
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from CommonParticles.Utils import updateDoD
from StandardParticles import StdLoosePions
from StandardParticles import StdLooseKaons
from StandardParticles import StdAllLooseKaons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV


class Bd2JpsieeKSFullDSTConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)

        # if name not set outside, set it to empty
        if name == None:
            name = ""

        self.name   = name
        self.config = config

        DiElectrons           = DataOnDemand( Location = "Phys/StdLooseDiElectron/Particles" )

        self.KsListLoose = MergedSelection( "StdLooseKsMergedForBetaS" + self.name,
                                            RequiredSelections = [DataOnDemand(Location = "Phys/StdLooseKsDD/Particles"),
                                                                  DataOnDemand(Location = "Phys/StdLooseKsLL/Particles")] )
        self.KSList = self.createSubSel( OutputList = "KsForBetaS" + self.name,
                                InputList = self.KsListLoose,
                                Cuts = "(VFASPF(VCHI2) < %(KSVCHI2)s) & (in_range(%(KSMassMin)s,MM,%(KSMassMax)s)) & (BPVDLS > %(KSBPVDLS)s)" % self.config )



        self._jpsi = FilterDesktop( Code = "   (MM > %(JpsiMassMin)s *MeV)" \
                                           " & (MM < %(JpsiMassMax)s *MeV)" \
                                           " & (MINTREE('e+'==ABSID,PIDe-PIDpi) > %(ElectronPID)s )" \
                                           " & (MINTREE('e+'==ABSID,PT) > %(ElectronPT)s *MeV)" \
                                           " & (MINTREE('e+'==ABSID,PROBNNe) > %(EProbNNe)s)" \
                                           " & (MAXTREE('e+'==ABSID,TRCHI2DOF) < %(ElectronTrackCHI2pDOF)s)" \
                                           " & (VFASPF(VCHI2/VDOF) < %(JpsiVertexCHI2pDOF)s)" % self.config )

        self.Jpsi = Selection( "SelJpsi2eeFor" + self.name,
                               Algorithm = self._jpsi,
                               RequiredSelections = [DiElectrons] )


        self.makeBd2JpsieeKSFullDST()

        # Tests CPU time required for construction of StdLooseDiElectron
        # self.DielectronTestLine      = self._DielectronTestLine( DiElectrons, "DielectronTest", config )
        # self.registerLine( self.DielectronTestLine )


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                      Algorithm = filter,
                      RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                          DecayDescriptor,
                          DaughterLists,
                          DaughterCuts = {} ,
                          PreVertexCuts = "ALL",
                          PostVertexCuts = "ALL",
                          ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = ReFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def makeBd2JpsieeKSFullDST( self ) :
        Bd2JpsieeKSFullDST = self.createCombinationSel( OutputList = self.name + "Bd2JpsiKSFullDST",
                                                 DecayDescriptor = "B0 -> J/psi(1S) KS0",
                                                 DaughterLists  = [ self.Jpsi, self.KSList ],
                                                 PreVertexCuts = "in_range(%(BdMassMin)s,AM,%(BdMassMax)s) & (ADOCA(1,2) < %(B0_DOCA)s) & (ACHI2DOCA(1,2) < %(B0_DOCA_chi2)s)" % self.config,
                                                 PostVertexCuts = "(BPVDIRA> %(B0_BPV_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(BdVertexCHI2pDOF)s) &  ( BPVIP() < %(B0_BPV_IP)s *mm ) & (DTF_CTAU('KS0'==ABSID, False) > %(KSTau)s *ps/c_light) & (DTF_CUT(4600*MeV < M < 5900*MeV, False , 'J/psi(1S)' ))" % self.config )

        Bd2JpsieeKSFullDSTDetached = self.createSubSel( InputList = Bd2JpsieeKSFullDST,
                                                 OutputList = Bd2JpsieeKSFullDST.name() + "Detached",
                                                 Cuts = "(BPVLTIME() > %(BPVLTIME)s*ps)" % self.config )

        Bd2JpsieeKSFullDSTDetachedLine = StrippingLine( self.name + "Bd2JpsieeKSFullDSTDetachedLine",
                                                 algos = [ Bd2JpsieeKSFullDSTDetached ],
                                                 EnableFlavourTagging = True )#, MDSTFlag = True )

        self.registerLine(Bd2JpsieeKSFullDSTDetachedLine)
