###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = ['Xabier Cid Vidal']
## inspired by StrippingD2XMuMuSSConf, from Malcolm John, B. Viaud and O. Kochebina
__date__ = '10/3/2020'
__version__ = '$Revision: 2.0 $'

'''
D(s)+-> pi+etap(mumupipi),
D(s)+-> pi+eta(mumupipi),
and Bu->eta_prime(mumupipi)K+, and norm channels
Ds+-> phi(mumu)pipipi and Bu->J/PsiK+
'''

__all__ = ('StrippingEtap2pipimumuConf','default_config','makeDaughters',
           'makeEtap', 'make2body', 'makeDsNorm')

default_config =  {
    'NAME' : 'Etap2pipimumu',
    'BUILDERTYPE' : 'StrippingEtap2pipimumuConf',
    'WGs' : [ 'BandQ' ],
    'STREAMS' : [ 'Charm' ],
    'CONFIG':{
               ## eta and eta' daughters
               'EtapMuonP'         : 0. ,    #MeV
               'EtapMuonPT'        : 0.  ,    #MeV
               'EtapMuonMINIPCHI2' : 5     ,    #adminensional
               'EtapMuonPIDmu'     : -5,   #adimensional    
               
               'EtapPionP'         : 0. ,    #MeV
               'EtapPionPT'        : 0.  ,    #MeV
               'EtapPionMINIPCHI2' : 5     ,    #adminensional
               
               ## Ds decay
               'DsPionP'         : 200. ,    #MeV
               'DsPionPT'        : 25.  ,    #MeV
               'DsPionMINIPCHI2' : 5     ,    #adminensional
               
               ## B+ decay
               'BuKaonP'         : 2000. ,    #MeV
               'BuKaonPT'        : 300.  ,    #MeV
               'BuKaonMINIPCHI2' : 5     ,    #adminensional
               'BuKaonPIDk'      : -5,   #adimensional
               
               #eta -> pipimumu
               'Eta_PT'         : 50 , #MeV                  
               'Eta_VCHI2DOF'   : 20    ,    #adminensional         
               'Eta_MAXDOCA'   : 2  ,    #mm
               'Eta_FDCHI2'     : 2.5     ,   #adimensional
               'Eta_IPCHI2'     : 2.5    ,    #adimensional
               'Eta_MASSWIN'    : 100    ,    #MeV

               #eta and eta' -> pipimumu
               'Etap_PT'         : 100 , #MeV                  
               'Etap_VCHI2DOF'   : 10    ,    #adminensional         
               'Etap_MAXDOCA'   : 1  ,    #mm
               'Etap_FDCHI2'     : 5     ,   #adimensional
               'Etap_IPCHI2'     : 5    ,    #adimensional
               'Etap_MASSWIN'    : 50    ,    #MeV
               
               #Ds+ -> pi+eta and pi+pi-pi+phi
               'Ds_PT'         : 300 , #MeV                  
               'Ds_VCHI2DOF'   : 15     ,    #adminensional         
               'Ds_FDCHI2'     : 10     ,   #adimensional
               'Ds_IPCHI2'     : 10    ,    #adimensional, should be close to 0
               'Ds_MINMASS'    : 1770    ,    #MeV
               'Ds_MAXMASS'    : 2070    ,    #MeV
                           
               #B+->eta'K+ and J/PsiK+
               'Bu_PT'         : 300 , #MeV                  
               'Bu_VCHI2DOF'   : 8     ,    #adminensional         
               'Bu_FDCHI2'     : 36     ,   #adimensional
               'Bu_IPCHI2'     : 20    ,    #adimensional
               'Bu_MASSWIN'    : 100    ,    #MeV
               
               #normalization
               #phi -> mumu
               'Phi_PT'         : 100 , #MeV                  
               'Phi_VCHI2DOF'   : 10     ,    #adminensional         
               'Phi_MAXDOCA'   : 1  ,    #mm
               'Phi_FDCHI2'     : 5     ,   #adimensional
               'Phi_IPCHI2'     : 5    ,    #adimensional
               'Phi_MASSWIN'    : 75    ,    #MeV
               
               #J/Psi -> mumu
               'JPsi_PT'         : 100 , #MeV                  
               'JPsi_VCHI2DOF'   : 10     ,    #adminensional         
               'JPsi_MAXDOCA'   : 1  ,    #mm
               'JPsi_FDCHI2'     : 5     ,   #adimensional
               'JPsi_IPCHI2'     : 5    ,    #adimensional
               'JPsi_MASSWIN'    : 75    ,    #MeV

               'Ds2Eta2PiPiMuMuLinePrescale'      : 1 ,
               'Ds2Eta2PiPiMuMuLinePostscale'     : 1 ,

               'Ds2Etap2PiPiMuMuLinePrescale'      : 1 ,
               'Ds2Etap2PiPiMuMuLinePostscale'     : 1 ,
               
               'Ds2Phi3PiLineForEtapPrescale'      : 0 ,
               'Ds2Phi3PiLineForEtapPostscale'     : 0 ,
               
               'Bu2Etap2PiPiMuMuLinePrescale'      : 0 ,
               'Bu2Etap2PiPiMuMuLinePostscale'     : 0 ,
               
               'Bu2JPsiKForEtapLinePrescale'      : 0 ,
               'Bu2JPsiKForEtapLinePostscale'     : 0 ,
               
               'RelatedInfoTools': [{'Type'              : 'RelInfoVertexIsolation',
                                     'Location'          : 'RelInfoVertexIsolation'
                                     },

                                    { "Type" : "RelInfoMuonIDPlus",
                                      "Variables" : ["MU_BDT"],
                                      "DaughterLocations"  : {
    "[D_s+ -> ( X0 -> ^mu+ mu- pi+ pi- ) pi+ ]CC": "Muon1BDT",
    "[D_s+ -> ( X0 -> mu+ ^mu- pi+ pi- ) pi+ ]CC": "Muon2BDT",
    "[D_s+ -> ( X0 -> mu+ mu- ^pi+ pi- ) pi+ ]CC": "Pi1BDT",
    "[D_s+ -> ( X0 -> mu+ mu- pi+ ^pi- ) pi+ ]CC": "Pi2BDT",
    "[D_s+ -> ( X0 -> mu+ mu- pi+ pi- ) ^pi+ ]CC": "Pi3BDT"}                                    
                                      },
                                    
                                    {'Type'              : 'RelInfoVertexIsolationBDT',
                                     'Location'          : 'RelInfoVertexIsolationBDT'
                                     },
                                    {'Type'              : 'RelInfoConeVariables',
                                     'ConeAngle'         : 1.0,
                                     'Location'          : 'RelInfoConeVariables_1.0',
                                     },
                                    {'Type'              : 'RelInfoConeVariables',
                                     'ConeAngle'         : 1.5,
                                     'Location'          : 'RelInfoConeVariables_1.5',
                                     },
                                    {'Type'              : 'RelInfoConeVariables',
                                     'ConeAngle'         : 2.0,
                                     'Location'          : 'RelInfoConeVariables_2.0',
                                     },
                                    {'Type'              : 'RelInfoTrackIsolationBDT',
                                     'Location'          : 'RelInfoTrackIsolationBDT',
                                     },
                                    {'Type'              : 'RelInfoTrackIsolationBDT2',
                                     'Location'          : 'RelInfoTrackIsolationBDT2',
                                     'Particles': [1,2]
                                     },
                                    {'Type'              : 'RelInfoBstautauCDFIso',
                                     'Location'          : 'RelInfoBstautauCDFIso'
                                     }]
               }
    }

from copy import copy

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons, StdAllLoosePions, StdAllLooseKaons

default_name = 'Etap2pipimumu'

class StrippingEtap2pipimumuConf(LineBuilder) :
    """
    Builder for Ds2etap2pipimumu
    """
            
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        
        Ds2EtapLine_name = name+"_Ds2Etap"
        Bu2EtapLine_name = name+"_Bu2Etap"
        Ds2Phi3PiLine_name = name+"_Ds2Phi3Pi"
        Bu2JPsiKLine_name = name+"_Bu2JPsiK"
        
        # 1 : Make pions and kaons
        selPionsEta = makeDaughters(name="PionsEtaFor"+name
                                    , P = config['EtapPionP']
                                    , PT = config['EtapPionPT']               
                                    , MINIPCHI2 = config['EtapPionMINIPCHI2']
                                    , decid = 211)
        
        
        selPionsDs = makeDaughters(name="PionsDsFor"+name
                                   , P = config['DsPionP']
                                   , PT = config['DsPionPT']
                                   , MINIPCHI2 = config['DsPionMINIPCHI2']
                                   , decid = 211)

        
        selKaonsBu = makeDaughters(name="KaonsBuFor"+name
                                   , P = config['BuKaonP']
                                   , PT = config['BuKaonPT']
                                   , MINIPCHI2 = config['BuKaonMINIPCHI2']
                                   , PID = config['BuKaonPIDk'],decid = 321)
        
        # 2 : Make muons
        selMuonsEta = makeDaughters(name="MuonsEtaFor"+name
                                    , P = config['EtapMuonP']
                                    , PT = config['EtapMuonPT']               
                                    , MINIPCHI2 = config['EtapMuonMINIPCHI2']
                                    , PID = config['EtapMuonPIDmu'],decid = 13)
        
        # 4 : makeEta and Etap
        selEta  = makeEtap("EtaFor"+name, [selPionsEta,selMuonsEta],
                           config['Eta_MASSWIN'],config['Eta_MAXDOCA'],
                           config['Eta_VCHI2DOF'],config['Eta_PT'],
                           config['Eta_FDCHI2'], config['Eta_IPCHI2'],1)
        
        
        selEtap  = makeEtap("EtapFor"+name, [selPionsEta,selMuonsEta],
                            config['Etap_MASSWIN'],config['Etap_MAXDOCA'],
                            config['Etap_VCHI2DOF'],config['Etap_PT'],
                            config['Etap_FDCHI2'], config['Etap_IPCHI2'])

        # 5: make Phi and Jpsi
        selPhi = make2body("PhiFor"+name,[selMuonsEta],
                           "Phi",
                           config['Phi_PT'],config['Phi_VCHI2DOF'],
                           config['Phi_FDCHI2'],config['Phi_IPCHI2'],
                           config['Phi_MASSWIN'])

        selJPsi = make2body("JPsiFor"+name,[selMuonsEta],
                            "JPsi",
                            config['JPsi_PT'],config['JPsi_VCHI2DOF'],
                            config['JPsi_FDCHI2'],config['JPsi_IPCHI2'],
                            config['JPsi_MASSWIN'])
        
        # 6: make Ds
        selDs2Eta = make2body("DsEtaFor"+name,[selPionsDs,selEta],
                              "DsSigEta",
                              config['Ds_PT'],config['Ds_VCHI2DOF'],
                              config['Ds_FDCHI2'],config['Ds_IPCHI2'],
                              config['Ds_MINMASS'],
                              config['Ds_MAXMASS']
                              )

        selDs2Etap = make2body("DsEtapFor"+name,[selPionsDs,selEtap],
                               "DsSigEtap",
                               config['Ds_PT'],config['Ds_VCHI2DOF'],
                               config['Ds_FDCHI2'],config['Ds_IPCHI2'],
                               config['Ds_MINMASS'],
                               config['Ds_MAXMASS']
                               )

        
        ## add cuts to ensure alignment of at least one pion with the pion in the Ds signal decay
        selDs2Phi = makeDsNorm("DsPhiFor"+name, [selPionsEta,selPhi],
                               config['Ds_PT'],config['Ds_VCHI2DOF'],
                               config['Ds_FDCHI2'],config['Ds_IPCHI2'],
                               config['Ds_MINMASS'],config['Ds_MAXMASS'],
                               config['DsPionMINIPCHI2'],config['DsPionPT'],
                               config['DsPionP'])

        # 7: make B+        
        selBu2Etap = make2body("BuEtapFor"+name,[selKaonsBu,selEtap],
                               "B+Sig",
                               config['Bu_PT'],config['Bu_VCHI2DOF'],
                               config['Bu_FDCHI2'],config['Bu_IPCHI2'],
                               config['Bu_MASSWIN'])

        selBu2JPsi = make2body("BuJPsiFor"+name,[selKaonsBu,selJPsi],
                               "B+Norm",
                               config['Bu_PT'],config['Bu_VCHI2DOF'],
                               config['Bu_FDCHI2'],config['Bu_IPCHI2'],
                               config['Bu_MASSWIN'])
        
        
       
        # 8 : Declare Lines
        self.Ds2EtaLine = StrippingLine(name+"Ds2EtaLine",
                                        prescale = config['Ds2Eta2PiPiMuMuLinePrescale'],
                                        postscale = config['Ds2Eta2PiPiMuMuLinePostscale'],
                                        selection = selDs2Eta,
                                        RelatedInfoTools = config['RelatedInfoTools'])
        
        self.Ds2EtaPLine = StrippingLine(name+"Ds2EtaPLine",
                                         prescale = config['Ds2Etap2PiPiMuMuLinePrescale'],
                                         postscale = config['Ds2Etap2PiPiMuMuLinePostscale'],
                                         selection = selDs2Etap,
                                         RelatedInfoTools = config['RelatedInfoTools'])


        #self.Ds2PhiLine = StrippingLine(name+"Ds2PhiLine",
        #                                prescale = config['Ds2Phi3PiLineForEtapPrescale'],
        #                                postscale = config['Ds2Phi3PiLineForEtapPostscale'],
        #                                selection = selDs2Phi,
        #                                RelatedInfoTools = config['RelatedInfoTools'])
        

        
        #self.Bu2EtapLine = StrippingLine(name+"Bu2EtapLine",
        #                                 prescale = config['Bu2Etap2PiPiMuMuLinePrescale'],
        #                                 postscale = config['Bu2Etap2PiPiMuMuLinePostscale'],
        #                                 selection = selBu2Etap,
        #                                 RelatedInfoTools = config['RelatedInfoTools'])


        #self.Bu2JpsiLine = StrippingLine(name+"Bu2JPsiLine",
        #                                 prescale = config['Bu2JPsiKForEtapLinePrescale'],
        #                                 postscale = config['Bu2JPsiKForEtapLinePostscale'],
        #                                 selection = selBu2JPsi,
        #                                 RelatedInfoTools = config['RelatedInfoTools'])

        # 9 : register them
        self.registerLine( self.Ds2EtaLine)
        self.registerLine( self.Ds2EtaPLine)
        #self.registerLine( self.Ds2PhiLine)
        #self.registerLine( self.Bu2EtapLine)
        #self.registerLine( self.Bu2JpsiLine)


#####################################################
#####################################################
#
# Out of class
#####################################################
#make pion or muon daughters, if PID is not str, it means it is a muon, cut in that PIDmu
def makeDaughters(name, P, PT, MINIPCHI2, PID=0,decid=0):
    """
    Daughters selection
    """
    
    _code = "(P > %(P)s *MeV) & "\
            "(PT > %(PT)s *MeV) & "\
            "(MIPCHI2DV(PRIMARY) > %(MINIPCHI2)s)" % locals()

    if decid==13:
        _code+= "& (PIDmu > %(PID)s)" %locals()
        mysel = StdAllLooseMuons
    elif decid==321:
        _code+= "& (PIDK > %(PID)s)" %locals()
        mysel = StdAllLooseKaons
    else: mysel = StdAllLoosePions

    _Filter = FilterDesktop(Code = _code)
    
    return Selection(name,
                     Algorithm = _Filter,
                     RequiredSelections = [ mysel] )


#####################################################

def makeEtap(name, inputs,
             MASSWIN, MAXDOCA, VCHI2DOF,PT, FDCHI2, IPCHI2,iseta=0): 

    """
    Makes the eta -> pi+ pi- mu+ mu- or eta' -> pi+ pi- mu+ mu-
    """

    if iseta:
        _combcut = "(ADAMASS('eta') < %(MASSWIN)s *MeV) & "\
                   "(AMAXDOCA('')<%(MAXDOCA)s) " %locals()
        decayd = "eta -> mu+ mu- pi+ pi-"
    else:
        _combcut = "(ADAMASS('eta_prime') < %(MASSWIN)s *MeV) & "\
                   "(AMAXDOCA('')<%(MAXDOCA)s) " %locals()
        decayd = "eta_prime -> mu+ mu- pi+ pi-"
    
    _bcut   = "(VFASPF(VCHI2/VDOF) < %(VCHI2DOF)s) & "\
              "(PT > %(PT)s *MeV) &"\
              "(BPVVDCHI2>%(FDCHI2)s) & (BPVIPCHI2()> %(IPCHI2)s) " %locals()

    _Combine = CombineParticles(DecayDescriptor = decayd,
                                CombinationCut = _combcut,
                                MotherCut = _bcut)
    return Selection(name,
                     Algorithm = _Combine,
                     RequiredSelections = inputs )


#####################################################
def make2body(name, inputs, decaycode,
              PT,VCHI2DOF,FDCHI2,IPCHI2,MASSWIN,MAXMASS=0):

    """
    Makes the D_s+ -> eta' pi+, B->eta'K+, B->J/PsiK+, J/Psi->mumu and phi->mumu
    """


    _bcut   = "(VFASPF(VCHI2/VDOF) < %(VCHI2DOF)s ) & "\
              "(PT > %(PT)s *MeV) & (BPVVDCHI2 > %(FDCHI2)s ) &" %locals()

    ## if it is the Ds or B+, it should point to the PV, else it shouldn't
    if ("DsSig" in decaycode) or decaycode == "B+Sig" or decaycode=="B+Norm":
        _bcut+= "(BPVIPCHI2()< %(IPCHI2)s) " %locals()
        refitpv = 1
        if decaycode == "DsSigEtap": decay = "[D_s+ -> eta_prime pi+ ]cc";mom = "D_s+"
        if decaycode == "DsSigEta": decay = "[D_s+ -> eta pi+ ]cc";mom = "D_s+"
        if decaycode == "B+Sig": decay = "[B+ -> eta_prime K+ ]cc";mom = "B+"
        if decaycode == "B+Norm": decay = "[B+ -> J/psi(1S) K+ ]cc";mom = "B+"
        
    else:
        _bcut+= "(BPVIPCHI2()> %(IPCHI2)s) " %locals()
        refitpv = 0
        if decaycode == "JPsi": decay = "J/psi(1S) -> mu+ mu-";mom = "J/psi(1S)"
        if decaycode == "Phi": decay = "phi(1020) -> mu+ mu-";mom = "phi(1020)"

    ## if MAXMASS exists, then MASSWIN corresponds really  to MINMASS!!!
    if MAXMASS: _combcut = "( AM > %(MASSWIN)s *MeV ) & ( AM < %(MAXMASS)s *MeV ) " %locals()
    else:  _combcut = "(ADAMASS('"+mom+"') < %(MASSWIN)s *MeV) " %locals()
    
    _Combine = CombineParticles(DecayDescriptor = decay,
                                CombinationCut = _combcut,
                                MotherCut = _bcut)

    ## only refit PV if stopping here
    if refitpv: _Combine.ReFitPVs = True
    return Selection(name,
                     Algorithm = _Combine,
                     RequiredSelections = inputs)
       

#####################################################
def makeDsNorm(name, inputs,
               PT,VCHI2DOF,FDCHI2,IPCHI2,MINMASS,MAXMASS,
               IPCHI2DSPI,PTDSPI,PDSPI):
       
    """
    Makes the D_s- -> phi pi+ pi- pi+
    """

    _combcut = "( AM > %(MINMASS)s *MeV ) & ( AM < %(MAXMASS)s *MeV ) " %locals()

    ## ensure one of the pions is similar to the Ds one
    _combcut+= "& ( AHASCHILD( (MIPCHI2DV(PRIMARY)>%(IPCHI2DSPI)s) &  "\
               "(PT>%(PTDSPI)s) & (P>%(PDSPI)s) ) )" %locals()
    
    _bcut   = "(VFASPF(VCHI2/VDOF) < %(VCHI2DOF)s) & "\
              "(PT > %(PT)s *MeV) & (BPVVDCHI2>%(FDCHI2)s) & "\
              "(BPVIPCHI2()< %(IPCHI2)s) " %locals()
    
    _Combine = CombineParticles(DecayDescriptor = "[D_s+ -> phi(1020) pi+ pi+ pi- ]cc",
                                CombinationCut = _combcut,
                                MotherCut = _bcut)
    
    _Combine.ReFitPVs = True
    return Selection(name,
                     Algorithm = _Combine,
                     RequiredSelections = inputs)
       

