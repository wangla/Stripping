###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Bs2st selection with B+ to ppipipi (missing Lambda c)
Bs2st2pKpipipiLine : Bs2* -> K- (B+ -> p+ pi+ pi+ pi- X )
Bs2st2pKpipipiSSLine : Bs2* -> K+ (B+ -> p+ pi+ pi+ pi- X )
Bs2st2pKpipipiWSLine : Bs2* -> K- (B+ -> p+ pi+ pi- pi- X )
Bs2st2pKpipipiWSSSLine : Bs2* -> K+ (B+ -> p+ pi+ pi- pi- X )
"""

__author__  = ['Matt Rudolph']
__date__    = '2021/3/14'
__version__ = 'v1r0'
__all__     = {'Bs2st2pKpipipiConf',
               'default_config'}

from Gaudi.Configuration                   import *
from PhysSelPython.Wrappers                import Selection
from StrippingConf.StrippingLine           import StrippingLine
from StrippingUtils.Utils                  import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, DaVinci__N4BodyDecays, CombineParticles

from StandardParticles                     import StdLooseANNPions
from StandardParticles                     import StdAllLooseANNKaons
from StandardParticles                     import StdLooseANNProtons

default_config = {
    'NAME'        : 'Bs2st2pKpipipi',
    'WGs'         : ['Charm'],
    'BUILDERTYPE' : 'Bs2st2pKpipipiConf',
    'CONFIG'      : {'Trk_MaxChi2Ndof'   : 4.0,
                     'Trk_MaxGhostProb'  : 0.4,
                     'Trk_MinIPChi2'     : 16.0,
                     'Trk_MinP'          : 1000.0,
                     'Trk_MinProbNNp'    : 0.1,
                     'Trk_MinProbNNpi'   : 0.1,
                     'Km_MaxIPChi2'      : 4,
                     'Km_MinPT'          : 600,
                     'Km_MinProbNNk'     : 0.2,
                     'B_MinM_4body'     : 2400.0,
                     'B_MaxM_4body'     : 3000.0,
                     'B_MinSumPT_4body' : 4000.0,
                     'B_MinPT_4body'    : 2000.0,
                     'B_MaxDOCAChi2'    : 20.0,
                     'B_MaxVtxChi2'     : 3.0,
                     'B_MinVDChi2'      : 120.0,
                     'B_MinDira'        : 0.999,
                     'BK_MaxDM'          : 700.0,
                     'Km_MinPperp'       : 70.0,
                     'Km_MaxPperp'       : 315.0,
                     'BK_MaxPVDZ'        : 1.0,
                     'BK_MinPT'          : 50.0,
                     'Prescale'          : 1.0,
                     'Postscale'         : 1.0
                     },
    'STREAMS'     : ['CharmCompleteEvent']
    }

class Bs2st2pKpipipiConf(LineBuilder) :
    """
    Builder of Bs2st -> K- (B+ -> ppipipiX) and associated charge flipped stripping lines.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)

        trkFilterP     = FilterDesktop(Code = '(MIPCHI2DV(PRIMARY) > %(Trk_MinIPChi2)s)    & \
                                               (TRGHOSTPROB        < %(Trk_MaxGhostProb)s) & \
                                               (TRCHI2DOF          < %(Trk_MaxChi2Ndof)s)  & \
                                               (P                  > %(Trk_MinP)s)         & \
                                               (PROBNNp            > %(Trk_MinProbNNp)s)' % config )

        trkFilterPi    = FilterDesktop(Code = '(MIPCHI2DV(PRIMARY) > %(Trk_MinIPChi2)s)    & \
                                               (TRGHOSTPROB        < %(Trk_MaxGhostProb)s) & \
                                               (TRCHI2DOF          < %(Trk_MaxChi2Ndof)s)  & \
                                               (P                  > %(Trk_MinP)s)         & \
                                               (PROBNNpi            > %(Trk_MinProbNNpi)s)' % config )

        trkFilterK = FilterDesktop(Code = '(MIPCHI2DV(PRIMARY) < %(Km_MaxIPChi2)s) & \
                                               (TRGHOSTPROB        < %(Trk_MaxGhostProb)s) & \
                                               (TRCHI2DOF          < %(Trk_MaxChi2Ndof)s)  & \
                                               (PT                  > %(Km_MinPT)s)         & \
                                               (PROBNNk            > %(Km_MinProbNNk)s)' % config )


        self.myProtons = Selection( 'ProtonsFor'+name,
                                    Algorithm = trkFilterP,
                                    RequiredSelections = [StdLooseANNProtons] )
        
        self.myPions   = Selection( 'PionsFor'+name,
                                    Algorithm = trkFilterPi,
                                    RequiredSelections = [StdLooseANNPions] )
        
        self.myKaons = Selection( 'KaonsFor'+name,
                                    Algorithm = trkFilterK,
                                    RequiredSelections = [StdAllLooseANNKaons] )

        self.makeB2ppipipi(name, config)

        self.makeBs2st2BK(name, config)

        self.lineBs2st2pKpipipi = StrippingLine(name+'Line',
                                         prescale         = config['Prescale'],
                                         postscale        = config['Postscale'],
                                         selection        = self.SelBs2pKpipipi)
        
        self.lineBs2st2pKpipipiSS = StrippingLine(name+'SSLine',
                                         prescale         = config['Prescale'],
                                         postscale        = config['Postscale'],
                                         selection        = self.SelBs2pKpipipiSS)
        
        self.registerLine(self.lineBs2st2pKpipipi)
        self.registerLine(self.lineBs2st2pKpipipiSS)
        
        self.lineBs2st2pKpipipiWS = StrippingLine(name+'WSLine',
                                               prescale         = config['Prescale'],
                                               postscale        = config['Postscale'],
                                               selection        = self.SelBs2pKpipipiWS)
        
        self.lineBs2st2pKpipipiWSSS = StrippingLine(name+'WSSSLine',
                                                 prescale         = config['Prescale'],
                                                 postscale        = config['Postscale'],
                                                 selection        = self.SelBs2pKpipipiWSSS)
        
        self.registerLine(self.lineBs2st2pKpipipiWS)
        self.registerLine(self.lineBs2st2pKpipipiWSSS)
        
        
    def makeB2ppipipi(self, name, config) :
        '''Make the ppipipi combinations, both ++ (for B+-> Lc- p+ (3pi)+) and 0 total charge'''
        
        # Define all the cuts
        _mass12CutPreVtx          = '(AM < (%s - 280)*MeV)'                                   % config['B_MaxM_4body']
        _doca12chi2CutPreVtx      = '(ACHI2DOCA(1,2) < %s)'                                   % config['B_MaxDOCAChi2']
        _combCuts12               = _mass12CutPreVtx+' & '+_doca12chi2CutPreVtx
        
        _mass123CutPreVtx         = '(AM < (%s - 140)*MeV)'                                   % config['B_MaxM_4body']
        _doca123chi2CutPreVtx13   = '(ACHI2DOCA(1,3) < %s)'                                   % config['B_MaxDOCAChi2']
        _doca123chi2CutPreVtx23   = '(ACHI2DOCA(2,3) < %s)'                                   % config['B_MaxDOCAChi2']
        _combCuts123              = _mass123CutPreVtx+' & '+_doca123chi2CutPreVtx13+' & '+_doca123chi2CutPreVtx23
        
        
        _massCutPreVtx            = '(AM > %s*MeV) & (AM             < %s*MeV)'               % (config['B_MinM_4body'], config['B_MaxM_4body'])
        _sumptCutPreVtx           = '( (APT1 + APT2 + APT3 + APT4) > %s*MeV)'                 % config['B_MinSumPT_4body']
        _ptCutPreVtx              = '(APT            > %s*MeV)'                               % config['B_MinPT_4body']
        _docachi2CutPreVtx14      = '(ACHI2DOCA(1,4) < %s)'                                   % config['B_MaxDOCAChi2']
        _docachi2CutPreVtx24      = '(ACHI2DOCA(2,4) < %s)'                                   % config['B_MaxDOCAChi2']
        _docachi2CutPreVtx34      = '(ACHI2DOCA(3,4) < %s)'                                   % config['B_MaxDOCAChi2']
        _combCuts                 = _massCutPreVtx+' & '+_sumptCutPreVtx+' & '+_ptCutPreVtx+' & '+\
                                    _docachi2CutPreVtx14+' & '+_docachi2CutPreVtx24+' & '+_docachi2CutPreVtx34
        
        _vtxChi2CutPostVtx        = '(VFASPF(VCHI2/VDOF)  < %s)'                                   % config['B_MaxVtxChi2']
        _fdChi2CutPostVtx         = '(BPVVDCHI2      > %s)'                                   % config['B_MinVDChi2']
        _diraCutPostVtx           = '(BPVDIRA        > %s)'                                   % config['B_MinDira']
        _motherCuts               = _vtxChi2CutPostVtx+' & '+_fdChi2CutPostVtx+' & '+_diraCutPostVtx
        
        _B                       = DaVinci__N4BodyDecays()
        _B.Combination12Cut      = _combCuts12
        _B.Combination123Cut     = _combCuts123
        _B.CombinationCut        = _combCuts
        _B.MotherCut             = _motherCuts
        _B.DecayDescriptor       = "[B+ -> p+ pi- pi+ pi-]cc"
        _B.ReFitPVs              = True

        _B_pp                       = DaVinci__N4BodyDecays()
        _B_pp.Combination12Cut      = _combCuts12
        _B_pp.Combination123Cut     = _combCuts123
        _B_pp.CombinationCut        = _combCuts
        _B_pp.MotherCut             = _motherCuts
        _B_pp.DecayDescriptor       = "[B+ -> p+ pi+ pi+ pi-]cc"
        _B_pp.ReFitPVs              = True
        
        self.selB2ppipipi           = Selection("ppipipi_for_"+name, Algorithm = _B, RequiredSelections = [self.myProtons, self.myPions])
        self.selB2ppipipi_plusplus  = Selection("ppipipi_pp_for_"+name, Algorithm = _B_pp, RequiredSelections = [self.myProtons, self.myPions])
        
    def makeBs2st2BK(self, name, config):
        '''Make the BK opposite and same sign combinations for B -> ppipipi'''
        _dmCut = '(AM - AM1 < %s*MeV)' % config['BK_MaxDM']
        
        _pperpMaxCut = '(Km_pperp < %s*MeV)' % config['Km_MaxPperp']
        _pperpMinCut = '(Km_pperp > %s*MeV)' % config['Km_MinPperp']
        _pperpCut = _pperpMaxCut + ' & ' + _pperpMinCut
        
        _pvzCut = '(abs(ACHILD(BPV(VZ),1)-ACHILD(BPV(VZ),2))< %s*mm)' % config['BK_MaxPVDZ']
        
        _ptCut = '(PT > %s*MeV)' % config['BK_MinPT']
        
        self.Bs2pKpipipi = CombineParticles( DecayDescriptor = "[B*_s20 -> B+ K-]cc")
        self.Bs2pKpipipi.CombinationCut = _dmCut + ' & ' + _pperpCut + ' & ' + _pvzCut
        self.Bs2pKpipipi.MotherCut      = _ptCut
        self.Bs2pKpipipi.ReFitPVs       = False

        ## Calculates the K- momentum perpendicular to B direction. Bs2* decays have a max at ~280 MeV
        self.Bs2pKpipipi.Preambulo = [ "from LoKiPhys.decorators import *",
                                    "dx = (ACHILD(VFASPF(VX),1)-ACHILD(BPV(VX),1))",
                                    "dy = (ACHILD(VFASPF(VY),1)-ACHILD(BPV(VY),1))",
                                    "dz = (ACHILD(VFASPF(VZ),1)-ACHILD(BPV(VZ),1))",
                                    "norm = (max(sqrt(dx*dx+dy*dy+dz*dz),0))",
                                    "B_xdir  = ((dx)/norm)",
                                    "B_ydir  = ((dy)/norm)",
                                    "B_zdir  = ((dz)/norm)",
                                    "pcos = (B_xdir*ACHILD(PX,2) + B_ydir*ACHILD(PY,2) + B_zdir*ACHILD(PZ,2))",
                                    "Km_pperp = sqrt(ACHILD(P2,2) - pow(pcos,2))",
        ]
        
        self.SelBs2pKpipipi = Selection(name,
                                      Algorithm          = self.Bs2pKpipipi,
                                      RequiredSelections = [self.selB2ppipipi_plusplus, self.myKaons])

        self.Bs2pKpipipiSS = CombineParticles( DecayDescriptor = "[B*_s20 -> B+ K+]cc")
        self.Bs2pKpipipiSS.CombinationCut = self.Bs2pKpipipi.CombinationCut
        self.Bs2pKpipipiSS.MotherCut      = self.Bs2pKpipipi.MotherCut
        self.Bs2pKpipipiSS.ReFitPVs       = self.Bs2pKpipipi.ReFitPVs
        self.Bs2pKpipipiSS.Preambulo      = self.Bs2pKpipipi.Preambulo
        
        self.SelBs2pKpipipiSS = Selection(name + "SS",
                                       Algorithm          = self.Bs2pKpipipiSS,
                                       RequiredSelections = [self.selB2ppipipi_plusplus, self.myKaons])

        ## WS (0 charge) B versions
        self.SelBs2pKpipipiWS = Selection(name + "WS",
                                      Algorithm          = self.Bs2pKpipipi,
                                      RequiredSelections = [self.selB2ppipipi, self.myKaons])
        self.SelBs2pKpipipiWSSS = Selection(name + "WSSS",
                                       Algorithm          = self.Bs2pKpipipiSS,
                                       RequiredSelections = [self.selB2ppipipi, self.myKaons])
        
