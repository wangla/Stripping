###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Speculative code for charmed baryon searches.
The following six channels are used:
  Lambda_c+ -> Lambda0 mu nu
  Lambda_c+ -> p+  h-  mu nu
  Xi_c+     -> Lambda0 mu nu
  Xi_c0     -> Xi-     mu nu
  Omega_c0  -> Omega-  mu nu
  Omega_c0  -> Xi-     mu nu
The following channels are used as the normalization channels:
  Lambda_c+ -> Lambda0 pi+
  Lambda_c+ -> p+  h-  pi+
  Xi_c+     -> Lambda0 pi+
  Xi_c0     -> Xi-     pi+
  Omega_c0  -> Omega-  pi+
  Omega_c0  -> Xi-     pi+
where the daughter baryon is reconstructed via:
  Lambda0 -> p pi- (from StdLooseLambdaLL/DD)
  Xi-     -> Lambda0 pi-
  Oemga-  -> Lambda0 K-
'''


__author__ = ['Xiao-Rui Lyu', 'Miroslav Saur', 'Ziyi Wang']
__date__ = '2021/06/09'
__version__ = '$Revision: 0.1 $'
__all__ = ('StrippingCharmedBaryonSL'
           ,'default_config')


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdLooseKaons, StdLooseDownKaons
from StandardParticles import  StdAllLoosePions, StdAllLooseKaons, StdLooseMuons, StdAllLooseProtons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
import sys


default_name='CharmedBaryonSL'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        : 'CharmedBaryonSL',
      'WGs'         : ['Charm'],
      'BUILDERTYPE' : 'StrippingCharmedBaryonSL',
      'STREAMS'     : ['CharmCompleteEvent'],
      'CONFIG'      : {
          'GEC_nLongTrk' : 250    # adimensional
        , 'signalPrescaleViaLc'           :   1.0
        , 'signalPrescaleViaLcL0DDMuNu'   :   0.8
        , 'signalPrescaleViaLcPPiMuNu'    :   0.25
        , 'signalPrescaleViaOmegac0'      :   1.0
        , 'signalPrescaleViaXic'          :   1.0
        , 'signalPrescaleViaXicL0LLMuNu'  :   0.8
        , 'signalPrescaleViaXicL0DDMuNu'  :   0.8
        , 'controlPrescaleViaLcL0LLPiNC'  :   0.4 
        , 'controlPrescaleViaLcL0DDPiNC'  :   0.8 
        , 'controlPrescaleViaLcPKPiNC'    :   0.09
        , 'controlPrescaleViaLcPPiPiNC'   :   0.06
        , 'controlPrescaleViaOmegac0NC'   :   1.0
        , 'controlPrescaleViaXicL0LLPiNC' :   0.4 
        , 'controlPrescaleViaXicL0DDPiNC' :   0.8 
        , 'controlPrescaleViaXicXiPiNC'   :   1.0
        , 'TRCHI2DOFMax'            :   3.0
        , 'GhostProb'               :   0.3
        , 'TrGhostProbMax'          :   0.25 # same for all particles
        , 'MINIPCHI2'               :   4.0  # adimensiional, orig. 9 
        , 'MuonP'                   :   3.0*GeV
        , 'MuonPT'                  :   500*MeV
        , 'MuonIPCHI2'              :   5.0 
        , 'MuonPIDmu'               :   0.0
        , 'PionP'                   :   3.0*GeV
        , 'PionPT'                  :   500*MeV
        , 'PionPIDK'                :  10.0
        , 'Pion_PIDpiPIDK_Min'      :   0.0
        , 'KaonP'                   :   3.0*GeV
        , 'KaonPT'                  :   500*MeV
        , 'KaonPIDK'                :  10.0
        , 'Kaon_PIDKPIDpi_Min'      :   5.0
        , 'ProtonP'                 :   3.0*GeV
        , 'ProtonPT'                :   500*MeV
        , 'Proton_PIDpPIDpi_Min'    :   5.0
        , 'Proton_PIDpPIDK_Min'     :   0.0
        , 'ProbNNpMin'              :   0.2 #ProbNNp cut for proton in L0, to suppress the bkg of L0 
        , 'ProbNNp'                 :   0.4
        , 'ProbNNk'                 :   0.4
        , 'ProbNNpi'                :   0.5
        , 'ProbNNpiMax'             :   0.95
        , 'LambdaLLPMin'            :5000.0*MeV
        , 'LambdaLLPTMin'           : 800.0*MeV
        , 'LambdaLLCutMass'         :  20.0*MeV
        , 'LambdaLLCutFDChi2'       : 100.0 ## unitless
        , 'LambdaDDPMin'            :5000.0*MeV
        , 'LambdaDDPTMin'           : 800.0*MeV
        , 'LambdaDDCutMass'         :  20.0*MeV
        , 'LambdaDDCutFDChi2'       : 100.0## unitless
        , 'LambdaLDPMin'            :3000.0*MeV
        , 'LambdaLDPTMin'           : 800.0*MeV
        , 'LambdaLDCutMass'         :  20.0*MeV
        , 'LambdaLDCutFDChi2'       : 100.0## unitless
        , 'LambdaDaugTrackChi2'     :   4.0## unitless
        , 'LambdaVertexChi2'        :   5.0## max chi2/ndf for Lambda0 vertex
        , 'LambdaCutDIRA'           :   0.99## unitless
        , 'LambdaLLMinVZ'           :-100.0*mm
        , 'LambdaLLMaxVZ'           : 400.0*mm
        , 'LambdaDDMinVZ'           : 400.0*mm
        , 'LambdaDDMaxVZ'           :2275.0*mm
        , 'LambdaPr_PT_MIN'         : 500.0*MeV
        , 'LambdaPi_PT_MIN'         : 100.0*MeV
        , 'Lc_ADMASS_HalfWin'       :  80.0*MeV
        , 'Lc_Daug_1of3_MIPCHI2DV_Min': 4.0
        , 'Lc_ADOCAMAX_Max'         :   0.5*mm
        , 'Lc_APT_Min'              :   1.0*GeV
        , 'Lc_VCHI2_Max'            :  30.0
        , 'Lc_BPVVDCHI2_Min'        :  16.0
        , 'Lc_BPVDIRA_Min'          :   0.9
        , 'Omegac0_ADAMASS_HalfWin' : 170.0*MeV
        , 'Omegac0_ADMASS_HalfWin'  : 120.0*MeV
        , 'Omegac0_4Dau_VCHI2_Max'  :  60.0
        , 'Omegac0_BPVVDCHI2_Min'   :  25.0
        , 'Omegac0_BPVDIRA_Min'     :   0.9
        , 'Xic_ADAMASS_HalfWin'     : 170.0*MeV
        , 'Xic_ADMASS_HalfWin'      : 120.0*MeV
        , 'Xic_BPVVDCHI2_Min'       :  25.0
        , 'Xic_BPVDIRA_Min'         :   0.9
        , 'L0Mu_AM_Min'             :  1250.0*MeV
        , 'pKMu_AM_Min'             :  1550.0*MeV
        , 'ppiMu_AM_Min'            :  1200.0*MeV
        , 'XiMu_AM_Min'             :  1430.0*MeV
        , 'OmMu_AM_Min'             :  1780.0*MeV
        , 'Lc_AM_Max'               :  2370.0*MeV
        , 'Oc_AM_Max'               :  2780.0*MeV
        , 'Xic_AM_Max'              :  2550.0*MeV
      } ## end of 'CONFIG' 
}  ## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingCharmedBaryonSL(LineBuilder) : 
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
       
        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                     "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        ##########################################################################
        ## Basic particles: mu, K, pi, p
        ##########################################################################
        self.selMuon = Selection( "SelMufor" + name,
                Algorithm = self._muonFilter("Mufor"+name),
                RequiredSelections = [StdLooseMuons])

        self.selKaon = Selection( "SelKfor" + name,
                Algorithm = self._kaonFilter("Kfor"+name),
                RequiredSelections = [StdAllLooseKaons])

        self.selPion = Selection( "SelPifor" + name,
                Algorithm = self._pionFilter("Pifor"+name),
                RequiredSelections = [StdAllLoosePions])

        self.selProton = Selection( "SelProtonfor" + name,
                Algorithm = self._protonFilter("Protonfor"+name),
                RequiredSelections = [StdAllLooseProtons])

        ##########################################################################
        ## Lambda0 -> p+ pi- 
        ##########################################################################
        _stdLooseLambdaLL = DataOnDemand("Phys/StdLooseLambdaLL/Particles")
        _stdLooseLambdaDD = DataOnDemand("Phys/StdLooseLambdaDD/Particles")
        _stdLooseLambdaLD = DataOnDemand("Phys/StdLooseLambdaLD/Particles")

        self.selLambdaLL = Selection("SelLambdaLLfor"+name,
                Algorithm = self._LambdaLLFilter("LambdaLLfor"+name),
                RequiredSelections = [_stdLooseLambdaLL])

        self.selLambdaDD = Selection("SelLambdaDDfor"+name,
                Algorithm = self._LambdaDDFilter("LambdaDDfor"+name),
                RequiredSelections = [_stdLooseLambdaDD])

        self.selLambdaLD = Selection("SelLambdaLDfor"+name,
                Algorithm = self._LambdaLDFilter("LambdaLDfor"+name),
                RequiredSelections = [_stdLooseLambdaLD])

        #self.selAllLambda = MergedSelection("SelAllLambdaFor"+name,
        #        RequiredSelections = [ self.selLambdaLL,self.selLambdaDD,self.selLambdaLD])

        self.selAllLambda = MergedSelection("SelAllLambdaFor"+name,
                RequiredSelections = [ self.selLambdaLL,self.selLambdaDD ])

        ##########################################################################
        ## Xi- -> Lambda0 pi- 
        ##########################################################################
        self.Ximinus2LambdaPi = self.createCombinationSel(OutputList = "Ximinus2LambdaPifor"+ self.name,
                DecayDescriptor = "[Xi- -> Lambda0 pi-]cc",
                DaughterLists   = [self.selPion, self.selAllLambda],
                DaughterCuts    = {"pi-" : "(MIPCHI2DV(PRIMARY)>4) "},
                PreVertexCuts   = "(ADAMASS('Xi-') < 80*MeV) & (ADOCACHI2CUT(30, ''))",
                PostVertexCuts  = "(ADMASS('Xi-')<64*MeV)"\
                                "& (VFASPF(VCHI2)<25)"\
                                "& (P> 1000*MeV) & (PT > 250*MeV)"\
                                "& (BPVVDCHI2 > 49.0 )"\
                                "& (VFASPF(VCHI2/VDOF) < 12.0)" )

        ##########################################################################
        ## Omega- -> Lambda0 K- 
        ##########################################################################
        self.Omegaminus2LambdaK = self.createCombinationSel(OutputList = "Omegaminus2LambdaKfor"+ self.name,
                DecayDescriptor = "[Omega- -> Lambda0 K-]cc",
                DaughterLists   = [self.selKaon, self.selAllLambda],
                DaughterCuts    = {"K-"      : "(MIPCHI2DV(PRIMARY)>4) "},
                PreVertexCuts   = "(ADAMASS('Omega-') < 80*MeV) & (ADOCACHI2CUT(30, ''))",
                PostVertexCuts  = "(ADMASS('Omega-')<64*MeV)"\
                                "& (VFASPF(VCHI2)<25)"\
                                "& (P> 1000*MeV) &(PT > 250*MeV)"\
                                "& (BPVVDCHI2 > 49.0 )"\
                                "& (VFASPF(VCHI2/VDOF) < 12.0)" )

        self.LambdacSLList = self.makeLambdacSL()
        self.Omegac0SLList = self.makeOmegac0SL()
        self.XicSLList = self.makeXicSL()

    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    # Sub Function
    #------------------------------------------------------------------------------------------
    #------------------------------------------------------------------------------------------
    def _muonFilter( self , _name):
        _code = "  (TRCHI2DOF< %(TRCHI2DOFMax)s)"\
                "& (PT>%(MuonPT)s) & (P>%(MuonP)s)"\
                "& (TRGHOSTPROB< %(GhostProb)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MuonIPCHI2)s)"\
                "& (PIDmu > %(MuonPIDmu)s)" % self.config
        _mu = FilterDesktop(Code = _code )
        return _mu

    def _pionFilter( self , _name):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(PionP)s) & (PT > %(PionPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK< %(PionPIDK)s)"\
                "& (PROBNNpi > %(ProbNNpi)s)" % self.config
        _pi = FilterDesktop(Code = _code )
        return _pi

    def _kaonFilter( self , _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (P>%(KaonP)s) & (PT > %(KaonPT)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PIDK> %(KaonPIDK)s) & (PROBNNpi < %(ProbNNpiMax)s)"\
                "& (PROBNNk > %(ProbNNk)s)"\
                "& (HASRICH)&(PIDK-PIDpi>%(Kaon_PIDKPIDpi_Min)s)"% self.config
        _ka = FilterDesktop(Code = _code )
        return _ka

    def _protonFilter( self, _name ):
        _code = "  (TRCHI2DOF < %(TRCHI2DOFMax)s)"\
                "& (PT > %(ProtonPT)s) & (P>%(ProtonP)s)"\
                "& (TRGHOSTPROB< %(TrGhostProbMax)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)"\
                "& (PROBNNp > %(ProbNNp)s) & (PROBNNpi < %(ProbNNpiMax)s)"\
                "& (HASRICH)&(PIDp-PIDpi>%(Proton_PIDpPIDpi_Min)s)"\
                "& (HASRICH)&(PIDp-PIDK>%(Proton_PIDpPIDK_Min)s)"% self.config
        _pr = FilterDesktop(Code = _code)
        return _pr

    def _LambdaLLFilter( self, _name):
        _code = " (P > %(LambdaLLPMin)s) & (PT > %(LambdaLLPTMin)s)" \
                " &(ADMASS('Lambda0')<%(LambdaLLCutMass)s)"\
                " &(BPVVDCHI2> %(LambdaLLCutFDChi2)s)" \
                " & (MAXTREE( 'p+'==ABSID, PT) > %(LambdaPr_PT_MIN)s ) "\
                " & (MAXTREE('pi-'==ABSID, PT) > %(LambdaPi_PT_MIN)s ) " \
                " & (MAXTREE( 'p+'==ABSID, PROBNNp) > %(ProbNNpMin)s ) "\
                " & (MINTREE('pi-'==ABSID, TRGHOSTPROB) < %(TrGhostProbMax)s )"\
                " & (MINTREE( 'p+'==ABSID, TRGHOSTPROB) < %(TrGhostProbMax)s )"\
                " & (MINTREE('pi-'==ABSID, TRCHI2DOF) < %(LambdaDaugTrackChi2)s )"\
                " & (MINTREE( 'p+'==ABSID, TRCHI2DOF) < %(LambdaDaugTrackChi2)s )"\
                " & (VFASPF(VCHI2PDOF) < %(LambdaVertexChi2)s)" \
                " & (VFASPF(VZ) > %(LambdaLLMinVZ)s ) " \
                " & (VFASPF(VZ) < %(LambdaLLMaxVZ)s ) " \
                " & (BPVDIRA > %(LambdaCutDIRA)s )" % self.config
        _l0LL = FilterDesktop(Code = _code)
        return _l0LL

    def _LambdaDDFilter( self , _name):
        _code = " (P> %(LambdaDDPMin)s) & (PT> %(LambdaDDPTMin)s)" \
                " & (ADMASS('Lambda0') < %(LambdaDDCutMass)s) "\
                " & (BPVVDCHI2> %(LambdaDDCutFDChi2)s)" \
                " & (MAXTREE( 'p+'==ABSID, PT) > %(LambdaPr_PT_MIN)s ) "\
                " & (MAXTREE('pi-'==ABSID, PT) > %(LambdaPi_PT_MIN)s ) " \
                " & (MAXTREE( 'p+'==ABSID, PROBNNp) > %(ProbNNpMin)s ) "\
                " & (MINTREE('pi-'==ABSID, TRCHI2DOF) < %(LambdaDaugTrackChi2)s )"\
                " & (MINTREE( 'p+'==ABSID, TRCHI2DOF) < %(LambdaDaugTrackChi2)s )"\
                " & (VFASPF(VCHI2PDOF) < %(LambdaVertexChi2)s)" \
                " & (VFASPF(VZ) < %(LambdaDDMaxVZ)s ) " \
                " & (VFASPF(VZ) > %(LambdaDDMinVZ)s )" \
                " & (BPVDIRA > %(LambdaCutDIRA)s )" % self.config
        _l0DD = FilterDesktop(Code = _code)
        return _l0DD

    def _LambdaLDFilter( self , _name):
        _code = " (P> %(LambdaLDPMin)s) & (PT> %(LambdaLDPTMin)s)" \
                " & (ADMASS('Lambda0') < %(LambdaLDCutMass)s)"\
                " & (BPVVDCHI2> %(LambdaLDCutFDChi2)s)" \
                " & CHILDCUT((TRCHI2DOF < %(LambdaDaugTrackChi2)s),1)" \
                " & CHILDCUT((TRCHI2DOF < %(LambdaDaugTrackChi2)s),2)" \
                " & CHILDCUT(('p+'==ABSID)&(PROBNNp>%(ProbNNpMin)s),1)"\
                " & (VFASPF(VCHI2PDOF) < %(LambdaVertexChi2)s)" \
                " & (BPVDIRA > %(LambdaCutDIRA)s )" % self.config
        _l0LD = FilterDesktop(Code = _code)
        return _l0LD

    ##------------------------------------------------------------------------------------------
    ##  -------------------- Begin to LambdacSL  ------------
    def makeLambdacSL( self ):

        _strCutCombfor = "(AMAXCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                    "& (ADOCAMAX('')<%(Lc_ADOCAMAX_Max)s)" \
                    "& (APT>%(Lc_APT_Min)s)" % self.config

        _strCutMothfor = "(VFASPF(VCHI2) < %(Lc_VCHI2_Max)s)" \
                      "& (BPVVDCHI2>%(Lc_BPVVDCHI2_Min)s)" \
                      "& (BPVDIRA>%(Lc_BPVDIRA_Min)s)" % self.config

        _strCutComb_Mass   = "(ADAMASS('Lambda_c+')<1.1*%(Lc_ADMASS_HalfWin)s)" % self.config
        _strCutMoth_Mass   = "(ADMASS('Lambda_c+')<%(Lc_ADMASS_HalfWin)s)" % self.config
        
        _strCutComb   = _strCutCombfor + '&' + _strCutComb_Mass
        _strCutMoth   = _strCutMothfor + '&' + _strCutMoth_Mass
        
        _strCutComb_MassL0MuSL  = "(AM>%(L0Mu_AM_Min)s)  & (AM<%(Lc_AM_Max)s)" % self.config
        _strCutComb_MasspKMuSL  = "(AM>%(pKMu_AM_Min)s)  & (AM<%(Lc_AM_Max)s)" % self.config
        _strCutComb_MassppiMuSL = "(AM>%(ppiMu_AM_Min)s) & (AM<%(Lc_AM_Max)s)" % self.config

        _strCutComb_L0MuSL  = _strCutCombfor + '&' + _strCutComb_MassL0MuSL 
        _strCutComb_pKMuSL  = _strCutCombfor + '&' + _strCutComb_MasspKMuSL 
        _strCutComb_ppiMuSL = _strCutCombfor + '&' + _strCutComb_MassppiMuSL
        _strCutMothSL = _strCutMothfor

        ''' Stripping Lambda_c+ -> Lambda0(LL) mu+ '''
        Lambdac2LambdaLLMuNu = self.createCombinationSel(OutputList = "Lambdac2LambdaLLMuNu" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 mu+]cc",
                DaughterLists   = [self.selLambdaLL, self.selMuon],
                PreVertexCuts   = _strCutComb_L0MuSL,
                PostVertexCuts  = _strCutMothSL )
        Lambdac2LambdaLLMuNuLine = StrippingLine( self.name + "Lambdac2LambdaLLMuNuLine", 
                prescale = self.config['signalPrescaleViaLc'],
                algos = [ Lambdac2LambdaLLMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2LambdaLLMuNuLine)

        ''' Stripping Lambda_c+ -> Lambda0(DD) mu+ '''
        Lambdac2LambdaDDMuNu = self.createCombinationSel(OutputList = "Lambdac2LambdaDDMuNu" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 mu+]cc",
                DaughterLists   = [self.selLambdaDD, self.selMuon],
                PreVertexCuts   = _strCutComb_L0MuSL,
                PostVertexCuts  = _strCutMothSL )
        Lambdac2LambdaDDMuNuLine = StrippingLine( self.name + "Lambdac2LambdaDDMuNuLine", 
                prescale = self.config['signalPrescaleViaLcL0DDMuNu'],
                algos = [ Lambdac2LambdaDDMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2LambdaDDMuNuLine)

        ''' Stripping Lambda_c+ -> p+ K- mu+ '''
        Lambdac2PKMuNu = self.createCombinationSel(OutputList = "Lambdac2PKMuNu" + self.name,
                DecayDescriptor = "[Lambda_c+ -> p+ K- mu+]cc",
                DaughterLists   = [self.selProton, self.selKaon, self.selMuon],
                PreVertexCuts   = _strCutComb_pKMuSL,
                PostVertexCuts  = _strCutMothSL )
        Lambdac2PKMuNuLine = StrippingLine( self.name + "Lambdac2PKMuNuLine", 
                prescale = self.config['signalPrescaleViaLc'],
                algos = [ Lambdac2PKMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2PKMuNuLine)

        ''' Stripping Lambda_c+ -> p+ pi- mu+ '''
        Lambdac2PPiMuNu = self.createCombinationSel(OutputList = "Lambdac2PPiMuNu" + self.name,
                DecayDescriptor = "[Lambda_c+ -> p+ pi- mu+]cc",
                DaughterLists   = [self.selProton, self.selPion, self.selMuon],
                PreVertexCuts   = _strCutComb_ppiMuSL,
                PostVertexCuts  = _strCutMothSL )
        Lambdac2PPiMuNuLine = StrippingLine( self.name + "Lambdac2PPiMuNuLine", 
                prescale = self.config['signalPrescaleViaLcPPiMuNu'],
                algos = [ Lambdac2PPiMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2PPiMuNuLine)

        ''' ---------------------- '''
        ''' Normalization channels '''
        ''' ---------------------- '''
        ''' NC Stripping Lambda_c+ -> Lambda0(LL) pi+ '''
        Lambdac2LambdaLLPiNC = self.createCombinationSel(OutputList = "Lambdac2LambdaLLPiNC" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 pi+]cc",
                DaughterLists   = [self.selLambdaLL, self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        Lambdac2LambdaLLPiNCLine = StrippingLine( self.name + "Lambdac2LambdaLLPiNCLine", 
                prescale = self.config['controlPrescaleViaLcL0LLPiNC'],
                algos = [ Lambdac2LambdaLLPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2LambdaLLPiNCLine)

        ''' NC Stripping Lambda_c+ -> Lambda0(DD) pi+ '''
        Lambdac2LambdaDDPiNC = self.createCombinationSel(OutputList = "Lambdac2LambdaDDPiNC" + self.name,
                DecayDescriptor = "[Lambda_c+ -> Lambda0 pi+]cc",
                DaughterLists   = [self.selLambdaDD, self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        Lambdac2LambdaDDPiNCLine = StrippingLine( self.name + "Lambdac2LambdaDDPiNCLine", 
                prescale = self.config['controlPrescaleViaLcL0DDPiNC'],
                algos = [ Lambdac2LambdaDDPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2LambdaDDPiNCLine)

        ''' NC Stripping Lambda_c+ -> p+ K- pi+ '''
        Lambdac2PKPiNC = self.createCombinationSel(OutputList = "Lambdac2PKPiNC" + self.name,
                DecayDescriptor = "[Lambda_c+ -> p+ K- pi+]cc",
                DaughterLists   = [self.selProton, self.selKaon, self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        Lambdac2PKPiNCLine = StrippingLine( self.name + "Lambdac2PKPiNCLine", 
                prescale = self.config['controlPrescaleViaLcPKPiNC'],
                algos = [ Lambdac2PKPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2PKPiNCLine)

        ''' NC Stripping Lambda_c+ -> p+ pi- pi+ '''
        Lambdac2PPiPiNC = self.createCombinationSel(OutputList = "Lambdac2PPiPiNC" + self.name,
                DecayDescriptor = "[Lambda_c+ -> p+ pi- pi+]cc",
                DaughterLists   = [self.selProton, self.selPion, self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        Lambdac2PPiPiNCLine = StrippingLine( self.name + "Lambdac2PPiPiNCLine", 
                prescale = self.config['controlPrescaleViaLcPPiPiNC'],
                algos = [ Lambdac2PPiPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Lambdac2PPiPiNCLine)
    ##  --------------------  End of LambdacSL  ------------
    ##------------------------------------------------------------------------------------------

    ##------------------------------------------------------------------------------------------
    ## --------------------  Begin to makeOmegac0SL  ------------
    def makeOmegac0SL( self ):

        _strCutCombfor = "( (AMAXCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                    "& (ADOCAMAX('')<%(Lc_ADOCAMAX_Max)s)"\
                    "& (APT>%(Lc_APT_Min)s) )" % self.config
        _strCutMothfor = "( (VFASPF(VCHI2)<%(Omegac0_4Dau_VCHI2_Max)s)" \
                    "& (BPVVDCHI2>%(Omegac0_BPVVDCHI2_Min)s)" \
                    "& (BPVDIRA>%(Omegac0_BPVDIRA_Min)s) )" % self.config
        
        _strCutComb_Mass = "(ADAMASS('Omega_c0') < %(Omegac0_ADAMASS_HalfWin)s )" % self.config
        _strCutMoth_Mass = "(ADMASS('Omega_c0') < %(Omegac0_ADMASS_HalfWin)s )" % self.config

        _strCutComb   = _strCutCombfor + '&' + _strCutComb_Mass
        _strCutMoth   = _strCutMothfor + '&' + _strCutMoth_Mass
        
        _strCutComb_MassXiMuSL = "(AM>%(XiMu_AM_Min)s) & (AM<%(Oc_AM_Max)s)" % self.config
        _strCutComb_MassOmMuSL = "(AM>%(OmMu_AM_Min)s) & (AM<%(Oc_AM_Max)s)" % self.config

        _strCutComb_XiMuSL = _strCutCombfor + '&' + _strCutComb_MassXiMuSL
        _strCutComb_OmMuSL = _strCutCombfor + '&' + _strCutComb_MassOmMuSL
        _strCutMothSL = _strCutMothfor

        ''' Stripping Omega_c0 -> Omega- mu+ '''
        Omegac02OmegaMuNu = self.createCombinationSel(OutputList = "Omegac02OmegaMuNu" + self.name,
                DecayDescriptor = "[Omega_c0 -> Omega- mu+]cc",
                DaughterLists   = [self.Omegaminus2LambdaK, self.selMuon],
                PreVertexCuts   = _strCutComb_OmMuSL,
                PostVertexCuts  = _strCutMothSL )
        Omegac02OmegaMuNuLine = StrippingLine( self.name + "Omegac02OmegaMuNuLine", 
                prescale = self.config['signalPrescaleViaOmegac0'],
                algos = [ Omegac02OmegaMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Omegac02OmegaMuNuLine)

        ''' Stripping Omega_c0 -> Xi- mu+ '''
        Omegac02XiMuNu = self.createCombinationSel(OutputList = "Omegac02XiMuNu" + self.name,
                DecayDescriptor = "[Omega_c0 -> Xi- mu+]cc",
                DaughterLists   = [self.Ximinus2LambdaPi, self.selMuon],
                PreVertexCuts   = _strCutComb_XiMuSL,
                PostVertexCuts  = _strCutMothSL )
        Omegac02XiMuNuLine = StrippingLine( self.name + "Omegac02XiMuNuLine", 
                prescale = self.config['signalPrescaleViaOmegac0'],
                algos = [ Omegac02XiMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Omegac02XiMuNuLine)

        ''' ---------------------- '''
        ''' Normalization channels '''
        ''' ---------------------- '''
        ''' NC Stripping Omega_c0 -> Omega- pi+ '''
        Omegac02OmegaPiNC = self.createCombinationSel(OutputList = "Omegac02OmegaPiNC" + self.name,
                DecayDescriptor = "[Omega_c0 -> Omega- pi+]cc",
                DaughterLists   = [self.Omegaminus2LambdaK, self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        Omegac02OmegaPiNCLine = StrippingLine( self.name + "Omegac02OmegaPiNCLine", 
                prescale = self.config['controlPrescaleViaOmegac0NC'],
                algos = [ Omegac02OmegaPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Omegac02OmegaPiNCLine)

        ''' NC Stripping Omega_c0 -> Xi- pi+ '''
        Omegac02XiPiNC = self.createCombinationSel(OutputList = "Omegac02XiPiNC" + self.name,
                DecayDescriptor = "[Omega_c0 -> Xi- pi+]cc",
                DaughterLists   = [self.Ximinus2LambdaPi, self.selPion],
                PreVertexCuts   = _strCutComb,
                PostVertexCuts  = _strCutMoth )
        Omegac02XiPiNCLine = StrippingLine( self.name + "Omegac02XiPiNCLine", 
                prescale = self.config['controlPrescaleViaOmegac0NC'],
                algos = [ Omegac02XiPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Omegac02XiPiNCLine)
    ## --------------------  End of makeOmegac0SL  ------------
    ##------------------------------------------------------------------------------------------

    ##------------------------------------------------------------------------------------------
    ## --------------------  Begin to makeXicSL  ------------
    def makeXicSL( self ):
        #Cut for Basic 
        _strCutCombfor = "( (AMAXCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                         "& (ADOCAMAX('')<%(Lc_ADOCAMAX_Max)s)" \
                         "& (APT>%(Lc_APT_Min)s) )" % self.config
        _strCutMothfor = "( (VFASPF(VCHI2)<%(Omegac0_4Dau_VCHI2_Max)s)" \
                         "& (BPVVDCHI2>%(Xic_BPVVDCHI2_Min)s)" \
                         "& (BPVDIRA>%(Xic_BPVDIRA_Min)s) )" % self.config

        _strCutComb_XicpMass = "(ADAMASS('Xi_c+') < %(Xic_ADAMASS_HalfWin)s )" % self.config
        _strCutMoth_XicpMass = "(ADMASS('Xi_c+') < %(Xic_ADMASS_HalfWin)s)" % self.config

        _strCutComb_Xic0Mass = "(ADAMASS('Xi_c0') < %(Xic_ADAMASS_HalfWin)s )" % self.config
        _strCutMoth_Xic0Mass = "(ADMASS('Xi_c0') < %(Xic_ADMASS_HalfWin)s)" % self.config
        
        _strCutCombforXicZero   = _strCutCombfor + '&' + _strCutComb_Xic0Mass
        _strCutMothforXicZero   = _strCutMothfor + '&' + _strCutMoth_Xic0Mass
        
        _strCutCombforXicPlus   = _strCutCombfor + '&' + _strCutComb_XicpMass
        _strCutMothforXicPlus   = _strCutMothfor + '&' + _strCutMoth_XicpMass

        _strCutComb_MassXiMuSL = "(AM>%(XiMu_AM_Min)s) & (AM<%(Xic_AM_Max)s)" % self.config
        _strCutComb_MassL0MuSL = "(AM>%(L0Mu_AM_Min)s) & (AM<%(Xic_AM_Max)s)" % self.config
        
        _strCutComb_XiMuSL = _strCutCombfor + '&' + _strCutComb_MassXiMuSL
        _strCutComb_L0MuSL = _strCutCombfor + '&' + _strCutComb_MassL0MuSL
        _strCutMothSL = _strCutMothfor

        ''' Stripping Xi_c+ -> Lambda0(LL) mu+ '''
        Xic2LambdaLLMuNu = self.createCombinationSel(OutputList = "Xic2LambdaLLMuNu" + self.name,
                DecayDescriptor = "[Xi_c+ -> Lambda0 mu+]cc",
                DaughterLists   = [self.selLambdaLL, self.selMuon],
                PreVertexCuts   = _strCutComb_L0MuSL,
                PostVertexCuts  = _strCutMothSL )
        Xic2LambdaLLMuNuLine = StrippingLine( self.name + "Xic2LambdaLLMuNuLine", 
                prescale = self.config['signalPrescaleViaXicL0LLMuNu'],
                algos = [ Xic2LambdaLLMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Xic2LambdaLLMuNuLine)

        ''' Stripping Xi_c+ -> Lambda0(DD) mu+ '''
        Xic2LambdaDDMuNu = self.createCombinationSel(OutputList = "Xic2LambdaDDMuNu" + self.name,
                DecayDescriptor = "[Xi_c+ -> Lambda0 mu+]cc",
                DaughterLists   = [self.selLambdaDD, self.selMuon],
                PreVertexCuts   = _strCutComb_L0MuSL,
                PostVertexCuts  = _strCutMothSL )
        Xic2LambdaDDMuNuLine = StrippingLine( self.name + "Xic2LambdaDDMuNuLine", 
                prescale = self.config['signalPrescaleViaXicL0DDMuNu'],
                algos = [ Xic2LambdaDDMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Xic2LambdaDDMuNuLine)

        ''' Stripping Xi_c0 -> Xi- mu+ '''
        Xic02XiMuNu = self.createCombinationSel(OutputList = "Xic02XiMuNu" + self.name,
                DecayDescriptor = "[Xi_c0 -> Xi- mu+]cc",
                DaughterLists   = [self.Ximinus2LambdaPi, self.selMuon],
                PreVertexCuts   = _strCutComb_XiMuSL,
                PostVertexCuts  = _strCutMothSL )
        Xic02XiMuNuLine = StrippingLine( self.name + "Xic02XiMuNuLine", 
                prescale = self.config['signalPrescaleViaXic'],
                algos = [ Xic02XiMuNu ], 
                EnableFlavourTagging = False )
        self.registerLine (Xic02XiMuNuLine)

        ''' ---------------------- '''
        ''' Normalization channels '''
        ''' ---------------------- '''
        ''' NC Stripping Xi_c+ -> Lambda0(LL) pi+ '''
        Xic2LambdaLLPiNC = self.createCombinationSel(OutputList = "Xic2LambdaLLPiNC" + self.name,
                DecayDescriptor = "[Xi_c+ -> Lambda0 pi+]cc",
                DaughterLists   = [self.selLambdaLL, self.selPion],
                PreVertexCuts   = _strCutCombforXicPlus,
                PostVertexCuts  = _strCutMothforXicPlus )
        Xic2LambdaLLPiNCLine = StrippingLine( self.name + "Xic2LambdaLLPiNCLine", 
                prescale = self.config['controlPrescaleViaXicL0LLPiNC'],
                algos = [ Xic2LambdaLLPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Xic2LambdaLLPiNCLine)

        ''' NC Stripping Xi_c+ -> Lambda0(DD) pi+ '''
        Xic2LambdaDDPiNC = self.createCombinationSel(OutputList = "Xic2LambdaDDPiNC" + self.name,
                DecayDescriptor = "[Xi_c+ -> Lambda0 pi+]cc",
                DaughterLists   = [self.selLambdaDD, self.selPion],
                PreVertexCuts   = _strCutCombforXicPlus,
                PostVertexCuts  = _strCutMothforXicPlus )
        Xic2LambdaDDPiNCLine = StrippingLine( self.name + "Xic2LambdaDDPiNCLine", 
                prescale = self.config['controlPrescaleViaXicL0DDPiNC'],
                algos = [ Xic2LambdaDDPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Xic2LambdaDDPiNCLine)

        ''' NC Stripping Xi_c0 -> Xi- pi+ '''
        Xic02XiPiNC = self.createCombinationSel(OutputList = "Xic02XiPiNC" + self.name,
                DecayDescriptor = "[Xi_c0 -> Xi- pi+]cc",
                DaughterLists   = [self.Ximinus2LambdaPi, self.selPion],
                PreVertexCuts   = _strCutCombforXicZero,
                PostVertexCuts  = _strCutMothforXicZero )
        Xic02XiPiNCLine = StrippingLine( self.name + "Xic02XiPiNCLine", 
                prescale = self.config['controlPrescaleViaXicXiPiNC'],
                algos = [ Xic02XiPiNC ], 
                EnableFlavourTagging = False )
        self.registerLine (Xic02XiPiNCLine)
    ##  --------------------  end of makeXicSL  ------------
    ##------------------------------------------------------------------------------------------

    ##########################################################################
    ## Basic Function
    ##########################################################################
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                Algorithm = filter,
                RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
            DecayDescriptor,
            DaughterLists,
            DaughterCuts = {} ,
            PreVertexCuts = "ALL",
            PostVertexCuts = "ALL") :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                DaughtersCuts = DaughterCuts,
                MotherCut = PostVertexCuts,
                CombinationCut = PreVertexCuts,
                ReFitPVs = True)
        return Selection ( OutputList,
                Algorithm = combiner,
                RequiredSelections = DaughterLists)
    #print "DEBUG here is :",__file__,sys._getframe().f_lineno 
