###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Speculative code for doubly-charmed baryon searches.
The following five final states are used:
  Xicc+  -> Xic0 mu nu
  Xicc++ -> Lambdac+ mu nu
  Xicc++ -> Xic+ mu nu
  Omegacc+ -> Omegac0 mu nu
  Omegacc+ -> Xic0 mu nu
where the daughter charmed baryon is reconstructed via:
  Lambdac+ -> p K- pi+ (from StdLooseLambdac2PKPi)
  Xic+ -> p K- pi+
  Xic0 -> p K- K- pi+
  Oemgac0 -> p K- K- pi+
Additional normalization channels for Omegacc decays are used:
  Omegacc+ -> Omegac0 pi+
  Omegacc+ -> Xic0 pi+
Based on original stripping code done by Patrick Spradlin and Mat Charles and Xicc Turbo lines

2017: part of Charm stream, went to MDST
2018: switched to CharmCompleteEvent, went to DST
'''


__author__ = ['Xiao-Rui Lyu', 'Miroslav Saur', 'Ziyi Wang']
__date__ = '2021/06/21'
__version__ = '$Revision: 0.2 $'
__all__ = ('StrippingXiccSL'
           ,'default_config')


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from StandardParticles import StdLoosePions, StdNoPIDsDownPions, StdLooseKaons, StdLooseDownKaons
from StandardParticles import  StdAllLoosePions, StdAllLooseKaons, StdLooseMuons, StdAllLooseProtons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
from Configurables import TisTosParticleTagger
import sys


default_name='XiccSL'
    #### This is the dictionary of all tunable cuts ########
default_config={
      'NAME'        : 'XiccSL',
      'WGs'         : ['Charm'],
      'BUILDERTYPE' : 'StrippingXiccSL',
      'STREAMS'     : ['CharmCompleteEvent'],
      'CONFIG'      : {
        'LcHlt2TisTosSpec' : { 'Hlt2CharmHadLcpToPpKmPip.*Decision%TOS' : 0, 'Hlt2Global%TIS' : 0 }
        , 'controlPrescaleLc'           : 0.05
        , 'controlPrescaleXic'          : 0.05
        , 'signalPrescaleViaLc'         : 1.0
        , 'signalPrescaleViaXicp'       : 1.0
        , 'signalPrescaleViaXicz'       : 1.0
        , 'signalPrescaleViaOmegaz'     : 0.5
        , 'controlPrescaleViaOmegaczPi' : 0.03
        , 'controlPrescaleViaXiczPi'    : 0.05
        , 'signalPrescaleViaLcWS'       : 0.5
        , 'TRCHI2DOFMax'            :   3.0
        , 'Xicc_Daug_TRCHI2DOF_Max' :   5.0
        , 'Xicc_Daug_P_Min'         :   2.0*GeV
        , 'Xicc_Daug_PT_Min'        : 250.0*MeV
        , 'Xicc_Daug_MIPCHI2DV_Min' :  -1.0
        , 'TrGhostProbMax'          :   0.5 #can be tightened if needed.  < 0.3 -> same for all particles
        , 'Xicc_P_PIDpPIDpi_Min'    :   5.0
        , 'Xicc_P_PIDpPIDK_Min'     :   0.0
        , 'Xicc_Pi_PIDpiPIDK_Min'   :   0.0
        , 'Xicc_K_PIDKPIDpi_Min'    :   5.0
        , 'PionPIDK'                :  10.0
        , 'TRCHI2'                  :   3.0
        , 'MuonP'                   :   6.0*GeV
        , 'MuonPT'                  :  1000*MeV
        , 'MuonIPCHI2'              :   5.0 
        , 'MuonPIDmu'               :   0.0
        , 'GhostProb'               :   0.35
        , 'Lc_Daug_TRCHI2DOF_Max'   :   5.0
        , 'Lc_Daug_PT_Min'          : 200.0*MeV
        , 'Lc_Daug_P_Min'           :   2.0*GeV
        , 'Lc_K_PIDKPIDpi_Min'      :   5.0
        , 'Lc_Pi_PIDpiPIDK_Min'     :   0.0
        , 'Lc_P_PIDpPIDpi_Min'      :   5.0
        , 'Lc_P_PIDpPIDK_Min'       :   0.0
        , 'Lc_ADMASS_HalfWin'       :  75.0*MeV
        , 'Lc_Daug_1of3_MIPCHI2DV_Min': 4.0
        , 'Lc_ADOCAMAX_Max'         :   0.5*mm
        , 'Lc_APT_Min'              :   1.0*GeV
        , 'Lc_VCHI2_Max'            :  30.0
        , 'Lc_BPVVDCHI2_Min'        :  16.0
        , 'Lc_BPVDIRA_Min'          :   0.99
        , 'Xicplus_ADAMASS_HalfWin' : 170.0*MeV
        , 'Xicplus_ADMASS_HalfWin'  : 120.0*MeV
        , 'Xicplus_BPVVDCHI2_Min'   :  25.0
        , 'Xicplus_BPVDIRA_Min'     :   0.99
        , 'Xic0_ADAMASS_HalfWin'    : 170.0*MeV
        , 'Xic0_ADMASS_HalfWin'     : 120.0*MeV
        , 'Xic0_BPVVDCHI2_Min'      :  25.0
        , 'Xic0_BPVDIRA_Min'        :   0.9
        , 'Omegac0_ADAMASS_HalfWin' : 170.0*MeV
        , 'Omegac0_ADMASS_HalfWin'  : 120.0*MeV
        , 'Omegac0_4Dau_VCHI2_Max'  :  60.0
        , 'Omegac0_BPVVDCHI2_Min'   :  25.0
        , 'Omegac0_BPVDIRA_Min'     :   0.9
        , 'Xicc_AM_Min'             :   2.3*GeV #maybe 3.1 if rates too high
        , 'Xicc_AM_Max'             :   5.0*GeV #maybe 4.0 if rates too high 
        , 'Xicc_APT_Min'            :   2.0*GeV
        , 'Xicc_ADOCAMAX_Max'       :   2.0*mm #increase form 0.5 to 2 
        , 'Xicc_Lc_delta_VZ_Min'    :  -5.0*mm #increase to -5 from -2 
        , 'Xicc_BPVVDCHI2_Min'      :  -1.0
        , 'Xicc_BPVDIRA_Min'        :   0.9 
        , 'Omegacc_AM_Min'          :   2.5*GeV
        , 'Omegacc_AM_Max'          :   5.2*GeV 
        , 'Omegacc_APT_Min'         :   2.0*GeV
        , 'Omegacc_ADOCAMAX_Max'    :   2.0*mm #increase form 0.5 to 2 
        , 'Omegacc_Lc_delta_VZ_Min' :  -5.0*mm #increase to -5 from -2 
        , 'Omegacc_BPVVDCHI2_Min'   :  -1.0
        , 'Omegacc_BPVDIRA_Min'     :   0.9 
        , 'BVCHI2DOF'               :  20.0 #increase from 9 to 20 
        , 'Omegacc_2Dau_VCHI2_Max'  :  20.0
        , 'Xicc_2Dau_VCHI2_Max'     :  60.0 #set to 60 from 20 
        , 'Xicc_3Dau_VCHI2_Max'     :  60.0 #set to 60 form 30 
        , 'Xicc_4Dau_VCHI2_Max'     :  60.0
        , 'MINIPCHI2'               :   4.0  # adimensiional, orig. 9 
        , 'MINIPCHI2Loose'          :   4.0  # adimensiional 
        , 'Xicc_Zero_BPVDIRA_Min'   :   0.0 # Neutral
        , 'Omegacc_Zero_BPVDIRA_Min':   0.0 # Neutral
      } ## end of 'CONFIG' 
}  ## end of default_config

#-------------------------------------------------------------------------------------------------------------
class StrippingXiccSL(LineBuilder) :
        __configuration_keys__ = default_config['CONFIG'].keys()

        def __init__(self, name, config) :
            LineBuilder.__init__(self, name, config)
            self.name = name
            self.config = config

            # All Loose Protons
            self.AllLooseProtonsList = MergedSelection("AllLooseProtonsFor" + self.name,
                    RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLooseProtons/Particles")])

            # All Loose Pions
            self.AllLoosePionsList = MergedSelection("AllLoosePionsFor" + self.name,
                    RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLoosePions/Particles")])

            # All Loose Kaons
            self.AllLooseKaonsList = MergedSelection("AllLooseKaonsFor" + self.name,
                    RequiredSelections =  [DataOnDemand(Location = "Phys/StdAllLooseKaons/Particles")])

            # Loose Muons
            self.LooseMuonsList = MergedSelection("LooseMuonsFor" + self.name,
                    RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseMuons/Particles")])

            # Good tracks - All Loose Protons
            self.GoodAllLooseProtonsList = self.createSubSel( OutputList = "GoodAllLooseProtonsFor" + self.name,
                    InputList = self.AllLooseProtonsList,
                    Cuts = "(TRCHI2DOF < %(Xicc_Daug_TRCHI2DOF_Max)s)"\
                            "& (P>%(Xicc_Daug_P_Min)s)"\
                            "& (PT>%(Xicc_Daug_PT_Min)s)"\
                            "& (MIPCHI2DV(PRIMARY)>%(Xicc_Daug_MIPCHI2DV_Min)s)"\
                            "& (HASRICH)&(PIDp-PIDpi>%(Xicc_P_PIDpPIDpi_Min)s)"\
                            "& (HASRICH)&(PIDp-PIDK>%(Xicc_P_PIDpPIDK_Min)s)"\
                            "& (TRGHOSTPROB<%(TrGhostProbMax)s)"\
                            "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)" % self.config )

            # Good tracks - All Loose Pions
            self.GoodAllLoosePionsList = self.createSubSel( OutputList = "GoodAllLoosePionsFor" + self.name,
                    InputList = self.AllLoosePionsList,
                    Cuts = "(TRCHI2DOF < %(Xicc_Daug_TRCHI2DOF_Max)s)"\
                            "& (P>%(Xicc_Daug_P_Min)s)" \
                            "& (PT>%(Xicc_Daug_PT_Min)s)" \
                            "& (MIPCHI2DV(PRIMARY)>%(Xicc_Daug_MIPCHI2DV_Min)s)"\
                            "& (HASRICH)&(PIDpi-PIDK>%(Xicc_Pi_PIDpiPIDK_Min)s)"\
                            "& (PIDK<%(PionPIDK)s)"\
                            "& (TRGHOSTPROB<%(TrGhostProbMax)s)"\
                            "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2Loose)s)" % self.config )

            # Good tracks - All Loose Kaons
            self.GoodAllLooseKaonsList = self.createSubSel( OutputList = "GoodAllLooseKaonsFor" + self.name,
                    InputList = self.AllLooseKaonsList,
                    Cuts = "(TRCHI2DOF < %(Xicc_Daug_TRCHI2DOF_Max)s)"\
                            "& (P>%(Xicc_Daug_P_Min)s)"\
                            "& (PT>%(Xicc_Daug_PT_Min)s)"\
                            "& (MIPCHI2DV(PRIMARY)>%(Xicc_Daug_MIPCHI2DV_Min)s)"\
                            "& (HASRICH)&(PIDK-PIDpi>%(Xicc_K_PIDKPIDpi_Min)s)"\
                            "& (TRGHOSTPROB<%(TrGhostProbMax)s)"\
                            "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2Loose)s)" % self.config )

            # For Neutral Xic/Oc - Good tracks - All Loose Protons
            self.GoodNeutralStatesProtonsList = self.createSubSel( OutputList = "GoodNeutralStatesProtonsFor" + self.name,
                    InputList = self.AllLooseProtonsList,
                    Cuts = "(TRCHI2DOF < %(Xicc_Daug_TRCHI2DOF_Max)s)"\
                            "& (P>%(Xicc_Daug_P_Min)s)"\
                            "& (PT>%(Xicc_Daug_PT_Min)s)"\
                            "& (MIPCHI2DV(PRIMARY)>%(Xicc_Daug_MIPCHI2DV_Min)s)"\
                            "& (HASRICH)&(PIDp-PIDpi>%(Xicc_P_PIDpPIDpi_Min)s)"\
                            "& (HASRICH)&(PIDp-PIDK>%(Xicc_P_PIDpPIDK_Min)s)"\
                            "& (TRGHOSTPROB<%(TrGhostProbMax)s)" % self.config )

            # For Neutral Xic/Oc - Good tracks - All Loose Pions
            self.GoodNeutralStatesPionsList = self.createSubSel( OutputList = "GoodNeutralStatesPionsFor" + self.name,
                    InputList = self.AllLoosePionsList,
                    Cuts = "(TRCHI2DOF < %(Xicc_Daug_TRCHI2DOF_Max)s)"\
                            "& (P>%(Xicc_Daug_P_Min)s)" \
                            "& (PT>%(Xicc_Daug_PT_Min)s)" \
                            "& (MIPCHI2DV(PRIMARY)>%(Xicc_Daug_MIPCHI2DV_Min)s)"\
                            "& (HASRICH)&(PIDpi-PIDK>%(Xicc_Pi_PIDpiPIDK_Min)s)"\
                            "& (PIDK<%(PionPIDK)s)"\
                            "& (TRGHOSTPROB<%(TrGhostProbMax)s)" % self.config )

            # For Neutral Xic/Oc - Good tracks - All Loose Kaons
            self.GoodNeutralStatesKaonsList = self.createSubSel( OutputList = "GoodNeutralStatesKaonsFor" + self.name,
                    InputList = self.AllLooseKaonsList,
                    Cuts = "(TRCHI2DOF < %(Xicc_Daug_TRCHI2DOF_Max)s)"\
                            "& (P>%(Xicc_Daug_P_Min)s)"\
                            "& (PT>%(Xicc_Daug_PT_Min)s)"\
                            "& (MIPCHI2DV(PRIMARY)>%(Xicc_Daug_MIPCHI2DV_Min)s)"\
                            "& (HASRICH)&(PIDK-PIDpi>%(Xicc_K_PIDKPIDpi_Min)s)"\
                            "& (TRGHOSTPROB<%(TrGhostProbMax)s)" % self.config )

            # Good tracks - Loose Muons
            self.GoodLooseMuonsList = self.createSubSel( OutputList = "GoodLooseMuonsFor" + self.name,
                    InputList = self.LooseMuonsList,
                    Cuts = "(TRCHI2DOF < %(TRCHI2)s)"\
                            "& (P>%(MuonP)s)"\
                            "& (PT>%(MuonPT)s)"\
                            "& (MIPCHI2DV(PRIMARY)>%(MuonIPCHI2)s)"\
                            "& (PIDmu > %(MuonPIDmu)s )" \
                            "& (TRGHOSTPROB < %(GhostProb)s)" % self.config )

            self.BasicList = self.makeBasicLcXicOmegac()
            self.XiccList = self.makeXicc()
            self.OmegaccList = self.makeOmegacc()

        #------------------------------------------------------------------------------------------
        #------------------------------------------------------------------------------------------

        def createSubSel( self, OutputList, InputList, Cuts ) :
            '''create a selection using a FilterDesktop'''
            filter = FilterDesktop(Code = Cuts)
            return Selection( OutputList,
                             Algorithm = filter,
                             RequiredSelections = [ InputList ] )

        def createCombinationSel( self, OutputList,
                                 DecayDescriptor,
                                 DaughterLists,
                                 DaughterCuts = {} ,
                                 PreVertexCuts = "ALL",
                                 PostVertexCuts = "ALL") :
            '''create a selection using a ParticleCombiner with a single decay descriptor'''
            combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                        DaughtersCuts = DaughterCuts,
                                        MotherCut = PostVertexCuts,
                                        CombinationCut = PreVertexCuts,
                                        ReFitPVs = True)
            return Selection ( OutputList,
                              Algorithm = combiner,
                              RequiredSelections = DaughterLists)

        def makeBasicLcXicOmegac( self ):
            # Make Lambda_c+
            # Pick up standard Lambdac -> p K- pi+ then filter it to reduce rate:
            self.filterLc = self.makeLc(self.name+'filterLc')
            #LambdacList = self.makeTisTos(self.name+"LambdacList", selection = self.filterLc, hltTisTosSpec = config['LcHlt2TisTosSpec'])
            self.LambdacList = self.makeTisTos(self.name+"LambdacList", selection = self.filterLc, hltTisTosSpec = { 'Hlt2CharmHadLcpToPpKmPip.*Decision%TOS' : 0, 'Hlt2Global%TIS' : 0 })

            # Make Xic+
            self.XicPlus2pKpi = self.createCombinationSel( OutputList = "XicPlus2pKpi"+ self.name,
                    DecayDescriptor = "[Xi_c+ -> p+ K- pi+]cc",
                    DaughterLists   = [self.GoodAllLooseProtonsList, self.GoodAllLooseKaonsList, self.GoodAllLoosePionsList],
                    PreVertexCuts   = "( (ADAMASS('Xi_c+') < %(Xicplus_ADAMASS_HalfWin)s )" \
                                    "& (AMAXCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                                    "& (ADOCAMAX('')<%(Lc_ADOCAMAX_Max)s)" \
                                    "& (APT>%(Lc_APT_Min)s) )" % self.config,
                    PostVertexCuts  = "( (VFASPF(VCHI2)<%(Lc_VCHI2_Max)s)" \
                                    "& (ADMASS('Xi_c+') < %(Xicplus_ADMASS_HalfWin)s)" \
                                    "& (BPVVDCHI2>%(Xicplus_BPVVDCHI2_Min)s)" \
                                    "& (BPVDIRA>%(Xicplus_BPVDIRA_Min)s) )" % self.config )
            # Make Xic0
            self.XicZero2pKKpi = self.createCombinationSel( OutputList = "XicZero2pKKpi"+ self.name,
                    DecayDescriptor = "[Xi_c0 -> p+ K- K- pi+]cc",
                    DaughterLists   = [self.GoodNeutralStatesProtonsList, self.GoodNeutralStatesKaonsList, self.GoodNeutralStatesKaonsList, self.GoodNeutralStatesPionsList],
                    PreVertexCuts   = "( (ADAMASS('Xi_c0') < %(Xic0_ADAMASS_HalfWin)s )" \
                                    "& (AMAXCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                                    "& (ADOCAMAX('')<%(Lc_ADOCAMAX_Max)s)" \
                                    "& (APT>%(Lc_APT_Min)s) )" % self.config,
                    PostVertexCuts  = "( (VFASPF(VCHI2)<%(Omegac0_4Dau_VCHI2_Max)s)" \
                                    "& (ADMASS('Xi_c0') < %(Xic0_ADMASS_HalfWin)s)" \
                                    "& (BPVVDCHI2>%(Xic0_BPVVDCHI2_Min)s)" \
                                    "& (BPVDIRA>%(Xic0_BPVDIRA_Min)s) )" % self.config)
            # Make Omegac0
            self.OmegacZero2pKKpi = self.createCombinationSel( OutputList = "OmegacZero2pKKpi"+ self.name,
                    DecayDescriptor = "[Omega_c0 -> p+ K- K- pi+]cc",
                    DaughterLists   = [self.GoodNeutralStatesProtonsList, self.GoodNeutralStatesKaonsList, self.GoodNeutralStatesKaonsList, self.GoodNeutralStatesPionsList],
                    PreVertexCuts   = "( (ADAMASS('Omega_c0') < %(Omegac0_ADAMASS_HalfWin)s )" \
                                    "& (AMAXCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                                    "& (ADOCAMAX('')<%(Lc_ADOCAMAX_Max)s)" \
                                    "& (APT>%(Lc_APT_Min)s) )" % self.config,
                    PostVertexCuts  = "( (VFASPF(VCHI2)<%(Omegac0_4Dau_VCHI2_Max)s)" \
                                    "& (ADMASS('Omega_c0') < %(Omegac0_ADMASS_HalfWin)s)" \
                                    "& (BPVVDCHI2>%(Omegac0_BPVVDCHI2_Min)s)" \
                                    "& (BPVDIRA>%(Omegac0_BPVDIRA_Min)s) )" % self.config)

        def makeXicc( self ):
            _strCutComb  = "  (AM>%(Xicc_AM_Min)s)"\
                           "& (AM<%(Xicc_AM_Max)s)"\
                           "& (APT>%(Xicc_APT_Min)s)"\
                           "& (ADOCAMAX('')<%(Xicc_ADOCAMAX_Max)s)" % self.config
            _strChi2Moth2 = "(VFASPF(VCHI2/VDOF)<%(BVCHI2DOF)s)" % self.config
            # Charged FS Xic/Lc
            _strCutMoth  = "(CHILD(VFASPF(VZ),1) - VFASPF(VZ) > %(Xicc_Lc_delta_VZ_Min)s)" \
                           "& (BPVDIRA > %(Xicc_BPVDIRA_Min)s)" % self.config
            # Neutral FS Xic0/Oc0
            _strCutMoth_Zero  = "(CHILD(VFASPF(VZ),1) - VFASPF(VZ) > %(Xicc_Lc_delta_VZ_Min)s)" \
                           "& (BPVDIRA > %(Xicc_Zero_BPVDIRA_Min)s)" % self.config
            
            _strCutMoth2       = _strChi2Moth2 + '&' + _strCutMoth
            _strCutMoth2_Zero  = _strChi2Moth2 + '&' + _strCutMoth_Zero

            ''' Stripping Xi_cc++ -> Lambda_c+ mu+ '''
            Xicc2LambdacMuNu = self.createCombinationSel(OutputList = "Xicc2LambdacMuNu" + self.name,
                    DecayDescriptor = "[Xi_cc++ -> Lambda_c+ mu+]cc",
                    DaughterLists   = [self.LambdacList, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2
                    )
            Xicc2LambdacMuNuLine = StrippingLine( self.name + "Xicc2LambdacMuNuLine", 
                    prescale = self.config['signalPrescaleViaLc'],
                    algos = [ Xicc2LambdacMuNu ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoXiccppD3Body())
            self.registerLine (Xicc2LambdacMuNuLine)

            ''' Stripping Xi_cc++ -> Xi_c+ mu+ '''
            Xicc2XicPlusMuNu = self.createCombinationSel(OutputList = "Xicc2XicPlusMuNu" + self.name,
                    DecayDescriptor = "[Xi_cc++ -> Xi_c+ mu+]cc",
                    DaughterLists   = [self.XicPlus2pKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2
                    )
            Xicc2XicPlusMuNuLine = StrippingLine( self.name + "Xicc2XicPlusMuNuLine", 
                    prescale = self.config['signalPrescaleViaXicp'],
                    algos = [ Xicc2XicPlusMuNu ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoXiccppD3Body())
            self.registerLine (Xicc2XicPlusMuNuLine)

            ''' Stripping Xi_cc+ -> Xi_c0 mu+ '''
            Xicc2XicZeroMuNu = self.createCombinationSel(OutputList = "Xicc2XicZeroMuNu" + self.name,
                    DecayDescriptor = "[Xi_cc+ -> Xi_c0 mu+]cc",
                    DaughterLists   = [self.XicZero2pKKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Xicc2XicZeroMuNuLine = StrippingLine( self.name + "Xicc2XicZeroMuNuLine", 
                    prescale = self.config['signalPrescaleViaXicz'],
                    algos = [ Xicc2XicZeroMuNu ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoXiccpD4Body())
            self.registerLine (Xicc2XicZeroMuNuLine)

            ''' -------------------------------- '''
            ''' Create wrong charge combinations '''
            ''' -------------------------------- '''
            ''' WS Stripping Xi_cc++ -> Lambda_c+ mu- '''
            Xicc2LambdacMuNuWS = self.createCombinationSel(OutputList = "Xicc2LambdacMuNuWS" + self.name,
                    DecayDescriptor = "[Xi_cc++ -> Lambda_c+ mu-]cc",
                    DaughterLists   = [self.LambdacList, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2
                    )
            Xicc2LambdacMuNuWSLine = StrippingLine( self.name + "Xicc2LambdacMuNuWSLine", 
                    prescale = self.config['signalPrescaleViaLcWS'],
                    algos = [ Xicc2LambdacMuNuWS ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoXiccppD3BodyWS())
            self.registerLine (Xicc2LambdacMuNuWSLine)

            ''' WS Stripping Xi_cc++ -> Xi_c+ mu- '''
            Xicc2XicPlusMuNuWS = self.createCombinationSel(OutputList = "Xicc2XicPlusMuNuWS" + self.name,
                    DecayDescriptor = "[Xi_cc++ -> Xi_c+ mu-]cc",
                    DaughterLists   = [self.XicPlus2pKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2
                    )
            Xicc2XicPlusMuNuWSLine = StrippingLine( self.name + "Xicc2XicPlusMuNuWSLine", 
                    prescale = self.config['signalPrescaleViaLcWS'],
                    algos = [ Xicc2XicPlusMuNuWS ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoXiccppD3BodyWS())
            self.registerLine (Xicc2XicPlusMuNuWSLine)

            ''' WS Stripping Xi_cc+ -> Xi_c0 mu- '''
            Xicc2XicZeroMuNuWS = self.createCombinationSel(OutputList = "Xicc2XicZeroMuNuWS" + self.name,
                    DecayDescriptor = "[Xi_cc+ -> Xi_c0 mu-]cc",
                    DaughterLists   = [self.XicZero2pKKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Xicc2XicZeroMuNuWSLine = StrippingLine( self.name + "Xicc2XicZeroMuNuWSLine", 
                    prescale = self.config['signalPrescaleViaLcWS'],
                    algos = [ Xicc2XicZeroMuNuWS ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoXiccpD4BodyWS())
            self.registerLine (Xicc2XicZeroMuNuWSLine)
        ##  --------------------  end of makeXicc  ------------
        #------------------------------------------------------------------------------------------

        def makeOmegacc( self ):
            _strCutComb  = "  (AM>%(Omegacc_AM_Min)s)"\
                           "& (AM<%(Omegacc_AM_Max)s)"\
                           "& (APT>%(Omegacc_APT_Min)s)"\
                           "& (ADOCAMAX('')<%(Omegacc_ADOCAMAX_Max)s)" % self.config
            _strChi2Moth2 = "(VFASPF(VCHI2/VDOF)<%(BVCHI2DOF)s)" % self.config
            # Charged FS Xic/Lc
            _strCutMoth  = "(CHILD(VFASPF(VZ),1) - VFASPF(VZ) > %(Omegacc_Lc_delta_VZ_Min)s)" \
                           "& (BPVDIRA > %(Omegacc_BPVDIRA_Min)s)" % self.config
            # Neutral FS Xic0/Oc0
            _strCutMoth_Zero  = "(CHILD(VFASPF(VZ),1) - VFASPF(VZ) > %(Omegacc_Lc_delta_VZ_Min)s)" \
                           "& (BPVDIRA > %(Omegacc_Zero_BPVDIRA_Min)s)" % self.config

            _strCutMoth2      = _strChi2Moth2 + '&' + _strCutMoth
            _strCutMoth2_Zero = _strChi2Moth2 + '&' + _strCutMoth_Zero

            ''' Stripping Omega_cc+ -> Omega_c0 mu+ '''
            Omegacc2OmegacMuNu = self.createCombinationSel(OutputList = "Omegacc2OmegacMuNu" + self.name,
                    DecayDescriptor = "[Omega_cc+ -> Omega_c0 mu+]cc",
                    DaughterLists   = [self.OmegacZero2pKKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Omegacc2OmegacMuNuLine = StrippingLine( self.name + "Omegacc2OmegacMuNuLine", 
                    prescale = self.config['signalPrescaleViaOmegaz'],
                    algos = [ Omegacc2OmegacMuNu ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoOmegaccpD4Body())
            self.registerLine (Omegacc2OmegacMuNuLine)

            ''' Stripping Omega_cc+ -> Xi_c0 mu+ '''
            Omegacc2XicMuNu = self.createCombinationSel(OutputList = "Omegacc2XicMuNu" + self.name,
                    DecayDescriptor = "[Omega_cc+ -> Xi_c0 mu+]cc",
                    DaughterLists   = [self.XicZero2pKKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Omegacc2XicMuNuLine = StrippingLine( self.name + "Omegacc2XicMuNuLine", 
                    prescale = self.config['signalPrescaleViaXicz'],
                    algos = [ Omegacc2XicMuNu ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoOmegaccpD4Body())
            self.registerLine (Omegacc2XicMuNuLine)

            ''' ---------------------- '''
            ''' Normalization channels '''
            ''' ---------------------- '''
            ''' NC Stripping Omega_cc+ -> Omega_c0 pi+ '''
            Omegacc2OmegacNC = self.createCombinationSel(OutputList = "Omegacc2OmegacNC" + self.name,
                    DecayDescriptor = "[Omega_cc+ -> Omega_c0 pi+]cc",
                    DaughterLists   = [self.OmegacZero2pKKpi, self.GoodAllLoosePionsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Omegacc2OmegacNCLine = StrippingLine( self.name + "Omegacc2OmegacNCLine", 
                    prescale = self.config['controlPrescaleViaOmegaczPi'],
                    algos = [ Omegacc2OmegacNC ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoOmegaccpD4BodyPi())
            self.registerLine (Omegacc2OmegacNCLine)

            ''' NC Stripping Omega_cc+ -> Xi_c0 pi+ '''
            Omegacc2XicNC = self.createCombinationSel(OutputList = "Omegacc2XicNC" + self.name,
                    DecayDescriptor = "[Omega_cc+ -> Xi_c0 pi+]cc",
                    DaughterLists   = [self.XicZero2pKKpi, self.GoodAllLoosePionsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Omegacc2XicNCLine = StrippingLine( self.name + "Omegacc2XicNCLine", 
                    prescale = self.config['controlPrescaleViaXiczPi'],
                    algos = [ Omegacc2XicNC ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoOmegaccpD4BodyPi())
            self.registerLine (Omegacc2XicNCLine)

            ''' -------------------------------- '''
            ''' Create wrong charge combinations '''
            ''' -------------------------------- '''
            ''' WS Stripping Omega_cc+ -> Omega_c0 mu- '''
            Omegacc2OmegacMuNuWS = self.createCombinationSel(OutputList = "Omegacc2OmegacMuNuWS" + self.name,
                    DecayDescriptor = "[Omega_cc+ -> Omega_c0 mu-]cc",
                    DaughterLists   = [self.OmegacZero2pKKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Omegacc2OmegacMuNuWSLine = StrippingLine( self.name + "Omegacc2OmegacMuNuWSLine", 
                    prescale = self.config['signalPrescaleViaLcWS'],
                    algos = [ Omegacc2OmegacMuNuWS ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoOmegaccpD4BodyWS())
            self.registerLine (Omegacc2OmegacMuNuWSLine)

            ''' WS Stripping Omega_cc+ -> Xi_c0 mu- '''
            Omegacc2XicMuNuWS = self.createCombinationSel(OutputList = "Omegacc2XicMuNuWS" + self.name,
                    DecayDescriptor = "[Omega_cc+ -> Xi_c0 mu-]cc",
                    DaughterLists   = [self.XicZero2pKKpi, self.GoodLooseMuonsList],
                    PreVertexCuts   = _strCutComb,
                    PostVertexCuts  = _strCutMoth2_Zero
                    )
            Omegacc2XicMuNuWSLine = StrippingLine( self.name + "Omegacc2XicMuNuWSLine", 
                    prescale = self.config['signalPrescaleViaLcWS'],
                    algos = [ Omegacc2XicMuNuWS ], 
                    EnableFlavourTagging = False,
                    RelatedInfoTools = self._getRelInfoOmegaccpD4BodyWS())
            self.registerLine (Omegacc2XicMuNuWSLine)
        ## --------------------  end of makeOmegaminus  ------------
        #------------------------------------------------------------------------------------------

        def makeLc(self, name) :
            ## Pick up standard Lambdac -> p K- pi+
            _LcPions   = DataOnDemand(Location = 'Phys/StdLoosePions/Particles')
            _LcKaons   = DataOnDemand(Location = 'Phys/StdLooseKaons/Particles')
            _LcProtons = DataOnDemand(Location = 'Phys/StdLooseProtons/Particles')

            ## Daughter cuts
            _strCutAllDaug = "(TRCHI2DOF<%(Lc_Daug_TRCHI2DOF_Max)s)" \
                             "& (PT>%(Lc_Daug_PT_Min)s)" \
                             "& (P>%(Lc_Daug_P_Min)s)" % self.config
            _strCutK  = _strCutAllDaug + '&' + "(HASRICH)&((PIDK - PIDpi)>%(Lc_K_PIDKPIDpi_Min)s)" % self.config
            _strCutpi = _strCutAllDaug + '&' + "(HASRICH)&((PIDpi- PIDK)>%(Lc_Pi_PIDpiPIDK_Min)s)" % self.config
            _strCutp  = _strCutAllDaug + '&' + "(HASRICH)&((PIDp - PIDpi)>%(Lc_P_PIDpPIDpi_Min)s)&((PIDp-PIDK)>%(Lc_P_PIDpPIDK_Min)s)" % self.config

            _daughterCuts = { 'p+'  : _strCutp,
                              'K-'  : _strCutK,
                              'pi+' : _strCutpi }

            ## Combination cuts
            _strCutComb = "(ADAMASS('Lambda_c+')<1.1*%(Lc_ADMASS_HalfWin)s)" \
                          "& (AMAXCHILD(MIPCHI2DV(PRIMARY))>%(Lc_Daug_1of3_MIPCHI2DV_Min)s)" \
                          "& (ADOCAMAX('')<%(Lc_ADOCAMAX_Max)s)" \
                          "& (APT>%(Lc_APT_Min)s)" % self.config

            _strCutMoth = "(VFASPF(VCHI2) < %(Lc_VCHI2_Max)s)" \
                          "& (ADMASS('Lambda_c+')<%(Lc_ADMASS_HalfWin)s)" \
                          "& (BPVVDCHI2>%(Lc_BPVVDCHI2_Min)s)" \
                          "& (BPVDIRA>%(Lc_BPVDIRA_Min)s)" % self.config

            _combineLambdac2PKPi = CombineParticles(
                DecayDescriptor = "[Lambda_c+ -> p+ K- pi+]cc",
                DaughtersCuts = _daughterCuts,
                CombinationCut = _strCutComb,
                MotherCut = _strCutMoth )
            return Selection ( name,
                               Algorithm = _combineLambdac2PKPi,
                               RequiredSelections = [ _LcProtons, _LcKaons, _LcPions ] )

        def makeTisTos( self, name, selection, hltTisTosSpec = { } ) :
            outSel = selection
            if len(hltTisTosSpec) > 0 :
                _tisTosFilter = TisTosParticleTagger( name + 'TisTos' , TisTosSpecs = hltTisTosSpec )
                outSel = Selection( name , Algorithm = _tisTosFilter , RequiredSelections = [ selection ] )
            return outSel

        def _getRelInfoD3Body(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.5,1.7,2.0]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [ { "Type"          : "RelInfoConeVariables",
                  "ConeAngle"    : coneAngle,
                  "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                  "IgnoreUnmatchedDescriptors" : True,
                  "DaughterLocations" : {
                       "^[Beauty -> (Charm -> p+ X- X+) mu+]CC" : 'P2ConeVar%s_DCB' % conestr,
                       "[Beauty -> ^(Charm -> p+ X- X+) mu+]CC" : 'P2ConeVar%s_CB'  % conestr,
                       "[Beauty -> (Charm -> ^p+ X- X+) mu+]CC" : 'P2ConeVar%s_P'   % conestr,
                       "[Beauty -> (Charm -> p+ ^X- X+) mu+]CC" : 'P2ConeVar%s_1'   % conestr,
                       "[Beauty -> (Charm -> p+ X- ^X+) mu+]CC" : 'P2ConeVar%s_2'   % conestr,
                       "[Beauty -> (Charm -> p+ X- X+) ^mu+]CC" : 'P2ConeVar%s_Mu'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                   "IgnoreUnmatchedDescriptors" : True,
                   "DaughterLocations" : {
                       "^[Beauty -> (Charm -> p+ X- X+) mu+]CC" : 'VertexIsoInfo_DCB',
                       "[Beauty -> ^(Charm -> p+ X- X+) mu+]CC" : 'VertexIsoInfo_CC'} } ]                        
            return relInfo 

        def _getRelInfoXiccppD3Body(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.5,1.7,2.0]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [ { "Type"          : "RelInfoConeVariables",
                  "ConeAngle"    : coneAngle,
                  "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                  "IgnoreUnmatchedDescriptors" : True,
                  "DaughterLocations" : {
                       "^[Xi_cc++ -> (Charm -> p+ X- X+) mu+]CC" : 'P2ConeVar%s_DCB' % conestr,
                       "[Xi_cc++ -> ^(Charm -> p+ X- X+) mu+]CC" : 'P2ConeVar%s_CB'  % conestr,
                       "[Xi_cc++ -> (Charm -> ^p+ X- X+) mu+]CC" : 'P2ConeVar%s_P'   % conestr,
                       "[Xi_cc++ -> (Charm -> p+ ^X- X+) mu+]CC" : 'P2ConeVar%s_1'   % conestr,
                       "[Xi_cc++ -> (Charm -> p+ X- ^X+) mu+]CC" : 'P2ConeVar%s_2'   % conestr,
                       "[Xi_cc++ -> (Charm -> p+ X- X+) ^mu+]CC" : 'P2ConeVar%s_Mu'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                   "IgnoreUnmatchedDescriptors" : True,
                   "DaughterLocations" : {
                       "^[Xi_cc++ -> (Charm -> p+ X- X+) mu+]CC" : 'VertexIsoInfo_DCB',
                       "[Xi_cc++ -> ^(Charm -> p+ X- X+) mu+]CC" : 'VertexIsoInfo_CC'} } ]                        
            return relInfo 

        def _getRelInfoXiccppD3BodyWS(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.5,1.7,2.0]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [ { "Type"          : "RelInfoConeVariables",
                  "ConeAngle"    : coneAngle,
                  "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                  "IgnoreUnmatchedDescriptors" : True,
                  "DaughterLocations" : {
                       "^[Xi_cc++ -> (Charm -> p+ X- X+) mu-]CC" : 'P2ConeVar%s_DCB' % conestr,
                       "[Xi_cc++ -> ^(Charm -> p+ X- X+) mu-]CC" : 'P2ConeVar%s_CB'  % conestr,
                       "[Xi_cc++ -> (Charm -> ^p+ X- X+) mu-]CC" : 'P2ConeVar%s_P'   % conestr,
                       "[Xi_cc++ -> (Charm -> p+ ^X- X+) mu-]CC" : 'P2ConeVar%s_1'   % conestr,
                       "[Xi_cc++ -> (Charm -> p+ X- ^X+) mu-]CC" : 'P2ConeVar%s_2'   % conestr,
                       "[Xi_cc++ -> (Charm -> p+ X- X+) ^mu-]CC" : 'P2ConeVar%s_Mu'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                   "IgnoreUnmatchedDescriptors" : True,
                   "DaughterLocations" : {
                       "^[Xi_cc++ -> (Charm -> p+ X- X+) mu-]CC" : 'VertexIsoInfo_DCB',
                       "[Xi_cc++ -> ^(Charm -> p+ X- X+) mu-]CC" : 'VertexIsoInfo_CC'} } ]                        
            return relInfo 

        def _getRelInfoXiccpD4BodyWS(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.7]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [
                { "Type"         : "RelInfoConeVariables",
                "ConeAngle"    : coneAngle,
                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                "DaughterLocations" : {
                    "^[Xi_cc+ -> (Charm -> p+ X- X- X+) mu-]CC" : 'P2ConeVar%s_DCB' % conestr,
                    "[Xi_cc+ -> ^(Charm -> p+ X- X- X+) mu-]CC" : 'P2ConeVar%s_CB'  % conestr,
                    "[Xi_cc+ -> (Charm -> ^p+ X- X- X+) mu-]CC" : 'P2ConeVar%s_P'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ ^X- X- X+) mu-]CC" : 'P2ConeVar%s_1'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ X- ^X- X+) mu-]CC" : 'P2ConeVar%s_2'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ X- X- ^X+) mu-]CC" : 'P2ConeVar%s_3'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ X- X- X+) ^mu-]CC" : 'P2ConeVar%s_Mu'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                "DaughterLocations" : {
                    "^[Xi_cc+ -> (Charm -> p+ X- X- X+) mu-]CC" : 'VertexIsoInfo_DCB',
                    "[Xi_cc+ -> ^(Charm -> p+ X- X- X+) mu-]CC" : 'VertexIsoInfo_CB'} } ]
            return relInfo

        def _getRelInfoXiccpD4Body(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.7]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [
                { "Type"         : "RelInfoConeVariables",
                "ConeAngle"    : coneAngle,
                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                "DaughterLocations" : {
                    "^[Xi_cc+ -> (Charm -> p+ X- X- X+) mu+]CC" : 'P2ConeVar%s_DCB' % conestr,
                    "[Xi_cc+ -> ^(Charm -> p+ X- X- X+) mu+]CC" : 'P2ConeVar%s_CB'  % conestr,
                    "[Xi_cc+ -> (Charm -> ^p+ X- X- X+) mu+]CC" : 'P2ConeVar%s_P'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ ^X- X- X+) mu+]CC" : 'P2ConeVar%s_1'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ X- ^X- X+) mu+]CC" : 'P2ConeVar%s_2'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ X- X- ^X+) mu+]CC" : 'P2ConeVar%s_3'   % conestr,
                    "[Xi_cc+ -> (Charm -> p+ X- X- X+) ^mu+]CC" : 'P2ConeVar%s_Mu'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                "DaughterLocations" : {
                    "^[Xi_cc+ -> (Charm -> p+ X- X- X+) mu+]CC" : 'VertexIsoInfo_DCB',
                    "[Xi_cc+ -> ^(Charm -> p+ X- X- X+) mu+]CC" : 'VertexIsoInfo_CB'} } ]
            return relInfo

        def _getRelInfoOmegaccpD4Body(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.7]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [
                { "Type"         : "RelInfoConeVariables",
                "ConeAngle"    : coneAngle,
                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                "DaughterLocations" : {
                    "^[Omega_cc+ -> (Charm -> p+ X- X- X+) mu+]CC" : 'P2ConeVar%s_DCB' % conestr,
                    "[Omega_cc+ -> ^(Charm -> p+ X- X- X+) mu+]CC" : 'P2ConeVar%s_CB'  % conestr,
                    "[Omega_cc+ -> (Charm -> ^p+ X- X- X+) mu+]CC" : 'P2ConeVar%s_P'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ ^X- X- X+) mu+]CC" : 'P2ConeVar%s_1'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- ^X- X+) mu+]CC" : 'P2ConeVar%s_2'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- X- ^X+) mu+]CC" : 'P2ConeVar%s_3'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- X- X+) ^mu+]CC" : 'P2ConeVar%s_Mu'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                "DaughterLocations" : {
                    "^[Omega_cc+ -> (Charm -> p+ X- X- X+) mu+]CC" : 'VertexIsoInfo_DCB',
                    "[Omega_cc+ -> ^(Charm -> p+ X- X- X+) mu+]CC" : 'VertexIsoInfo_CB'} } ]
            return relInfo

        def _getRelInfoOmegaccpD4BodyWS(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.7]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [
                { "Type"         : "RelInfoConeVariables",
                "ConeAngle"    : coneAngle,
                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                "DaughterLocations" : {
                    "^[Omega_cc+ -> (Charm -> p+ X- X- X+) mu-]CC" : 'P2ConeVar%s_DCB' % conestr,
                    "[Omega_cc+ -> ^(Charm -> p+ X- X- X+) mu-]CC" : 'P2ConeVar%s_CB'  % conestr,
                    "[Omega_cc+ -> (Charm -> ^p+ X- X- X+) mu-]CC" : 'P2ConeVar%s_P'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ ^X- X- X+) mu-]CC" : 'P2ConeVar%s_1'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- ^X- X+) mu-]CC" : 'P2ConeVar%s_2'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- X- ^X+) mu-]CC" : 'P2ConeVar%s_3'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- X- X+) ^mu-]CC" : 'P2ConeVar%s_Mu'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                "DaughterLocations" : {
                    "^[Omega_cc+ -> (Charm -> p+ X- X- X+) mu-]CC" : 'VertexIsoInfo_DCB',
                    "[Omega_cc+ -> ^(Charm -> p+ X- X- X+) mu-]CC" : 'VertexIsoInfo_CB'} } ]
            return relInfo


        def _getRelInfoOmegaccpD4BodyPi(self):
            relInfo = []
            for coneAngle in [0.8,1.0,1.3,1.7]:
                conestr = str(coneAngle).replace('.','')
                relInfo += [
                { "Type"         : "RelInfoConeVariables",
                "ConeAngle"    : coneAngle,
                "Variables"    : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                "DaughterLocations" : {
                    "^[Omega_cc+ -> (Charm -> p+ X- X- X+) pi+]CC" : 'P2ConeVar%s_DCB' % conestr,
                    "[Omega_cc+ -> ^(Charm -> p+ X- X- X+) pi+]CC" : 'P2ConeVar%s_CB'  % conestr,
                    "[Omega_cc+ -> (Charm -> ^p+ X- X- X+) pi+]CC" : 'P2ConeVar%s_P'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ ^X- X- X+) pi+]CC" : 'P2ConeVar%s_1'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- ^X- X+) pi+]CC" : 'P2ConeVar%s_2'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- X- ^X+) pi+]CC" : 'P2ConeVar%s_3'   % conestr,
                    "[Omega_cc+ -> (Charm -> p+ X- X- X+) ^pi+]CC" : 'P2ConeVar%s_Pi'  % conestr } }
                           ]
            relInfo += [ { "Type" : "RelInfoVertexIsolation",
                "DaughterLocations" : {
                    "^[Omega_cc+ -> (Charm -> p+ X- X- X+) pi+]CC" : 'VertexIsoInfo_DCB',
                    "[Omega_cc+ -> ^(Charm -> p+ X- X- X+) pi+]CC" : 'VertexIsoInfo_CB'} } ]
            return relInfo
