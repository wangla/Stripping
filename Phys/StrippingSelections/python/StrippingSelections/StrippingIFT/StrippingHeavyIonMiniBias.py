###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selections or Minimum Bias physics.
Based on those by Patrick K.
'''

__author__ = 'Yanxi ZHANG, Emilie MAURICE'
__date__ = '06/09/2017'
__version__ = '$Revision: 0 $'


__all__ = (
    'HeavyIonMiniBiasConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonMiniBias',
    'WGs'             : ['IFT'],
    'STREAMS'         : ['IFT'],
    'BUILDERTYPE'     : 'HeavyIonMiniBiasConf',
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],
        'CheckPV'    :  False,

        'MicroBiasPrescale'            :  1.0,
        'MBNoBiasPrescale'            :  1.0,
        'MicroBiasPostscale'           :  1.0, 
        'MBNoBiasPostscale'           :  1.0, 
        'MicroBiasNoBackwardCutPostscale'           :  0.5, 

        "MicroBiasHlt1Filter"         : "(HLT_PASS('Hlt1BEMicroBiasVeloDecision'))", 
        "MicroBiasHlt2Filter"         : None,
                                                                                                   
        "GEC_LowMultBackwardCut"       : "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 1) & ( recSummaryTrack(LHCb.RecSummary.nBackTracks, TrBACKWARD) < 10) ",
        "GEC_LowMult"       : "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 1)  ",

        #Only PbAr trigger available for LowMult MicroBias
        'MicroBiasLowMultPrescale'            :  0.01,
        'MicroBiasLowMultPostscale'           :  1.0, 
        "MicroBiasLowMultHlt1Filter"         : "(HLT_PASS('Hlt1BEMicroBiasLowMultVeloDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVelo
        "MicroBiasLowMultHlt2Filter"         : None,

        "MBNoBiasHlt1Filter"         : "(HLT_PASS('Hlt1BENoBiasDecision'))", #(HLT_PASS('Hlt1MB.*Decision')) #Hlt1MBMicroBiasVel
        "MBNoBiasHlt2Filter"         : None

        }
    }

from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from PhysSelPython.Wrappers import Selection, DataOnDemand

class HeavyIonMiniBiasConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
        print "inside MiniBias",config
        #        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])      
        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in config['odin'] if odin_type in ["NoBeam","Beam1","Beam2"]])      
        _filter1 = {'Code': config['GEC_LowMultBackwardCut'], 'Preambulo' : ["from LoKiTracks.decorators import *" ,
                                                                                        "from LoKiCore.functions    import * ",
                                                                                        "from GaudiKernel.SystemOfUnits import *"]}

        _filter2 = {'Code': config['GEC_LowMult'], 'Preambulo' : ["from LoKiTracks.decorators import *" ,
                                                                                        "from LoKiCore.functions    import * ",
                                                                                        "from GaudiKernel.SystemOfUnits import *"]}
        
        self.MicroBiasLine = StrippingLine( 
            name = 'MBBackwardCutMicroBias',
            prescale  = self.config['MicroBiasPrescale'],
            postscale  = self.config['MicroBiasPostscale'],                 
            FILTER = _filter1,
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
            ODIN      = odin
            )
        self.registerLine( self.MicroBiasLine )

        self.MicroBiasLine = StrippingLine( 
            name = 'MBMicroBias',
            prescale  = self.config['MicroBiasPrescale'],
            postscale  = self.config['MicroBiasNoBackwardCutPostscale'],                 
            FILTER = _filter2,
            HLT1       =self.config['MicroBiasHlt1Filter'],
            HLT2       =self.config['MicroBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
            ODIN      = odin
            )
        self.registerLine( self.MicroBiasLine )
        

        self.MicroBiasLowMultLine = StrippingLine( 
            name = 'MicroBiasLowMult',
            prescale  = self.config['MicroBiasLowMultPrescale'],
            postscale  = self.config['MicroBiasLowMultPostscale'],                             
            HLT1       =self.config['MicroBiasLowMultHlt1Filter'],
            HLT2       =self.config['MicroBiasLowMultHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
            ODIN      = odin
            )
        self.registerLine( self.MicroBiasLowMultLine )
        
        self.NoMicroBiasLine = StrippingLine( 
            name = 'MBNoBias',
            prescale  = self.config['MBNoBiasPrescale'],
            postscale  = self.config['MBNoBiasPostscale'],                 
            HLT1       =self.config['MBNoBiasHlt1Filter'],
            HLT2       =self.config['MBNoBiasHlt2Filter'],
            checkPV   = self.config['CheckPV'],
            RequiredRawEvents = ["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker", "HC"],
            ODIN      = odin
            )
        self.registerLine( self.NoMicroBiasLine )
