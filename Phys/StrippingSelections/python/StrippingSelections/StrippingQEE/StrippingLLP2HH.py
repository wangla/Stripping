###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping lines for dark pions into light hadrons analysis. 
Revisited by CVS w.r.t. previous campaigns.
'''

__author__ = ['Wouter Hulsbergen', 'Igor Kostiuk', 'Carlos Vazquez Sierra']
__date__ = '23/06/2021'

__all__ = ('LLPV0_sConf','default_config')

from Gaudi.Configuration import *
from StandardParticles import StdAllNoPIDsPions
from PhysSelPython.Wrappers import CombineSelection, PassThroughSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import GeV, picosecond
from LoKiPhys.functions import SOURCE, SIZE

default_config = {
    'NAME'              : 'LLPV0',
    'WGs'               : ['QEE'],
    'BUILDERTYPE'       : 'LLPV0_sConf',
    'CONFIG'    : {
                         '2HHSinglePrescale'     :  0.0135 
                 ,       '2HHMultiPrescale'      :  1.0
                 ,       '2HHSingleKaonPrescale' :  0.045
                 ,       '2HHDoubleKaonPrescale' :  1.0
                 ,       '2mumuSinglePrescale'   :  0. # line removed
                 ,       '2MixedPrescale'        :  1.0
                 ,       'dimuProbNNmu'          :  0.
                 ,       'dihProbNNmu'           :  0.
                 ,       'dihProbNNk'            :  0.01
                 ,       'dikProbNNk'            :  0.55
                 ,       'dimuProbNNk'           :  0.5
                 ,       'mixedProbNNk'          :  0.01
                 ,       'dihhalt'               :  100
                 ,       'dihHHSingle'           :  1
                 ,       'dihHHMulti'            :  6
                 ,       'dikHHSingleKaon'       :  1
                 ,       'dikHHDoubleKaon'       :  2
                 ,       'dimuMuonsSingle'       :  1
                 ,       'dihMuons'              :  0
                 ,       'dimuMixed'             :  1
                 ,       'dihMixed'              :  2
                 ,       'daugsP'                :  5.*GeV
                 ,       'daugsPT'               :  1.*GeV
                 ,       'daugsChi2dv'           :  25.
                 ,       'combChi2'              :  25.
                 ,       'motherChi2vx'          :  16.
                 ,       'motherLifetime'        :  1.5*picosecond
                  },
    'STREAMS' : ['BhadronCompleteEvent']
    }

### Lines stored in this file:
# StrippingLLPV02HHSingleLine
# StrippingLLPV02HHMultiLine
# StrippingLLPV02HHSingleKaonLine
# StrippingLLPV02HHDoubleKaonLine
# StrippingLLPV02MixedLine

class LLPV0_sConf(LineBuilder) :
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        self.makeV02HH()

    ### Stripping lines:

    def makeV02HH( self ):
        V02HH = CombineSelection( "V02HH" + self.name,
                                  StdAllNoPIDsPions,
                                  DecayDescriptor = "KS0 -> pi+ pi-",
                                  DaughtersCuts  = {"pi+" : "(P > %(daugsP)s) & (MIPCHI2DV(PRIMARY) > %(daugsChi2dv)s) & (PT > %(daugsPT)s)" % self.config,
                                                   "pi-" : "(P > %(daugsP)s) & (MIPCHI2DV(PRIMARY) > %(daugsChi2dv)s) & (PT > %(daugsPT)s)" % self.config },
                                  CombinationCut = "( ADOCACHI2CUT(%(combChi2)s, '') )" % self.config,
                                  MotherCut = "( CHI2VX < %(motherChi2vx)s ) & ( BPVLTIME() > %(motherLifetime)s )" % self.config
                                  )

        ### Single dipion line, no pid cuts, heavily prescaled

        V02HHSingle = self.MultiMHSelection("HHSingleSelection", V02HH,
            self.config['dihhalt'], self.config['dihhalt'], self.config['dihHHSingle'],
            self.config['dihProbNNk'], self.config['dihProbNNmu'])

        V02HHSingleLine = StrippingLine( self.name + "2HHSingleLine", algos = [ V02HHSingle ],
                                         prescale = self.config['2HHSinglePrescale'] )

        ### Multi dipion line, no pid cuts

        V02HHMulti =  self.MultiMHSelection("HHMultiSelection", V02HH,
            self.config['dihhalt'], self.config['dihhalt'], self.config['dihHHMulti'],
            self.config['dihProbNNk'], self.config['dihProbNNmu'])

        V02HHMultiLine = StrippingLine( self.name + "2HHMultiLine", algos = [ V02HHMulti ],
                                        prescale = self.config['2HHMultiPrescale'] )

        ### Single dikaon line

        V02HHSingleKaon =  self.MultiMHSelection("HHSingleKaonSelection", V02HH,
            self.config['dihhalt'], self.config['dihhalt'], self.config['dikHHSingleKaon'],
            self.config['dikProbNNk'], self.config['dihProbNNmu'])

        V02HHSingleKaonLine = StrippingLine( self.name + "2HHSingleKaonLine", algos = [ V02HHSingleKaon ],
                                            prescale = self.config['2HHSingleKaonPrescale'] )

        ### Double dikaon line

        V02HHDoubleKaon =  self.MultiMHSelection("HHDoubleKaonSelection", V02HH,
            self.config['dihhalt'], self.config['dihhalt'], self.config['dikHHDoubleKaon'],
            self.config['dikProbNNk'], self.config['dihProbNNmu'])

        V02HHDoubleKaonLine = StrippingLine( self.name + "2HHDoubleKaonLine", algos = [ V02HHDoubleKaon ],
                                            prescale = self.config['2HHDoubleKaonPrescale'] )

        ### Single dimuon line

        V02mumuSingle = self.MultiMHSelection("MuMuSingleSelection", V02HH,
            self.config['dimuMuonsSingle'], self.config['dihMuons'], self.config['dihhalt'],
            self.config['dimuProbNNk'], self.config['dimuProbNNmu'])

        V02mumuSingleLine = StrippingLine( self.name + "2mumuSingleLine", algos = [ V02mumuSingle ],
                                           prescale = self.config['2mumuSinglePrescale'] )

        ### Mixed: Single dimuon + single dipion (no pid cuts on dipion) line

        V02Mixed = self.MultiMHSelection("MixedSelection", V02HH,
            self.config['dimuMixed'], self.config['dihMixed'], self.config['dihhalt'],
            self.config['mixedProbNNk'], self.config['dimuProbNNmu'])

        V02MixedLine = StrippingLine( self.name + "2MixedLine", algos = [ V02Mixed ],
                                      prescale = self.config['2MixedPrescale'] )

        self.registerLine(V02HHSingleLine)
        self.registerLine(V02HHMultiLine)
        self.registerLine(V02HHSingleKaonLine)
        self.registerLine(V02HHDoubleKaonLine)
        self.registerLine(V02mumuSingleLine)
        self.registerLine(V02MixedLine)

    def MultiMHSelection(self           ,
                         name           ,
                         input          ,
                         mindimu    = 0 ,
                         mindimudih = 0 ,
                         mindih     = 0 ,
                         hprobnnk   = 0 ,
                         hprobnnmu  = 0
                        ) :

        """
        This module looks for dihadrons which satisfy
        a certain ProbNNk cut and dimuons which satisfy
        a ProbNNmu cut and then selects events following this logic:
        (#dimuons>=mindimu & #dihadrons>=mindimudih) | #dihadrons>=mindih
        """

        preamb = [
          "ndih  = SOURCE ( '%s', NINGENERATION(PROBNNk>=%s, 1) == 2 ) >> SIZE" % ( input.outputLocation(), hprobnnk ),
          "ndimu = SOURCE ( '%s', NINGENERATION(PROBNNmu>=%s, 1) == 2 ) >> SIZE" % ( input.outputLocation(), hprobnnmu ),
          ]

        code = "( ((ndimu >= %s) & (ndih >= %s)) | (ndih >= %s) )" % ( mindimu, mindimudih, mindih )

        from GaudiConfUtils.ConfigurableGenerators import LoKi__VoidFilter as _VFilter_
        return PassThroughSelection   (
            name,
            Algorithm         = _VFilter_(Code = code , Preambulo = preamb),
            RequiredSelection = input )
