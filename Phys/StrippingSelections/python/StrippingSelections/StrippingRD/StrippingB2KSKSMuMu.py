###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Vitalii Lisovskyi'
__date__    = '18/06/2021'
__version__ = '$Revision: 1 $'

__all__ = ( 'B2KSKSMuMuConf', 'default_config' )

"""
Selections for B -> KS KS mu mu
"""

default_config = {
    'NAME'                       : 'B2KSKSMuMu',
    'BUILDERTYPE'                : 'B2KSKSMuMuConf',
    'CONFIG'                     :
        {
        'BFlightCHI2'            : 36 #100
        , 'BDIRA'                : 0.999
        , 'BIPCHI2'              : 25
        , 'BVertexCHI2'          : 16
        , 'DiLeptonPT'           : 0
        , 'DiLeptonFDCHI2'       : 16
        , 'DiLeptonIPCHI2'       : 0
        , 'LeptonIPCHI2'         : 9
        , 'LeptonPT'             : 300
        , 'KSMassWindow'         : 30
        , 'KSDIRA'               : 0.9
        , 'UpperMass'            : 5500
        , 'BMassWindow'          : 1500
        , 'K1_MassWindow_Lo'     : 0
        , 'K1_MassWindow_Hi'     : 6100
        , 'K1_VtxChi2'           : 20 #25
        , 'KsKs_VtxChi2'       : 36
        , 'V0TAU'               : 0.0005
        , 'Bu2mmLinePrescale'    : 1
        },
    'WGs'     : [ 'RD' ],
    'STREAMS' : [ 'Dimuon' ]
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder



class B2KSKSMuMuConf(LineBuilder) :
    """
    Builder for B->KSKS mumu and B->KSKSKS mumu
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        'BFlightCHI2'
        , 'BDIRA'
        , 'BIPCHI2'
        , 'BVertexCHI2'
        , 'DiLeptonPT'
        , 'DiLeptonFDCHI2'
        , 'DiLeptonIPCHI2'
        , 'LeptonIPCHI2'
        , 'LeptonPT'
        , 'KSMassWindow'
        , 'KSDIRA'
        , 'UpperMass'
        , 'BMassWindow'
        , 'K1_MassWindow_Lo'
        , 'K1_MassWindow_Hi'
        , 'K1_VtxChi2'
        , 'KsKs_VtxChi2'
        , 'V0TAU'
        , 'Bu2mmLinePrescale'
      )

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name
        mmXLine_name   = name

        from StandardParticles import StdVeryLooseKsLL as KsLL
        from StandardParticles import StdLooseKsDD as KsDD
        from StandardParticles import StdLooseKsLD as KsLD

        SelKsKs = self._makeKSKS( name   = "KsKsFor" + self._name,
                               ksLL  = KsLL,
                               ksDD  = KsDD,
                               ksLD  = KsLD,
                               params = config )

        SelKsKsKs = self._makeKSKSKS( name   = "KsKsKsFor" + self._name,
                               ksLL  = KsLL,
                               ksDD  = KsDD,
                               ksLD  = KsLD,
                               params = config )

        # Make Dileptons

        from StandardParticles import StdLooseDiMuon as DiMuons
        MuonID = "(HASMUON)&(ISMUON)"
        DiMuonID     = "(2 == NINTREE((ABSID==13)&(HASMUON)&(ISMUON)&(PROBNNmu>0.01)))"

        SelDiMuon = self._filterDiLepton( "SelDiMuonsFor" + self._name,
                                          dilepton = DiMuons,
                                          params   = config,
                                          idcut    = DiMuonID )

        # Combine Particles


        SelB2mmX = self._makeB2LLX(mmXLine_name,
                                   dilepton = SelDiMuon,
                                   hadrons  = [ SelKsKs, SelKsKsKs ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        # Declare Lines

        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 900 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }

        self.B2mmXLine = StrippingLine(mmXLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB2mmX,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents = [],
                                       MDSTFlag          = False )

        # Register Lines
        self.registerLine( self.B2mmXLine )


#####################################################
    def _filterDiLepton( self, name, dilepton, params, idcut = None ) :
        """
        Handy interface for dilepton filter
        """

        _Code = "(ID=='J/psi(1S)') & "\
                "(PT > %(DiLeptonPT)s *MeV) & "\
                "(MM < %(UpperMass)s *MeV) & "\
                "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "\
                "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "\
                "(VFASPF(VCHI2/VDOF) < 10) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "\
                "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params

        # add additional cut on PID if requested
        if idcut : _Code += ( " & " + idcut )

        _Filter = FilterDesktop( Code = _Code )

        return Selection(name, Algorithm = _Filter, RequiredSelections = [ dilepton ] )

#####################################################
    def _makeKSKS( self, name, ksLL, ksDD, ksLD, params ) :
        """
        Make an f2 -> KS0 KS0
        f2 is just a proxy to get the two-body combination
        """

        _Decays = "f'_2(1525) -> KS0 KS0"

         # define all the cuts

        _CombinationCut    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2PDOF) < %(KsKs_VtxChi2)s)" % params
        _daughtersCutsKS = "(BPVLTIME() > %(V0TAU)s * ns) & (BPVDIRA > %(KSDIRA)s) & (ADMASS('KS0') <  %(KSMassWindow)s *MeV)"% params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "KS0"  : _daughtersCutsKS, }


        _sel_KsKsAll = Selection(name+"_All",
            RequiredSelections = [ ksLL, ksLD, ksDD ],
            Algorithm = _Combine)

        return _sel_KsKsAll


    def _makeKSKSKS( self, name, ksLL, ksDD, ksLD, params ) :
        """
        Make an f2 -> KS0 KS0 KS0
        f2 is just a proxy to get the two-body combination
        """

        _Decays = "f_2(2300) -> KS0 KS0 KS0"

         # define all the cuts

        _CombinationCut    = "(AM > %(K1_MassWindow_Lo)s*MeV) & (AM < %(K1_MassWindow_Hi)s*MeV)" % params
        _MotherCut  = "(VFASPF(VCHI2PDOF) < %(KsKs_VtxChi2)s)" % params
        _daughtersCutsKS = "(BPVLTIME() > %(V0TAU)s * ns) & (BPVDIRA > %(KSDIRA)s) & (ADMASS('KS0') <  %(KSMassWindow)s *MeV)"% params

        _Combine = CombineParticles( DecayDescriptor = _Decays,
                                     CombinationCut  = _CombinationCut,
                                     MotherCut       = _MotherCut)

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "KS0"  : _daughtersCutsKS, }


        _sel_KsKsKsAll = Selection(name+"_All",
            RequiredSelections = [ ksLL, ksDD, ksLD ],
            Algorithm = _Combine)

        return _sel_KsKsKsAll

#####################################################
    def _makeB2LLX( self, name, dilepton, hadrons, params, masscut = "(ADAMASS('B+')< 1500 *MeV" ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "B_s0 -> J/psi(1S) f'_2(1525)",
                    "B_s0 -> J/psi(1S) f_2(2300)",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )
#####################################################
