###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for
    Bs to phi_3(1850)/phi(1020) tau tau
    Bs to K* K* tau tau
    B0 to K* tau tau
    Lb to p K tau tau
Same-sign combinations are included.
"""

__author__ = 'H. Tilquin'
__date__ = '03/06/2021'
__version__ = '$Revision: 0.1 $'

__all__ = ('B2XTauTauMuonicConf', 'default_config')

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N3BodyDecays

from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

default_config = {
    'NAME': 'B2XTauTauMuonic',
    'BUILDERTYPE': 'B2XTauTauMuonicConf',
    'CONFIG':
        {
            "Bs_Comb_MassHigh": 7000.0,
            "Bd_Comb_MassHigh": 6500.0,
            "Lb_Comb_MassHigh": 7500.0,
            "Bs_VertDist": -24,
            "Bd_VertDist": -5,
            "Lb_VertDist": -25,
            "B_DIRA": 0.999,
            "Lb_DIRA": 0.999,
            "Bs_VertexCHI2": 150.0,
            "B0_VertexCHI2": 100.0,
            "Lb_VertexCHI2": 150.0,
            "Dau_DIRA": 0.99,
            "Dimuon_DIRA": 0.0,
            "Hadron_MinIPCHI2": 16.0,
            "Muon_MinIPCHI2": 9.0,
            "DimuonUPPERMASS": 4350.0,
            "Phi_FlightChi2": 16.0,
            "Phi_Comb_MassHigh": 1850.0,
            "Phi_PT": 600,
            "Phi_VertexCHI2": 4,
            "Kstar_for_B2Kstar_FlightChi2": 150.0,
            "Kstar_for_B2Kstar_Comb_MassHigh": 1750.0,
            "Kstar_for_B2Kstar_Comb_MassLow": 795.0,
            "Kstar_for_B2Kstar_PT": 1500,
            "Kstar_for_B2Kstar_VertexCHI2": 3,
            "Kstar_for_B2KstarKstar_MassWindow": 100,
            "Kstar_for_B2KstarKstar_PT": 600,
            "Kstar_for_B2KstarKstar_VertexCHI2": 6,
            "KstarKstar_DOCACHI2": 25,
            "KstarKstar_Comb_MassHigh": 2400,
            "Lambdastar_FlightChi2": 25,
            "Lambdastar_Comb_MassHigh": 5000.0,
            "Lambdastar_Comb_MassLow": 1400.0,
            "Lambdastar_PT": 1500,
            "Lambdastar_VertexCHI2": 3,
            "Dimuon_VertexCHI2": 20,
            "MuonPID": 0.0,
            "KaonPID": 4.0,
            "KaonPID_B2Kstar": 5.0,
            "ProtonPID": 5.0,
            "Pion_ProbNN": 0.3,
            "Pion_ProbNN_B2Kstar": 0.8,
            "Kaon_Pion_ProbNN_B2Kstar": 0.8,
            "Hadron_P": 3000,
            "SpdMult": 600,
            "Track_GhostProb": 0.3,
            "Track_TRCHI2": 3,
            "UseNoPIDsHadrons": False,
            "UseNoPIDsMuons": False,
            "HLT1_FILTER": None,
            "HLT2_FILTER": None,
            "L0DU_FILTER": None,
        },

    'WGs': ['RD'],
    'STREAMS': ['Dimuon']
}


class B2XTauTauMuonicConf(LineBuilder):
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name

        self.BsCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bs_VertDist)s * mm)" % config
        self.Bs_for_B2KstarKstarCombCut = "(AM < %(Bs_Comb_MassHigh)s * MeV)" % config
        self.BsCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Bs_VertexCHI2)s)" % config

        self.BdCombCut = "(AM < %(Bd_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Bd_VertDist)s * mm)" % config
        self.BdCut = "(BPVDIRA > %(B_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(B0_VertexCHI2)s)" % config

        self.LambdaBCombCut = "(AM < %(Lb_Comb_MassHigh)s * MeV) & (ACHILD(VFASPF(VZ), 2) - ACHILD(VFASPF(VZ), 1) > %(Lb_VertDist)s * mm)" % config
        self.LambdaBCut = "(BPVDIRA> %(Lb_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Lb_VertexCHI2)s)" % config

        DaughterCuts = "(BPVDIRA> %(Dau_DIRA)s)" % config

        self.PhiCombCut = "(AM < %(Phi_Comb_MassHigh)s * MeV)" % config
        self.PhiCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Phi_VertexCHI2)s) & (PT > %(Phi_PT)s * MeV) & " \
                                     "(BPVVDCHI2 > %(Phi_FlightChi2)s)" % config

        self.Kstar_for_B2KstarCombCut = "(AM < %(Kstar_for_B2Kstar_Comb_MassHigh)s*MeV) & (AM > %(Kstar_for_B2Kstar_Comb_MassLow)s*MeV)" % config
        self.Kstar_for_B2KstarCut = DaughterCuts + " & (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2Kstar_VertexCHI2)s) & " \
                                                   "(PT > %(Kstar_for_B2Kstar_PT)s * MeV) & (BPVVDCHI2 > %(Kstar_for_B2Kstar_FlightChi2)s)" % config

        self.Kstar_for_B2KstarKstarCombCut = "(ADAMASS('K*(892)0')< %(Kstar_for_B2KstarKstar_MassWindow)s * MeV )" % config
        self.Kstar_for_B2KstarKstarCut = DaughterCuts + " &  (VFASPF(VCHI2/VDOF) < %(Kstar_for_B2KstarKstar_VertexCHI2)s) & " \
                                                        "(PT > %(Kstar_for_B2KstarKstar_PT)s * MeV)" % config

        self.LambdaStarCombCut = "(AM < %(Lambdastar_Comb_MassHigh)s * MeV) & (AM > %(Lambdastar_Comb_MassLow)s * MeV)" % config
        self.LambdaStarCut = DaughterCuts + " & (PT > %(Lambdastar_PT)s * MeV) & (VFASPF(VCHI2/VDOF) < %(Lambdastar_VertexCHI2)s)" % config

        self.DiMuonCombCut = "(AM < %(DimuonUPPERMASS)s * MeV) " % config
        self.DiMuonCut = "(BPVDIRA> %(Dimuon_DIRA)s) & (VFASPF(VCHI2/VDOF) < %(Dimuon_VertexCHI2)s)" % config

        self.TrackCuts = "(TRGHP < %(Track_GhostProb)s) & (TRCHI2DOF < %(Track_TRCHI2)s)" % config

        self.HadronCuts = "(MIPCHI2DV(PRIMARY) > %(Hadron_MinIPCHI2)s)" % config

        self.KaonCutBase = self.TrackCuts + " & " + self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.KaonCut = self.KaonCutBase + " & (PIDK > %(KaonPID)s) & (~ISMUON)" % config
        self.KaonCut_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) > %(Kaon_Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.KaonCutNoPID_B2Kstar = self.KaonCutBase + " & (PROBNNK * (1-PROBNNpi) < %(Kaon_Pion_ProbNN_B2Kstar)s)" % config
        self.KaonCutNoPID = self.KaonCutBase + " & (PIDK < %(KaonPID)s)" % config

        self.PionCutBase = self.TrackCuts + " & " + self.HadronCuts
        self.PionCut_B2Kstar = self.PionCutBase + " & (PROBNNpi > %(Pion_ProbNN_B2Kstar)s) & (~ISMUON)" % config
        self.PionCutNoPID_B2Kstar = self.PionCutBase + " & (PROBNNpi < %(Pion_ProbNN_B2Kstar)s)" % config

        self.MuonCutBase = self.TrackCuts + " & (MIPCHI2DV(PRIMARY) > %(Muon_MinIPCHI2)s)" % config
        self.MuonCut = self.MuonCutBase + " & (PIDmu> %(MuonPID)s) & (ISMUON)" % config
        self.MuonCutNoPID = self.MuonCutBase + " & (PIDmu < %(MuonPID)s) " % config

        self.ProtonCutBase = self.TrackCuts + " & " + self.HadronCuts + " & (P > %(Hadron_P)s * MeV)" % config
        self.ProtonCut = self.ProtonCutBase + " & (PIDp > %(ProtonPID)s)  & (~ISMUON)" % config
        self.ProtonCutNoPID = self.ProtonCutBase + " & (PIDp < %(ProtonPID)s)" % config

        self.Kaons = self.__Kaons__(config)
        self.FakeKaons = self.__FakeKaons__()

        self.Kaons_Kstar = self.__Kaons__(config, sel_name="_B2Kstar")
        self.FakeKaons_Kstar = self.__FakeKaons__(sel_name="_B2Kstar")

        self.Pions_Kstar = self.__Pions__(config, sel_name="_B2Kstar")
        self.FakePions_Kstar = self.__FakePions__(sel_name="_B2Kstar")

        self.Muons = self.__Muons__(config)
        self.FakeMuons = self.__FakeMuons__()

        self.Protons = self.__Protons__(config)
        self.FakeProtons = self.__FakeProtons__()

        self.Dimuon = self.__Dimuon__(self.Muons)
        self.FakeDimuon = self.__FakeDimuon__(self.Muons, self.FakeMuons, conf=config)

        self.Phi = self.__Phi__(self.Kaons, conf=config)
        self.FakePhi = self.__Phi__(self.Kaons, self.FakeKaons, conf=config)

        self.Kstar_for_B2KstarKstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2KstarKstar")
        self.FakePionKstar_for_B2KstarKstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar, pid_selection="NoPIDPi_",
                                                             sel_name="_B2KstarKstar")
        self.FakeKaonKstar_for_B2KstarKstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar, pid_selection="NoPIDK_",
                                                             sel_name="_B2KstarKstar")

        self.Kstar_for_B2Kstar = self.__Kstar__(self.Kaons_Kstar, self.Pions_Kstar, sel_name="_B2Kstar")
        self.FakePionKstar_for_B2Kstar = self.__Kstar__(self.Kaons_Kstar, self.FakePions_Kstar,
                                                        pid_selection="NoPIDPi_", sel_name="_B2Kstar")
        self.FakeKaonKstar_for_B2Kstar = self.__Kstar__(self.FakeKaons_Kstar, self.Pions_Kstar,
                                                        pid_selection="NoPIDK_", sel_name="_B2Kstar")

        self.LambdaStar = self.__Lambdastar__(self.Protons, self.Kaons)
        self.FakeProtonLambdaStar = self.__Lambdastar__(self.FakeProtons, self.Kaons, pid_selection="NoPIDp_")
        self.FakeKaonLambdaStar = self.__Lambdastar__(self.Protons, self.FakeKaons, pid_selection="NoPIDK_")

        self.DeclaredDaughters = [self.Phi, self.Dimuon]
        self.Bs = self.__Bs_Phi__(daughters=self.DeclaredDaughters)

        self.DeclaredDaughters = [self.Phi, self.FakeDimuon]
        self.Bs_NoPIDmu = self.__Bs_Phi__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmu_")

        self.DeclaredDaughters = [self.FakePhi, self.Dimuon]
        self.Bs_NoPIDK = self.__Bs_Phi__(daughters=self.DeclaredDaughters, pid_selection="NoPIDK_")

        self.DeclaredDaughters = [self.Kstar_for_B2KstarKstar, self.Dimuon]
        self.Bs_kstarkstar = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config)

        self.DeclaredDaughters = [self.FakeKaonKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dimuon]
        self.Bs_kstarkstar_nopidK = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config, pid_selection="NoPIDK_")

        self.DeclaredDaughters = [self.FakePionKstar_for_B2KstarKstar, self.Kstar_for_B2KstarKstar, self.Dimuon]
        self.Bs_kstarkstar_nopidpi = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config, pid_selection="NoPIDpi_")

        self.DeclaredDaughters = [self.Kstar_for_B2KstarKstar, self.FakeDimuon]
        self.Bs_kstarkstar_nopidpmu = self.__Bs_KstarKstar__(daughters=self.DeclaredDaughters, conf=config, pid_selection="NoPIDmu_")

        self.DeclaredDaughters = [self.Kstar_for_B2Kstar, self.Dimuon]
        self.Bs_kstar = self.__B0_Kstar__(daughters=self.DeclaredDaughters)

        self.DeclaredDaughters = [self.FakeKaonKstar_for_B2Kstar, self.Dimuon]
        self.Bs_kstar_nopidK = self.__B0_Kstar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDK_")

        self.DeclaredDaughters = [self.FakePionKstar_for_B2Kstar, self.Dimuon]
        self.Bs_kstar_nopidpi = self.__B0_Kstar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDpi_")

        self.DeclaredDaughters = [self.Kstar_for_B2Kstar, self.FakeDimuon]
        self.Bs_kstar_nopidpmu = self.__B0_Kstar__(daughters=self.DeclaredDaughters, pid_selection="NoPIDmu_")

        self.LambdaB_pk = self.__LambdaB_pK__(daughters=[self.Dimuon, self.LambdaStar])
        self.LambdaB_pk_noPIDK = self.__LambdaB_pK__(daughters=[self.Dimuon, self.FakeKaonLambdaStar],
                                                     pid_selection="NoPIDK_")
        self.LambdaB_pk_noPIDp = self.__LambdaB_pK__(daughters=[self.Dimuon, self.FakeProtonLambdaStar],
                                                     pid_selection="NoPIDp_")
        self.LambdaB_pk_noPIDmu = self.__LambdaB_pK__(daughters=[self.FakeDimuon, self.LambdaStar],
                                                      pid_selection="NoPIDmu_")

        self.FilterSPD = {'Code': " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config,
                          'Preambulo': ["from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb"]}

        self.Bs2phi_line = StrippingLine(
            self.name + "_Bs2Phi_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs], MDSTFlag=False, MaxCandidates=1000)
        self.registerLine(self.Bs2phi_line)

        self.Bs2Phi_NoPIDmu_line = StrippingLine(
            self.name + "_Bs2Phi_NoPIDMuLine", prescale=0.04,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_NoPIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_NoPIDmu_line)

        self.Bs2Phi_NoPIDK_line = StrippingLine(
            self.name + "_Bs2Phi_NoPIDKLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_NoPIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2Phi_NoPIDK_line)

        self.Bs2KstarKstar_line = StrippingLine(
            self.name + "_Bs2KstarKstar_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_line)

        self.Bs2KstarKstar_NoPIDK_line = StrippingLine(
            self.name + "_Bs2KstarKstar_NoPIDKLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_nopidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_NoPIDK_line)

        self.Bs2KstarKstar_NoPIDpi_line = StrippingLine(
            self.name + "_Bs2KstarKstar_NoPIDPiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_nopidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_NoPIDpi_line)

        self.Bs2KstarKstar_NoPIDmu_line = StrippingLine(
            self.name + "_Bs2KstarKstar_NoPIDMuLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstarkstar_nopidpmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bs2KstarKstar_NoPIDmu_line)

        self.Bd2Kstar_line = StrippingLine(
            self.name + "_B02KstarLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_line)

        self.Bd2Kstar_NoPIDK_line = StrippingLine(
            self.name + "_B02Kstar_NoPIDKLine", prescale=0.3,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar_nopidK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_NoPIDK_line)

        self.Bd2Kstar_NoPIDpi_line = StrippingLine(
            self.name + "_B02Kstar_NoPIDPiLine", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar_nopidpi], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_NoPIDpi_line)

        self.Bd2Kstar_NoPIDmu_line = StrippingLine(
            self.name + "_B02Kstar_NoPIDMuLine", prescale=0.03,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.Bs_kstar_nopidpmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Bd2Kstar_NoPIDmu_line)

        self.Lb2pK_line = StrippingLine(
            self.name + "_Lb2pK_Line", prescale=1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_line)

        self.Lb2pK_NoPIDK_line = StrippingLine(
            self.name + "_Lb2pK_NoPIDKLine", prescale=0.2,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_noPIDK], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_NoPIDK_line)

        self.Lb2pK_NoPIDp_line = StrippingLine(
            self.name + "_Lb2pK_NoPIDpLine", prescale=0.1,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_noPIDp], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_NoPIDp_line)

        self.Lb2pK_NoPIDmu_line = StrippingLine(
            self.name + "_Lb2pK_NoPIDmuLine", prescale=0.04,
            HLT2=config['HLT2_FILTER'], HLT1=config['HLT1_FILTER'], L0DU=config['L0DU_FILTER'],
            FILTER=self.FilterSPD, algos=[self.LambdaB_pk_noPIDmu], MDSTFlag=False, MaxCandidates=4000)
        self.registerLine(self.Lb2pK_NoPIDmu_line)

    def __Muons__(self, conf):
        from StandardParticles import StdAllLooseMuons, StdAllNoPIDsMuons
        muons = StdAllNoPIDsMuons if conf['UseNoPIDsMuons'] else StdAllLooseMuons
        muon_cut = self.MuonCutBase if conf['UseNoPIDsMuons'] else self.MuonCut
        _filter = FilterDesktop(Code=muon_cut)
        _sel = Selection("Selection_" + self.name + "_StdLooseMuons", RequiredSelections=[muons],
                         Algorithm=_filter)
        return _sel

    def __FakeMuons__(self):
        from StandardParticles import StdAllNoPIDsMuons
        _filter = FilterDesktop(Code=self.MuonCutNoPID)
        _sel = Selection("Selection_" + self._name + "StdAllNoPIDsMuons", Algorithm=_filter,
                         RequiredSelections=[StdAllNoPIDsMuons])
        return _sel

    def __Protons__(self, conf):
        from StandardParticles import StdLooseProtons, StdNoPIDsProtons
        protons = StdNoPIDsProtons if conf['UseNoPIDsHadrons'] else StdLooseProtons
        proton_cuts = self.ProtonCutBase if conf['UseNoPIDsHadrons'] else self.ProtonCut
        _filter = FilterDesktop(Code=proton_cuts)
        _sel = Selection("Selection_" + self.name + "_StdLooseProtons", RequiredSelections=[protons], Algorithm=_filter)
        return _sel

    def __FakeProtons__(self):
        from StandardParticles import StdNoPIDsProtons
        _filter = FilterDesktop(Code=self.ProtonCutNoPID)
        _sel = Selection("Selection_" + self._name + "_StdAllNoPIDsProtons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsProtons])
        return _sel

    def __Kaons__(self, conf, sel_name="_"):
        from StandardParticles import StdLooseKaons, StdNoPIDsKaons
        kaons = StdNoPIDsKaons if conf['UseNoPIDsHadrons'] else StdLooseKaons
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.KaonCutBase)
        elif "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCut_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCut)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLooseKaons", RequiredSelections=[kaons],
                         Algorithm=_filter)
        return _sel

    def __FakeKaons__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsKaons
        if "Kstar" in sel_name:
            _filter = FilterDesktop(Code=self.KaonCutNoPID_B2Kstar)
        else:
            _filter = FilterDesktop(Code=self.KaonCutNoPID)
        _sel = Selection("Selection_" + self._name + sel_name + "StdAllNoPIDsKaons", Algorithm=_filter,
                         RequiredSelections=[StdNoPIDsKaons])
        return _sel

    def __Pions__(self, conf, sel_name="_"):
        from StandardParticles import StdLoosePions, StdNoPIDsPions
        pions = StdNoPIDsPions if conf['UseNoPIDsHadrons'] else StdLoosePions
        if conf['UseNoPIDsHadrons']:
            _filter = FilterDesktop(Code=self.PionCutBase)
        else:
            _filter = FilterDesktop(Code=self.PionCut_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdLoosePions", RequiredSelections=[pions],
                         Algorithm=_filter)
        return _sel

    def __FakePions__(self, sel_name="_"):
        from StandardParticles import StdNoPIDsPions
        _filter = FilterDesktop(Code=self.PionCutNoPID_B2Kstar)
        _sel = Selection("Selection_" + self.name + sel_name + "StdAllNoPIDsPions", RequiredSelections=[StdNoPIDsPions],
                         Algorithm=_filter)
        return _sel

    def __Dimuon__(self, Muons):
        CombineDiMuon = CombineParticles()
        CombineDiMuon.DecayDescriptors = ["D0 -> mu- mu+", "D0 -> mu+ mu+", " D0 -> mu- mu-"]
        CombineDiMuon.CombinationCut = self.DiMuonCombCut
        CombineDiMuon.MotherCut = self.DiMuonCut
        _sel = Selection("Selection_" + self.name + "_DiMuon", Algorithm=CombineDiMuon, RequiredSelections=[Muons])
        return _sel

    def __FakeDimuon__(self, muons, fakemuons, conf):
        CombineDiMuon = CombineParticles()
        CombineDiMuon.DecayDescriptors = ["D0 -> mu- mu+", "D0 -> mu+ mu+", " D0 -> mu- mu-"]
        CombineDiMuon.CombinationCut = self.DiMuonCombCut + " & (AHASCHILD((PIDmu < %(MuonPID)s)))" % conf
        CombineDiMuon.MotherCut = self.DiMuonCut
        _sel = Selection("Selection_" + self.name + "_DiMuonNoPID", Algorithm=CombineDiMuon,
                         RequiredSelections=[muons, fakemuons])
        return _sel

    def __Phi__(self, Kaons, fakekaon=None, conf=None):
        _phi2kk = CombineParticles()
        _phi2kk.DecayDescriptors = ["phi(1020) -> K+ K-", "phi(1020) -> K+ K+", "phi(1020) -> K- K-"]
        _phi2kk.MotherCut = self.PhiCut
        if fakekaon is None:
            _phi2kk.CombinationCut = self.PhiCombCut
            _sel = Selection("Phi_selection_for" + self.name, Algorithm=_phi2kk, RequiredSelections=[Kaons])
        else:
            _phi2kk.CombinationCut = self.PhiCombCut + " & (AHASCHILD((PIDK < %(KaonPID)s)))" % conf
            _sel = Selection("Phi_NoPIDK_selection_for" + self.name, Algorithm=_phi2kk,
                             RequiredSelections=[Kaons, fakekaon])
        return _sel

    def __Kstar__(self, Kaons, Pions, pid_selection="_", sel_name="_B2KstarKstar"):

        _kstar2kpi = CombineParticles()
        if sel_name == "_B2KstarKstar":
            _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarKstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarKstarCut
        else:
            _kstar2kpi.DecayDescriptors = ["[K*(892)0 -> K+ pi-]cc", "K*(892)0 -> K+ pi+", "K*(892)0 -> K- pi-"]
            _kstar2kpi.CombinationCut = self.Kstar_for_B2KstarCombCut
            _kstar2kpi.MotherCut = self.Kstar_for_B2KstarCut
        _sel = Selection("Kstar_for" + sel_name + pid_selection + "selection_for" + self.name, Algorithm=_kstar2kpi,
                         RequiredSelections=[Kaons, Pions])
        return _sel

    def __Lambdastar__(self, Protons, Kaons, pid_selection="_"):
        _lstar2pk = CombineParticles()
        _lstar2pk.DecayDescriptors = ["[Lambda(1520)0 -> p+ K-]cc", "Lambda(1520)0 -> p+ K+", "Lambda(1520)0 -> p~- K-"]
        _lstar2pk.CombinationCut = self.LambdaStarCombCut
        _lstar2pk.MotherCut = self.LambdaStarCut
        _sel = Selection("Lambdastar" + pid_selection + "selection_for" + self.name, Algorithm=_lstar2pk,
                         RequiredSelections=[Protons, Kaons])
        return _sel

    def __Bs_Phi__(self, daughters, pid_selection="_"):
        _b2phitautau = CombineParticles(DecayDescriptors=["B_s0 -> phi(1020) D0"],
                                        MotherCut=self.BsCut, CombinationCut=self.BsCombCut)
        sel = Selection("Phi" + pid_selection + "for" + self.name, Algorithm=_b2phitautau, RequiredSelections=daughters)
        return sel

    def __Bs_KstarKstar__(self, daughters, conf, pid_selection="_"):
        _b2kstarkstartautau = DaVinci__N3BodyDecays()
        _b2kstarkstartautau.DecayDescriptors = ["B_s0 -> K*(892)0 K*(892)~0 D0", "B_s0 -> K*(892)0 K*(892)0 D0", "B_s0 -> K*(892)~0 K*(892)~0 D0"]
        _b2kstarkstartautau.CombinationCut = self.Bs_for_B2KstarKstarCombCut
        _b2kstarkstartautau.MotherCut = self.BsCut
        _b2kstarkstartautau.Combination12Cut = "(ACHI2DOCA(1,2) < %(KstarKstar_DOCACHI2)s) & (AM < %(KstarKstar_Comb_MassHigh)s * MeV)" % conf
        sel = Selection("KstarKstar" + pid_selection + "for" + self.name + "_daughters", Algorithm=_b2kstarkstartautau,
                        RequiredSelections=daughters)
        return sel

    def __B0_Kstar__(self, daughters, pid_selection="_"):
        _b2kstartautau = CombineParticles(DecayDescriptors=["B0 -> K*(892)0 D0", "B~0 -> K*(892)~0 D0"],
                                          MotherCut=self.BdCut, CombinationCut=self.BdCombCut)
        sel = Selection("Kstar" + pid_selection + "for" + self.name, Algorithm=_b2kstartautau,
                        RequiredSelections=daughters)
        return sel

    def __LambdaB_pK__(self, daughters, pid_selection="_"):
        _b2lambdatautau = CombineParticles(DecayDescriptors=["Lambda_b0 -> Lambda(1520)0 D0", "Lambda_b~0 -> Lambda(1520)~0 D0"],
                                           MotherCut=self.LambdaBCut, CombinationCut=self.LambdaBCombCut)
        sel = Selection("Lambda" + pid_selection + "for" + self.name, Algorithm=_b2lambdatautau,
                        RequiredSelections=daughters)
        return sel
