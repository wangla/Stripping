###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for construction of baryonic lepton-flavor (and baryon-number) violating stripping selections and lines
Template partially borrowed from tau->3mu lines.

Channels included (several cahrge combinations each, unless specified):
Xi_b -> 3mu
Xi_b -> 5mu
Xi_b -> 2mue
Xi_b -> 2mutau
Xi_b -> emutau
Xi_b -> Delta++ mu-mu-mu-
B0 -> p+p+ mu-mu-
B0 -> p+p+p+ mu-mu-mu-
Xib- -> pi+pi+ mu-mu-mu- (also KK)
Xib0 -> pi+pi+pi+ mu-mu-mu- (also KKK)
'''

__author__ = ['Vitalii Lisovskyi']
__date__ = '28/02/2021'
__version__ = '$Revision: 0.0 $'

__all__ = ( 'BaryonicLFVConf', 'default_config' )


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays as Combine5Particles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.PhysicalConstants import c_light

default_config = {
    'NAME'        : 'BaryonicLFV',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'BaryonicLFVConf',
    'CONFIG'      : {
    'BaryLFVPrescale'      :1.,
    'BaryLFVPostscale'     :1.,
    'ProbNNCutTight'       : 0.1,
    'PTCut'                : 250,
    'ProtonP'              : 5000,
    'DiHadronMassTight'    : 2600,
    'DiHadronMassLoose'    : 6000,
    'DiHadronVtxCHI2'      : 25,
    'DiHadronADOCACHI2'    : 30,
    'HadronIPCHI2'           : 9,
    'TrackGhostProb'           :0.4
},
    #'STREAMS' : [ 'Dimuon' ]
      'STREAMS'     : { 'Leptonic' : ['StrippingBaryonicLFVXib23MuLooseLine'],
                        'Dimuon' : ['StrippingBaryonicLFVXib22MuTauLine','StrippingBaryonicLFVXib22ETauLine','StrippingBaryonicLFVXib2EMuTauLine','StrippingBaryonicLFVXib23MuLine','StrippingBaryonicLFVXib25MuLine','StrippingBaryonicLFVXib22MuELine','StrippingBaryonicLFVppmumuSSLine','StrippingBaryonicLFV3p3muSSLine','StrippingBaryonicLFVDelta3muSSLine','StrippingBaryonicLFVpipi3muSSLine','StrippingBaryonicLFVpipipi3muSSLine']}
     #}

    }



class BaryonicLFVConf(LineBuilder) :
    """
    Builder
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        self.name = name
        self.__confdict__ = config


        LineBuilder.__init__(self, name, config)

        Xib3mu_name=name+'Xib23Mu'
        Xib3mu_name_loose=name+'Xib23MuLoose'
        Xib2mue_name=name+'Xib22MuE'
        Xib2mutau_name=name+'Xib22MuTau'
        Xib2emutau_name=name+'Xib2EMuTau'
        Xib2etau_name=name+'Xib22ETau'
        Xib5mu_name=name+'Xib25Mu'
        B2ppmm_name=name+'ppmumuSS'
        B23p3m_name=name+'3p3muSS'
        B2Delta3m_name=name+'Delta3muSS'
        B2pipi3m_name=name+'pipi3muSS'
        B2pipipi3m_name=name+'pipipi3muSS'


        selXib23Mu = makeXib23Mu(Xib3mu_name,config)
        selXib23MuLoose = makeXib23MuLoose(Xib3mu_name_loose,config)
        selXib22MuE = makeXib22MuE(Xib2mue_name,config)
        selXib22MuTau = makeXib22MuTau(Xib2mutau_name,config)
        selXib22ETau = makeXib22ETau(Xib2etau_name,config)
        selXib22EMuTau = makeXib2EMuTau(Xib2emutau_name,config)
        selXib25Mu = makeXib25Mu(Xib5mu_name,config)

        selMuMuSS = _makeMuMuSS("MuMuSSFor"+name,config)
        sel3MuSS = _make3MuSS("3MuSSFor"+name,config)
        selppSS = _makeppSS("ppSSFor"+name,config)
        sel3pSS = _make3pSS("3pSSFor"+name,config)
        selDeltaSS = _makeDeltaSS("DeltaSSFor"+name,config)
        selpipiSS = _makepipiSS("pipiSSFor"+name,config)
        selpipipiSS = _makepipipiSS("pipipiSSFor"+name,config)
        selKKSS = _makeKKSS("KKSSFor"+name,config)
        selKKKSS = _makeKKKSS("KKKSSFor"+name,config)

        selB2ppmm = _makeppmumu("ppmmFor"+name,selMuMuSS,selppSS,config)
        selB23p3m = _make3p3mu("3p3mFor"+name,sel3MuSS,sel3pSS,config)
        selB2Delta3m = _makeDeltapp3mu("Delta3mFor"+name,sel3MuSS,selDeltaSS,config)
        selB2pipi3m = _makepipi3mu("pipi3mFor"+name,sel3MuSS,selpipiSS,selKKSS,config)
        selB2pipipi3m = _makepipipi3mu("pipipi3mFor"+name,sel3MuSS,selpipipiSS,selKKKSS,config)


        self.Xib23MuLine = StrippingLine(Xib3mu_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selXib23Mu,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Xi_b- -> ^mu+ mu- mu- ]CC" : "Muon1BDT",
                                         "[ Xi_b- -> mu+ ^mu- mu- ]CC" : "Muon2BDT",
                                         "[ Xi_b- -> mu+ mu- ^mu- ]CC" : "Muon3BDT"}
                                         },
                                     ],
                                     MDSTFlag = False )

        self.Xib23MuLooseLine = StrippingLine(Xib3mu_name_loose+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selXib23MuLoose,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> ^l l l ]CC" : "Muon1BDT",
                                         "[ Beauty -> l ^l l ]CC" : "Muon2BDT",
                                         "[ Beauty -> l l ^l ]CC" : "Muon3BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.Xib22MuELine = StrippingLine(Xib2mue_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selXib22MuE,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> ^[mu+]cc [mu+]cc [e+]cc ]CC" : "Muon1BDT",
                                         "[ Beauty -> [mu+]cc ^[mu+]cc [e+]cc ]CC" : "Muon2BDT",
                                         # "[ Beauty -> X ^X X ]CC" : "Muon2BDT",
                                         # "[ Beauty -> X X ^X ]CC" : "Muon3BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.Xib22MuTauLine = StrippingLine(Xib2mutau_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selXib22MuTau,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> ^StableCharged StableCharged l ]CC" : "Muon1BDT",
                                         "[ Beauty -> StableCharged ^StableCharged l ]CC" : "Muon2BDT",
                                         #"[ Beauty -> StableCharged StableCharged ^l ]CC" : "Muon3BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.Xib22ETauLine = StrippingLine(Xib2etau_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selXib22ETau,
                                     MDSTFlag = False )

        self.Xib2EMuTauLine = StrippingLine(Xib2emutau_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selXib22EMuTau,
                                     MDSTFlag = False )

        self.Xib25MuLine = StrippingLine(Xib5mu_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selXib25Mu,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Xi_b- -> ^X X X X X ]CC" : "Muon1BDT",
                                         "[ Xi_b- -> X ^X X X X ]CC" : "Muon2BDT",
                                         "[ Xi_b- -> X X ^X X X ]CC" : "Muon3BDT",
                                         "[ Xi_b- -> X X X ^X X ]CC" : "Muon4BDT",
                                         "[ Xi_b- -> X X X X ^X ]CC" : "Muon5BDT"}
                                         },
                                     ],
                                     MDSTFlag = False )

        self.B2ppmmLine = StrippingLine(B2ppmm_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selB2ppmm,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> X (X -> ^mu- mu-) ]CC" : "Muon1BDT",
                                         "[ Beauty -> X (X -> mu- ^mu-) ]CC" : "Muon2BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.B23p3mLine = StrippingLine(B23p3m_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selB23p3m,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> X (X -> ^mu- mu- mu-) ]CC" : "Muon1BDT",
                                         "[ Beauty -> X (X -> mu- ^mu- mu-) ]CC" : "Muon2BDT",
                                         "[ Beauty -> X (X -> mu- mu- ^mu-) ]CC" : "Muon3BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.B2Delta3mLine = StrippingLine(B2Delta3m_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selB2Delta3m,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> X (X -> ^mu- mu- mu-) ]CC" : "Muon1BDT",
                                         "[ Beauty -> X (X -> mu- ^mu- mu-) ]CC" : "Muon2BDT",
                                         "[ Beauty -> X (X -> mu- mu- ^mu-) ]CC" : "Muon3BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.B2pipi3mLine = StrippingLine(B2pipi3m_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selB2pipi3m,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> X (X -> ^mu- mu- mu-) ]CC" : "Muon1BDT",
                                         "[ Beauty -> X (X -> mu- ^mu- mu-) ]CC" : "Muon2BDT",
                                         "[ Beauty -> X (X -> mu- mu- ^mu-) ]CC" : "Muon3BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.B2pipipi3mLine = StrippingLine(B2pipipi3m_name+"Line",
                                     prescale = config['BaryLFVPrescale'],
                                     postscale = config['BaryLFVPostscale'],
                                     selection = selB2pipipi3m,
                                     RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> X (X -> ^mu- mu- mu-) ]CC" : "Muon1BDT",
                                         "[ Beauty -> X (X -> mu- ^mu- mu-) ]CC" : "Muon2BDT",
                                         "[ Beauty -> X (X -> mu- mu- ^mu-) ]CC" : "Muon3BDT",
                                         }
                                     },
                                     ],
                                     MDSTFlag = False )

        self.registerLine(self.Xib23MuLine)
        self.registerLine(self.Xib23MuLooseLine)
        self.registerLine(self.Xib25MuLine)
        self.registerLine(self.B2ppmmLine)
        self.registerLine(self.B23p3mLine)
        self.registerLine(self.B2Delta3mLine)
        self.registerLine(self.B2pipi3mLine)
        self.registerLine(self.B2pipipi3mLine)
        self.registerLine(self.Xib22MuELine)
        self.registerLine(self.Xib22MuTauLine)
        self.registerLine(self.Xib22ETauLine)
        self.registerLine(self.Xib2EMuTauLine)

#builders!
#####################################################
def makeXib23Mu(name, config):
    """
    Make Xib / Omega_b -> 3 muons
    """

    Xib2MuMuMu = Combine3Particles(
               DecayDescriptors = ["[ Xi_b- -> mu+ mu- mu- ]cc"] ,#,"[ Xi_b- -> mu- mu- mu- ]cc"],
               DaughtersCuts = { "mu+" : " ( PT > 250 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) "\
               "& ( BPVIPCHI2 () >  9 ) " % config},
               Combination12Cut = "(AM < 7200*MeV)",
               CombinationCut = "(AM < 7300*MeV) & (AM > 5300*MeV) & ( ANUM(( 'mu-' == ABSID ) & ISMUON )> 1.5 ) &  ( ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.3)) > 0.5)  & (APT>2000*MeV) & AHASCHILD(( PT > 900 * MeV ))  & AHASCHILD(( BPVIPCHI2 () >  16 ))", # requiring 2 IsMuon and 1 ProbNNmu>0.xx
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 9 ) &
            (BPVVDCHI2 > 40) &
            (BPVDIRA > 0.9992 )
            """
            )

    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

    return Selection (name,
                      Algorithm = Xib2MuMuMu,
                      RequiredSelections = [ _stdNoPIDLooseMuons ])

#####################################################
def makeXib23MuLoose(name, config):
    """
    Make Xib / Omega_b -> 3 muons
    """

    Xib2MuMuMu = Combine3Particles(
               DecayDescriptors = ["[ Xi_b- -> mu+ mu- mu- ]cc","[ Xi_b- -> mu- mu- mu- ]cc"],
               DaughtersCuts = { "mu+" : " ( PT > 200 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) "\
               "& ( BPVIPCHI2 () >  6 ) " % config},
               Combination12Cut = "(AM < 7500*MeV)",
               CombinationCut = "(AM < 7600*MeV) & (AM > 5000*MeV) & ( ANUM(( 'mu-' == ABSID ) & ISMUON )> 1.5 ) &  ( ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.1)) > 0.5)", # requiring 2 IsMuon and 1 ProbNNmu>0.1
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 10 ) &
            (BPVVDCHI2 > 36) &
            (BPVDIRA > 0.995 )
            """
            )

    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

    return Selection (name,
                      Algorithm = Xib2MuMuMu,
                      RequiredSelections = [ _stdNoPIDLooseMuons ])


#####################################################
def makeXib22MuE(name, config):
    """
    Make Xib / Omega_b -> mu mu e
    """

    Xib2MuMuE = Combine3Particles(
               DecayDescriptors = ["[ Xi_b- -> mu+ mu- e- ]cc", "[ Xi_b- -> mu- mu- e+ ]cc"],
               DaughtersCuts = { "mu+" : " ( PT > 200 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config,
               "e+" : " ( PT > 200 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config
               },
               Combination12Cut = "(AM < 7300*MeV)",
               CombinationCut = "(AM < 7400*MeV) & (AM > 5100*MeV) &  ( ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.15)) > 0.5)", # requiring 1 ProbNNmu>0.1
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 9 ) &
            (BPVVDCHI2 > 36) &
            (BPVDIRA > 0.99 )
            """
            )

    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdLooseElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")

    return Selection (name,
                      Algorithm = Xib2MuMuE,
                      RequiredSelections = [ _stdLooseMuons, _stdLooseElectrons ])

#####################################################
def makeXib22MuTau(name, config):
    """
    Make Xib / Omega_b -> mu mu tau
    """

    Xib2MuMuE = Combine3Particles(
               DecayDescriptors = ["[ Xi_b- -> mu+ mu- tau- ]cc","[ Xi_b- -> mu- mu- tau+ ]cc","[ Xi_b- -> mu- mu- tau- ]cc"],
               DaughtersCuts = { "mu+" : " ( PT > 200 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) " % config,
               "tau+" : " (PT>600*MeV) " #% config
               },
               Combination12Cut = "(AM < 10000*MeV)",
               CombinationCut = "(AM < 10000*MeV) & (AM > 2000*MeV) &  ( ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.2)) > 1.5)", # requiring 1 ProbNNmu>0.1
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 9 ) &
            (BPVVDCHI2 > 40) &
            (BPVDIRA > 0.95 )
            """
            )

    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdLooseTaus = DataOnDemand(Location = "Phys/StdTightDetachedTau3pi/Particles")

    return Selection (name,
                      Algorithm = Xib2MuMuE,
                      RequiredSelections = [ _stdLooseMuons, _stdLooseTaus ])

#####################################################
def makeXib2EMuTau(name, config):
    """
    Make Xib / Omega_b -> e mu tau
    """

    Xib2TauMuE = Combine3Particles(
               DecayDescriptors = ["[ Xi_b- -> e+ mu- tau- ]cc","[ Xi_b- -> e- mu- tau+ ]cc","[ Xi_b- -> e- mu+ tau- ]cc"], #,"[ Xi_b- -> e- mu- tau- ]cc"],
               DaughtersCuts = { "mu+" : " ( PT > 200 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) & (PROBNNmu>0.2) " % config,
               "e+" : " ( PT > 200 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) & (PROBNNe>0.3) & (PIDe>3)" % config,
               "tau+" : " (ALL) "
               },
               Combination12Cut = "(AM < 10000*MeV)",
               CombinationCut = "(AM < 9500*MeV) & (AM > 2000*MeV)",
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 9 ) &
            (BPVVDCHI2 > 49) &
            (BPVDIRA > 0.99 )
            """
            )

    _stdLooseElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")
    _stdLooseMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
    _stdLooseTaus = DataOnDemand(Location = "Phys/StdTightDetachedTau3pi/Particles")

    return Selection (name,
                      Algorithm = Xib2TauMuE,
                      RequiredSelections = [ _stdLooseMuons, _stdLooseTaus, _stdLooseElectrons ])

#####################################################
def makeXib22ETau(name, config):
    """
    Make Xib / Omega_b -> e e tau
    """

    Xib22ETau = Combine3Particles(
               DecayDescriptors = ["[ Xi_b- -> e+ e- tau- ]cc","[ Xi_b- -> e- e- tau+ ]cc"],#,"[ Xi_b- -> e- e- tau- ]cc"],
               DaughtersCuts = {
               "e+" : " ( PT > 200 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  9 ) & (PROBNNe>0.3) & (PIDe>3)" % config,
               "tau+" : " ((BPVDIRA>0.992) & (PT>1200*MeV) & ( VFASPF(VCHI2PDOF) < 9 )) "
               },
               Combination12Cut = "(AM < 10000*MeV) & (APT>600*MeV)",
               CombinationCut = "(AM < 9500*MeV) & (AM > 2000*MeV)",
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 9 ) &
            (BPVVDCHI2 > 45) &
            (BPVDIRA > 0.95 ) & (PT>3500*MeV)
            """
            )

    _stdLooseElectrons = DataOnDemand(Location = "Phys/StdLooseElectrons/Particles")
    _stdLooseTaus = DataOnDemand(Location = "Phys/StdTightDetachedTau3pi/Particles")

    return Selection (name,
                      Algorithm = Xib22ETau,
                      RequiredSelections = [ _stdLooseTaus, _stdLooseElectrons ])

#####################################################


def makeXib25Mu(name, config):
    """
    Make Xi_b / Omega_b -> 5 muons
    """

    Xib2MuMuMuMuMu = Combine5Particles(
                   DecayDescriptors = ["[Xi_b- -> mu+ mu+ mu- mu- mu-]cc","[Xi_b- -> mu- mu- mu- mu+ mu-]cc","[Xi_b- -> mu- mu- mu- mu- mu-]cc"],
                   DaughtersCuts = { "mu+" : " ( PT > 100 * MeV ) & ( TRGHOSTPROB < %(TrackGhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  6 ) " % config },
                   Combination12Cut = "(AM<7000*MeV)  & ( ANUM(( 'mu-' == ABSID ) & ISMUON )> 0.5 ) & (APT>350*MeV)",   # 1778 + 400 - 3*100
                   Combination123Cut = "(AM<7100*MeV) & ( ANUM(( 'mu-' == ABSID ) & ISMUON )> 1.5 ) & (APT>600*MeV)",   # 1778 + 400 - 2*100
                   Combination1234Cut = "(AM<7200*MeV) & ( ANUM(( 'mu-' == ABSID ) & ISMUON )> 2.5 )",   # 1778 + 400 - 1*100
                   CombinationCut = "(AM < 7400*MeV) & (AM > 5100*MeV) & ( ANUM(( 'mu-' == ABSID ) & ISMUON )> 3.5 ) &  ( ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.1)) > 2.5)",
                   MotherCut = """
            ( VFASPF(VCHI2PDOF) < 10 ) &
            (BPVVDCHI2 > 36) &
            (BPVDIRA > 0.999 )
            """
                   )

    _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

    return Selection (name,
                      Algorithm = Xib2MuMuMuMuMu,
                      RequiredSelections = [ _stdNoPIDLooseMuons ])


#####################################################
def _makeMuMuSS(  name, params):
        """
        Makes MuMu same sign combinations
        """
        from StandardParticles import StdLooseMuons as Muons

        _DecayDescriptor = "[rho(770)- -> mu- mu-]cc"

        _MassCut = "(AM < 7000*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"

        _DaughtersCut = "(PT > %(PTCut)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s)" % params

        _Combine = CombineParticles( DecayDescriptor = _DecayDescriptor,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _Combine.DaughtersCuts = {
            "mu-" : _DaughtersCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons ] )

#####################################################
def _make3MuSS(  name, params):
        """
        Makes MuMu same sign combinations
        """
        from StandardParticles import StdLooseMuons as Muons

        _DecayDescriptor = "[rho(770)- -> mu- mu- mu-]cc"

        _MassCut12 = "(AM < 6900*MeV)"
        _MassCut = "(AM < 7000*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"

        _DaughtersCut = "(PT > %(PTCut)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s)" % params

        _Combine = Combine3Particles( DecayDescriptor = _DecayDescriptor,
                                     Combination12Cut  = _MassCut12,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _Combine.DaughtersCuts = {
            "mu-" : _DaughtersCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons ] )

#####################################################
def _make2MuESS( name, params):
        """
        Makes MuMu same sign combinations
        """
        from StandardParticles import StdLooseMuons as Muons
        from StandardParticles import StdLooseElectrons as Electrons

        _DecayDescriptor = "[rho(770)- -> mu- mu- e-]cc"

        _MassCut12 = "(AM < 6900*MeV)"
        _MassCut = "(AM < 7000*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"

        _DaughtersCut = "(PT > %(PTCut)s) & " \
                        "(MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s)" % params

        _Combine = Combine3Particles( DecayDescriptor = _DecayDescriptor,
                                     Combination12Cut  = _MassCut12,
                                     CombinationCut  = _MassCut,
                                     MotherCut       = _MotherCut )

        _Combine.DaughtersCuts = {
            "mu-" : _DaughtersCut,
            }

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ Muons, Electrons ] )


#####################################################
def _makeppSS( name, params):
    """
    Make a same-sign f2 -> p p in entire range.
    """

    _Decays = "[Delta++ -> p+ p+]cc"

    _CombinationCut = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < %(DiHadronMassLoose)s *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s)" % params

    _DaughterCut = "(PT > %(PTCut)s *MeV) & (P > %(ProtonP)s *MeV) & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s)) & (PROBNNp > %(ProbNNCutTight)s)" % params

    _Combine = CombineParticles( DecayDescriptor = _Decays,
                                 CombinationCut  = _CombinationCut,
                                 MotherCut       = _MotherCut)

    _Combine.DaughtersCuts = {
        "p+"  : _DaughterCut,
        }
    _stdProtons = DataOnDemand(Location = "Phys/StdAllLooseProtons/Particles")

    return Selection(name, Algorithm = _Combine, RequiredSelections = [ _stdProtons ] )
#####################################################
def _make3pSS( name, params):
    """
    Make a same-sign f2 -> p p in entire range.
    """

    _Decays = "[Delta++ -> p+ p+ p+]cc"

    _CombinationCut12 = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < (%(DiHadronMassLoose)s - 100) *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _CombinationCut = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < %(DiHadronMassLoose)s *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s)" % params

    _DaughterCut = "(PT > %(PTCut)s *MeV)  & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s)) & (PROBNNp > %(ProbNNCutTight)s)" % params

    _Combine = Combine3Particles( DecayDescriptor = _Decays,
                                 Combination12Cut  = _CombinationCut,
                                 CombinationCut  = _CombinationCut,
                                 MotherCut       = _MotherCut)

    _Combine.DaughtersCuts = {
        "p+"  : _DaughterCut,
        }
    _stdProtons = DataOnDemand(Location = "Phys/StdAllLooseProtons/Particles")

    return Selection(name, Algorithm = _Combine, RequiredSelections = [ _stdProtons ] )
#####################################################
def _makeDeltaSS( name, params):
    """
    Make a same-sign Delta++ -> p pi+ in the restricted mass range.
    """

    _Decays = "[Delta++ -> p+ pi+]cc"

    _CombinationCut = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < %(DiHadronMassTight)s *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s)" % params

    _DaughterCut_p = "(PT > %(PTCut)s *MeV) & (P > %(ProtonP)s *MeV) & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s)) & (PROBNNp > %(ProbNNCutTight)s)" % params
    _DaughterCut_pi = "(PT > %(PTCut)s *MeV) & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s))" % params # & (PROBNNpi > %(ProbNNCut)s))

    _Combine = CombineParticles( DecayDescriptor = _Decays,
                                 CombinationCut  = _CombinationCut,
                                 MotherCut       = _MotherCut)

    _Combine.DaughtersCuts = {
        "p+"  : _DaughterCut_p,
        "pi+" : _DaughterCut_pi
        }

    _stdProtons = DataOnDemand(Location = "Phys/StdAllLooseProtons/Particles")
    _stdPions = DataOnDemand(Location = "Phys/StdAllLoosePions/Particles")

    return Selection(name, Algorithm = _Combine, RequiredSelections = [ _stdProtons, _stdPions ] )
#####################################################
def _makepipiSS( name, params):
    """
    Make a same-sign Delta++ -> pi+ pi+ in the restricted mass range.
    """

    _Decays = "[Delta++ -> pi+ pi+]cc"

    _CombinationCut = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < %(DiHadronMassLoose)s *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s)" % params

    _DaughterCut_pi = "(PT > %(PTCut)s *MeV) & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s))" % params # & (PROBNNpi > %(ProbNNCut)s))

    _Combine = CombineParticles( DecayDescriptor = _Decays,
                                 CombinationCut  = _CombinationCut,
                                 MotherCut       = _MotherCut)

    _Combine.DaughtersCuts = {
        "pi+" : _DaughterCut_pi
        }


    _stdPions = DataOnDemand(Location = "Phys/StdAllLoosePions/Particles")

    return Selection(name, Algorithm = _Combine, RequiredSelections = [ _stdPions ] )
#####################################################
def _makepipipiSS( name, params):
    """
    Make a same-sign Delta++ -> pi+ pi+ pi+ in the restricted mass range.
    """

    _Decays = "[Delta++ -> pi+ pi+ pi+]cc"

    _CombinationCut = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < %(DiHadronMassLoose)s *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s)" % params

    _DaughterCut_pi = "(PT > %(PTCut)s *MeV) & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s))" % params # & (PROBNNpi > %(ProbNNCut)s))

    _Combine = CombineParticles( DecayDescriptor = _Decays,
                                 CombinationCut  = _CombinationCut,
                                 MotherCut       = _MotherCut)

    _Combine.DaughtersCuts = {
        "pi+" : _DaughterCut_pi
        }


    _stdPions = DataOnDemand(Location = "Phys/StdAllLoosePions/Particles")

    return Selection(name, Algorithm = _Combine, RequiredSelections = [ _stdPions ] )

#####################################################
def _makeKKSS( name, params):
    """
    Make a same-sign Delta++ -> K+K+ in the restricted mass range.
    """

    _Decays = "[Delta++ -> K+ K+]cc"

    _CombinationCut = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < %(DiHadronMassLoose)s *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s)" % params

    _DaughterCut_K = "(PT > %(PTCut)s *MeV) & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s))" % params # & (PROBNNk > %(ProbNNCut)s))

    _Combine = CombineParticles( DecayDescriptor = _Decays,
                                 CombinationCut  = _CombinationCut,
                                 MotherCut       = _MotherCut)

    _Combine.DaughtersCuts = {
        "K+" : _DaughterCut_K
        }


    _stdKaons = DataOnDemand(Location = "Phys/StdAllLooseKaons/Particles")

    return Selection(name, Algorithm = _Combine, RequiredSelections = [ _stdKaons ] )
#####################################################
def _makeKKKSS( name, params):
    """
    Make a same-sign Delta++ -> K+K+K+ in the restricted mass range.
    """

    _Decays = "[Delta++ -> K+ K+ K+]cc"

    _CombinationCut = "(APT > %(PTCut)s *MeV) & " \
                      "(AM < %(DiHadronMassLoose)s *MeV) & " \
                      "(ADOCACHI2CUT( %(DiHadronADOCACHI2)s  , ''))" % params

    _MotherCut = "(VFASPF(VCHI2PDOF) < %(DiHadronVtxCHI2)s)" % params

    _DaughterCut_K = "(PT > %(PTCut)s *MeV) & " \
               "(ISBASIC & (MIPCHI2DV(PRIMARY) > %(HadronIPCHI2)s))" % params # & (PROBNNk > %(ProbNNCut)s))

    _Combine = CombineParticles( DecayDescriptor = _Decays,
                                 CombinationCut  = _CombinationCut,
                                 MotherCut       = _MotherCut)

    _Combine.DaughtersCuts = {
        "K+" : _DaughterCut_K
        }


    _stdKaons = DataOnDemand(Location = "Phys/StdAllLooseKaons/Particles")

    return Selection(name, Algorithm = _Combine, RequiredSelections = [ _stdKaons ] )

#####################################################
def _makeppmumu(name, dimuons, pps, config):
    """
    Make B0 -> p+ p+ mu- mu-
    """

    ppmm = CombineParticles(
               DecayDescriptors = [" [ Xi_b0 -> Delta++ rho(770)- ]cc",  " [ Xi_b0 -> Delta++ rho(770)+ ]cc"],
               DaughtersCuts = { "Delta++" : " ( ALL ) " ,
                                 "rho(770)-" : " ( ALL ) " ,
               },
               CombinationCut = "(AM < 7500*MeV) & (AM > 4800*MeV)",
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 16 ) &
            (BPVVDCHI2 > 36) &
            (BPVDIRA > 0.999 )
            """
            )

    return Selection (name,
                      Algorithm = ppmm,
                      RequiredSelections = [ pps, dimuons ])
#####################################################
def _make3p3mu(name, trimuons, trips, config):
    """
    Make B0 -> p+ p+ p+ mu- mu- mu-
    """

    ppmm = CombineParticles(
               DecayDescriptors = [" [ Xi_b0 -> Delta++ rho(770)- ]cc",  " [ Xi_b0 -> Delta++ rho(770)+ ]cc"],
               DaughtersCuts = { "Delta++" : " ( ALL ) " ,
                                 "rho(770)-" : " ( ALL ) " ,
               },
               CombinationCut = "(AM < 7500*MeV) & (AM > 4800*MeV)",
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 16 ) &
            (BPVVDCHI2 > 36) &
            (BPVDIRA > 0.999 )
            """
            )

    return Selection (name,
                      Algorithm = ppmm,
                      RequiredSelections = [ trips, trimuons ])
#####################################################
def _makeDeltapp3mu(name, trimuons, deltas, config):
    """
    Make Xi_b- -> Delta++ mu- mu- mu-
    """

    ppmm = CombineParticles(
               DecayDescriptors = [" [ Xi_b0 -> Delta++ rho(770)- ]cc",  " [ Xi_b0 -> Delta++ rho(770)+ ]cc"],
               DaughtersCuts = { "Delta++" : " ( ALL ) " ,
                                 "rho(770)-" : " ( ALL ) " ,
               },
               CombinationCut = "(AM < 7500*MeV) & (AM > 4800*MeV)",
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 16 ) &
            (BPVVDCHI2 > 25) &
            (BPVDIRA > 0.999 )
            """
            )

    return Selection (name,
                      Algorithm = ppmm,
                      RequiredSelections = [ deltas, trimuons ])

#####################################################
def _makepipi3mu(name, trimuons, pipis, kks, config):
    """
    Make Xi_b- -> pi+ pi+ mu- mu- mu- (same with kaons) 1903 09675
    """

    ppmm = CombineParticles(
               DecayDescriptors = [" [ Xi_b0 -> Delta++ rho(770)- ]cc",  " [ Xi_b0 -> Delta++ rho(770)+ ]cc"],
               DaughtersCuts = { "Delta++" : " ( ALL ) " ,
                                 "rho(770)-" : " ( NINTREE(( 'mu-' == ABSID ) & (PROBNNmu>0.))==3  ) " ,
               },
               CombinationCut = "(AM < 7500*MeV) & (AM > 4800*MeV)",
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 12 ) &
            (BPVVDCHI2 > 25) &
            (BPVDIRA > 0.999 )
            """
            )

    return Selection (name,
                      Algorithm = ppmm,
                      RequiredSelections = [ pipis, kks, trimuons ])

#####################################################
def _makepipipi3mu(name, trimuons, pipipis, kkks, config):
    """
    Make Xi_b0 -> pi+ pi+ pi+ mu- mu- mu- (same with kaons)
    """

    ppmm = CombineParticles(
               DecayDescriptors = [" [ Xi_b0 -> Delta++ rho(770)- ]cc",  " [ Xi_b0 -> Delta++ rho(770)+ ]cc"],
               DaughtersCuts = { "Delta++" : " ( ALL ) " ,
                                 "rho(770)-" : " ( NINTREE(( 'mu-' == ABSID ) & (PROBNNmu>0.))==3  ) " ,
               },
               CombinationCut = "(AM < 7500*MeV) & (AM > 4800*MeV)",
               MotherCut = """
            ( VFASPF(VCHI2PDOF) < 12 ) &
            (BPVVDCHI2 > 25) &
            (BPVDIRA > 0.999 )
            """
            )

    return Selection (name,
                      Algorithm = ppmm,
                      RequiredSelections = [ pipipis, kkks, trimuons ])
