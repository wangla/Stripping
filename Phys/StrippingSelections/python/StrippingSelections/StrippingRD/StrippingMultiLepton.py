###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Vitalii Lisovskyi'
__date__    = '05/03/2021'
__version__ = '$Revision: 0 $'

__all__ = ( 'MultiLeptonConf', 'default_config' )

"""
Selections for channels with >3 leptons

+ B -> 4mu K
+ Bs -> 4mu phi
+ B0 -> 4mu KS
+ B0 -> 4mu K*
+ Lb -> 4mu L
+ Lb -> 4mu pK

Various inclusive lines for 4, 5, 6, 8 muons in the final state;
also combinations of X muons and (4-X) electrons where X=0,1,2,3.

"""

default_config = {
    'NAME'                       : 'MultiLepton',
    'BUILDERTYPE'                : 'MultiLeptonConf',
    'CONFIG'                     :
        {
        'BFlightCHI2'            : 36 #100
        , 'BDIRA'                : 0.999
        , 'BIPCHI2'              : 25
        , 'BVertexCHI2'          : 16
        , 'DiLeptonPT'           : 0
        , 'DiLeptonFDCHI2'       : 10
        , 'DiLeptonIPCHI2'       : 0
        , 'LeptonIPCHI2'         : 9
        , 'LeptonPT'             : 300
        , 'KaonIPCHI2'           : 9
        , 'KaonPT'               : 250
        , 'KaonPTTight'          : 400
        , 'UpperMass'            : 5500
        , 'BMassWindow'          : 1500
        , 'Trk_Chi2'             : 3
        , 'Trk_GhostProb'        : 0.3
        , 'K1_MassWindow_Lo'     : 0
        , 'K1_MassWindow_Hi'     : 6300
        , 'K1_VtxChi2'           : 25
        , 'K1_SumPTHad'          : 800
        , 'Mu_SumIPChi2Had'      : 12.0
        , 'LamLam_VtxChi2'       : 36
        , 'V0TAU'                : 0.0005
        , 'DiHadronADOCA'        : 0.75
        , 'Bu2mmLinePrescale'    : 1
        },
    'WGs'     : [ 'RD' ],
    'STREAMS'     : { 'Leptonic' :
                       [
                      'StrippingMultiLepton_B24muXLine','StrippingMultiLepton_B24muXUpLine','StrippingMultiLepton_Jpsi22Mu2EPromptLine','StrippingMultiLepton_Jpsi23MuEDetachedLine','StrippingMultiLepton_Jpsi23MuEPromptLine','StrippingMultiLepton_Jpsi2Mu3EDetachedLine','StrippingMultiLepton_Jpsi2Mu3EPromptLine','StrippingMultiLepton_InclDet4muLowMassLine','StrippingMultiLepton_InclDet4muLowMassUpLine','StrippingMultiLepton_Jpsi24MuPromptLine',
                      'StrippingMultiLepton_B24muXLongLivedLine','StrippingMultiLepton_B26muXLongLivedLine'],
                       'Dimuon' : ['StrippingMultiLepton_B24muXTightLine','StrippingMultiLepton_B22mu2eXTightLine','StrippingMultiLepton_InclDet4muLine','StrippingMultiLepton_InclDet2mu2muLine','StrippingMultiLepton_InclDet6muLine','StrippingMultiLepton_InclDet5muLine','StrippingMultiLepton_InclPrompt6muLine','StrippingMultiLepton_Incl8muLine','StrippingMultiLepton_Incl8mu4bodyLine','StrippingMultiLepton_InclDet6mu3bodyLine','StrippingMultiLepton_Jpsi24MuPromptTightLine','StrippingMultiLepton_Jpsi22Mu2EDetachedLine','StrippingMultiLepton_Jpsi22E2EDetachedLine','StrippingMultiLepton_Jpsi22Mu2ESSDetachedLine',
                       'StrippingMultiLepton_Incl8mu4bodyLongLivedLine','StrippingMultiLepton_InclDet6mu3bodyLongLivedLine']
                      }
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, DaVinci__N3BodyDecays, DaVinci__N4BodyDecays, DaVinci__N5BodyDecays, DaVinci__N6BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import *
from CommonParticles.Utils import *


class MultiLeptonConf(LineBuilder) :
    """
    Builder
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        'BFlightCHI2'
        , 'BDIRA'
        , 'BIPCHI2'
        , 'BVertexCHI2'
        , 'DiLeptonPT'
        , 'DiLeptonFDCHI2'
        , 'DiLeptonIPCHI2'
        , 'LeptonIPCHI2'
        , 'LeptonPT'
        , 'KaonIPCHI2'
        , 'KaonPT'
        , 'KaonPTTight'
        , 'UpperMass'
        , 'BMassWindow'
        , 'Trk_Chi2'
        , 'Trk_GhostProb'
        , 'K1_MassWindow_Lo'
        , 'K1_MassWindow_Hi'
        , 'K1_VtxChi2'
        , 'K1_SumPTHad'
        , 'Mu_SumIPChi2Had'
        , 'LamLam_VtxChi2'
        , 'V0TAU'
        , 'DiHadronADOCA'
        , 'Bu2mmLinePrescale'
      )

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name
        fourmXLine_name   = name+'_B24muX'
        fourmX_longlived_Line_name   = name+'_B24muXLongLived'
        sixmX_longlived_Line_name   = name+'_B26muXLongLived'
        fourmXUpLine_name   = name+'_B24muXUp'
        fourmXLineTight_name   = name+'_B24muXTight'
        twomutwoeXLineTight_name  = name+'_B22mu2eXTight'
        Incl4mLine_name = name+'_InclDet4mu'
        Incl4mLowLine_name = name+'_InclDet4muLowMass'
        Incl4mLowUpLine_name = name+'_InclDet4muLowMassUp'
        Incl2m2mLine_name = name+'_InclDet2mu2mu'
        Incl6mLine_name = name+'_InclDet6mu'
        Incl6m3xLine_name = name+'_InclDet6mu3body'
        Incl6m_longlived_Line_name = name+'_InclDet6mu3bodyLongLived'
        InclPrompt6mLine_name = name+'_InclPrompt6mu'
        Incl8mLine_name = name+'_Incl8mu'
        Incl8m4xLine_name = name+'_Incl8mu4body'
        Incl8m4x_longlived_Line_name = name+'_Incl8mu4bodyLongLived'
        Incl5mLine_name = name+'_InclDet5mu'
        Jpsi24MuPromptLine_name   = name+'_Jpsi24MuPrompt'
        Jpsi24MuPromptTightLine_name   = name+'_Jpsi24MuPromptTight'
        #Ups24MuPromptLine_name   = name+'_Ups24MuPrompt'
        #Phi24MuPromptLine_name   = name+'_Phi24MuPrompt'
        Jpsi22E2EDetLine_name = name+'_Jpsi22E2EDetached'
        Jpsi22Mu2EPromptLine_name   = name+'_Jpsi22Mu2EPrompt'
        Jpsi22Mu2EDetLine_name   = name+'_Jpsi22Mu2EDetached'
        Jpsi22Mu2ESSDetLine_name   = name+'_Jpsi22Mu2ESSDetached'
        Jpsi23MuEDetLine_name   = name+'_Jpsi23MuEDetached'
        Jpsi23MuEPromptLine_name   = name+'_Jpsi23MuEPrompt'
        Jpsi2Mu3EDetLine_name   = name+'_Jpsi2Mu3EDetached'
        Jpsi2Mu3EPromptLine_name   = name+'_Jpsi2Mu3EPrompt'

        from StandardParticles import StdLoosePions as Pions
        from StandardParticles import StdLooseKaons as Kaons
        from StandardParticles import StdLooseProtons as Protons
        from StandardParticles import StdVeryLooseKsLL as KshortsLL
        from StandardParticles import StdLooseKsDD as KshortsDD
        from StandardParticles import StdVeryLooseLambdaLL as LambdasLL
        from StandardParticles import StdLooseLambdaDD as LambdasDD
        from StandardParticles import StdLooseLambdaLD as LambdasLD
        from StandardParticles import StdLoosePhi2KK as Phi2KK
        from StandardParticles import StdLooseKstar2Kpi as KstartoKpi
        from StandardParticles import StdLooseLambdastar2pK as Lstar

        # 1 : Make particles



        #hadrons for B-> H+4mu lines

        SelKaons  = self._filterKaon( name   = "KaonsFor" + self._name,
                                        sel    = Kaons,
                                        params = config )

        SelPhis  = self._filterPhi( name   = "PhisFor" + self._name,
                                        sel    = Phi2KK,
                                        params = config )

        SelKstars  = self._filterKst( name   = "KstarsFor" + self._name,
                                        sel    = KstartoKpi,
                                        params = config )

        SelLstars  = self._filterLst( name   = "LstarsFor" + self._name,
                                        sel    = Lstar,
                                        params = config )

        SelKshortsLL = self._filterHadron( name   = "KshortsLLFor" + self._name,
                                           sel    = KshortsLL,
                                           params = config )

        SelKshortsDD = self._filterHadron( name   = "KshortsDDFor" + self._name,
                                           sel    =  KshortsDD,
                                           params = config )

        SelLambdasLL = self._filterHadron( name   = "LambdasLLFor" + self._name,
                                           sel    =  LambdasLL,
                                           params = config )

        SelLambdasDD = self._filterHadron( name   = "LambdasDDFor" + self._name,
                                           sel    =  LambdasDD,
                                           params = config )

        #handy leptons

        SelDiMuonDetached = self._makeDiMuonDetached( name   = "2muDetFor" + self._name,
                               params = config )

        SelDiMuonPrompt = self._makeDiMuonPrompt( name   = "2muPromptFor" + self._name,
                               params = config )

        SelDiMuonLongLived = self._makeDiMuonLongLived( name   = "2muLongLivedFor" + self._name,
                               params = config )

        SelFourMuonDetached = self._makeFourMuonDetached( name   = "4muDetFor" + self._name,
                               #dimuons=SelDiMuonDetached,
                               params = config )

        SelFourMuonDetachedUp = self._makeFourMuonDetachedUp( name   = "4muDetUpFor" + self._name,
                               #dimuons=SelDiMuonDetached,
                               params = config )

        SelDiElectron = self._makeDiElectron( name   = "2eFor" + self._name,
                               params = config )

        SelMuE = self._makeMuEDetached( name   = "mueFor" + self._name,
                               params = config )

        Sel2Mu2EforExclLine = self._make2Mu2EInclDetLoose(name = "2Mu2EFor" + self._name,
                                   dimuon = SelDiMuonDetached,
                                   dielectron = SelDiElectron,
                                   params   = config)


        #  Combine Particles: final combiners for stripping lines


        SelB24mX = self._makeB24muX(fourmXLine_name,
                                   fourlepton = SelFourMuonDetached,
                                   hadrons  = [ SelKaons, SelPhis, SelKstars, SelLstars, SelKshortsLL, SelKshortsDD, SelLambdasDD, SelLambdasLL ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        SelB24mX_LongLived = self._makeB24muX_LongLived(fourmX_longlived_Line_name,
                                   dilepton = SelDiMuonLongLived,
                                   hadrons  = [ SelKaons, SelPhis, SelKstars, SelLstars, SelLambdasDD, SelLambdasLL ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        SelB26mX_LongLived = self._makeB26muX_LongLived(sixmX_longlived_Line_name,
                                   dilepton = SelDiMuonLongLived,
                                   hadrons  = [ SelKaons, SelPhis, SelKstars, SelLstars, SelLambdasDD, SelLambdasLL ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        SelB24mXUp = self._makeB24muX(fourmXUpLine_name,
                                   fourlepton = SelFourMuonDetachedUp,
                                   hadrons  = [ SelKaons, SelPhis, SelKstars, SelLstars, SelKshortsLL, SelKshortsDD, SelLambdasDD, SelLambdasLL ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        SelB24mXTight = self._makeB24muXTight(fourmXLineTight_name,
                                   fourlepton = SelFourMuonDetached,
                                   hadrons  = [ SelKaons, SelPhis, SelKstars, SelLstars, SelKshortsLL, SelKshortsDD, SelLambdasDD, SelLambdasLL  ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        SelB22m2eXTight = self._makeB22mu2EXTight(twomutwoeXLineTight_name,
                                   fourlepton = Sel2Mu2EforExclLine,
                                   hadrons  = [ SelKaons, SelPhis, SelKstars, SelLstars, SelKshortsLL, SelKshortsDD, SelLambdasDD, SelLambdasLL  ],
                                   params   = config,
                                   masscut  = "ADAMASS('B+') <  %(BMassWindow)s *MeV"% config)

        Sel2mu2muDetachedIncl = self._make2mu2muInclDet(Incl2m2mLine_name,
                                   dilepton = SelDiMuonDetached,
                                   params   = config)

        SelFourMuonDetachedIncl = self._make4muInclDet(Incl4mLine_name,
                                   #dilepton = SelDiMuonDetached,
                                   params   = config)

        SelFourMuonDetachedInclLow = self._make4muInclDetLowMass(Incl4mLowLine_name,
                                   #dilepton = SelDiMuonDetached,
                                   params   = config)

        SelFourMuonDetachedInclLowUp = self._make4muInclDetLowMassUp(Incl4mLowUpLine_name,
                                   params   = config)

        SelSixMuonDetachedIncl = self._make6muInclDet(Incl6mLine_name,
#                                   dilepton = SelDiMuonDetached,
                                   params   = config)

        SelSixMuonLongLivedIncl = self._make6mu3bodyInclLongLived(Incl6m_longlived_Line_name,
                                   dilepton = SelDiMuonLongLived,
                                   params   = config)

        SelSixMuon3xDetachedIncl = self._make6mu3bodyInclDet(Incl6m3xLine_name,
                                   dilepton = SelDiMuonDetached,
                                   params   = config)

        SelSixMuonPromptIncl = self._make6muInclPrompt(InclPrompt6mLine_name,
#                                   dilepton = SelDiMuonDetached,
                                   params   = config)

        SelOttoMuonPromptIncl = self._make8muIncl(Incl8mLine_name,
#                                   dilepton = SelDiMuonDetached,
                                   params   = config)

        SelOttoMuon4xPromptIncl = self._make8mu4bodyIncl(Incl8m4xLine_name,
#                                   dilepton = SelDiMuonPrompt,
                                   params   = config)

        SelOttoMuon4xLongLivedIncl = self._make8mu4bodyLongLived(Incl8m4x_longlived_Line_name,
                                   dilepton = SelDiMuonLongLived,
                                   params   = config)

        SelFiveMuonDetachedIncl = self._make5Mu(Incl5mLine_name,
                                   params   = config)

        SelJpsi24MuPrompt = self._makeFourMuonPromptJpsi(Jpsi24MuPromptLine_name,
                                   dilepton = SelDiMuonPrompt,
                                   params   = config)

        SelJpsi24MuPromptTight = self._makeFourMuonPromptJpsiTight(Jpsi24MuPromptTightLine_name,
                                   dilepton = SelDiMuonPrompt,
                                   params   = config)

        SelJpsi22Mu2EPrompt = self._make2Mu2EPrompt(Jpsi22Mu2EPromptLine_name,
                                   dimuon = SelDiMuonPrompt,
                                   dielectron = SelDiElectron,
                                   params   = config)

        SelJpsi22Mu2EDetachedIncl = self._make2Mu2EInclDet(Jpsi22Mu2EDetLine_name,
                                   dimuon = SelDiMuonDetached,
                                   dielectron = SelDiElectron,
                                   params   = config)

        SelJpsi22E2EDetachedIncl = self._make2E2EInclDet(Jpsi22E2EDetLine_name,
                                   dielectron = SelDiElectron,
                                   params   = config)

        SelJpsi22Mu2ESSDetachedIncl = self._make2Mu2ESSInclDet(Jpsi22Mu2ESSDetLine_name,
                                   dilepton = SelMuE,
                                   params   = config)

        SelJpsi23MuEDetachedIncl = self._make3MuEInclDet(Jpsi23MuEDetLine_name,
#                                   dimuon = SelDiMuonDetached,
#                                   mue = SelMuE,
                                   params   = config)

        SelJpsi23MuEPromptIncl = self._make3MuEInclPrompt(Jpsi23MuEPromptLine_name,
                                   params   = config)

        SelJpsi2Mu3EDetachedIncl = self._makeMu3EInclDet(Jpsi2Mu3EDetLine_name,
                                   dielectron = SelDiElectron,
                                   mue = SelMuE,
                                   params   = config)

        SelJpsi2Mu3EPromptIncl = self._makeMu3EInclPrompt(Jpsi2Mu3EPromptLine_name,
                                   dielectron = SelDiElectron,
                                   # mue = SelMuE,
                                   params   = config)
        #  Declare Lines

        SPDFilter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 900 )" ,
            'Preambulo' : [ "from LoKiNumbers.decorators import *", "from LoKiCore.basic import LHCb" ]
            }


        self.B24mXLine = StrippingLine(fourmXLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB24mX,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> (X -> ^mu+ mu+ mu- mu-) X ]CC" : "Muon1BDT",
                                         "[ Beauty -> (X -> mu+ ^mu+ mu- mu-) X ]CC" : "Muon2BDT",
                                         "[ Beauty -> (X -> mu+ mu+ ^mu- mu-) X ]CC" : "Muon3BDT",
                                         "[ Beauty -> (X -> mu+ mu+ mu- ^mu-) X ]CC" : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B24mXLine_LongLived = StrippingLine(fourmX_longlived_Line_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB24mX_LongLived,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> (X -> ^mu+ mu-) (X -> mu+ mu-) X ]CC" : "Muon1BDT",
                                         "[ Beauty -> (X -> mu+ ^mu-) (X -> mu+ mu-) X ]CC" : "Muon2BDT",
                                         "[ Beauty -> (X -> mu+ mu-) (X -> ^mu+ mu-) X ]CC" : "Muon3BDT",
                                         "[ Beauty -> (X -> mu+ mu-) (X -> mu+ ^mu-) X ]CC" : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B26mXLine_LongLived = StrippingLine(sixmX_longlived_Line_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB26mX_LongLived,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> (X -> ^mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-) X ]CC" : "Muon1BDT",
                                         "[ Beauty -> (X -> mu+ ^mu-) (X -> mu+ mu-) (X -> mu+ mu-) X ]CC" : "Muon2BDT",
                                         "[ Beauty -> (X -> mu+ mu-) (X -> ^mu+ mu-) (X -> mu+ mu-) X ]CC" : "Muon3BDT",
                                         "[ Beauty -> (X -> mu+ mu-) (X -> mu+ ^mu-) (X -> mu+ mu-) X ]CC" : "Muon4BDT",
                                         "[ Beauty -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> ^mu+ mu-) X ]CC" : "Muon5BDT",
                                         "[ Beauty -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ ^mu-) X ]CC" : "Muon6BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B24mXUpLine = StrippingLine(fourmXUpLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB24mXUp,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> (X -> ^mu+ mu+ mu- mu-) X ]CC" : "Muon1BDT",
                                         "[ Beauty -> (X -> mu+ ^mu+ mu- mu-) X ]CC" : "Muon2BDT",
                                         "[ Beauty -> (X -> mu+ mu+ ^mu- mu-) X ]CC" : "Muon3BDT",
                                         "[ Beauty -> (X -> mu+ mu+ mu- ^mu-) X ]CC" : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B24mXTightLine = StrippingLine(fourmXLineTight_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB24mXTight,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> (X -> ^mu+ mu+ mu- mu-) X ]CC" : "Muon1BDT",
                                         "[ Beauty -> (X -> mu+ ^mu+ mu- mu-) X ]CC" : "Muon2BDT",
                                         "[ Beauty -> (X -> mu+ mu+ ^mu- mu-) X ]CC" : "Muon3BDT",
                                         "[ Beauty -> (X -> mu+ mu+ mu- ^mu-) X ]CC" : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B22m2eXTightLine = StrippingLine(twomutwoeXLineTight_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelB22m2eXTight,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ Beauty -> (X -> ( X0 -> ^mu+ mu-) X0) X ]CC" : "Muon1BDT",
                                         "[ Beauty -> (X -> ( X0 -> mu+ ^mu-) X0) X ]CC" : "Muon2BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B24mInclLowLine = StrippingLine(Incl4mLowLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelFourMuonDetachedInclLow,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> ^mu+ mu+ mu- mu- " : "Muon1BDT",
                                         " X -> mu+ ^mu+ mu- mu- " : "Muon2BDT",
                                         " X -> mu+ mu+ ^mu- mu- " : "Muon3BDT",
                                         " X -> mu+ mu+ mu- ^mu- " : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B24mInclLowUpLine = StrippingLine(Incl4mLowUpLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelFourMuonDetachedInclLowUp,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> ^mu+ mu+ mu- mu- " : "Muon1BDT",
                                         " X -> mu+ ^mu+ mu- mu- " : "Muon2BDT",
                                         " X -> mu+ mu+ ^mu- mu- " : "Muon3BDT",
                                         " X -> mu+ mu+ mu- ^mu- " : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B24mInclLine = StrippingLine(Incl4mLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelFourMuonDetachedIncl,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> ^mu+ mu+ mu- mu- " : "Muon1BDT",
                                         " X -> mu+ ^mu+ mu- mu- " : "Muon2BDT",
                                         " X -> mu+ mu+ ^mu- mu- " : "Muon3BDT",
                                         " X -> mu+ mu+ mu- ^mu- " : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B22m2mInclLine = StrippingLine(Incl2m2mLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = Sel2mu2muDetachedIncl,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) (X -> mu+ mu-) " : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) (X -> mu+ mu-) " : "Muon2BDT",
                                         " X -> (X -> mu+ mu-) (X -> ^mu+ mu-) " : "Muon3BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ ^mu-) " : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B26mInclLine = StrippingLine(Incl6mLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelSixMuonDetachedIncl,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu- mu- mu-) (X -> mu+ mu+ mu+) " : "Muon1BDT",
                                         " X -> (X -> mu- ^mu- mu-) (X -> mu+ mu+ mu+) " : "Muon2BDT",
                                         " X -> (X -> mu- mu- ^mu-) (X -> mu+ mu+ mu+) " : "Muon3BDT",
                                         " X -> (X -> mu- mu- mu-) (X -> ^mu+ mu+ mu+) " : "Muon4BDT",
                                         " X -> (X -> mu- mu- mu-) (X -> mu+ ^mu+ mu+) " : "Muon5BDT",
                                         " X -> (X -> mu- mu- mu-) (X -> mu+ mu+ ^mu+) " : "Muon6BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.B26m3xInclLine = StrippingLine(Incl6m3xLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelSixMuon3xDetachedIncl,
                                       FILTER            = SPDFilter,
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon2BDT",
                                         " X -> (X -> mu+ mu-) (X -> ^mu+ mu-) (X -> mu+ mu-)" : "Muon3BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ ^mu-) (X -> mu+ mu-)" : "Muon4BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> ^mu+ mu-)" : "Muon5BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ ^mu-)" : "Muon6BDT",
                                         }
                                         },
                                       ],
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.B26m3xInclLongLivedLine = StrippingLine(Incl6m_longlived_Line_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelSixMuonLongLivedIncl,
                                       FILTER            = SPDFilter,
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon2BDT",
                                         " X -> (X -> mu+ mu-) (X -> ^mu+ mu-) (X -> mu+ mu-)" : "Muon3BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ ^mu-) (X -> mu+ mu-)" : "Muon4BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> ^mu+ mu-)" : "Muon5BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ ^mu-)" : "Muon6BDT",
                                         }
                                         },
                                       ],
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.B28m4xInclLongLivedLine = StrippingLine(Incl8m4x_longlived_Line_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelOttoMuon4xLongLivedIncl,
                                       FILTER            = SPDFilter,
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon2BDT",
                                         " X -> (X -> mu+ mu-) (X -> ^mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon3BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ ^mu-) (X -> mu+ mu-) (X -> mu+ mu-)" : "Muon4BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> ^mu+ mu-) (X -> mu+ mu-)" : "Muon5BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ ^mu-) (X -> mu+ mu-)" : "Muon6BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-) (X -> ^mu+ mu-)" : "Muon7BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ mu-) (X -> mu+ ^mu-)" : "Muon8BDT",
                                         }
                                         },
                                       ],
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.B25mInclLine = StrippingLine(Incl5mLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelFiveMuonDetachedIncl,
                                       FILTER            = SPDFilter,
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X -> ^mu+ mu+ mu- mu- mu-]CC" : "Muon1BDT",
                                         "[ X -> mu+ ^mu+ mu- mu- mu-]CC" : "Muon2BDT",
                                         "[ X -> mu+ mu+ ^mu- mu- mu-]CC" : "Muon3BDT",
                                         "[ X -> mu+ mu+ mu- ^mu- mu-]CC" : "Muon4BDT",
                                         "[ X -> mu+ mu+ mu- mu- ^mu-]CC" : "Muon5BDT",
                                         }
                                         },
                                       ],
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.B26mInclPromptLine = StrippingLine(InclPrompt6mLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelSixMuonPromptIncl,
                                       FILTER            = SPDFilter,
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu- mu- mu-) (X -> mu+ mu+ mu+) " : "Muon1BDT",
                                         " X -> (X -> mu- ^mu- mu-) (X -> mu+ mu+ mu+) " : "Muon2BDT",
                                         " X -> (X -> mu- mu- ^mu-) (X -> mu+ mu+ mu+) " : "Muon3BDT",
                                         " X -> (X -> mu- mu- mu-) (X -> ^mu+ mu+ mu+) " : "Muon4BDT",
                                         " X -> (X -> mu- mu- mu-) (X -> mu+ ^mu+ mu+) " : "Muon5BDT",
                                         " X -> (X -> mu- mu- mu-) (X -> mu+ mu+ ^mu+) " : "Muon6BDT",
                                         }
                                         },
                                       ],
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.B28mInclPromptLine = StrippingLine(Incl8mLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelOttoMuonPromptIncl,
                                       FILTER            = SPDFilter,
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu- mu- mu- mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon1BDT",
                                         " X -> (X -> mu- ^mu- mu- mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon2BDT",
                                         " X -> (X -> mu- mu- ^mu- mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon3BDT",
                                         " X -> (X -> mu- mu- mu- ^mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon4BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> ^mu+ mu+ mu+ mu+) " : "Muon5BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> mu+ ^mu+ mu+ mu+) " : "Muon6BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> mu+ mu+ ^mu+ mu+) " : "Muon7BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> mu+ mu+ mu+ ^mu+) " : "Muon8BDT",
                                         }
                                         },
                                       ],
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.B28m4xInclPromptLine = StrippingLine(Incl8m4xLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelOttoMuon4xPromptIncl,
                                       FILTER            = SPDFilter,
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu- mu- mu- mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon1BDT",
                                         " X -> (X -> mu- ^mu- mu- mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon2BDT",
                                         " X -> (X -> mu- mu- ^mu- mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon3BDT",
                                         " X -> (X -> mu- mu- mu- ^mu-) (X -> mu+ mu+ mu+ mu+) " : "Muon4BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> ^mu+ mu+ mu+ mu+) " : "Muon5BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> mu+ ^mu+ mu+ mu+) " : "Muon6BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> mu+ mu+ ^mu+ mu+) " : "Muon7BDT",
                                         " X -> (X -> mu- mu- mu- mu-) (X -> mu+ mu+ mu+ ^mu+) " : "Muon8BDT",
                                         }
                                         },
                                       ],
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.Jpsi24MuPromptLine = StrippingLine(Jpsi24MuPromptLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi24MuPrompt,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) (X -> mu+ mu-) " : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) (X -> mu+ mu-) " : "Muon2BDT",
                                         " X -> (X -> mu+ mu-) (X -> ^mu+ mu-) " : "Muon3BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ ^mu-) " : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi24MuPromptTightLine = StrippingLine(Jpsi24MuPromptTightLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi24MuPromptTight,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) (X -> mu+ mu-) " : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) (X -> mu+ mu-) " : "Muon2BDT",
                                         " X -> (X -> mu+ mu-) (X -> ^mu+ mu-) " : "Muon3BDT",
                                         " X -> (X -> mu+ mu-) (X -> mu+ ^mu-) " : "Muon4BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi22Mu2EPromptLine = StrippingLine(Jpsi22Mu2EPromptLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi22Mu2EPrompt,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) X " : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) X " : "Muon2BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi22Mu2EDetachedLine = StrippingLine(Jpsi22Mu2EDetLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi22Mu2EDetachedIncl,
                                       FILTER            = SPDFilter,
                                       RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         " X -> (X -> ^mu+ mu-) X " : "Muon1BDT",
                                         " X -> (X -> mu+ ^mu-) X " : "Muon2BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi22E2EDetachedLine = StrippingLine(Jpsi22E2EDetLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi22E2EDetachedIncl,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       MDSTFlag          = False )

        self.Jpsi22Mu2ESSDetachedLine = StrippingLine(Jpsi22Mu2ESSDetLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi22Mu2ESSDetachedIncl,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X -> (X -> ^mu+ X) (X -> mu+ X) ]CC" : "Muon1BDT",
                                         "[ X -> (X -> mu+ X) (X -> ^mu+ X) ]CC" : "Muon2BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi23MuEDetachedLine = StrippingLine(Jpsi23MuEDetLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi23MuEDetachedIncl,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X ->  ^mu+ mu+ mu- X ]CC" : "Muon1BDT",
                                         "[ X ->  mu+ ^mu+ mu- X ]CC" : "Muon2BDT",
                                         "[ X ->  mu+ mu+ ^mu- X ]CC" : "Muon3BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi23MuEPromptLine = StrippingLine(Jpsi23MuEPromptLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi23MuEPromptIncl,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X ->  ^mu+ mu+ mu- X ]CC" : "Muon1BDT",
                                         "[ X ->  mu+ ^mu+ mu- X ]CC" : "Muon2BDT",
                                         "[ X ->  mu+ mu+ ^mu- X ]CC" : "Muon3BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi2Mu3EDetachedLine = StrippingLine(Jpsi2Mu3EDetLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi2Mu3EDetachedIncl,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X ->  (X -> ^mu+ X) X ]CC" : "Muon1BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )

        self.Jpsi2Mu3EPromptLine = StrippingLine(Jpsi2Mu3EPromptLine_name + "Line",
                                       prescale          = config['Bu2mmLinePrescale'],
                                       postscale         = 1,
                                       selection         = SelJpsi2Mu3EPromptIncl,
                                       FILTER            = SPDFilter,
                                       #RequiredRawEvents=["Trigger", "Muon", "Calo", "Rich", "Velo", "Tracker"],
                                       RelatedInfoTools = [
                                         { "Type" : "RelInfoMuonIDPlus",
                                         "Variables" : ["MU_BDT"],
                                         "DaughterLocations"  : {
                                         "[ X ->  ^mu+ X X ]CC" : "Muon1BDT",
                                         }
                                         },
                                       ],
                                       MDSTFlag          = False )
        # 6 : Register Lines


        self.registerLine( self.B24mXLine )
        self.registerLine( self.B24mXTightLine )
        self.registerLine( self.B22m2eXTightLine )
        self.registerLine( self.B24mXUpLine )

        self.registerLine( self.B24mInclLine )
        self.registerLine( self.B24mInclLowLine )
        self.registerLine( self.B24mInclLowUpLine )
        self.registerLine( self.B22m2mInclLine )
        self.registerLine( self.B25mInclLine )
        self.registerLine( self.B26mInclLine )
        self.registerLine( self.B26m3xInclLine )
        self.registerLine( self.B26mInclPromptLine )
        self.registerLine( self.B28mInclPromptLine )
        self.registerLine( self.B28m4xInclPromptLine )

        self.registerLine( self.Jpsi24MuPromptLine )
        self.registerLine( self.Jpsi24MuPromptTightLine )
        self.registerLine( self.Jpsi22Mu2EPromptLine )
        self.registerLine( self.Jpsi22Mu2EDetachedLine )
        self.registerLine( self.Jpsi22E2EDetachedLine )
        self.registerLine( self.Jpsi22Mu2ESSDetachedLine )
        self.registerLine( self.Jpsi23MuEDetachedLine )
        self.registerLine( self.Jpsi23MuEPromptLine )
        self.registerLine( self.Jpsi2Mu3EDetachedLine )
        self.registerLine( self.Jpsi2Mu3EPromptLine )

#added for the 2018 restripping

        self.registerLine( self.B24mXLine_LongLived )
        self.registerLine( self.B26mXLine_LongLived )
        self.registerLine( self.B26m3xInclLongLivedLine )
        self.registerLine( self.B28m4xInclLongLivedLine )

#####################################################
    def _filterKaon( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > %(KaonPTTight)s *MeV) & (P > 2000 *MeV) & " \
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))) & " \
                "(PROBNNk > 0.15)" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )
#####################################################
    def _filterHadron( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > %(KaonPT)s *MeV) & " \
                "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | " \
                "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )
#####################################################
    def _filterPhi( self, name, sel, params ):
        """
        Filter for Std Phi
        """

        _Code = "(PT > %(KaonPT)s *MeV) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)  & (0.5 < NINTREE((ABSID==321) & (PROBNNk>0.1)))"% params # \
                #"((2 == NINTREE((ABSID==321) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )
#####################################################
    def _filterKst( self, name, sel, params ):
        """
        Filter for Std Kstar
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > 500 *MeV) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (1 == NINTREE((ABSID==321) & (PROBNNk>0.2)))"% params # \
                #"((2 == NINTREE((ABSID==321) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )
#####################################################
    def _filterLst( self, name, sel, params ):
        """
        Filter for Std Lambdastar
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = "(PT > 500 *MeV) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s) & (1 == NINTREE((ABSID==321) & (PROBNNk>0.2)))  & (1 == NINTREE((ABSID==2212) & (PROBNNp>0.2) & (P>7000)))"% params # \
                #"((2 == NINTREE((ABSID==321) & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )

#####################################################
    def _filterLongLivedHadron( self, name, sel, params ):
        """
        Filter for all hadronic final states
        """

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        _Code = "(PT > %(KaonPT)s *MeV) & (BPVLTIME() > %(V0TAU)s * ns)" % params

        _Filter = FilterDesktop( Code = _Code )

        return Selection( name, Algorithm = _Filter, RequiredSelections = [ sel ] )

#####################################################
#leptons
#####################################################
    def _makeDiMuonDetached( self, name, params ) :
        """
        Make a dimuon
        rho(770)0 is just a proxy to get the two-body combination
        """

        _Decays = "rho(770)0 -> mu+ mu-"

         # define all the cuts
        _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)" % params
        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons ] )

#####################################################
    def _makeMuEDetached( self, name, params ) :
        """
        Make a mu+e-
        rho(1700) is just a proxy to get the two-body combination.
        It is made charged so that particle != antiparticle.
        """

        _Decays = "[rho(1700)+ -> e+ mu-]cc"

         # define all the cuts
        _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)" % params
        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params
        _daughtersCutse = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (PIDe>-1)" % params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu,
            "e-"  : _daughtersCutse }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
        _stdAllLooseElectrons = DataOnDemand(Location = "Phys/StdAllLooseElectrons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons, _stdAllLooseElectrons ] )

#####################################################
    def _makeDiElectron( self, name, params ) :
        """
        Make a dielectron
        omega(782) is just a proxy to get the two-body combination
        """

        from Configurables import DiElectronMaker, ProtoParticleCALOFilter

        dieLL = DiElectronMaker('MyDiElectronFromTracks')
        dieLL.Particle = "omega(782)"
        selector = trackSelector ( dieLL , trackTypes = ["Long"] )
        dieLL.addTool( ProtoParticleCALOFilter, name='Electron' )
        dieLL.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'-2.0'"]
        dieLL.DiElectronMassMax = 500000.*MeV
        dieLL.DiElectronMassMin = 0.*MeV
        dieLL.DiElectronPtMin = 200.*MeV


        return Selection( name, Algorithm = dieLL )

#####################################################
    def _makeDiMuonPrompt( self, name, params ) :
        """
        Make a dimuon
        rho(770)0 is just a proxy to get the two-body combination
        """

        _Decays = "rho(770)0 -> mu+ mu-"

         # define all the cuts
        _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) " % params
        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (ISMUON) & (PROBNNmu>0.2) " % params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        _stdLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdLooseMuons ] )
# added for the 2018 restripping: dileptons from the KS intermediate particle, to allow for lifetime in the vertex fit
#####################################################
    # def _makeDiElectronLongLived( self, name, params ) :
    #     """
    #     Make a dielectron
    #     KS0 is just a proxy to get the two-body combination
    #     """
    #
    #     from Configurables import DiElectronMaker, ProtoParticleCALOFilter
    #
    #     dieLL = DiElectronMaker('MyDiElectronFromTracks')
    #     dieLL.Particle = "KS0"
    #     selector = trackSelector ( dieLL , trackTypes = ["Long"] )
    #     dieLL.addTool( ProtoParticleCALOFilter, name='Electron' )
    #     dieLL.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'-1.0'"]
    #     dieLL.DiElectronMassMax = 500000.*MeV
    #     dieLL.DiElectronMassMin = 0.*MeV
    #     dieLL.DiElectronPtMin = 200.*MeV
    #
    #
    #     return Selection( name, Algorithm = dieLL )

#####################################################
    def _makeDiMuonLongLived( self, name, params ) :
        """
        Make a dimuon
        KS0 is just a proxy to get the two-body combination
        """

        _Decays = "KS0 -> mu+ mu-"

         # define all the cuts
        _CombCuts    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2>16)"

        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s)" % params# & (ISMUON) & (PROBNNmu>0.1) " % params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        _stdLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdLooseMuons ] )

#####################################################
    def _makeFourMuonDetached( self, name, params ) :
        """
        Make a 4-muon
        Jpsi is just a proxy to get the 4-body combination
        """
        _Decays = "J/psi(1S) -> mu+ mu+ mu- mu-"
        #_Decays = "J/psi(1S) -> rho(770)0 rho(770)0"

         # define all the cuts
        _CombCuts12    = "(ACUTDOCACHI2(9,'')) & (AM<6700*MeV) & ( ANUM (( 'mu-' == ABSID ) & ISMUON )> 0.5)" % params
        _CombCuts123    = "(ACUTDOCACHI2(9,'')) & (APT > 500*MeV) & (AM<6800*MeV)" % params

        _CombCuts    = "(ACUTDOCACHI2(10,'')) & (AM<6900*MeV) & ( ANUM (( 'mu-' == ABSID ) & ISMUON )> 2.5) & (1.5<ANUM( ('mu+'==ABSID) & (MIPCHI2DV(PRIMARY)>6)))" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)  & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.2) )> 1.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 1.5  )" % params
        _daughtersCuts = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons ] )

#####################################################
    def _makeFourMuonDetachedUp( self, name, params ) :
        """
        Make a 4-muon with 1 upstream and 3 long tracks.
        Jpsi is just a proxy to get the 4-body combination
        """
        _Decays = "J/psi(1S) -> mu+ mu+ mu- mu-"
        #_Decays = "J/psi(1S) -> rho(770)0 rho(770)0"

         # define all the cuts
        _CombCuts12    = "(ACUTDOCACHI2(9,'')) & (AM<6700*MeV) & ( ANUM (( 'mu-' == ABSID ) & ISMUON )> 0.5) & ( ANUM (( 'mu-' == ABSID ) & ISLONG )> 0.5)" % params
        _CombCuts123    = "(ACUTDOCACHI2(9,'')) & (APT > 500*MeV) & (AM<6800*MeV) & ( ANUM (( 'mu-' == ABSID ) & ISLONG )> 1.5)" % params

        _CombCuts    = "(ACUTDOCACHI2(10,'')) & (AM<6900*MeV) & ( ANUM (( 'mu-' == ABSID ) & ISMUON )> 2.5) & (1.5<ANUM( ('mu+'==ABSID) & (MIPCHI2DV(PRIMARY)>6)))  & ( ANUM (( 'mu-' == ABSID ) & ISLONG )==3)  & ( ANUM (( 'mu-' == ABSID ) & ISUP )==1)" % params

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)  & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.2) )> 1.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 1.5  )" % params
        _daughtersCuts = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & ((ISLONG & ISMUON) | (ISUP & (PPINFO(LHCb.ProtoParticle.RichDLLmu,-1000)>-2)))" % params

        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
        _stdNoPIDUpMuons = DataOnDemand(Location = "Phys/StdNoPIDsUpMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons, _stdNoPIDUpMuons ] )


#####################################################
    def _makeFourMuonPromptJpsi( self, name, dilepton, params ) :
        """
        Make a 4-muon in charmonium window
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> rho(770)0 rho(770)0"

         # define all the cuts
        _CombCuts    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE('mu+'==ABSID,PT) > 400.0 *MeV)  & (MAXTREE('mu+'==ABSID,PROBNNmu) > 0.4)  & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        _daughtersCuts = "(PT > 400*MeV) & ( NINTREE (( 'mu-' == ABSID ) & (PROBNNmu>0.1))> 1.5)" #% params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCuts }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton ] )

#####################################################
    def _makeFourMuonPromptJpsiTight( self, name, dilepton, params ) :
        """
        Make a 4-muon in charmonium window
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> rho(770)0 rho(770)0"

         # define all the cuts
        _CombCuts    = "(ACUTDOCACHI2(10,''))" #% params #(AM > 2000*MeV) & (AM < 4600*MeV) &

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 9) & (MAXTREE('mu+'==ABSID,PT) > 500.0 *MeV)  & (MAXTREE('mu+'==ABSID,PROBNNmu) > 0.5)  & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        _daughtersCuts = "(PT > 550*MeV) & ( NINTREE (( 'mu-' == ABSID ) & (PROBNNmu>0.3))> 1.5)" #% params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCuts }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton ] )

#####################################################
    def _make2Mu2EPrompt( self, name, dimuon, dielectron, params ) :
        """
        Make a mu+mu-e+e- in charmonium window
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> rho(770)0 omega(782)"

         # define all the cuts
        _CombCuts    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        _daughtersCutsMuMu = "(PT > 400*MeV) & (MAXTREE('mu+'==ABSID,PROBNNmu) > 0.3)" #% params
        _daughtersCutsEE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.1)  & (MINTREE('e+'==ABSID,PIDe) > 1)"

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCutsMuMu,
            "omega(782)"  : _daughtersCutsEE, }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dimuon, dielectron ] )


#####################################################
    # def _make2Mu2EDetached( self, name, dimuon, dielectron, params ) :
    #     """
    #     Make a mu+mu-e+e- in charmonium window
    #     Jpsi is a proxy to get the 4-body combination
    #     """
    #
    #     _Decays = "J/psi(1S) -> rho(770)0 omega(782)"
    #
    #      # define all the cuts
    #     _CombCuts    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &
    #
    #     _MotherCuts  = "(VFASPF(VCHI2PDOF) < %(K1_VtxChi2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & ( NINTREE ( ( 14>ABSID ) & (PT>400) )> 1.5  )" % params
    #     #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
    #     _daughtersCutsMuMu = "(PT > 400*MeV) & (MAXTREE('mu+'==ABSID,PROBNNmu) > 0.1)" #% params
    #     _daughtersCutsEE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.1)  & (MINTREE('e+'==ABSID,PIDe) > 0)"
    #
    #     _Combine = CombineParticles()
    #
    #     _Combine.DecayDescriptor = _Decays
    #
    #     _Combine.DaughtersCuts = {
    #         "rho(770)0"  : _daughtersCutsMuMu,
    #         "omega(782)"  : _daughtersCutsEE, }
    #
    #     _Combine.CombinationCut   = _CombCuts
    #     _Combine.MotherCut        = _MotherCuts
    #
    #
    #     return Selection( name, Algorithm = _Combine, RequiredSelections = [ dimuon, dielectron ] )
#####################################################


#####################################################
    def _makeB24muX( self, name, fourlepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B+ -> J/psi(1S) K+ ]cc",
                    "B_s0 -> J/psi(1S) phi(1020)",
                    " B0 -> J/psi(1S) KS0 ",
                    "[ B0 -> J/psi(1S) K*(892)0 ]cc",
                    "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc ",
                    "[Lambda_b0 -> J/psi(1S) Lambda0]cc ",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Combine.DaughtersCuts = {
            "J/psi(1S)"  : "(M < 6100)  & (NINTREE ( ( 'mu-' == ABSID ) & (ISLONG) & ((PROBNNmu>0.15)) )>2.5)" }

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ fourlepton, _Merge ] )

#####################################################
    def _makeB24muXTight( self, name, fourlepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B+ -> J/psi(1S) K+ ]cc",
                    "B_s0 -> J/psi(1S) phi(1020)",
                    " B0 -> J/psi(1S) KS0 ",
                    "[ B0 -> J/psi(1S) K*(892)0 ]cc",
                    "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc ",
                    "[Lambda_b0 -> J/psi(1S) Lambda0]cc ",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > 55))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Combine.DaughtersCuts = {
            "J/psi(1S)"  : "(M < 6100) & (NINTREE ( ( 'mu-' == ABSID ) & (ISMUON & (PROBNNmu>0.15)) )==4)" }

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ fourlepton, _Merge ] )

#####################################################
    def _makeB24muX_LongLived( self, name, dilepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B+ -> KS0 KS0 K+ ]cc",
                    "B_s0 -> KS0 KS0 phi(1020)",
                    #" B0 -> J/psi(1S) KS0 ",
                    "[ B0 -> KS0 KS0 K*(892)0 ]cc",
                    "[Lambda_b0 -> KS0 KS0 Lambda(1520)0]cc ",
                    "[Lambda_b0 -> KS0 KS0 Lambda0]cc ",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 2.5)"\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Combine.DaughtersCuts = {
            "KS0"  : "(M < 6100)  & (NINTREE ( ( 'mu-' == ABSID ) & (ISLONG) & ((PROBNNmu>0.1)) )>0.5)" }

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )

#####################################################
    def _makeB26muX_LongLived( self, name, dilepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B+ -> KS0 KS0 KS0 K+ ]cc",
                    "B_s0 -> KS0 KS0 KS0 phi(1020)",
                    #" B0 -> J/psi(1S) KS0 ",
                    "[ B0 -> KS0 KS0 KS0 K*(892)0 ]cc",
                    "[Lambda_b0 -> KS0 KS0 KS0 Lambda(1520)0]cc ",
                    "[Lambda_b0 -> KS0 KS0 KS0 Lambda0]cc ",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 4.5)"\
               "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Combine.DaughtersCuts = {
            "KS0"  : "(M < 6100)  & (NINTREE ( ( 'mu-' == ABSID ) & (ISLONG) & ((PROBNNmu>0.1)) )>0.5)" }

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ dilepton, _Merge ] )

#####################################################
    def _makeB22mu2EXTight( self, name, fourlepton, hadrons, params, masscut ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
                    "[ B+ -> J/psi(1S) K+ ]cc",
                    "B_s0 -> J/psi(1S) phi(1020)",
                    " B0 -> J/psi(1S) KS0 ",
                    "[ B0 -> J/psi(1S) K*(892)0 ]cc",
                    "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc ",
                    "[Lambda_b0 -> J/psi(1S) Lambda0]cc ",
                  ]

        _Cut = "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "\
               "& (BPVIPCHI2() < %(BIPCHI2)s) "\
               "& (BPVDIRA > %(BDIRA)s) "\
               "& (BPVVDCHI2 > 50))" % params

        _Combine = CombineParticles( DecayDescriptors = _Decays,
                                     CombinationCut   = masscut,
                                     MotherCut        = _Cut )

        _Combine.DaughtersCuts = {
            "J/psi(1S)"  : "(M < 6300)" } #" & (NINTREE ( ( 'mu-' == ABSID ) & ISMUON )==2) & (NINTREE ( ( 'e-' == ABSID ) & PIDe>0 )==2)" }

        _Merge = MergedSelection( "Merge" + name, RequiredSelections = hadrons )

        return Selection(name, Algorithm = _Combine, RequiredSelections = [ fourlepton, _Merge ] )

# #####################################################

#####################################################
    def _make2mu2muInclDet( self, name, dilepton, params ) :
        """
        Make a 4-muon consisting of 2 dimuons
        This makes some freedom for dimuon pairs to fly, but can lead to duplicates.
        Jpsi is just a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> rho(770)0 rho(770)0"

         # define all the cuts
        _CombCuts    = "(AM < 7700*MeV) " #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &

        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & (MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.2) )> 2.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 1.5  ) & (BPVDIRA > 0.99) " % params
        _MotherCuts  = "(BPVDLS>3) & (VFASPF(VCHI2PDOF) < 12) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.2) )> 2.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>300) )> 1.5) & (MAXTREE('mu+'==ABSID,PT) > 550.0 *MeV)  & (BPVDIRA > 0.999) & (M<7500*MeV)" % params
        _daughtersCuts = "(NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) & (PROBNNmu>0.15) )>1.5) " #% params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCuts }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton ] )

#####################################################
    def _make4muInclDet( self, name, params ) :
        """
        Make a 4-muon
        Jpsi is just a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> mu+ mu+ mu- mu-"

         # define all the cuts
        _CombCuts12    = "(ACUTDOCACHI2(10,''))" #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &
        _CombCuts123    = "(ACUTDOCACHI2(16,'')) & (APT > 500*MeV)"
        _CombCuts    = "(ACUTDOCACHI2(16,'')) & ( AMAXCHILD('mu+'==ABSID,PT) > 500.0 *MeV) & ( ANUM ( ( 'mu-' == ABSID ) & (PT>300) )> 1.5)"

        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & (MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.2) )> 2.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 1.5  ) & (BPVDIRA > 0.99) " % params
        _MotherCuts  = "(((BPVDLS>3))) & (VFASPF(VCHI2PDOF) < 9) & (BPVDIRA > 0.995)" % params
        _daughtersCuts = "ISMUON & (PROBNNmu>0.2)" #% params

        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons ] )

#####################################################
    def _make4muInclDetLowMass( self, name, params ) :
        """
        Make a 4-muon below 2 GeV
        phi is just a proxy to get the 4-body combination
        """

        _Decays = "phi(1020) -> mu+ mu+ mu- mu-"

         # define all the cuts
        _CombCuts12    = "(AM<1800*MeV) & (ACUTDOCACHI2(9,'')) & (ANUM(('mu-' == ABSID) & (ISMUON))> 0.5)" #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &
        _CombCuts123    = "(APT > 300*MeV) & (AM<1900*MeV) & (ACUTDOCACHI2(9,''))  & (ANUM(('mu-' == ABSID) & (ISMUON))> 1.5)"
        _CombCuts    = "(AM<2000*MeV) & (ACUTDOCACHI2(9,'')) & ( AMAXCHILD('mu+'==ABSID,PT) > 500.0 *MeV) & ( AMAXCHILD('mu+'==ABSID,PROBNNmu) > 0.35) & ( AMAXCHILD('mu+'==ABSID,PT) > 500) & (ANUM(('mu-' == ABSID) & (ISMUON) & (PROBNNmu>0.1))> 2.5) "

        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & (MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.2) )> 2.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 1.5  ) & (BPVDIRA > 0.99) " % params
        _MotherCuts  = "(BPVDLS>3) & (VFASPF(VCHI2PDOF) < 9) & (BPVDIRA > 0.995)" % params
        _daughtersCuts = "(ISMUON) | (PIDmu>0)" #% params

        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons ] )

#####################################################
    def _make4muInclDetLowMassUp( self, name, params ) :
        """
        Make a 4-muon below 2 GeV, allowing for 1 upstream track.
        phi is just a proxy to get the 4-body combination
        """

        _Decays = "phi(1020) -> mu+ mu+ mu- mu-"

         # define all the cuts
        _CombCuts12    = "(AM<1800*MeV) & (ACUTDOCACHI2(9,'')) & (ANUM(('mu-' == ABSID) & (ISMUON))> 0.5) & ( ANUM (( 'mu-' == ABSID ) & ISLONG )> 0.5)" #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &
        _CombCuts123    = "(APT > 300*MeV) & (AM<1900*MeV) & (ACUTDOCACHI2(9,''))  & (ANUM(('mu-' == ABSID) & (ISMUON))> 1.5)"
        _CombCuts    = "(AM<2000*MeV) & (ACUTDOCACHI2(9,'')) & ( AMAXCHILD('mu+'==ABSID,PT) > 500.0 *MeV) & ( AMAXCHILD('mu+'==ABSID,PROBNNmu) > 0.35) & ( AMAXCHILD('mu+'==ABSID,PT) > 500) & (ANUM(('mu-' == ABSID) & (ISMUON) & (PROBNNmu>0.2))> 1.5) & ( ANUM (( 'mu-' == ABSID ) & ISLONG )==3)  & ( ANUM (( 'mu-' == ABSID ) & ISUP )==1)"

        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & (MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.2) )> 2.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 1.5  ) & (BPVDIRA > 0.99) " % params
        _MotherCuts  = "(BPVDLS>3) & (VFASPF(VCHI2PDOF) < 9) & (BPVDIRA > 0.999)" % params
        #_daughtersCuts = "(ISMUON) | (PIDmu>0)" #% params
        _daughtersCuts = "((ISLONG & ((ISMUON) | (PIDmu>2))) | (ISUP & (PPINFO(LHCb.ProtoParticle.RichDLLmu,-1000)>2)))" % params


        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "mu+"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
        _stdNoPIDUpMuons = DataOnDemand(Location = "Phys/StdNoPIDsUpMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons, _stdNoPIDUpMuons ] )


#####################################################
    def _make6muInclDet( self, name, params ) :
        """
        First, make a mu-mu-mu-
        rho(770)+ is just a proxy to get the two-body combination
        """

        _Decays3 = "[rho(770)+ -> mu+ mu+ mu+]cc"

         # define all the cuts
        _CombCuts3    = "(ACUTDOCACHI2(10,'')) & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.075) )>1.5) & ( ANUM ( ( 'mu-' == ABSID ) & (PT>300*MeV) )> 1.5) & (APT > 700*MeV)" % params

        _MotherCuts3  = "(VFASPF(VCHI2PDOF) < 9) & (MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)" % params
        _daughtersCutsmu3 = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _Combine3m = CombineParticles()

        _Combine3m.DecayDescriptor = _Decays3

        _Combine3m.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu3 }

        _Combine3m.CombinationCut   = _CombCuts3
        _Combine3m.MotherCut        = _MotherCuts3

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        trimuon = Selection( "3MuFor6MuDet", Algorithm = _Combine3m, RequiredSelections = [ _stdNoPIDLooseMuons ] )

        """
        Make a 6-muon
        Jpsi is just a proxy to get the 6-body combination
        """
#        _Decays = "J/psi(1S) -> mu+ mu+ mu+ mu- mu- mu-"
        _Decays = "J/psi(1S) -> rho(770)+ rho(770)- "

         # define all the cuts
        # _CombCuts12  = "(ADOCA(1,2)<0.7*mm) & (APT>300)"
        # _CombCuts123  = "ACUTDOCACHI2(20,'') & (ANUM (( 'mu-' == ABSID ) & ISMUON )>0.5)  & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.05) )>0.5)"
        # _CombCuts1234  = "ACUTDOCACHI2(20,'')"
        _CombCuts    = "ACUTDOCACHI2(16,'')" #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &
# & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)
        _MotherCuts  = "(BPVDLS>3) & (VFASPF(VCHI2PDOF) < 12) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 3.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 3.5  ) & (BPVDIRA > 0.995) & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 4.5)" % params
        #_MotherCuts  = "(((BPVDLS>3))) & (VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 3.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>250) )> 3.5) & (MAXTREE('mu+'==ABSID,PT) > 350.0 *MeV)  & (BPVDIRA > 0.999)" % params
        _daughtersCuts = "(PT>700*MeV)" #(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params #"(NINTREE ( ( 'mu-' == ABSID ) & ISMUON )>0.5) & (PT>300)" #% params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)+"  : _daughtersCuts }

        # _Combine.Combination12Cut   = _CombCuts12
        # _Combine.Combination123Cut   = _CombCuts123
        # _Combine.Combination1234Cut   = _CombCuts1234
        # _Combine.Combination12345Cut   = _CombCuts1234
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        #_stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ trimuon ] )

#####################################################
    def _make6muInclPrompt( self, name, params ) :
        """
        First, make a mu-mu-mu-
        rho(770)+ is just a proxy to get the 3-body combination
        """

        _Decays3 = "[rho(770)+ -> mu+ mu+ mu+]cc"

         # define all the cuts
        _CombCuts3    = "(ACUTDOCACHI2(10,'')) & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.1) )>2.5) & ( ANUM ( ( 'mu-' == ABSID ) & (PT>250*MeV) )> 1.5) & (APT > 600*MeV)" % params

        _MotherCuts3  = "(VFASPF(VCHI2PDOF) < 10)" % params
        _daughtersCutsmu3 = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _Combine3m = CombineParticles()

        _Combine3m.DecayDescriptor = _Decays3

        _Combine3m.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu3 }

        _Combine3m.CombinationCut   = _CombCuts3
        _Combine3m.MotherCut        = _MotherCuts3

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")

        trimuon = Selection( "3MuFor6MuPrompt", Algorithm = _Combine3m, RequiredSelections = [ _stdNoPIDLooseMuons ] )

        """
        Make a 6-muon
        Jpsi is just a proxy to get the 6-body combination
        """
#        _Decays = "J/psi(1S) -> mu+ mu+ mu+ mu- mu- mu-"
        _Decays = "J/psi(1S) -> rho(770)+ rho(770)- "

         # define all the cuts
        # _CombCuts12  = "(ADOCA(1,2)<0.7*mm) & (APT>300)"
        # _CombCuts123  = "ACUTDOCACHI2(20,'') & (ANUM (( 'mu-' == ABSID ) & ISMUON )>0.5)  & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.05) )>0.5)"
        # _CombCuts1234  = "ACUTDOCACHI2(20,'')"
        _CombCuts    = "ACUTDOCACHI2(16,'')" #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &
# & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)
        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 5.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 3.5  ) & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 5.5)" % params
        #_MotherCuts  = "(((BPVDLS>3))) & (VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 3.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>250) )> 3.5) & (MAXTREE('mu+'==ABSID,PT) > 350.0 *MeV)  & (BPVDIRA > 0.999)" % params
        _daughtersCuts = "(PT>800*MeV)" #(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params #"(NINTREE ( ( 'mu-' == ABSID ) & ISMUON )>0.5) & (PT>300)" #% params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)+"  : _daughtersCuts }

        # _Combine.Combination12Cut   = _CombCuts12
        # _Combine.Combination123Cut   = _CombCuts123
        # _Combine.Combination1234Cut   = _CombCuts1234
        # _Combine.Combination12345Cut   = _CombCuts1234
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        #_stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ trimuon ] )

#####################################################
    def _make6mu3bodyInclDet( self, name, dilepton, params ) :
        """
        Make a 6-muon
        Jpsi is just a proxy to get the 6-body combination
        """
#        _Decays = "J/psi(1S) -> mu+ mu+ mu+ mu- mu- mu-"
        _Decays = "J/psi(1S) -> rho(770)0 rho(770)0 rho(770)0"

         # define all the cuts
        _CombCuts12  = "(ADOCA(1,2)<0.7*mm) & (APT>500)  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> 2.5)"
        # _CombCuts123  = "ACUTDOCACHI2(20,'') & (ANUM (( 'mu-' == ABSID ) & ISMUON )>0.5)  & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.05) )>0.5)"
        # _CombCuts1234  = "ACUTDOCACHI2(20,'')"
        _CombCuts    = "ACUTDOCACHI2(16,'')  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> 4.5)" #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &
# & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)
        _MotherCuts  = "(BPVDLS>3) & (VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 3.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 3.5  ) & (BPVDIRA > 0.995)" % params
        #_MotherCuts  = "(((BPVDLS>3))) & (VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 3.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>250) )> 3.5) & (MAXTREE('mu+'==ABSID,PT) > 350.0 *MeV)  & (BPVDIRA > 0.999)" % params
        _daughtersCuts = "(PT>400*MeV)  & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 0.5)" #(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params #"(NINTREE ( ( 'mu-' == ABSID ) & ISMUON )>0.5) & (PT>300)" #% params

        _Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        # _Combine.Combination123Cut   = _CombCuts123
        # _Combine.Combination1234Cut   = _CombCuts1234
        # _Combine.Combination12345Cut   = _CombCuts1234
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        #_stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton ] )

#####################################################
    def _make6mu3bodyInclLongLived( self, name, dilepton, params ) :
        """
        Make a 6-muon
        Jpsi is just a proxy to get the 6-body combination
        """
        _Decays = "J/psi(1S) -> KS0 KS0 KS0"

         # define all the cuts
        _CombCuts12  = "(ADOCA(1,2)<0.7*mm) & (APT>500)"#"  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> -0.5)" #2.5
        _CombCuts    = "ACUTDOCACHI2(16,'')"#"  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> 0.5)" #4.5
        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 4.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 3.5  )" % params

        _daughtersCuts = "(PT>400*MeV)  & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 0.5)"

        _Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "KS0"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        # _Combine.Combination123Cut   = _CombCuts123
        # _Combine.Combination1234Cut   = _CombCuts1234
        # _Combine.Combination12345Cut   = _CombCuts1234
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        #_stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton ] )

#####################################################
    def _make8muIncl( self, name, params ) :
        """
        First, make a mu-mu-mu-mu-
        rho(770)+ is just a proxy to get the combination
        """

        _Decays4 = "[rho(770)+ -> mu+ mu+ mu+ mu+]cc"
        _CombCuts4_12  = "(ACUTDOCACHI2(10,'')) & (APT>400)  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) & (PROBNNmu>0.1) )> 0.5)"
        _CombCuts4_123  = "(ACUTDOCACHI2(10,'')) & (APT>500)  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) & (PROBNNmu>0.1)  )> 1.5)"
         # define all the cuts
        _CombCuts4    = "(ACUTDOCACHI2(10,'')) & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.1) )>2.5) & ( ANUM ( ( 'mu-' == ABSID ) & (PT>250*MeV) )> 1.5) & (APT > 600*MeV)" % params

        _MotherCuts4  = "(VFASPF(VCHI2PDOF) < 10)" % params
        _daughtersCutsmu4 = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) & (ISMUON | (PIDmu>-5.))" % params

        _Combine4m = DaVinci__N4BodyDecays()

        _Combine4m.DecayDescriptor = _Decays4

        _Combine4m.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu4 }

        _Combine4m.Combination12Cut   = _CombCuts4_12
        _Combine4m.Combination123Cut   = _CombCuts4_123
        _Combine4m.CombinationCut   = _CombCuts4
        _Combine4m.MotherCut        = _MotherCuts4

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        quadmuon = Selection( "4MuFor8MuPrompt", Algorithm = _Combine4m, RequiredSelections = [ _stdNoPIDLooseMuons ] )

        """
        Make a 8-muon
        Jpsi is just a proxy to get the 6-body combination
        """
#        _Decays = "J/psi(1S) -> mu+ mu+ mu+ mu- mu- mu-"
        _Decays = "J/psi(1S) -> rho(770)+ rho(770)- "

         # define all the cuts
        # _CombCuts12  = "(ADOCA(1,2)<0.7*mm) & (APT>300)"
        # _CombCuts123  = "ACUTDOCACHI2(20,'') & (ANUM (( 'mu-' == ABSID ) & ISMUON )>0.5)  & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.05) )>0.5)"
        # _CombCuts1234  = "ACUTDOCACHI2(20,'')"
        _CombCuts    = "ACUTDOCACHI2(16,'')" #(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params # (AM <6000*MeV) &
# & (BPVVDCHI2 > %(DiLeptonFDCHI2)s)
        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 6.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 4.5  ) & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 6.5)" % params
        #_MotherCuts  = "(((BPVDLS>3))) & (VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.1) )> 3.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>250) )> 3.5) & (MAXTREE('mu+'==ABSID,PT) > 350.0 *MeV)  & (BPVDIRA > 0.999)" % params
        _daughtersCuts = "(PT>800*MeV)" #(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params #"(NINTREE ( ( 'mu-' == ABSID ) & ISMUON )>0.5) & (PT>300)" #% params

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)+"  : _daughtersCuts }

        # _Combine.Combination12Cut   = _CombCuts12
        # _Combine.Combination123Cut   = _CombCuts123
        # _Combine.Combination1234Cut   = _CombCuts1234
        # _Combine.Combination12345Cut   = _CombCuts1234
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts
        #_stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ quadmuon ] )

#####################################################
    def  _make8mu4bodyIncl( self, name, params ) :
        """
        Make a 8-muon
        Jpsi is just a proxy to get the 8-body combination
        Start with a dimuon
        """
        _DecaysD = "rho(770)0 -> mu+ mu-"

        _CombCutsD    = "(AM > 0*MeV) & (ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params
        _MotherCutsD  = "(VFASPF(VCHI2PDOF) < 9) " % params
        _daughtersCutsmu = "(TRCHI2DOF < %(Trk_Chi2)s) & (TRGHOSTPROB < %(Trk_GhostProb)s) " % params

        _CombineD = CombineParticles()

        _CombineD.DecayDescriptor = _DecaysD

        _CombineD.DaughtersCuts = {
            "mu+"  : _daughtersCutsmu }

        _CombineD.CombinationCut   = _CombCutsD
        _CombineD.MotherCut        = _MotherCutsD

        _stdLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")

        seldimu = Selection( "DiMuonsFor8Muons", Algorithm = _CombineD, RequiredSelections = [ _stdLooseMuons ] )

        #now, build the Jpsi
        _Decays = "J/psi(1S) -> rho(770)0 rho(770)0 rho(770)0 rho(770)0"

         # define all the cuts
        _CombCuts12  = "(ADOCA(1,2)<0.7*mm) & (APT>500)  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> 2.5)"
        _CombCuts123  = "ACUTDOCACHI2(16,'') & (ANUM (( 'mu-' == ABSID ) & ISMUON )>4.5)" #  & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.05) )>0.5)"
        # _CombCuts1234  = "ACUTDOCACHI2(20,'')"
        _CombCuts    = "ACUTDOCACHI2(16,'')  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> 6.5)"
        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.05) )> 5.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 4.5  ) & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 6.5)" % params

        _daughtersCuts = "(PT>400*MeV)  & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 0.5)"

        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        # _Combine.Combination1234Cut   = _CombCuts1234
        # _Combine.Combination12345Cut   = _CombCuts1234
        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ seldimu ] )

#####################################################
    def  _make8mu4bodyLongLived( self, name, dilepton, params ) :
        """
        Make a 8-muon
        Jpsi is just a proxy to get the 8-body combination
        Start with a dimuon
        """

        #now, build the Jpsi
        _Decays = "J/psi(1S) -> KS0 KS0 KS0 KS0"

         # define all the cuts
        _CombCuts12  = "(ADOCA(1,2)<0.7*mm) & (APT>500)"#"  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> 0.5)" #2.5
        _CombCuts123  = "ACUTDOCACHI2(16,'')"#" & (ANUM (( 'mu-' == ABSID ) & ISMUON )>0.5)" #  & (ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.05) )>0.5)" #4.5
        # _CombCuts1234  = "ACUTDOCACHI2(20,'')"
        _CombCuts    = "ACUTDOCACHI2(16,'')"#"  & ( ANUM ( ( 'mu-' == ABSID ) & (ISMUON) )> 0.5)" #6.5
        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & ( NINTREE ( ( 'mu-' == ABSID ) & (PROBNNmu>0.05) )> 4.5) & ( NINTREE ( ( 'mu-' == ABSID ) & (PT>400) )> 4.5  ) & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 4.5)" % params #5.5 , 6.5

        _daughtersCuts = "(PT>400*MeV)  & ( NINTREE ( ( 'mu-' == ABSID ) & (ISMUON) )> 0.5)"

        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "KS0"  : _daughtersCuts }

        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton ] )

#####################################################
    def _make2Mu2EInclDetLoose( self, name, dimuon, dielectron, params ) :
        """
        Make a mu+mu-e+e-
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> rho(770)0 omega(782)"

         # define all the cuts
        _CombCuts    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 25) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & ( NINTREE ( ( 14>ABSID ) & (PT>400*MeV) )> 1.5  )" % params
        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        _daughtersCutsMuMu = "(PT > 400*MeV) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.2) & (MAXTREE('mu+'==ABSID,MIPCHI2DV(PRIMARY)) > 9) " #% params
        _daughtersCutsEE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.2)  & (MINTREE('e+'==ABSID,PIDe) > 2.)"

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCutsMuMu,
            "omega(782)"  : _daughtersCutsEE, }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dimuon, dielectron ] )
#####################################################
    def _make2Mu2EInclDet( self, name, dimuon, dielectron, params ) :
        """
        Make a mu+mu-e+e-
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> rho(770)0 omega(782)"

         # define all the cuts
        _CombCuts    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &
#& (BPVVDCHI2 > %(DiLeptonFDCHI2)s)
        _MotherCuts  = "(BPVDLS>3) & (BPVDIRA > 0.995) & (VFASPF(VCHI2PDOF) < 9)  & ( NINTREE ( ( 14>ABSID ) & (PT>400*MeV) )> 1.5  )" % params
        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        _daughtersCutsMuMu = "(PT > 400*MeV) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.15) & (MAXTREE('mu+'==ABSID,MIPCHI2DV(PRIMARY)) > 9) " #% params
        _daughtersCutsEE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.2)  & (MINTREE('e+'==ABSID,PIDe) > 2.)  & (MAXTREE(14>ABSID,MIPCHI2DV(PRIMARY)) > 6)"

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(770)0"  : _daughtersCutsMuMu,
            "omega(782)"  : _daughtersCutsEE, }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dimuon, dielectron ] )

#####################################################
    def _make2E2EInclDet( self, name, dielectron, params ) :
        """
        Make a e+e-e+e-
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "J/psi(1S) -> omega(782) omega(782)"

         # define all the cuts
        _CombCuts    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &
#& (BPVVDCHI2 > %(DiLeptonFDCHI2)s)
        _MotherCuts  = "(BPVDLS>3) & (BPVDIRA > 0.995) & (VFASPF(VCHI2PDOF) < 9)  & ( NINTREE ( ( 14>ABSID ) & (PT>400*MeV) )> 1.5  )" % params

        _daughtersCutsEE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.2)  & (MINTREE('e+'==ABSID,PIDe) > 2.)  & (MAXTREE(14>ABSID,MIPCHI2DV(PRIMARY)) > 6)"

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "omega(782)"  : _daughtersCutsEE, }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [  dielectron ] )
#####################################################
    def _make2Mu2ESSInclDet( self, name, dilepton, params ) :
        """
        Make a mu+mu+e-e-
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "[B0 -> rho(1700)+ rho(1700)+]cc"

         # define all the cuts
        _CombCuts    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &

        _MotherCuts  = "(VFASPF(VCHI2PDOF) < 16) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & ( NINTREE ( ( 14>ABSID ) & (PT>300*MeV) )> 1.5  )" % params
        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        _daughtersCutsEMu = "(PT > 400*MeV) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.2) & (MINTREE('e+'==ABSID,PROBNNe) > 0.2)  & (MINTREE('e+'==ABSID,PIDe) > 2.)"

        _Combine = CombineParticles()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(1700)+"  : _daughtersCutsEMu }

        _Combine.CombinationCut   = _CombCuts
        _Combine.MotherCut        = _MotherCuts


        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dilepton ] )
#####################################################
    def _make3MuEInclDet( self, name, params ) :
        """
        Make a mu+mu-mu+e-
        Jpsi is a proxy to get the 4-body combination
        """

#        _Decays = "[J/psi(1S) -> rho(770)0 rho(1700)+]cc"
        _Decays = "[J/psi(1S) -> mu+ mu- mu+ e-]cc"

         # define all the cuts
        _CombCuts12    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm) & (APT > 400*MeV)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &
        _CombCuts123    = "(ACUTDOCACHI2(12,'')) & (APT > 500*MeV) " % params
        _CombCuts    = "(ACUTDOCACHI2(12,'')) & (APT > 600*MeV)" % params

        _MotherCuts  = "(BPVDLS>3) & (VFASPF(VCHI2PDOF) < 9)  & ( NINTREE ( ( 14>ABSID ) & (PT>500*MeV) )> 1.5  ) & (MINTREE('e+'==ABSID,PROBNNe) > 0.2)  & (MINTREE('e+'==ABSID,PIDe) > 2.5) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.2)" % params
        #& (BPVVDCHI2 > 2*%(DiLeptonFDCHI2)s)
        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        #_daughtersCutsMuMu = "(PT > 400*MeV) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.1) "#"& (MAXTREE('mu+'==ABSID,MIPCHI2DV(PRIMARY)) > 9) " #% params
        #_daughtersCutsMuE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.1)  & (MINTREE('e+'==ABSID,PIDe) > 1.) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.1)"

#        _Combine = CombineParticles()
        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "e+"   : "(PROBNNe > 0.1) & (PIDe > 0.)",
            "mu+"  : "ISMUON & (PROBNNmu > 0.1)", }

        _Combine.CombinationCut   = _CombCuts
        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        _Combine.MotherCut        = _MotherCuts

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
        _stdAllLooseElectrons = DataOnDemand(Location = "Phys/StdAllLooseElectrons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons, _stdAllLooseElectrons ] )

#####################################################
    def _make3MuEInclPrompt( self, name, params ) :
        """
        Make a mu+mu-mu+e-
        Jpsi is a proxy to get the 4-body combination
        """

#        _Decays = "[J/psi(1S) -> rho(770)0 rho(1700)+]cc"
        _Decays = "[J/psi(1S) -> mu+ mu- mu+ e-]cc"

         # define all the cuts
        _CombCuts12    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm) & (APT > 500*MeV)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &
        _CombCuts123    = "(ACUTDOCACHI2(10,'')) & (APT > 1000*MeV) " % params
        _CombCuts    = "(ACUTDOCACHI2(10,'')) & (APT > 1200*MeV)" % params

        _MotherCuts  = "(PT>1500*MeV) & (VFASPF(VCHI2PDOF) < 9)  & ( NINTREE ( ( 14>ABSID ) & (PT>500*MeV) )> 1.5  ) & (MINTREE('e+'==ABSID,PROBNNe) > 0.3)  & (MINTREE('e+'==ABSID,PIDe) > 3) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.4)" % params
        #& (BPVVDCHI2 > 2*%(DiLeptonFDCHI2)s)
        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        #_daughtersCutsMuMu = "(PT > 400*MeV) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.1) "#"& (MAXTREE('mu+'==ABSID,MIPCHI2DV(PRIMARY)) > 9) " #% params
        #_daughtersCutsMuE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.1)  & (MINTREE('e+'==ABSID,PIDe) > 1.) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.1)"

#        _Combine = CombineParticles()
        _Combine = DaVinci__N4BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "e+"   : "(PROBNNe > 0.3) & (PIDe > 3.)",
            "mu+"  : "ISMUON & (PROBNNmu > 0.3)", }

        _Combine.CombinationCut   = _CombCuts
        _Combine.Combination12Cut   = _CombCuts12
        _Combine.Combination123Cut   = _CombCuts123
        _Combine.MotherCut        = _MotherCuts

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")
        _stdAllLooseElectrons = DataOnDemand(Location = "Phys/StdAllLooseElectrons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ _stdNoPIDLooseMuons, _stdAllLooseElectrons ] )

#####################################################
    def _makeMu3EInclDet( self, name, mue, dielectron, params ) :
        """
        Make a mu+mu-mu+e-
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "[J/psi(1S) -> rho(1700)+ omega(782)]cc"

         # define all the cuts
        #_CombCuts12    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm) & (APT > 400*MeV)" % params #(AM > 2000*MeV) & (AM < 4600*MeV) &
        #_CombCuts123    = "(ACUTDOCACHI2(12,'')) & (APT > 500*MeV) " % params
        _CombCuts    = "(ACUTDOCACHI2(9,'')) & (APT > 600*MeV)" % params

        _MotherCuts  = "(BPVDLS>3) & (VFASPF(VCHI2PDOF) < 9)  & ( NINTREE ( ( 14>ABSID ) & (PT>500*MeV) )> 1.5  ) & (MINTREE('e+'==ABSID,PROBNNe) > 0.3)  & (MINTREE('e+'==ABSID,PIDe) > 3) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.3)" % params
        #& (BPVVDCHI2 > 2*%(DiLeptonFDCHI2)s)
        #_MotherCuts  = "(VFASPF(VCHI2PDOF) < 10) & (MAXTREE(14>ABSID,PT) > 400.0 *MeV) & ( NINTREE ( ( 14>ABSID ) & (PT>300) )> 2.5  )" % params #(M > 2500*MeV) & (M < 4400*MeV) &
        #_daughtersCutsMuMu = "(PT > 400*MeV) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.1) "#"& (MAXTREE('mu+'==ABSID,MIPCHI2DV(PRIMARY)) > 9) " #% params
        #_daughtersCutsMuE = "(PT > 400*MeV) & (MINTREE('e+'==ABSID,PROBNNe) > 0.1)  & (MINTREE('e+'==ABSID,PIDe) > 1.) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.1)"

        _Combine = CombineParticles()
        #_Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "rho(1700)+"  : "(MINTREE('e+'==ABSID,PIDe) > 1) & (MINTREE('mu+'==ABSID,PROBNNmu) > 0.1)",
            "omega(782)"  : "(MINTREE('e+'==ABSID,PIDe) > 1)",
            }

        _Combine.CombinationCut   = _CombCuts
        #_Combine.Combination12Cut   = _CombCuts12
        #_Combine.Combination123Cut   = _CombCuts123
        _Combine.MotherCut        = _MotherCuts

        #_stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
        #_stdAllLooseElectrons = DataOnDemand(Location = "Phys/StdAllLooseElectrons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ mue, dielectron ] )

#####################################################
    def _makeMu3EInclPrompt( self, name, dielectron, params ) :
        """
        Make a mu+e-e+e-
        Jpsi is a proxy to get the 4-body combination
        """

        _Decays = "[J/psi(1S) -> mu+ e- omega(782)]cc"

         # define all the cuts
        _CombCuts12    = "(ADOCA(1,2)<%(DiHadronADOCA)s*mm) & (APT > 900*MeV)" % params
        #_CombCuts123    = "(ACUTDOCACHI2(10,'')) & (APT > 1000*MeV) " % params
        _CombCuts    = "(ACUTDOCACHI2(10,'')) & (APT > 1200*MeV) & (AM<20000*MeV)" % params

        _MotherCuts  = "(M<15000*MeV) & (PT>2000*MeV) & (VFASPF(VCHI2PDOF) < 9)  & ( NINTREE ( ( 14>ABSID ) & (PT>500*MeV) )> 1.5  ) & (MINTREE('e+'==ABSID,PROBNNe) > 0.3)  & (MINTREE('e+'==ABSID,PIDe) > 3)" % params

        _Combine = DaVinci__N3BodyDecays()

        _Combine.DecayDescriptor = _Decays

        _Combine.DaughtersCuts = {
            "omega(782)"  : "(MINTREE('e+'==ABSID,PIDe) > 3) & (PT>900*MeV)",
            "e+"   : "(PROBNNe > 0.3) & (PIDe > 3.) & (PT>350*MeV)",
            "mu+"  : "ISMUON & (PROBNNmu*(1-PROBNNpi)*(1-PROBNNk) > 0.2) & (PT>500*MeV)", }

        _Combine.CombinationCut   = _CombCuts
        _Combine.Combination12Cut   = _CombCuts12
        #_Combine.Combination123Cut   = _CombCuts123
        _Combine.MotherCut        = _MotherCuts

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")
        _stdAllLooseElectrons = DataOnDemand(Location = "Phys/StdAllLooseElectrons/Particles")

        return Selection( name, Algorithm = _Combine, RequiredSelections = [ dielectron, _stdNoPIDLooseMuons, _stdAllLooseElectrons ] )

#####################################################

    def _make5Mu(self, name, params):
        """
        Make inclusive detached -> 5 muons
        """

        X2MuMuMuMuMu = CombineParticles(
                       DecayDescriptor = " [ Xi_b- -> mu+ mu+ mu- mu- mu-]cc",
                       DaughtersCuts = { "mu+" : "( PT > 100 * MeV ) & ( TRGHOSTPROB < %(Trk_GhostProb)s ) & ( TRCHI2DOF < 3  ) & ( BPVIPCHI2 () >  4 ) " % params },
                       #Combination12Cut = "AM<7000*MeV",   # 1778 + 400 - 3*100
                       #Combination123Cut = "AM<7100*MeV",   # 1778 + 400 - 2*100
                       #Combination1234Cut = "AM<7200*MeV",   # 1778 + 400 - 1*100
                       CombinationCut = " ( ANUM (( 'mu-' == ABSID ) & (PROBNNmu>0.05)) > 3.5)",
                       MotherCut = """
                ( VFASPF(VCHI2PDOF) < 25 ) &
                (BPVVDCHI2 > 25) &
                (BPVDIRA > 0.995 )
                """
                       )

        _stdNoPIDLooseMuons = DataOnDemand(Location = "Phys/StdAllLooseMuons/Particles")

        return Selection (name,
                          Algorithm = X2MuMuMuMuMu,
                          RequiredSelections = [ _stdNoPIDLooseMuons ])
