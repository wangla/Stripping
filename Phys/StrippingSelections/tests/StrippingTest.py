#!/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

'''Script to test the Stripping in pretty much any way possible. In a DaVinci environment, do
./StrippingTest.py -h
to see the options.'''

from Configurables import DaVinci, Stripping
from Gaudi.Configuration import importOptions
from argparse import ArgumentParser
import os, sys

argparser = ArgumentParser()
argparser.add_argument('-d', '--datafile', help = 'Data file to use. Environment variables will be expanded.')
argparser.add_argument('-i', '--importoptions', default = [], nargs = '*', help = 'Extra options files to import.')
argparser.add_argument('--genpydoc', action = 'store_true', help = 'Generate the python documentation.')
argparser.add_argument('--pydocdvver', default = '', help = 'DaVinci version to be set in the python documentation.')
argparser.add_argument('--pydocstepid', default = '', help = 'Production step ID to be set in the python documentation.')
argparser.add_argument('--pydocfname', default = '', help = 'Name of file to which to write the python doc. Defaults to $STRIPPINGDOCROOT/python/StrippingDoc/<version>.py')

# Set defaults for tests.
Stripping().ReportFrequency = 10000
Stripping().MonitorTiming = True
DaVinci().HistogramFile = 'dv_stripping_histos.root'

dvargs = {}
for prop in DaVinci().getDefaultProperties() :
    default = DaVinci().getProp(prop)
    dvargs[prop] = argparser.add_argument('--' + prop, default = default, type = default.__class__,
                                          help = 'DaVinci: ' + str(DaVinci().getPropertiesWithDescription()[prop][1]) \
                                              + '\nDefault: ' + repr(default))

strippingprops = Stripping().getDefaultProperties().keys()
strippingprops.remove('Simulation')

strippingargs = {}
listprops = 'WGs', 'FilterConfigs', 'FilterLines'
for prop in listprops :
    strippingargs[prop] = argparser.add_argument('--' + prop, default = Stripping().getProp(prop),
                                                 nargs = '*', help = 'Stripping: ' + str(Stripping().getPropertiesWithDescription()[prop][1]) \
                                                     + '\nDefault: ' + repr(Stripping().getProp(prop)))
    strippingprops.remove(prop)
for prop in strippingprops :
    default = Stripping().getProp(prop)
    if isinstance(default, bool) :
        defaulttype = eval
    else :
        defaulttype = default.__class__
    strippingargs[prop] = argparser.add_argument('--' + prop, default = default,
                                                 type = defaulttype, 
                                                 help = 'Stripping: ' + str(Stripping().getPropertiesWithDescription()[prop][1]) \
                                                     + '\nDefault: ' + repr(default))
strippingargs['Simulation'] = dvargs['Simulation']

args = argparser.parse_args()

# Import the data file and any other extra options
importopts = args.importoptions
datafile = os.path.expandvars(args.datafile)
importopts.insert(0, datafile)
if os.path.exists(datafile.replace('.py', '_DV.py')) :
    importopts.insert(1, datafile.replace('.py', '_DV.py'))
for opts in importopts :
    importOptions(opts)

# Configure DaVinci
for prop in DaVinci().getDefaultProperties() :
    # Only set attributes explicitly requested from the commandline.
    if not any(opt in sys.argv for opt in dvargs[prop].option_strings) :
        continue
    setattr(DaVinci(), prop, getattr(args, prop))
# Configure Stripping
for prop in Stripping().getDefaultProperties() :
    # Only set attributes explicitly requested from the commandline.
    if not any(opt in sys.argv for opt in strippingargs[prop].option_strings) :
        continue
    setattr(Stripping(), prop, getattr(args, prop))
Stripping().Simulation = DaVinci().getProp('Simulation')

DaVinci().appendToMainSequence([Stripping().sequence])

from GaudiPython.Bindings import AppMgr

appmgr = AppMgr()
appmgr.initialize()

if args.genpydoc :
    Stripping().gen_python_doc(dvversion = args.pydocdvver, 
                               datatype = DaVinci().getProp('DataType'), 
                               stepid = args.pydocstepid, 
                               fname = args.pydocfname)

evtmax = DaVinci().getProp('EvtMax')
ievt = 0
if evtmax != -1 :
    keep_going = (lambda : ievt < evtmax)
else :
    keep_going = (lambda : True)

while keep_going() :
    appmgr.run(1)
    if not appmgr.evtsvc().getObject('Rec/Header') :
        break
    ievt += 1
    
