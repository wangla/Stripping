#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

dvversion=v42r5
stepid=131451
datatype=2017
opts=$PWD/Stripping-S29.py

./run \$STRIPPINGSELECTIONSROOT/tests/StrippingTest.py --datafile \$STRIPPINGSELECTIONSROOT/tests/data/Reco16_Run182594.py --genpydoc --pydocdvver $dvversion --DataType $datatype --pydocstepid $stepid --EvtMax 0 --importoptions $opts --HLT2Rate 0.