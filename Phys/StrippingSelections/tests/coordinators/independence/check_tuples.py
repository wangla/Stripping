###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TFile, TTree, TTreeFormula
import os, sys

job_all = 572

jobs_lines = range(387, 572)

gangadir = '/afs/cern.ch/work/m/malexand/public/stripping/20170515-S29/test-retentions/ganga/gangadir/workspace/malexand/LocalXML/' # Location of ganga job logs

with open('independence.log', 'w') as f :
    pass

path2 = gangadir + str(job_all)
fullname2 = path2 + '/0/output/DaVinciTuples.root'
file2 = TFile(fullname2)
tuple_all = file2.Get("StrippingDecsTuple/EventTuple")
nevents = tuple_all.GetEntries()

for job in jobs_lines : 

    print 'job', job

    path1 = gangadir + str(job)
    fullname1 = path1 + '/0/output/DaVinciTuples.root'
    if not os.path.isfile(fullname1) or not os.path.isfile(fullname2) : continue

    file1 = TFile(fullname1)
    tuple_line = file1.Get("StrippingDecsTuple/EventTuple")
        
    branches = tuple_line.GetListOfBranches()

    for br in branches:

        if "EventInSequence"==br.GetName(): continue
        if "StrippingStream" in br.GetName(): continue
        if "Stripping" not in br.GetName(): continue
        events = 0 
        selected = 0
        linename = br.GetName()
        print 'Checking line %s (job %d)' % (linename, job)

        if tuple_line.GetEntries() != nevents : 
            print 'Number of events does not match: %d (all lines), %d (%s line)' % (nevents, tuple_line.GetEntries(), linename)
            #sys.exit(0)
            continue

        list_all = []
        allform = TTreeFormula('allform', linename, tuple_all)
        for i in xrange(tuple_all.GetEntries()) : 
            tuple_all.LoadTree(i)
            allform.GetNdata()
            list_all += [ allform.EvalInstance() ]

        list_line = []
        lineform = TTreeFormula('lineform', linename, tuple_line)
        for i in xrange(tuple_line.GetEntries()) : 
            tuple_line.LoadTree(i)
            lineform.GetNdata()
            list_line += [ lineform.EvalInstance() ]

        for k, v in enumerate(list_all) :
            if list_line[k] != v : 
                l = "!!! Stripping decision is different: line %s, event %d (job %d): all=%d, single=%d\n" % (linename, k, job, v, list_line[k])
                for name, ntuple in ('all', tuple_all), ('single', tuple_line) :
                    ntuple.GetEntry(k)
                    l += '--- Lines firing in ' + name + ' tuple:\n'
                    for branch in (br.GetName() for br in ntuple.GetListOfBranches()) :
                        if branch.startswith('Stripping') and not branch.startswith('StrippingStream') \
                                and getattr(ntuple, branch) != 0. :
                            l += branch + '\n'
                    l += 'Run/event number: ' + str(ntuple.runNumber) + ' ' + str(ntuple.eventNumber) + '\n'
                l += '\n'
                print l
                with open('independence.log', 'a') as f :
                    f.write(l)
            events += 1
            if v == 1: selected += 1

        print 'checked, %d events, %d selected' % (events, selected)
        
    file1.Close()
file2.Close()
