###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################################


jobs = {
  "B2CC"      : "B2CC", 
  "B2OC"      : "B2OC", 
  "BandQ"     : "BandQ", 
  "Charm"     : "Charm", 
  "BnoC"      : "BnoC", 
  "QEE"       : "QEE", 
  "RD"        : "RD", 
  "SL"        : "SL", 
  "Calib"     : "Calib"
}

dumpDetailedTable = True  # Dump the table with lines' retentions in twiki format
triggerRate = 8600.        # Trigger rate for BW calculation in Hz
gangadir = '/afs/cern.ch/user/s/sreicher/work/Stripping/DV_S24r0p1_patch/' # Location of ganga job logs

streamEventSize = {
  "B2CC"      : {'Bhadron' : 289984.,
                 'BhadronCompleteEvent' : 0.,
                 'Calibration' : 0.,
                 'Charm' : 0.,
                 'CharmCompleteEvent' : 0.,
                 'Dimuon' : 2061020.,
                 'EW' : 0.,
                 'Leptonic' : 5651807.,
                 'MiniBias' : 0.,
                 'Radiative' : 0.,
                 'Semileptonic' : 0.,
                 'FTAG' : 0.
                },
  "B2OC"      : {'Bhadron' : 16754246.,
                 'BhadronCompleteEvent' : 11726614.,
                 'Calibration' : 0.,
                 'Charm' : 0.,
                 'CharmCompleteEvent' : 0.,
                 'Dimuon' : 0.,
                 'EW' : 0.,
                 'MiniBias' : 0.,
                 'Leptonic' : 0.,
                 'Radiative' : 0.,
                 'Semileptonic' : 0.,
                 'FTAG' : 0.
                 },
  "BandQ"     : {'Bhadron' : 15767119.,
                 'BhadronCompleteEvent' : 0.,
                 'Calibration' : 0.,
                 'Charm' : 1051178.,
                 'CharmCompleteEvent' : 1293751.,
                 'Dimuon' : 0.,
                 'EW' : 0.,
                 'Leptonic' : 31711.,
                 'MiniBias' : 0.,
                 'Radiative' : 0.,
                 'Semileptonic' : 0., 
                 'FTAG' : 0. 
                 },
  "Charm"     : {'Bhadron' : 0.,
                'BhadronCompleteEvent' : 0.,
                'MiniBias' : 0.,
                'Calibration' : 0.,
                'Charm' : 18389965.,
                'CharmCompleteEvent' : 6278611.,
                'Dimuon' : 0.,
                'EW' : 0.,
                'Leptonic' : 0.,
                'Radiative' : 0.,
                'Semileptonic' : 0.,
                'FTAG': 0.},
  "BnoC" : {'Bhadron' : 12038082.,
                 'BhadronCompleteEvent' : 28383580.,
                 'Calibration' : 0.,
                 'Charm' : 0.,
                 'CharmCompleteEvent' : 0.,
                 'Dimuon' : 0.,
                 'EW' : 0.,
                 'Leptonic' : 0.,
                 'MiniBias' : 0.,
                 'Radiative' : 0.,
                 'Semileptonic' : 0., 
                 'FTAG' : 2621076. 
                },
  "QEE"       : {'Bhadron' : 0.,
                'BhadronCompleteEvent' : 500875.,
                'MiniBias' : 0.,
                'Calibration' : 0.,
                'Charm' : 0.,
                'CharmCompleteEvent' : 0.,
                'Dimuon' : 1189838.,
                'EW' : 35367149.,
                'Leptonic' : 1362177.,
                'Radiative' : 0.,
                'Semileptonic' : 0. ,
                'FTAG': 0.},
  "RD"        : {'Bhadron' : 3710103.,
                'BhadronCompleteEvent' : 0.,
                'MiniBias' : 0.,
                'Calibration' : 0.,
                'Charm' : 0.,
                'CharmCompleteEvent' : 0.,
                'Dimuon' : 17748721.,
                'EW' : 0.,
                'Leptonic' : 50911722.,
                'Radiative' : 17431751.,
                'Semileptonic' : 0.,
                'FTAG': 0. },
  "SL"        : {'Bhadron' : 0.,
                'BhadronCompleteEvent' : 0.,
                'MiniBias' : 0.,
                'Calibration' : 0.,
                'Charm' : 0.,
                'CharmCompleteEvent' : 0.,
                'Dimuon' : 0.,
                'EW' : 0.,
                'Leptonic' : 0.,
                'Radiative' : 0.,
                'Semileptonic' : 8633538.,
                'FTAG': 0. },
  "Calib"     : {'Bhadron' : 0.,
                'BhadronCompleteEvent' : 0.,
                'MiniBias' : 0.,
                'Calibration' : 0.,
                'Charm' : 0.,
                'CharmCompleteEvent' : 0.,
                'Dimuon' : 0.,
                'EW' : 0.,
                'Leptonic' : 0.,
                'Radiative' : 0.,
                'Semileptonic' : 0.,
                'FTAG': 0. }
}



#################################################################################################

import os
from copy import copy
from math import sqrt

#streams = sorted(streamEventSize['Charmless'].keys())

#['Bhadron', 'BhadronCompleteEvent', 'Calibration', 'Charm',
#           'CharmCompleteEvent', 'CharmToBeSwum', 'Dimuon', 'EW',
#           'Leptonic', 'MiniBias', 'PID', 'Radiative', 'Semileptonic']

streamRate = {}
eventSize = {}

dd = {}

for (wg, job) in jobs.iteritems() : 
  d = {}

  path = gangadir + str(job)
  files = os.listdir(path)
  nfiles = 0
  curr_stream = None
  nevents = 0
  for filename in files : 
    reachedLastEvent = False
    foundStrippingReport = False

    fullname = path + '/' + filename
    if not os.path.isfile(fullname) : continue
    f = file(fullname)
    nfiles += 1
    if "log" in filename:    
    
      for l in f : 
        if l.find('Xeon') > 0 : 
#            print l
            speed = float(l.split()[8])
#            print speed
        if l.find('Application Manager Stopped successfully') > 0 : 
            reachedLastEvent = True
            continue
#        if l.find('Stripping') <= 0 : continue
        if reachedLastEvent : 
#    	    print l
            if l.find('StrippingReport') == 0: 
                #print l
                #if "[min]" not in str(l.split()[6]) and ("[ms]" not in str(l.split()[6])):
                nevents += int(l.split()[6])
                print nevents
            if l.find('StrippingGlobal') > 0 : 
                foundStrippingReport = True
                ls = l.split('|')
 #               print l
                if 'ALL' not in d.keys() :  
                    d['ALL'] = [ int(ls[3]), float(ls[5])*speed/1.5, [], {}, 0, 0 ]
                else : 
                    d['ALL'][0] += int(ls[3])
                    d['ALL'][1] += float(ls[5])*speed/1.5
            elif foundStrippingReport and l.find('StrippingSequenceStream') > 0 : 
                ls = l.split('|')
                name = ls[1].strip().rstrip('_')[24:]
                curr_stream = name
#                    print ls
                nev = int(ls[3])
#		if name == 'MiniBias' : nev = float(nev)/100.
                if name not in d.keys() : 
                    d[name] = [ nev, float(ls[5])*speed/1.5, [], {}, 0, 0 ]
                else : 
                    d[name][0] += nev
                    d[name][1] += float(ls[5])*speed/1.5
            elif foundStrippingReport and l.find('Stripping') > 0 : 
                #print l
                ls = l.split('|')
                name = ls[1].strip()[1:]
                #print ls[3]
                nev = int(ls[3])
                if len(ls[5].strip())>1 : 
                    time = float(ls[5])*speed/1.5
                else :
                    time = 0
                    
                if name not in d[curr_stream][2] : 
                    d[curr_stream][2] += [ name ]
                    d[curr_stream][3][name] = [ nev, time, 0, 0 ]
                else : 
                    #print d[curr_stream][3][name][0]
                    #print nev
            	    d[curr_stream][3][name][0] += nev
            	    #print d[curr_stream][3][name][0]
            	    d[curr_stream][3][name][1] += time
            elif foundStrippingReport : 
#                print l 
                break
    f.close()

  print '%10s: events=%10d, files=%4d' % (wg, nevents, nfiles)

  if nevents == 0 : continue

  for k in d.keys() : 
    d[k][4] = sqrt(float(d[k][0]))/float(nevents)
    d[k][5] = d[k][0]
    d[k][0] /= float(nevents)
    d[k][1] /= float(nfiles)
    for sl in d[k][2] : 
        d[k][3][sl][2] = sqrt(float(d[k][3][sl][0]))/float(nevents)
        d[k][3][sl][3] = d[k][3][sl][0]
        d[k][3][sl][0] /= float(nevents)
        d[k][3][sl][1] /= float(nfiles)

  dd[wg] = copy(d)

#f = open('wg_table.tex', 'w')

#s = '%16s ' % 'WG / Stream'
#s2 = '%16s ' % 'Event size, kb'
#for stream in streamEventSize['BnoC'].keys() :
    #s += '& \\begin{sideways}%5s\\end{sideways}' % stream
    #s2 += '& %5.1f' % streamEventSize['BnoC'][stream]

##    if stream not in streamRate.keys() : 
##        eventSize[stream] = [ streamEventSize[stream] ]
##        streamRate[stream] = [ d['ALL'][stream][0] ]
##    else : 
##        eventSize[stream] += [ streamEventSize[stream] ]
##        streamRate[stream] += [ d['ALL'][stream][0] ]

#s += '& \\begin{sideways}%5s\\end{sideways}' % 'Total, MB/s\n'
#s2 += '&       \n'
#s += '& \\begin{sideways}%5s\\end{sideways} \\\\' % 'Time, ms/event\n'
#s2 += '&       \\\\\n'
#f.write(s)
#f.write('\\hline\n')
#f.write(s2)
#f.write('\\hline\n')

#for wg in sorted(jobs.keys()) :
    #if wg not in dd.keys() : continue

    #tot_bw = 0.
    #s = '%16s ' % wg

##    print dd[wg].keys()

    #for stream in streamEventSize[wg].keys() :
        #if stream in dd[wg].keys() :
            #retention = dd[wg][stream][0]
            #bw = streamEventSize[wg][stream]*retention*triggerRate/1e3
            ##print stream
            #if stream=="FTAG.DST": bw = bw*0.25
            #print "yeah"
            #print stream
            #print wg
            #print bw
            #print retention
            #print streamEventSize[wg][stream]
            #tot_bw += bw
            #s += '& %5.2f' % bw
        #else :
            #s += '&      '
    #if 'ALL' in dd[wg].keys() : 
        #s += '& %5.2f & %5.1f \\\\' % (tot_bw, dd[wg]['ALL'][1])
        #if wg == 'All' : print '\\hline'
    #else : 
        #s += '&       &       \\\\'
    #f.write(s + '\n')

#f.close()

#if dumpDetailedTable : 

    #f = open('line_retentions.txt', 'w')
    #f2 = open('large_retention.txt', 'w')
    #f2.write(' |*%-19s*|*%-19s*|*%-49s*|*N evnts*|*Rate, %%*|\n' % ('WG', 'Stream', 'Line name') )

    #for wg in sorted(jobs.keys()) : 
        #if wg not in dd.keys() : continue
        #if wg == 'All' : continue
        #f.write('---+ ' + wg)
        #f.write('\n\n')
        #for stream in streamEventSize[wg].keys() : 
            #if stream in dd[wg].keys() : 
##            print ' |_%-50s|         |        |' % (stream+'_')
                #f.write('---+++ ' +  stream + '\n')
                #f.write(' |*%-49s*|*N evnts*|*Rate, %%*|*ms/evt*|\n' % 'Line name')
                #for sl in dd[wg][stream][2] : 
                    #f.write(' |!%-50s| %7d | %7.4f | %6.3f |\n' % (sl, dd[wg][stream][3][sl][0]*(nevents), dd[wg][stream][3][sl][0]*100, dd[wg][stream][3][sl][1]))
                    #if stream in ['BhadronCompleteEvent', 'CharmCompleteEvent', 'Dimuon', 'Radiative', 'Semileptonic', 'EW'] and \
                       #dd[wg][stream][3][sl][0]*100 > 0.05 : 
                       #f2.write('  |!%-20s|!%-20s|!%-50s| %7d | %7.4f |\n' % (wg, stream, sl, dd[wg][stream][3][sl][0]*(nevents), dd[wg][stream][3][sl][0]*100) )
                #f.write('\n')
        #f.write('\n')
        #f2.write('\n')
        
    #f.close()
    #f2.close()

f = open('wg_table.tex', 'w')

s = '%16s ' % 'WG / Stream'
s2 = '%16s ' % 'Event size, kb'
for stream in streamEventSize['Charm'].keys() :
  for wg in jobs.keys():
    try:
      size = streamEventSize[wg][stream]/dd[wg][stream][5]/1000 #size in kB
    except:
      pass 
  s += '& \\begin{sideways}%5s\\end{sideways}' % stream
  s2 += '& %5.1f' % size
#    if stream not in streamRate.keys() : 
#        eventSize[stream] = [ streamEventSize[stream] ]
#        streamRate[stream] = [ d['ALL'][stream][0] ]
#    else : 
#        eventSize[stream] += [ streamEventSize[stream] ]
#        streamRate[stream] += [ d['ALL'][stream][0] ]

s += '& \\begin{sideways}%5s\\end{sideways} \\\\' % 'Total, MB/s\n'
#s2 += '&   \n'
#s += '& \\begin{sideways}%5s\\end{sideways} \\\\' % 'Time, ms/event\n'
s2 += '&       \\\\\n'
f.write(s)
f.write('\\hline\n')
f.write(s2)
f.write('\\hline\n')

for wg in sorted(jobs.keys()) :
    if wg not in dd.keys() : continue
    tot_bw = 0.
    s = '%16s ' % wg
    for stream in streamEventSize[wg].keys() :
        if stream in dd[wg].keys() :
            retention = dd[wg][stream][0]
            events = dd[wg][stream][5]
            bw = streamEventSize[wg][stream]*triggerRate/float(nevents)/1000/1000 #easier way, Total Event of DST * trigger rate / Total number of Events run. This is equivalent to eventSize * retention 
            tot_bw += bw
            s += '& %5.2f' % bw
        else :
            s += '&      '
    if 'ALL' in dd[wg].keys() : 
        #s += '& %5.2f & %5.1f \\\\' % (tot_bw, dd[wg]['ALL'][1])
        s += '& %5.2f\\\\' % (tot_bw)
        if wg == 'All' : print '\\hline'
    else : 
        s += '&      \\\\'
    f.write(s + '\n')

f.close()

if dumpDetailedTable : 

    f = open('line_retentions.txt', 'w')
    f2 = open('large_retention.txt', 'w')
    f2.write(' |*%-19s*|*%-19s*|*%-49s*|*N evnts*|*Rate, %%*|\n' % ('WG', 'Stream', 'Line name') )

    for wg in sorted(jobs.keys()) : 
        if wg not in dd.keys() : continue
        if wg == 'All' : continue
        f.write('---+ ' + wg)
        f.write('\n\n')
        for stream in streamEventSize[wg].keys() : 
            if stream in dd[wg].keys() : 
#            print ' |_%-50s|         |        |' % (stream+'_')
                f.write('---+++ ' +  stream + '\n')
                f.write(' |*%-49s*|*N evnts*|*Rate, %%*|*ms/evt*|\n' % 'Line name')
                for sl in dd[wg][stream][2] : 
                    #print dd[wg][stream][3][sl][0]
                    #print nevents
                    #print dd[wg][stream][3][sl][0]*nevents
                    f.write(' |!%-50s| %7d | %7.5f | %6.3f |\n' % (sl, dd[wg][stream][3][sl][0]*nevents, dd[wg][stream][3][sl][0]*100, dd[wg][stream][3][sl][1]))
                    if stream in ['BhadronCompleteEvent', 'CharmCompleteEvent', 'Dimuon', 'Radiative', 'Semileptonic', 'EW'] and \
                       dd[wg][stream][3][sl][0]*100 > 0.05 : 
                       f2.write('  |!%-20s|!%-20s|!%-50s| %7d | %7.5f |\n' % (wg, stream, sl, dd[wg][stream][3][sl][0]*(nevents), dd[wg][stream][3][sl][0]*100) )
                f.write('\n')
        f.write('\n')
        f2.write('\n')
        
    f.close()
    f2.close()
