###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Protonion13/FULL.DST/00032860/0000/00032860_00005453_1.full.dst',
'LFN:/lhcb/LHCb/Protonion13/FULL.DST/00032860/0000/00032860_00005616_1.full.dst',
'LFN:/lhcb/LHCb/Protonion13/FULL.DST/00032860/0000/00032860_00005634_1.full.dst',
'LFN:/lhcb/LHCb/Protonion13/FULL.DST/00032860/0000/00032860_00005549_1.full.dst',
], clear=True)


from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco14r1_Protonion13_Run135710.xml' ]

