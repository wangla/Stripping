###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Tue May  8 21:38:39 2018
#-- Contains event types : 
#--   90000000 - 8 files - 136227 events - 7.34 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000005_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000006_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000007_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000008_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000009_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000010_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000011_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074101/0000/00074101_00000012_1.rdst'
], clear=True)

from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco18_Run206538.xml' ]

