###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed May  2 09:17:01 2018
#-- Contains event types : 
#--   90000000 - 16 files - 644281 events - 44.58 GBytes

# lb-run LHCbDirac/prod dirac-bookkeeping-get-files -B /validation/Collision18/Beam6500GeV-VeloClosed-MagUp/RealData/Reco18/90000000/RDST

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/validation/Collision18/RDST/00073878/0000/00073878_00000014_1.rdst',
'LFN:/lhcb/validation/Collision18/RDST/00073878/0000/00073878_00000015_1.rdst'
], clear=True)

from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco18_Run206574_validation.xml' ]

