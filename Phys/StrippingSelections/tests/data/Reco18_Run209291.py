###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed Jul 18 16:31:17 2018
#-- Contains event types : 
#--   90000000 - 8 files - 537617 events - 29.55 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030781_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030844_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030847_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030865_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00030916_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00037075_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00037092_1.rdst',
'LFN:/lhcb/LHCb/Collision18/RDST/00074708/0003/00074708_00037467_1.rdst'
], clear=True)


from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco18_Run209291.xml' ]

