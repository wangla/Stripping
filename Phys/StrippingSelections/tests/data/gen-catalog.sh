#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Generate the xml catalog for one or more data files, including ancestors if 
# it's an .rdst dataset.

function gen_catalog() {
    local fname=$1
    local rdst=$(grep '\.rdst' $fname)
    if [ -z "$rdst" ] ; then
	local depth=1
    else 
	local depth=2
    fi
    # I'm really not sure if the Sites and SEs options do anything, cause I still get all sites and SEs in the xml. 
    local cmd="lb-run LHCbDirac/prod dirac-bookkeeping-genXMLCatalog --Options=$fname --Catalog=tmpcatalog.xml --Depth=$depth --Sites=[LCG.CERN.cern] --SEs=[CERN-SWTEST]"
    eval "$cmd"
    local xmlcode=$?
    if [ 0 != $xmlcode ] ; then
	echo 'Failed to generate xml catalog using:'
	echo "$cmd"
	return $xmlcode
    fi
    if [ -e tmpcatalog.py ] ; then
	rm tmpcatalog.py
    fi
    local xmlname="${fname/\.py/.xml}"
    mv tmpcatalog.xml $xmlname
    if [ -z "$(grep $xmlname $fname)" ] ; then
	echo "
from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs += [ 'xmlcatalog_file:\$STRIPPINGSELECTIONSROOT/tests/data/${xmlname}' ]
" >> $fname
    fi
}

for fname in $@ ; do
    gen_catalog $fname
done
