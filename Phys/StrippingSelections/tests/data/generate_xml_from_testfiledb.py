###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from PRConfig import TestFileDB

datasets = {'TestData_restripping_collision15_reco15a': 'Reco15a_DataType2015_Run164524',
            'TestData_restripping_collision16_reco16' : 'Reco16_DataType2016_Run174858' ,
            'TestData_restripping_collision17_reco17' : 'Reco17_DataType2017_Run195969' ,
            'TestData_restripping_collision18_reco18' : 'Reco18_DataType2018_Run214741'}

for dataset in datasets.keys():
  fn = TestFileDB.test_file_db[dataset].filenames
  f = open('tmp.py','w')
  f.write('from Gaudi.Configuration import *\n')
  f.write('from GaudiConf import IOHelper\n')
  f.write("IOHelper('ROOT').inputFiles([\n")
  for fns in fn:
    fnsd = fns.replace('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest', 'LFN:')
    f.write("'"+fnsd+"',\n")
  f.write("], clear=True)\n")
  f.close()
  os.system('lb-run LHCbDirac/prod dirac-bookkeeping-genXMLCatalog --Options=tmp.py --Catalog='+datasets[dataset]+'.xml --Depth=2')

