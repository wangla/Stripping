#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Tests of StrippingStreams instantiation.'''

__author__ = 'Juan Palacios juan.palacios@nikhef.nl'

def test_instantiate_StrippingConf_with_all_streams() :
    '''
    Test that Stripping Streams can be instantiated.
    This is the most basic test that should be run when modifying or
    adding Stripping Streams to StrippingSelections.Streams.allStreams.
    '''

    from StrippingConf.Configuration import StrippingConf
    from StrippingSelections import buildersConf
    stripping = buildersConf()
    from StrippingSelections.Utils import buildStreams

    streams = buildStreams(stripping)

    sc = StrippingConf (Streams = streams)

if __name__ == '__main__' :
    test_instantiate_StrippingConf_with_all_streams()
