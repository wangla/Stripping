###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
CaloPID = {
    "BUILDERTYPE": "CaloPIDConf",
    "CONFIG": {
        "D_APT": 2000.0,
        "D_BPVLTIME": 0.25,
        "D_DIRA": 0.999975,
        "D_IP": 0.05,
        "D_IP_Chi2": 10.0,
        "D_Mass_Max": 2100.0,
        "D_Mass_Min": 1800.0,
        "D_Pi_BACH_PT": 600.0,
        "D_Vtx_Chi2ndof": 4.0,
        "Dsst_DIRA": 0.999975,
        "Dsst_IP": 0.05,
        "Dsst_IP_Chi2": 10.0,
        "Dsst_Mass_Max": 2250.0,
        "Dsst_Mass_Min": 2050.0,
        "Dsst_gamma_PT": 500.0,
        "Eta_DOCA": 0.2,
        "Eta_DiMu_FDChi2": 45,
        "Eta_DiMu_PT": 1000,
        "Eta_GhostProb": 0.3,
        "Eta_Mass_Max": 700.0,
        "Eta_Mass_Min": 400.0,
        "Eta_MuIPChi2": 6.0,
        "Eta_MuP": 10000,
        "Eta_MuPT": 500,
        "Eta_MuPTPROD": 1,
        "Eta_MuProbNNmu": 0.8,
        "Eta_VChi2": 10,
        "Eta_gamma_PT": 200,
        "Etap_Mass_Max": 1020.0,
        "Etap_Mass_Min": 900.0,
        "Etap_Pi_MIPCHI2DV": 16.0,
        "Etap_Pi_P": 1000.0,
        "Etap_Pi_PID": 0,
        "Etap_Pi_PT": 500.0,
        "Etap_Pi_TRGHOSTPROB": 0.5,
        "Etap_Pi_Track_Chi2ndof": 5.0,
        "Etap_gamma_PT": 1000.0,
        "PrescaleDs2EtapPi": 1,
        "PrescaleDsst2DsGamma": 1,
        "PrescaleEta2MuMuGamma": 1
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

CharmFromBSemiForHadronAsy = {
    "BUILDERTYPE": "CharmFromBSemiForHadronAsyAllLinesConf",
    "CONFIG": {
        "D0to3H_3pi_DeltaMass_MAX": 350.0,
        "D0to3H_3pi_MASS_MAX": 1400.0,
        "D0to3H_3pi_MASS_MIN": 900.0,
        "D0to3H_B_DIRA_MIN": 0.99,
        "D0to3H_B_DOCACHI2_MAX": 50.0,
        "D0to3H_B_MASS_MAX": 4900.0,
        "D0to3H_B_MASS_MIN": 1800.0,
        "D0to3H_B_VCHI2NDF_MAX": 15.0,
        "D0to3H_DOCACHI2_MAX": 10.0,
        "D0to3H_DZ": 2.0,
        "D0to3H_K2pi_DeltaMass_MAX": 250.0,
        "D0to3H_K2pi_MASS_MAX": 1800.0,
        "D0to3H_K2pi_MASS_MIN": 1300.0,
        "D0to3H_REQUIRE_TOS": True,
        "D0to3H_SUMPT_MIN": 1800.0,
        "D0to3H_VCHI2NDF_MAX": 3.0,
        "GEC_nLongTrk": 250.0,
        "GHOSTPROB_MAX": 0.35,
        "H_PT": 250.0,
        "K_PIDKMin": 6.0,
        "Lc2Kpi_B_DIRA_MIN": 0.99,
        "Lc2Kpi_B_DOCACHI2_MAX": 50.0,
        "Lc2Kpi_B_FDCHI2_MIN": 20.0,
        "Lc2Kpi_B_MASS_MAX": 4300.0,
        "Lc2Kpi_B_MASS_MIN": 2200.0,
        "Lc2Kpi_B_VCHI2NDF_MAX": 15.0,
        "Lc2Kpi_DOCACHI2_MAX": 10.0,
        "Lc2Kpi_DZ": 1.0,
        "Lc2Kpi_DeltaMass_MAX": 700.0,
        "Lc2Kpi_FDCHI2_MIN": 20.0,
        "Lc2Kpi_MASS_MAX": 1350.0,
        "Lc2Kpi_MASS_MIN": 800.0,
        "Lc2Kpi_REQUIRE_TOS": True,
        "Lc2Kpi_SUMPT_MIN": 1500.0,
        "Lc2Kpi_VCHI2NDF_MAX": 3.0,
        "MuPiPi_CHI2NDF": 3.0,
        "MuPiPi_DOCACHI2_MAX": 15.0,
        "MuPiPi_FDCHI2_MIN": 20.0,
        "MuPi_CHI2NDOF_MAX": 3.0,
        "MuPi_DIRA_MIN": -99.0,
        "MuPi_DOCACHI2_MAX": 8.0,
        "MuPi_FDCHI2_MIN": 20.0,
        "MuPi_SUMPT_MIN": 1300.0,
        "Mu_PT": 800.0,
        "PiPi_CHI2NDF": 3.0,
        "PiPi_DOCACHI2_MAX": 15.0,
        "PiPi_MASS_MAX": 500.0,
        "PiPi_SUMPT_MIN": 600.0,
        "Pi_PIDKMax": 6.0,
        "Slowpi_PIDKMax": 10.0,
        "Slowpi_PIDeMax": 99.0,
        "Slowpi_PTMin": 200.0,
        "prescale_D0to3piRS": 1.0,
        "prescale_D0to3piWS": 0.2,
        "prescale_D0toK2piRS": 1.0,
        "prescale_D0toK2piWS": 0.2,
        "prescale_LbRS": 1.0,
        "prescale_LbWS": 0.2
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

CharmFromBSemiForProtonPID = {
    "BUILDERTYPE": "CharmFromBSemiForProtonPIDAllLinesConf",
    "CONFIG": {
        "GEC_nLongTrk": 250,
        "H_PT": 300,
        "K_PIDKMin": 8,
        "LambdaB_DIRA": 0.999,
        "LambdaB_DOCACHI2_MAX": 6,
        "LambdaB_MASS_MAX": 5000,
        "LambdaB_MASS_MIN": 3000,
        "LambdaB_VCHI2NDF": 3,
        "LambdaC_DIRA": 0.99,
        "LambdaC_DOCACHI2_MAX": 6,
        "LambdaC_DZ": 0.5,
        "LambdaC_FDCHI2": 20,
        "LambdaC_MASSWIN": 80,
        "LambdaC_PTSUM": 2000,
        "LambdaC_VCHI2NDF": 3,
        "MAXGHOSTPROB": 0.35,
        "Mu_PT": 1000,
        "Pi_PIDKMax": -2,
        "prescale": 1
    },
    "STREAMS": ["Bhadron"],
    "WGs": ["Calib"]
}

D02KPiPi0 = {
    "BUILDERTYPE": "StrippingD02KPiPi0Conf",
    "CONFIG": {
        "D0MaxIPChi2": 9,
        "D0MaxM": 2100,
        "D0MinDIRA": 0.9999,
        "D0MinM": 1600,
        "D0MinVVDChi2": 64,
        "D0MinVtxProb": 0.001,
        "MergedLinePostscale": 1.0,
        "MergedLinePrescale": 0.5,
        "Pi0MinPT_M": 2000,
        "Pi0MinPT_R": 1000,
        "ResPi0MinGamCL": 0.2,
        "ResolvedLinePostscale": 1.0,
        "ResolvedLinePrescale": 0.5,
        "TrackMaxGhostProb": 0.3,
        "TrackMinIPChi2": 16,
        "TrackMinPT_M": 300,
        "TrackMinPT_R": 600,
        "TrackMinTrackProb": 1e-06
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

D2KPiPi0_PartReco = {
    "BUILDERTYPE": "D2KPiPi0_PartRecoBuilder",
    "CONFIG": {
        "BDIRA": 0.999,
        "BFDCHI2HIGH": 100.0,
        "BPVVDZcut": 0.0,
        "BVCHI2DOF": 6,
        "DELTA_MASS_MAX": 190,
        "ElectronPIDe": 5.0,
        "ElectronPT": 0,
        "HadronMINIPCHI2": 9,
        "HadronPT": 800.0,
        "KLepMassHigh": 2500,
        "KLepMassLow": 500,
        "KaonPIDK": 5.0,
        "PionPIDK": -1.0,
        "Slowpion_PIDe": 5,
        "Slowpion_PT": 300,
        "Slowpion_TRGHOSTPROB": 0.35,
        "TOSFilter": {
            "Hlt2CharmHad.*HHX.*Decision%TOS": 0
        },
        "TRGHOSTPROB": 0.35
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

Dst2D0Pi = {
    "BUILDERTYPE": "StrippingDst2D0PiConf",
    "CONFIG": {
        "D0MaxIPChi2": 9,
        "D0MaxM": 2100,
        "D0MinDIRA": 0.9999,
        "D0MinM": 1600,
        "D0MinVVDChi2": 64,
        "D0MinVtxProb": 0.001,
        "Dst2D0PiLinePostscale": 1,
        "Dst2D0PiLinePrescale": 1,
        "Dst_PT_MIN": 4000,
        "Dst_VCHI2PDOF_MAX": 9,
        "Dst_VIPCHI2_MAX": 9,
        "Dst_VVDCHI2_MAX": 16,
        "Dst_dAM_MAX": 195.421,
        "Dst_dAM_MIN": 95.421,
        "Dst_dM_MAX": 155.421,
        "Dst_dM_MIN": 135.421,
        "GammaMinPT": 2000,
        "SoftPionMaxIPChi2": 16,
        "TrackMaxGhostProb": 0.3,
        "TrackMinIPChi2": 16,
        "TrackMinPT": 300,
        "TrackMinTrackProb": 1e-06
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

DstarD02KShhForTrackingEff = {
    "BUILDERTYPE": "DstarD02KShh_ForTrackingEffBuilder",
    "CONFIG": {
        "DIRA_D0_MIN":
        0.999,
        "DeltaM_MAX":
        250.0,
        "DeltaM_MIN":
        0.0,
        "FDCHI2_D0_MIN":
        80.0,
        "HLTFILTER":
        "(HLT_PASS_RE('Hlt2CharmHadD02HHXDst.*Decision')|HLT_PASS('Hlt2(Phi)?IncPhiDecision'))",
        "Hlt2TisTosSpec": {
            "Hlt2(Phi)?IncPhiDecision%TOS": 0,
            "Hlt2CharmHadD02HHXDst.*Decision%TOS": 0
        },
        "IPCHI2_MAX_Child_MIN":
        16.0,
        "IPCHI2_PiSlow_MAX":
        9.0,
        "KAON_PIDK_MIN":
        3.0,
        "KKprescale":
        1.0,
        "KMinusPiPlusprescale":
        0.1,
        "KPlusPiMinusprescale":
        0.1,
        "LongTrackGEC":
        150,
        "M_MAX":
        1800.0,
        "M_MIN":
        0.0,
        "PION_PIDK_MAX":
        0.0,
        "PT_Dstar_MIN":
        2500.0,
        "PairMaxDoca_Dstar":
        100.0,
        "Pair_AMINDOCA_MAX":
        0.1,
        "Pair_BPVCORRM_MAX":
        3000.0,
        "Pair_BPVVD_MIN":
        0.0,
        "Pair_SumAPT_MIN":
        2500.0,
        "PhiM_MAX":
        1040.0,
        "PhiM_MIN":
        1000.0,
        "PiPiprescale":
        0.1,
        "TrkChi2_MAX_Child_MAX":
        2.25,
        "TrkChi2_SlowPion":
        2.25,
        "TrkP_SlowPion":
        3000.0,
        "TrkPt_SlowPion":
        0.0,
        "Trk_GHOST_MAX":
        0.4,
        "Trk_PT_MIN":
        600.0,
        "Trk_P_MIN":
        10000.0,
        "VCHI2_D0_MAX":
        4.0,
        "postscale":
        1.0
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

DstarD2hhGammaCalib = {
    "BUILDERTYPE": "DstarD2hhGammaCalib",
    "CONFIG": {
        "CombMassHigh": 2220.0,
        "CombMassHigh_HH": 1820.0,
        "CombMassLow": 1330.0,
        "CombMassLow_HH": 500.0,
        "Daug_TRCHI2DOF_MAX": 3,
        "Dstar_AMDiff_MAX": 180.0,
        "Dstar_MDiff_MAX": 165.0,
        "Dstar_VCHI2VDOF_MAX": 15.0,
        "HighPIDK": -1,
        "Hlt1Filter": None,
        "Hlt1Tos": {
            "Hlt1.*Track.*Decision%TOS": 0
        },
        "Hlt2Filter": None,
        "Hlt2Tos": {
            "Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS": 0,
            "Hlt2PhiIncPhiDecision%TOS": 0
        },
        "LowPIDK": 5,
        "MassHigh": 2200.0,
        "MassHigh_HH": 1810.0,
        "MassLow": 1350.0,
        "MassLow_HH": 600.0,
        "MaxADOCACHI2": 10.0,
        "MaxIPChi2": 15,
        "MaxVCHI2NDOF": 10.0,
        "MaxVCHI2NDOF_HH": 10.0,
        "MinBPVDIRA": 0.995,
        "MinBPVTAU": 0.0001,
        "MinCombPT": 2000.0,
        "MinPT": 2500.0,
        "MinPhotonCL": 0.25,
        "MinTrkIPChi2": 10,
        "MinTrkPT": 500.0,
        "MinVDCHI2_HH": 500.0,
        "MinVDCHI2_HHComb": 300.0,
        "PiSoft_PT_MIN": 250.0,
        "PrescaleDstarD2KKGamma": 1,
        "PrescaleDstarD2KPiGamma": 1,
        "TrChi2": 4,
        "TrGhostProb": 0.5,
        "photonPT": 200.0
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

ElectronRecoEff = {
    "BUILDERTYPE": "StrippingElectronRecoEffLines",
    "CONFIG": {
        "DetachedEEK": {
            "AMTAP": 6000.0,
            "MHIGH": 5700.0,
            "MLOW": 5000.0,
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            },
            "VCHI2TAP": 22,
            "bmass_ip_constraint": -2.5,
            "overlapCut": 0.95,
            "probePcutMax": 150000.0,
            "probePcutMin": 750.0
        },
        "DetachedEEKstar": {
            "AMTAP": 6000.0,
            "MHIGH": 5700.0,
            "MLOW": 5000.0,
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            },
            "VCHI2TAP": 22,
            "bmass_ip_constraint": -2.0,
            "overlapCut": 0.95,
            "probePcutMax": 150000.0,
            "probePcutMin": 750.0
        },
        "DetachedEEPhi": {
            "AMTAP": 6000.0,
            "MHIGH": 5700.0,
            "MLOW": 5000.0,
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            },
            "VCHI2TAP": 22,
            "bmass_ip_constraint": -2.0,
            "overlapCut": 0.95,
            "probePcutMax": 150000.0,
            "probePcutMin": 750.0
        },
        "DetachedEK": {
            "AM": 5500.0,
            "DIRA": 0.95,
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            },
            "VCHI2": 10,
            "VDCHI2": 36,
            "bCandFlightDist": 4.0
        },
        "DetachedEKstar": {
            "AM": 5500.0,
            "DIRA": 0.95,
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            },
            "VCHI2": 10,
            "VDCHI2": 36,
            "bCandFlightDist": 3.0
        },
        "DetachedEPhi": {
            "AM": 5500.0,
            "DIRA": 0.9,
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            },
            "VCHI2": 10,
            "VDCHI2": 36,
            "bCandFlightDist": 3.0
        },
        "DetachedKstar": {
            "AMMAX": 1050.0,
            "AMMIN": 600.0,
            "APTMIN": 500,
            "KST_IPCHI2": 4,
            "K_IPCHI2": 8,
            "K_PROBNNk": 0.1,
            "K_PT": 250.0,
            "MMAX": 1000.0,
            "MMIN": 750.0,
            "Pi_IPCHI2": 8,
            "Pi_PROBNNpi": 0.1,
            "Pi_PT": 250.0,
            "VCHI2": 20
        },
        "DetachedMuK": {
            "AM": 5400.0,
            "DIRA": 0.95,
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            },
            "VCHI2": 10,
            "VDCHI2": 100,
            "bCandFlightDist": 4.0
        },
        "DetachedMuKstar": {
            "AM": 5400.0,
            "DIRA": 0.95,
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            },
            "VCHI2": 10,
            "VDCHI2": 100,
            "bCandFlightDist": 3.0
        },
        "DetachedMuMuK": {
            "AMTAP": 6000.0,
            "MHIGH": 5700.0,
            "MLOW": 5000.0,
            "POINTINGMASSMAX": 7200.0,
            "POINTINGMASSMIN": 3200.0,
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            },
            "VCHI2TAP": 20,
            "bmass_ip_constraint": -2.75,
            "probePcutMax": 150000.0,
            "probePcutMin": 750.0
        },
        "DetachedMuMuKstar": {
            "AMTAP": 6000.0,
            "MHIGH": 5700.0,
            "MLOW": 5000.0,
            "POINTINGMASSMAX": 7000.0,
            "POINTINGMASSMIN": 3500.0,
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            },
            "VCHI2TAP": 20,
            "bmass_ip_constraint": -2.5,
            "probePcutMax": 150000.0,
            "probePcutMin": 750.0
        },
        "DetachedMuMuPhi": {
            "AMTAP": 6000.0,
            "MHIGH": 5700.0,
            "MLOW": 5000.0,
            "POINTINGMASSMAX": 7000.0,
            "POINTINGMASSMIN": 3500.0,
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            },
            "VCHI2TAP": 20,
            "bmass_ip_constraint": -1.0,
            "probePcutMax": 150000.0,
            "probePcutMin": 750.0
        },
        "DetachedMuPhi": {
            "AM": 5400.0,
            "DIRA": 0.95,
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            },
            "VCHI2": 10,
            "VDCHI2": 100,
            "bCandFlightDist": 3.0
        },
        "DetachedPhi": {
            "AMMAX": 1100,
            "AMMIN": 900,
            "K_IPCHI2": 7,
            "K_PIDK": 1,
            "K_PT": 200,
            "MMAX": 1050,
            "MMIN": 980,
            "PHI_IPCHI2": 4,
            "PHI_PT": 400,
            "VCHI2": 100
        },
        "DoVeloDecoding": False,
        "EtaMaxVelo": 5.1,
        "EtaMinVelo": 1.9,
        "Hlt1Req": {
            "DetachedEK": "Hlt1TrackMVA.*Decision",
            "DetachedEKstar": "Hlt1TrackMVA.*Decision",
            "DetachedEPhi": "Hlt1TrackMVA.*Decision",
            "DetachedMuK": "Hlt1Track*MVA.*Decision",
            "DetachedMuKstar": "Hlt1Track*MVA.*Decision",
            "DetachedMuPhi": "Hlt1Track*MVA.*Decision"
        },
        "Hlt2Req": {
            "DetachedEK": "Hlt2Topo(E)?2BodyDecision",
            "DetachedEKstar": "Hlt2Topo(E)?2BodyDecision",
            "DetachedEPhi": "Hlt2Topo(E)?2BodyDecision",
            "DetachedMuK": "Hlt2Topo(Mu)?2BodyDecision",
            "DetachedMuKstar": "Hlt2Topo(Mu)?2BodyDecision",
            "DetachedMuPhi": "Hlt2Topo(Mu)?2BodyDecision"
        },
        "L0Req": {
            "DetachedEK": "L0_CHANNEL_RE('Electron')",
            "DetachedEKstar": "L0_CHANNEL_RE('Electron')",
            "DetachedEPhi": "L0_CHANNEL_RE('Muon|Electron|Hadron')",
            "DetachedMuK": "L0_CHANNEL_RE('Muon|Hadron')",
            "DetachedMuKstar": "L0_CHANNEL_RE('Muon|Hadron')",
            "DetachedMuPhi": "L0_CHANNEL_RE('Muon|Hadron')"
        },
        "L0TOS": {
            "DetachedEK": "L0ElectronDecision",
            "DetachedMuK": "L0MuonDecision"
        },
        "LooseSharedChild": {
            "EtaMaxEle": 5.1,
            "EtaMaxMu": 5.1,
            "EtaMinEle": 1.8,
            "EtaMinMu": 1.8,
            "IPChi2Ele": 8,
            "IPChi2Mu": 8,
            "IPEle": 0.0,
            "IPKaon": 0.0,
            "IPMu": 0.0,
            "IPPion": 0.0,
            "ProbNNe": 0.2,
            "ProbNNk": 0.1,
            "ProbNNmu": 0.2,
            "PtEle": 1000.0,
            "PtKaon": 500.0,
            "PtMu": 1000.0,
            "TrChi2Ele": 5,
            "TrChi2Kaon": 5,
            "TrChi2Mu": 5,
            "TrChi2Pion": 5
        },
        "SharedChild": {
            "EtaMaxEle": 5.1,
            "EtaMaxMu": 5.1,
            "EtaMinEle": 1.8,
            "EtaMinMu": 1.8,
            "IPChi2Ele": 12,
            "IPChi2Kaon": 12,
            "IPChi2Mu": 12,
            "IPChi2Pion": 36,
            "IPEle": 0.0,
            "IPKaon": 0.0,
            "IPMu": 0.0,
            "IPPion": 0.0,
            "ProbNNe": 0.2,
            "ProbNNk": 0.2,
            "ProbNNmu": 0.5,
            "ProbNNpi": 0.8,
            "PtEle": 1200.0,
            "PtKaon": 500.0,
            "PtMu": 1200.0,
            "PtPion": 1000.0,
            "TrChi2Ele": 5,
            "TrChi2Kaon": 5,
            "TrChi2Mu": 5,
            "TrChi2Pion": 5
        },
        "TrackGEC": 150,
        "VeloFitter": "SimplifiedGeometry",
        "VeloMINIP": 0.04,
        "VeloTrackChi2": 5.0
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["Calib"]
}

JPsiForSL = {
    "BUILDERTYPE": "JPsiForSLAllLinesConf",
    "CONFIG": {
        "BCombMassMax": 6000,
        "BCombMassMin": 3400,
        "BDIRAMin": 0.9995,
        "BDocaChi2Max": 10.0,
        "BFDChi2Min": 200.0,
        "BPTMin": 800.0,
        "BVChi2NdofMax": 3.0,
        "MuonChi2": 3.0,
        "MuonGhostProbMax": 0.5,
        "MuonIPChi2": 4.0,
        "MuonP": 6000.0,
        "MuonPT": 1200.0,
        "PrescaleSemiIncJpsi2mumu": 1.0,
        "PsiDIRAMin": 0.99,
        "PsiDocaChi2Max": 10,
        "PsiFDChi2Min": 100,
        "PsiMasswin": 140,
        "PsiMasswinPreFit": 150,
        "PsiPT": 500,
        "PsiVChi2NdofMax": 4.0,
        "TrackChi2": 3.0,
        "TrackGhostProbMax": 0.35,
        "TrackIPChi2": 9.0,
        "TrackP": 3000.0,
        "TrackPT": 500.0,
        "nLongTrackMax": 250
    },
    "STREAMS": ["Semileptonic"],
    "WGs": ["Semileptonic"]
}

K23PiForDownstreamTrackEff = {
    "BUILDERTYPE": "K23PiForDownstreamTrackEffConf",
    "CONFIG": {
        "DipionDira": 0.9995,
        "DipionFlightDistanceChi2": 100,
        "DipionMCorrPseudoErrNarrow": 30,
        "DipionMCorrPseudoErrWide": 100,
        "DipionMaxMCorrNarrow": 600,
        "DipionMaxMCorrWide": 700,
        "DipionMaxMassNarrow": 500,
        "DipionMaxMassWide": 600,
        "DipionMaxVertexZ": 2200.0,
        "DipionMinMassNarrow": 275,
        "DipionMinMassWide": 260,
        "DipionMinP": 3000,
        "DipionMinPT": 300,
        "DipionMinVertexZ": 100.0,
        "DipionOSNarrowDownstreamPrescale": 0.4,
        "DipionOSNarrowLongPrescale": 0.2,
        "DipionOSWidePrescale": 0.05,
        "DipionSSNarrowDownstreamPrescale": 1.0,
        "DipionSSNarrowLongPrescale": 0.2,
        "DipionSSWidePrescale": 0.05,
        "DipionVertexChi2PerDoF": 2,
        "PionIPCHI2": 16.0,
        "PionMinP": 1500,
        "PionMinPT": 125,
        "PionProbNNghost": 0.25,
        "PionProbNNp": 0.1,
        "PionProbNNpi": 0.6,
        "TripionFlightDistanceChi2": 100,
        "TripionMaxMass": 550,
        "TripionMaxVertexZ": 2200,
        "TripionMinMass": 450,
        "TripionMinVertexZ": 0,
        "TripionOSPrescale": 1.0,
        "TripionSSPrescale": 1.0,
        "TripionVertexChi2PerDoF": 5
    },
    "STREAMS": ["BhadronCompleteEvent"],
    "WGs": ["Calib"]
}

LowPTMuID = {
    "BUILDERTYPE": "LowPTMuConf",
    "CONFIG": {
        "DIRA": 0.9995,
        "FDCHI2": 225,
        "MassWindow": 200,
        "Prescale": 1,
        "ProbeIPCHI2": 25,
        "TagIPCHI2": 45,
        "TagPT": 1000,
        "VertexCHI2": 5
    },
    "STREAMS": ["Leptonic"],
    "WGs": ["Calib"]
}

MuIDCalib = {
    "BUILDERTYPE": "MuIDCalibConf",
    "CONFIG": {
        "DetachedNoMIPHiPPrescale": 0.0,
        "DetachedNoMIPKPrescale": 1.0,
        "DetachedNoMIPPrescale": 1,
        "DetachedPrescale": 0.0,
        "FromLambdacPrescale": 1.0,
        "KFromLambdacPrescale": 0.0,
        "KISMUONFromLambdacPrescale": 0.0,
        "PFromLambdacPrescale": 0.0,
        "PISMUONFromLambdacPrescale": 0.0,
        "PiFromLambdacPrescale": 0.0,
        "PiISMUONFromLambdacPrescale": 0.0,
        "PromptPrescale": 0.0
    },
    "STREAMS": {
        "Leptonic": [
            "StrippingMuIDCalib_JpsiFromBNoPIDNoMip",
            "StrippingMuIDCalib_JpsiKFromBNoPIDNoMip",
            "StrippingMuIDCalib_FromLambdacDecay"
        ]
    },
    "WGs": ["Calib"]
}

PIDCalib = {
    "BUILDERTYPE": "PIDCalibLineBuilder",
    "CONFIG": {
        "Bu2Jpsi_eeK": {
            "CheckPV":
            True,
            "CombinationCut":
            "in_range(4.1*GeV,AM,6.1*GeV)",
            "DaughtersCuts": {
                "J/psi(1S)": "(BPVDLS > 5)",
                "K+": "( PT > 1.0*GeV ) & (BPVIPCHI2()> 9.0) & ( PIDK > 0 )",
                "e+":
                "( P  > 3*GeV )   & ( PT > 500*MeV ) & (BPVIPCHI2()> 9.0) "
            },
            "DecayDescriptor":
            "[B+ -> J/psi(1S) K+]cc",
            "DecayDescriptorJpsi":
            "J/psi(1S) -> e+ e-",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": ["Phys/StdTightKaons/Particles"],
            "InputTESJpsi": ["Phys/StdNoPIDsElectrons/Particles"],
            "JpsiCombinationCut":
            "(in_range(2100, AM, 4300))",
            "JpsiMotherCut":
            "(VFASPF(VCHI2)< 9.0) & (in_range(2200.0 *MeV, MM, 4200.0 *MeV)) ",
            "MDST.DST":
            False,
            "MotherCut":
            "(VFASPF(VCHI2PDOF)<9) & in_range(4.2*GeV,M, 6.0*GeV)",
            "Prescale":
            1.0,
            "RawEvent": ["Muon"],
            "RefitPV":
            False,
            "TagAndProbeCut":
            "(ACHILD(PIDe, #tag) > 5.0) & (ACHILD(PT, #tag) > 1500) & (ACHILD(P, #tag) > 6*GeV) "
        },
        "Bu2Jpsi_mumuK": {
            "CheckPV":
            True,
            "CombinationCut":
            "ADAMASS('B+') < 500.*MeV",
            "DaughtersCuts": {
                "K+": "(MIPCHI2DV(PRIMARY)>25)",
                "mu+": "(BPVIPCHI2()> 10.0) "
            },
            "DecayDescriptor":
            "[B+ -> J/psi(1S) K+]cc",
            "DecayDescriptorJpsi":
            "J/psi(1S) -> mu+ mu-",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": ["Phys/StdNoPIDsKaons/Particles"],
            "InputTESJpsi": ["Phys/StdNoPIDsMuons/Particles"],
            "JpsiCombinationCut":
            "(ADAMASS('J/psi(1S)')<200*MeV)",
            "JpsiMotherCut":
            "(VFASPF(VCHI2/VDOF)<5) & (BPVVDCHI2 > 225) ",
            "MDST.DST":
            False,
            "MotherCut":
            "(BPVIPCHI2()<25)",
            "Prescale":
            1.0,
            "RawEvent": ["Calo"],
            "RefitPV":
            False,
            "TagAndProbeCut":
            "(ACHILDCUT(ISMUON, #tag)) & (ACHILD(P, #tag) > 6*GeV) & (ACHILD(PT, #tag) > 1.5*GeV) & (ACHILD(MIPCHI2DV(PRIMARY), #tag) > 25)"
        },
        "Bu2KMuMu": {
            "CheckPV":
            True,
            "Combination12Cut":
            "(in_range(3096-200, AM, 3096+200)) & (ACHI2DOCA(1,2) < 100)",
            "CombinationCut":
            "(in_range(5279-500, AM, 5279+500)) & (ACHI2DOCA(1,3) < 100) & (ACHI2DOCA(2,3) < 100)",
            "DaughtersCuts": {
                "K+": "(MIPCHI2DV(PRIMARY)>25)",
                "mu+": "(MIPCHI2DV(PRIMARY)>10)"
            },
            "DecayDescriptor":
            "[B+ -> mu+ mu- K+]cc",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES":
            ["Phys/StdNoPIDsKaons/Particles", "Phys/StdNoPIDsMuons/Particles"],
            "MDST.DST":
            False,
            "MotherCut":
            "(VFASPF(VCHI2)<100) & (BPVVDCHI2 > 25) & (BPVIPCHI2()<25)",
            "Prescale":
            1.0,
            "RawEvent": ["Muon"],
            "RefitPV":
            False,
            "TagAndProbeCut":
            "(ACHILDCUT(ISMUON, #tag)) & (ACHILD(P, #tag) > 6*GeV) & (ACHILD(PT, #tag) > 1.5*GeV) & (ACHILD(MIPCHI2DV(PRIMARY), #tag) > 25)",
            "TagAndProbeIndices": [1, 2]
        },
        "Bu2Kee": {
            "CheckPV":
            True,
            "Combination12Cut":
            "(in_range(2100, AM, 4300)) & (ACHI2DOCA(1,2) < 9)",
            "CombinationCut":
            "(in_range(5279-1200, AM, 5279+1000)) & (ACHI2DOCA(1,3) < 18) & (ACHI2DOCA(2,3) < 18)",
            "DaughtersCuts": {
                "K+": "( PT > 1.0*GeV ) & (BPVIPCHI2()> 9.0) & (PIDK > 0)",
                "e+":
                "( P  > 3*GeV )   & ( PT > 500*MeV ) & (BPVIPCHI2()> 9.0) "
            },
            "DecayDescriptor":
            "[B+ -> e+ e- K+]cc",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": [
                "Phys/StdTightKaons/Particles",
                "Phys/StdNoPIDsElectrons/Particles"
            ],
            "MDST.DST":
            False,
            "MotherCut":
            "(VFASPF(VCHI2)<18) & (BPVVDCHI2 > 25)",
            "Prescale":
            1.0,
            "RawEvent": ["Calo"],
            "RefitPV":
            False,
            "TagAndProbeCut":
            "(ACHILD(PIDe, #tag) > 5.0) & (ACHILD(PT, #tag) > 1500) & (ACHILD(P, #tag) > 6*GeV) ",
            "TagAndProbeIndices": [1, 2]
        },
        "Jpsi2MuMu": {
            "CheckPV":
            True,
            "CombinationCut":
            "(in_range(3096-200, AM, 3096+200)) & (ACHI2DOCA(1,2) < 10)",
            "DaughtersCuts": {
                "mu+":
                "(ISLONG) & (P>3*GeV) & (PT>800*MeV) & (MIPCHI2DV(PRIMARY)>10) & (TRCHI2DOF<3)"
            },
            "DecayDescriptor":
            "J/psi(1S) -> mu+ mu-",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": ["Phys/StdNoPIDsMuons/Particles"],
            "MDST.DST":
            False,
            "MotherCut":
            "(VFASPF(VCHI2)<5) & (BPVVDCHI2 > 225)",
            "Prescale":
            1.0,
            "RawEvent": ["Muon"],
            "RefitPV":
            False,
            "TagAndProbeCut":
            "(ACHILDCUT(ISMUON, #tag)) & (ACHILD(P, #tag) > 6*GeV) & (ACHILD(PT, #tag) > 1.5*GeV) & (ACHILD(MIPCHI2DV(PRIMARY), #tag) > 25)",
            "TagAndProbeIndices": [1, 2]
        },
        "Jpsi2ee": {
            "CheckPV":
            True,
            "CombinationCut":
            "(APT > 1.2*GeV) & (in_range(2100, AM, 4300)) & (ACHI2DOCA(1,2) < 18)",
            "DaughtersCuts": {
                "e+": "( P > 3*GeV ) & ( PT > 500*MeV ) & (BPVIPCHI2()> 9.0)  "
            },
            "DecayDescriptor":
            "J/psi(1S) -> e+ e-",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": ["Phys/StdNoPIDsElectrons/Particles"],
            "MDST.DST":
            False,
            "MotherCut":
            "(VFASPF(VCHI2)<9) & (PT > 2*GeV) & (BPVVDCHI2 > 50)",
            "Prescale":
            1.0,
            "RawEvent": ["Calo"],
            "RefitPV":
            False,
            "TagAndProbeCut":
            "(ACHILD(PIDe, #tag) > 5.0) & (ACHILD(PT, #tag) > 1500*MeV) & (ACHILD(P, #tag) > 6*GeV) ",
            "TagAndProbeIndices": [1, 2]
        },
        "Ks02pipi": {
            "CheckPV":
            True,
            "CombinationCut":
            " ( ADAMASS('KS0') < 100 ) & (ACHI2DOCA(1,2) < 20 )",
            "DaughtersCuts": {
                "pi+": " ( 2.0 * GeV < P ) & ( MIPCHI2DV(PRIMARY) > 25 )"
            },
            "DecayDescriptor":
            "KS0 -> pi+ pi-",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": ["Phys/StdNoPIDsPions/Particles"],
            "MDST.DST":
            False,
            "MotherCut":
            "( ADMASS ( 'KS0') < 50 ) &  in_range ( 0 , VFASPF ( VCHI2 ) , 16 ) &  ( VFASPF ( VZ ) < 2200 ) &  (BPVVDCHI2 > 25) &  ( ADWM( 'Lambda0' , WM( 'p+' , 'pi-') ) > 9 ) &  ( ADWM( 'Lambda0' , WM( 'pi+' , 'p~-') ) > 9 )",
            "Prescale":
            0.024,
            "RawEvent": ["Muon"],
            "RefitPV":
            False
        },
        "Ks02pipiDD": {
            "CheckPV":
            True,
            "CloneLine":
            "Ks02pipi",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": ["Phys/StdNoPIDsDownPions/Particles"],
            "MDST.DST":
            False,
            "MotherCut":
            "( ADMASS ( 'KS0') < 50 ) &  in_range ( 0 , VFASPF ( VCHI2 ) , 16 ) &  ( VFASPF ( VZ ) < 2200 ) &  (BPVVDCHI2 > 25) &  ( ADWM( 'Lambda0' , WM( 'p+' , 'pi-') ) > 18 ) &  ( ADWM( 'Lambda0' , WM( 'pi+' , 'p~-') ) > 18 )",
            "Prescale":
            0.024,
            "RawEvent": ["Muon"],
            "RefitPV":
            False
        },
        "L02ppi": {
            "CheckPV":
            True,
            "CombinationCut":
            "(AM < 1.5 * GeV) & (ACHI2DOCA(1,2) < 20)",
            "DaughtersCuts": {
                "p+":
                "( 2.0 * GeV < P ) &  ( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 35 )",
                "pi-":
                "( 2.0 * GeV < P ) &  ( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 35 )"
            },
            "DecayDescriptor":
            "[Lambda0 -> p+ pi-]cc",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": [
                "Phys/StdAllNoPIDsPions/Particles",
                "Phys/StdAllNoPIDsProtons/Particles"
            ],
            "MDST.DST":
            False,
            "MotherCut":
            "   ( ADMASS ( 'Lambda0') < 25 )  & ( in_range ( 0 , VFASPF ( VCHI2 ) , 16 ) ) & ( VFASPF ( VZ ) < 2200 )  & ( in_range ( 0 , BPVLTFITCHI2() , 49 ) ) & ( BPVLTIME() * c_light > 5  )  & ( ADWM( 'KS0' , WM( 'pi+' , 'pi-') ) > 20 )",
            "Prescale":
            0.14,
            "RawEvent": ["Muon"],
            "RefitPV":
            False
        },
        "L02ppiDD": {
            "CloneLine":
            "L02ppi",
            "InputTES": [
                "Phys/StdNoPIDsDownPions/Particles",
                "Phys/StdNoPIDsDownProtons/Particles"
            ],
            "Prescale":
            0.14
        },
        "L02ppiDDHighPT": {
            "CloneLine":
            "L02ppiHighPT",
            "InputTES": [
                "Phys/StdNoPIDsDownPions/Particles",
                "Phys/StdNoPIDsDownProtons/Particles"
            ],
            "Prescale":
            1.0
        },
        "L02ppiDDVeryHighPT": {
            "CloneLine":
            "L02ppiVeryHighPT",
            "InputTES": [
                "Phys/StdNoPIDsDownPions/Particles",
                "Phys/StdNoPIDsDownProtons/Particles"
            ],
            "Prescale":
            1.0
        },
        "L02ppiHighPT": {
            "CloneLine": "L02ppi",
            "DaughtersCuts": {
                "p+":
                "(PT > 3.5*GeV) & ( 2.0*GeV < P ) &( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 25 )",
                "pi-":
                "( 2.0*GeV < P ) &( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 25 )"
            },
            "Prescale": 1.0
        },
        "L02ppiIsMuon": {
            "CloneLine": "L02ppi",
            "DaughtersCuts": {
                "p+":
                "(ISMUON) & ( 2.0*GeV < P ) &( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 25 )",
                "pi-":
                "( 2.0*GeV < P ) &( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 25 )"
            },
            "Prescale": 1.0
        },
        "L02ppiVeryHighPT": {
            "CloneLine": "L02ppi",
            "DaughtersCuts": {
                "p+":
                "(PT > 10*GeV) & ( 2.0*GeV < P ) &( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 25 )",
                "pi-":
                "( 2.0*GeV < P ) &( TRCHI2DOF < 5 ) & ( MIPCHI2DV(PRIMARY) > 25 )"
            },
            "Prescale": 1.0
        },
        "Lc2pKpi": {
            "CheckPV":
            True,
            "Combination12Cut":
            "( ACHI2DOCA(1,2) < 25 )",
            "CombinationCut":
            "( ADAMASS('Lambda_c+')<150*MeV ) & ( APT>1.*GeV ) & ( ADOCACHI2CUT(50, '') )",
            "DaughtersCuts": {
                "K+":
                "( PT>250*MeV ) & ( P>2*GeV ) & ( TRPCHI2>0.0001 ) & ( MIPCHI2DV(PRIMARY)>8. ) ",
                "p+":
                "( PT>250*MeV ) & ( P>2*GeV ) & ( TRPCHI2>0.0001 ) & ( MIPCHI2DV(PRIMARY)>8. ) ",
                "pi+":
                "( PT>250*MeV ) & ( P>2*GeV ) & ( TRPCHI2>0.0001 ) & ( MIPCHI2DV(PRIMARY)>8. ) "
            },
            "DecayDescriptor":
            "[Lambda_c+ -> K- p+ pi+]cc",
            "HLT1":
            "HLT_PASS_RE('Hlt1.*Decision')",
            "HLT2":
            "HLT_PASS_RE('Hlt2.*Decision')",
            "InputTES": [
                "Phys/StdNoPIDsKaons/Particles",
                "Phys/StdNoPIDsPions/Particles",
                "Phys/StdNoPIDsProtons/Particles"
            ],
            "MDST.DST":
            False,
            "MotherCut":
            "( M > 2.240*GeV ) &  ( M<2.330*GeV ) &  ( VFASPF(VCHI2/VDOF)<8 ) &  ( BPVDIRA>0.99999 ) &  ( MIPCHI2DV(PRIMARY)<4. ) &  in_range( 0.85*GeV, M13, 0.95*GeV ) &  ( (WM( 'K-' , 'pi+' , 'pi+' )>1.89*GeV) | (WM( 'K-' , 'pi+' , 'pi+' )<1.80*GeV) )",
            "Prescale":
            1.0,
            "RawEvent": ["Muon"],
            "RefitPV":
            False
        }
    },
    "STREAMS": {
        "Leptonic": ["StrippingPIDCalibBu2KeeLine"]
    },
    "WGs": ["Calib"]
}

SigmacForPID = {
    "BUILDERTYPE": "SigmacForPIDConf",
    "CONFIG": {
        "BasicCuts":
        " & ( 9 < MIPCHI2DV() ) ",
        "CheckPV":
        True,
        "KaonCuts":
        " & ( 2 < PIDK  - PIDpi ) & ( -5 < PIDK  - PIDp ) ",
        "Monitor":
        False,
        "PionCuts":
        " & ( 2 < PIDpi - PIDK  ) & ( -5 < PIDpi - PIDp  ) ",
        "Preambulo": [
            "chi2vx = VFASPF(VCHI2) ",
            "from GaudiKernel.PhysicalConstants import c_light",
            "ctau_9   = BPVLTIME (   9 ) * c_light "
        ],
        "SigmaCPrescale":
        1.0,
        "SlowPionCuts":
        " TRCHI2DOF < 5 ",
        "TrackCuts":
        "\n    ( TRCHI2DOF < 4     ) &\n    ( PT > 250 * MeV    ) &\n    ( TRGHOSTPROB < 0.5 ) & \n    in_range  ( 2 , ETA , 5 )\n    ",
        "pT(Lc+)":
        3000.0
    },
    "STREAMS": ["Charm"],
    "WGs": ["Calib"]
}

TrackEffD0ToK3Pi = {
    "BUILDERTYPE": "TrackEffD0ToK3PiAllLinesConf",
    "CONFIG": {
        "D0_MAX_DOCA": 0.05,
        "D0_MIN_FD": 5.0,
        "DoVeloDecoding": False,
        "Dst_MAX_DTFCHI2": 3.0,
        "Dst_MAX_M": 2035.0000000000002,
        "HLT2": "HLT_PASS_RE('Hlt2.*CharmHad.*HHX.*Decision')",
        "Kaon_MIN_PIDK": 7,
        "LC_MIN_FD": 2.0,
        "Pion_MAX_PIDK": 4,
        "RequireDstFirst": False,
        "Sc_MAX_DTFCHI2": 3.0,
        "Sc_MAX_M": 2500.0,
        "TTSpecs": {
            "Hlt2.*CharmHad.*HHX.*Decision%TOS": 0
        },
        "VeloFitter": "SimplifiedGeometry",
        "VeloLineForTiming": False,
        "VeloMINIP": 0.05
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

TrackEffD0ToKPi = {
    "BUILDERTYPE": "TrackEffD0ToKPiAllLinesConf",
    "CONFIG": {
        "Dst_DTFCHI2_MAX": 10,
        "Dst_M_MAX": 2100,
        "HLT1": "HLT_PASS_RE('Hlt1TrackMVADecision')",
        "HLT2": "HLT_PASS_RE('Hlt2TrackEff_D0.*Decision')",
        "Kaon_MIN_PIDK": 0,
        "Monitor": False,
        "Pion_MAX_PIDK": 20,
        "TTSpecs": {
            "Hlt1TrackMVADecision%TOS": 0,
            "Hlt2TrackEff_D0.*Decision%TOS": 0
        },
        "Tag_MIN_PT": 1000.0,
        "VeloMINIPCHI2": 4.0
    },
    "STREAMS": ["CharmCompleteEvent"],
    "WGs": ["Calib"]
}

TrackEffDownMuon = {
    "BUILDERTYPE": "StrippingTrackEffDownMuonConf",
    "CONFIG": {
        "DataType": "2011",
        "HLT1PassOnAll": True,
        "HLT1TisTosSpecs": {
            "Hlt1SingleMuonNoIPDecision%TOS": 0,
            "Hlt1TrackMuonDecision%TOS": 0
        },
        "HLT2PassOnAll": False,
        "HLT2TisTosSpecs": {
            "Hlt2SingleMuon.*Decision%TOS": 0,
            "Hlt2TrackEffDiMuonDownstream.*Decision%TOS": 0
        },
        "JpsiDoca": 5.0,
        "JpsiHlt1Filter": "Hlt1.*Decision",
        "JpsiHlt2Filter": "Hlt2.*Decision",
        "JpsiLinePostscale": 1.0,
        "JpsiLinePrescale": 1.0,
        "JpsiMassPostComb": 200.0,
        "JpsiMassPreComb": 2000.0,
        "JpsiProbeP": 5.0,
        "JpsiProbePt": 0.5,
        "JpsiProbeTrackChi2": 10.0,
        "JpsiTagMinIP": 0.5,
        "JpsiTagP": 5.0,
        "JpsiTagPID": -2.0,
        "JpsiTagPt": 0.7,
        "JpsiTagTrackChi2": 10.0,
        "JpsiVertexChi2": 5.0,
        "MuMom": 2.0,
        "MuTMom": 0.2,
        "NominalLinePostscale": 1.0,
        "NominalLinePrescale": 0.5,
        "SeedingMinP": 1500.0,
        "TrChi2": 10.0,
        "UpsilonHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        },
        "UpsilonHLT2TisTosSpecs": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        },
        "UpsilonLinePostscale": 1.0,
        "UpsilonLinePrescale": 1.0,
        "UpsilonMassPostComb": 1500.0,
        "UpsilonMassPreComb": 100000.0,
        "UpsilonMuMom": 0.0,
        "UpsilonMuTMom": 0.5,
        "ValidationLinePostscale": 1.0,
        "ValidationLinePrescale": 0.0015,
        "ZHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        },
        "ZHLT2TisTosSpecs": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        },
        "ZLinePostscale": 1.0,
        "ZLinePrescale": 1.0,
        "ZMassPostComb": 1500.0,
        "ZMassPreComb": 100000.0,
        "ZMuMaxEta": 4.5,
        "ZMuMinEta": 2.0,
        "ZMuMom": 0.0,
        "ZMuTMom": 20.0
    },
    "STREAMS": {
        "Dimuon": [
            "StrippingTrackEffDownMuonNominalLine",
            "StrippingTrackEffDownMuonValidationLine",
            "StrippingTrackEffDownMuonLine1", "StrippingTrackEffDownMuonLine2",
            "StrippingTrackEffDownMuonUpsilonLine"
        ]
    },
    "WGs": ["Calib"]
}

TrackEffMuonTT = {
    "BUILDERTYPE": "StrippingTrackEffMuonTTConf",
    "CONFIG": {
        "BJpsiKHlt2TriggersTOS": {
            "Hlt2TopoMu2BodyDecision%TOS": 0
        },
        "BJpsiKHlt2TriggersTUS": {
            "Hlt2TopoMu2BodyDecision%TUS": 0
        },
        "BJpsiKPrescale": 1.0,
        "BJpsiVertexChi2": 5,
        "BMassWin": 500,
        "Hlt1PassOnAll": True,
        "Hlt2PassOnAllLowMult": False,
        "JpsiHlt1Filter": "Hlt1.*Decision",
        "JpsiHlt1Triggers": {
            "Hlt1TrackMuonDecision%TOS": 0
        },
        "JpsiHlt2Filter": "Hlt2.*Decision",
        "JpsiHlt2Triggers": {
            "Hlt2SingleMuon.*Decision%TOS": 0,
            "Hlt2TrackEffDiMuonMuonTT.*Decision%TOS": 0
        },
        "JpsiLowMultHlt1Filter": "Hlt1.*Decision",
        "JpsiLowMultHlt1Triggers": {
            "Hlt1LowMultMuonDecision%TOS": 0
        },
        "JpsiLowMultHlt2Filter": "Hlt2.*Decision",
        "JpsiLowMultHlt2Triggers": {
            "Hlt2LowMultMuonDecision%TOS": 0
        },
        "JpsiLowMultMassWin": 500.0,
        "JpsiLowMultPrescale": 1.0,
        "JpsiLowMultProbePt": 0.5,
        "JpsiLowMultPt": 0.0,
        "JpsiLowMultTagP": 0.0,
        "JpsiLowMultTagPt": 0.5,
        "JpsiLowMultTagTrackChi2": 5.0,
        "JpsiMassWin": 500.0,
        "JpsiMinIP": 0.8,
        "JpsiPrescale": 1.0,
        "JpsiProbeP": 5.0,
        "JpsiProbePt": 0.5,
        "JpsiPt": 1.0,
        "JpsiTagP": 10.0,
        "JpsiTagPID": -2.0,
        "JpsiTagPt": 1.3,
        "JpsiTagTrackChi2": 5.0,
        "JpsiVertexChi2": 2.0,
        "Postscale": 1.0,
        "UpsilonHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        },
        "UpsilonHlt2Triggers": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        },
        "UpsilonMassWin": 1500,
        "UpsilonPrescale": 1.0,
        "UpsilonProbePt": 0.5,
        "UpsilonTagPID": 2.0,
        "UpsilonTagPt": 1.0,
        "UpsilonVertexChi2": 5.0,
        "ZHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        },
        "ZHlt2Triggers": {
            "Hlt2EWSingleMuonVHighPtDecision%TOS": 0
        },
        "ZMassWin": 40000,
        "ZPrescale": 1.0,
        "ZProbePt": 0.5,
        "ZPt": 0.0,
        "ZTagPID": 2.0,
        "ZTagPt": 10.0,
        "ZVertexChi2": 5.0
    },
    "STREAMS": {
        "Dimuon": [
            "StrippingTrackEffMuonTT_JpsiLine1",
            "StrippingTrackEffMuonTT_JpsiLine2",
            "StrippingTrackEffMuonTT_JpsiLowMultLine1",
            "StrippingTrackEffMuonTT_JpsiLowMultLine2",
            "StrippingTrackEffMuonTT_UpsilonLine1",
            "StrippingTrackEffMuonTT_UpsilonLine2",
            "StrippingTrackEffMuonTT_ZLine1", "StrippingTrackEffMuonTT_ZLine2",
            "StrippingTrackEffMuonTT_BJpsiKLine1",
            "StrippingTrackEffMuonTT_BJpsiKLine2"
        ]
    },
    "WGs": ["Calib"]
}

TrackEffVeloMuon = {
    "BUILDERTYPE": "StrippingTrackEffVeloMuonConf",
    "CONFIG": {
      'Jpsi': {"ResonanceName":   "J/psi(1S)"
              ,   "DecayDescriptor": "J/psi(1S) -> mu+ mu-"
              ,   "CombPT" :             0.5 # GeV
              ,   "TrP" :            5. # GeV
              ,   "TrPT":            0.5 # GeV
              ,   "TrChi2":          5.0 # adimensional
              ,   "LongP":           7. # GeV
              ,   "LongPT":          0.5 # GeV
              ,   "LongChi2":        3.0 
              ,   "LongMinIP":       0.2 # mm
              ,   "VertChi2":        2.0
              ,   "MuDLL"   :        -1.
              ,   "MassPreComb":     1000. # MeV
              ,   "MassPostComb":    500. # MeV
              ,   "Prescale":        1
              ,   "Postscale":       1
              ,   "Hlt1Filter":      'Hlt1.*Decision'
              ,   "Hlt2Filter":      'Hlt2.*Decision'
              ,   "HLT1TisTosSpecs": { "Hlt1TrackMuonDecision%TOS" : 0, "Hlt1SingleMuonNoIPDecision%TOS" : 0 }
              ,   "HLT1PassOnAll":   True
              ,   "HLT2TisTosSpecs": { "Hlt2SingleMuon.*Decision%TOS" : 0, "Hlt2TrackEffDiMuonVeloMuon.*Decision%TOS" : 0 }
              ,   "HLT2PassOnAll":   False
              ,   "vemucut"      :   "(TRCHI2DOF < %(TrChi2)s) & (PT > %(TrPT)s*GeV) & (P > %(TrP)s*GeV)"
              ,   "longcut"      :   "(TRCHI2DOF < %(LongChi2)s) & (P > %(LongP)s*GeV) & (PIDmu > %(MuDLL)s) & (MIPDV(PRIMARY)>%(LongMinIP)s*mm)"
              ,   "resonancecut" :   "(VFASPF(VCHI2/VDOF) < %(VertChi2)s) & (PT > %(CombPT)s*GeV)"
              ,   "linename"     :   "Line"
              },
        "Upsilon": { "ResonanceName":   "Upsilon(1S)"
                  ,   "DecayDescriptor": "Upsilon(1S) -> mu+ mu-"
                  ,   "CombPT" :             0.5 # GeV
                  ,   "TrP" :            0. # GeV
                  ,   "TrPT":            0.5 # GeV
                  ,   "TrChi2":          5.
                  ,   "LongP":           7. # GeV
                  ,   "LongPT":          0.5 # GeV
                  ,   "LongChi2":        3.
                  ,   "VertChi2":        10000.
                  ,   "MuDLL"   :        1.
                  ,   "MassPreComb":     100000. # MeV
                  ,   "MassPostComb":    1500. # MeV
                  ,   "Prescale":        1
                  ,   "Postscale":       1
                  ,   "Hlt1Filter":      'Hlt1.*Decision'
                  ,   "Hlt2Filter":      'Hlt2.*Decision'
                  ,   "HLT1TisTosSpecs": { "Hlt1SingleMuonHighPTDecision%TOS" : 0 }
                  ,   "HLT1PassOnAll":   True
                  ,   "HLT2TisTosSpecs": { "Hlt2SingleMuonLowPTDecision%TOS" : 0 }
                  ,   "HLT2PassOnAll":   False
                  ,   "vemucut"      :   "((PT > %(TrPT)s*GeV))"
                  ,   "longcut"      :   "((PT > %(LongPT)s*GeV))"
                  ,   "resonancecut" :   "(PT > %(CombPT)s*GeV)"
                  ,   "linename"     :   "UpsilonLine"
                      },
        "Z": {    "ResonanceName":   "Z0"
               ,   "DecayDescriptor": "Z0 -> mu+ mu-"
               ,   "CombPT" :             0.5 # GeV
               ,   "TrP" :            0. # GeV
               ,   "TrPT":            20. # GeV
               ,   "TrChi2":          5.
               ,   "LongP":           7. # GeV
               ,   "LongPT":          0.5 # GeV
               ,   "LongChi2":        3.
               ,   "TrMinEta":        2.0
               ,   "TrMaxEta":        4.5
               ,   "LongMinEta":        2.0
               ,   "LongMaxEta":        4.5
               ,   "VertChi2":        10000. 
               ,   "MuDLL"   :        1.
               ,   "MassPreComb":     100000. # MeV
               ,   "MassPostComb":    40000. # MeV
               ,   "Prescale":        1
               ,   "Postscale":       1
               ,   "Hlt1Filter":      'Hlt1.*Decision'
               ,   "Hlt2Filter":      'Hlt2.*Decision'
               ,   "HLT1TisTosSpecs": { "Hlt1SingleMuonHighPTDecision%TOS" : 0 }
               ,   "HLT1PassOnAll":   True
               ,   "HLT2TisTosSpecs": { "Hlt2SingleMuonHighPTDecision%TOS" : 0 }
               ,   "HLT2PassOnAll":   False
               ,   "vemucut"      :   "((PT > %(TrPT)s*GeV) & (ETA > %(TrMinEta)s) & (ETA < %(TrMaxEta)s) )"
               ,   "longcut"      :   "((PT > %(LongPT)s*GeV) & (ETA > %(LongMinEta)s) & (ETA < %(LongMaxEta)s) )"
               ,   "resonancecut" :   "(PT > %(CombPT)s*GeV)"
               ,   "linename"     :   "ZLine"
               } 
       
    },
    "STREAMS": {
        "Dimuon": [
            "StrippingTrackEffVeloMuonLine1",
            "StrippingTrackEffVeloMuonLine2",
            "StrippingTrackEffVeloMuonZLine1",
            "StrippingTrackEffVeloMuonZLine2",
            "StrippingTrackEffVeloMuonUpsilonLine1",
            "StrippingTrackEffVeloMuonUpsilonLine2"
        ]
    },
    "WGs": ["Calib"]
}

VznoPID = {
    "BUILDERTYPE": "StrippingV0ForPIDConf",
    "CONFIG": {
        "CTauK0S":
        1.0,
        "CTauK0S_DD":
        10.0,
        "CTauLambda0":
        5.0,
        "CTauLambda0_DD":
        20.0,
        "DaughtersIPChi2":
        25,
        "DeltaMassK0S":
        50.0,
        "DeltaMassLambda":
        25.0,
        "HLT":
        "HLT_PASS_RE('Hlt1MB.*Decision')",
        "KS0DD_Prescale":
        0.024,
        "KS0LL_Prescale":
        0.02,
        "LTimeFitChi2":
        49,
        "LamDDIsMUON_Prescale":
        1.0,
        "LamDD_Prescale":
        0.1,
        "LamLLIsMUON_Prescale_HiP":
        1.0,
        "LamLLIsMUON_Prescale_LoP":
        1.0,
        "LamLL_Prescale_HiP":
        1.0,
        "LamLL_Prescale_LoP":
        0.14,
        "MaxZ":
        2200.0,
        "Monitor":
        False,
        "Preambulo": [
            "from GaudiKernel.PhysicalConstants import c_light",
            "DD =    CHILDCUT ( ISDOWN , 1 ) & CHILDCUT ( ISDOWN , 2 ) ",
            "LL =    CHILDCUT ( ISLONG , 1 ) & CHILDCUT ( ISLONG , 2 ) "
        ],
        "Proton_IsMUONCut":
        "(INTREE( (ABSID=='p+') & ISMUON ) )",
        "TrackChi2":
        5,
        "VertexChi2":
        16,
        "WrongMassK0S":
        9.0,
        "WrongMassK0S_DD":
        18.0,
        "WrongMassLambda":
        20.0,
        "WrongMassLambda_DD":
        40.0
    },
    "STREAMS": ["Charm"],
    "WGs": ["Calib"]
}

noPIDDstar = {
    "BUILDERTYPE": "NoPIDDstarWithD02RSKPiConf",
    "CONFIG": {
        "D0BPVDira": 0.9999,
        "D0FDChi2": 49,
        "D0IPChi2": 30,
        "D0MassWin": 75.0,
        "D0Pt": 1500.0,
        "D0VtxChi2Ndof": 13,
        "DCS_WrongMass": 25.0,
        "DaugIPChi2": 16,
        "DaugP": 2000.0,
        "DaugPt": 250.0,
        "DaugTrkChi2": 5,
        "DeltaM_Max": 155.0,
        "DeltaM_Min": 130.0,
        "DstarPt": 2200.0,
        "DstarVtxChi2Ndof": 13,
        "KK_WrongMass": 25.0,
        "Monitor": False,
        "PiPi_WrongMass": 25.0,
        "Postscale": 1.0,
        "Prescale": 0.8,
        "SlowPiPt": 150.0,
        "SlowPiTrkChi2": 5
    },
    "STREAMS": ["Charm"],
    "WGs": ["Calib"]
}
