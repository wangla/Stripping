###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  34r0p2                         ##
##                                                                            ##
##  Configuration for Charm WG                                                ##
##  Contact person: Shantam Taneja   (shantam.taneja@cern.ch)                 ##
################################################################################

from GaudiKernel.SystemOfUnits import *

######################################################################
## StrippingBs2st2pKpipipiLine (DST.DST)
## StrippingBs2st2pKpipipiSSLine (DST.DST)
## StrippingBs2st2pKpipipiWSLine (DST.DST)
## StrippingBs2st2pKpipipiWSSSLine (DST.DST)

######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingBs2st2Kpipipi.py
## Authors: Matt Rudolph
######################################################################

Bs2st2pKpipipi = {
    "BUILDERTYPE": "Bs2st2pKpipipiConf",
    "CONFIG": {
        "BK_MaxDM": 700.0,
        "BK_MaxPVDZ": 1.0,
        "BK_MinPT": 50.0,
        "B_MaxDOCAChi2": 20.0,
        "B_MaxM_4body": 3000.0,
        "B_MaxVtxChi2": 3.0,
        "B_MinDira": 0.999,
        "B_MinM_4body": 2400.0,
        "B_MinPT_4body": 2000.0,
        "B_MinSumPT_4body": 4000.0,
        "B_MinVDChi2": 120.0,
        "Km_MaxIPChi2": 4,
        "Km_MaxPperp": 315.0,
        "Km_MinPT": 600,
        "Km_MinPperp": 70.0,
        "Km_MinProbNNk": 0.2,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "Trk_MaxChi2Ndof": 4.0,
        "Trk_MaxGhostProb": 0.4,
        "Trk_MinIPChi2": 16.0,
        "Trk_MinP": 1000.0,
        "Trk_MinProbNNp": 0.1,
        "Trk_MinProbNNpi": 0.1
    },
    "STREAMS": [ "CharmCompleteEvent" ],
    "WGs": [ "Charm" ]
}

######################################################################
## Secondary Charm decays from semiloptin B-decays

## -------------------------------------------------------------------
## Lines defined in StrippingCharmFromBSemi.py
## Authors: Tomasso Pajero + others from previous campaigns
######################################################################


CharmFromBSemi = {
    "BUILDERTYPE": "CharmFromBSemiAllLinesConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BVCHI2DOF": 6.0, 
        "CombMassHigh_HH": 1820, 
        "CombMassLow_HH": 0, 
        "D02HHGammaAMassWin": 220, 
        "D02HHGammaMassWin": 210, 
        "D02HHHHPrescale": 1, 
        "D02HHPi0AMassWin": 220, 
        "D02HHPi0DocaCut": 6.0, 
        "D02HHPi0MassWin": 210, 
        "D02KSHHPi0AMassWin": 260, 
        "D02KSHHPi0MassWin": 230, 
        "D02KSHHPi0_D0PT": 1000, 
        "D02KSHHPi0_D0PTComb": 1000, 
        "D02KSHHPi0_PTSUMLoose": 1000, 
        "D0decaysPrescale": 1, 
        "D0radiativePrescale": 1, 
        "DDocaChi2Max": 20, 
        "DZ": 0, 
        "Ds2HHHHPrescale": 1, 
        "DsAMassWin": 100.0, 
        "DsDIRA": 0.99, 
        "DsFDCHI2": 100.0, 
        "DsIP": 7.4, 
        "DsMassWin": 80.0, 
        "DsVCHI2DOF": 6.0, 
        "DsdecaysPrescale": 1, 
        "Dstar_Chi2": 8.0, 
        "Dstar_SoftPion_PIDe": 2.0, 
        "Dstar_SoftPion_PT": 80.0, 
        "Dstar_wideDMCutLower": 0.0, 
        "Dstar_wideDMCutUpper": 170.0, 
        "Dto4hADocaChi2Max": 7, 
        "Dto4h_AMassWin": 65.0, 
        "Dto4h_MassWin": 60.0, 
        "DtoXgammaADocaChi2Max": 10, 
        "GEC_nLongTrk": 250, 
        "HLT2": "HLT_PASS_RE('Hlt2.*SingleMuon.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "KPiPT": 250.0, 
        "KSCutDIRA": 0.99, 
        "KSCutZFDFromD": 10.0, 
        "KSDDCutFDChi2": 100, 
        "KSDDCutMass": 35, 
        "KSDDPMin": 2000, 
        "KSDDPTMin": 200, 
        "KSDaugTrackChi2": 4, 
        "KSLLCutFDChi2": 100, 
        "KSLLCutMass": 35, 
        "KSLLPMin": 2000, 
        "KSLLPTMin": 200, 
        "KSVertexChi2": 6, 
        "KaonPIDK": 4.0, 
        "KaonPIDKloose": -5, 
        "LambdaCutDIRA": 0.99, 
        "LambdaDDCutFDChi2": 100, 
        "LambdaDDCutMass": 30, 
        "LambdaDDPMin": 3000, 
        "LambdaDDPTMin": 800, 
        "LambdaDaugTrackChi2": 4, 
        "LambdaLLCutFDChi2": 100, 
        "LambdaLLCutMass": 30, 
        "LambdaLLPMin": 2000, 
        "LambdaLLPTMin": 500, 
        "LambdaVertexChi2": 6, 
        "Lc2HHHPrescale": 1, 
        "Lc2HHPrescale": 1, 
        "MINIPCHI2": 9.0, 
        "MINIPCHI2Loose": 4.0, 
        "MassHigh_HH": 1810, 
        "MassLow_HH": 0, 
        "MaxBMass": 7500, 
        "MaxConvPhDDChi": 9, 
        "MaxConvPhDDMass": 100, 
        "MaxConvPhLLChi": 9, 
        "MaxConvPhLLMass": 100, 
        "MaxVCHI2NDOF_HH": 10.0, 
        "MinBMass": 2500, 
        "MinConvPhDDPT": 800, 
        "MinConvPhLLPT": 800, 
        "MinVDCHI2_HH": 1000.0, 
        "MuonIPCHI2": 4.0, 
        "MuonPT": 600.0, 
        "PIDmu": -0.0, 
        "PTSUM": 1800.0, 
        "PTSUMLoose": 1400.0, 
        "PTSUM_HHGamma": 1800.0, 
        "PTSUM_HHPi0": 1800.0, 
        "PhotonCL": 0.25, 
        "PhotonPT": 1500, 
        "Pi0PMin": 2000, 
        "Pi0PtMin": 1000, 
        "PionPIDK": 10.0, 
        "PionPIDKTight": 4.0, 
        "TRCHI2": 4.0, 
        "TTSpecs": {
            "Hlt1.*Track.*Decision%TOS": 0, 
            "Hlt2.*SingleMuon.*Decision%TOS": 0, 
            "Hlt2Global%TIS": 0, 
            "Hlt2Topo.*Decision%TOS": 0
        }, 
        "TrGhostProbMax": 0.5, 
        "b2DstarMuXPrescale": 1
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingD2pimuLine
## StrippingD2KmuLine
## StrippingD2piELine
## StrippingD2KELine
## StrippingD2KmuSSLine
## StrippingD2piESSLine
## StrippingD2KESSLine

######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingD2HMuNu.py
## Authors: Liang Sun, Adam Davis
######################################################################

D2HMuNu = {
    "BUILDERTYPE": "D2HLepNuBuilder",
    "CONFIG": {
        "BDIRA": 0.999,
        "BFDCHI2HIGH": 100.0,
        "BPVVDZcut": 0.0,
        "BVCHI2DOF": 20,
        "DELTA_MASS_MAX": 400,
        "ElectronPIDe": 1.0,
        "ElectronPT": 500,
        "GEC_nLongTrk": 160.0,
        "KLepMassHigh": 2000,
        "KLepMassLow": 500,
        "KaonMINIPCHI2": 9,
        "KaonP": 3000.0,
        "KaonPIDK": 5.0,
        "KaonPIDmu": 5.0,
        "KaonPIDp": 5.0,
        "KaonPT": 800.0,
        "MuonGHOSTPROB": 0.35,
        "MuonPIDK": 0.0,
        "MuonPIDmu": 3.0,
        "MuonPIDp": 0.0,
        "MuonPT": 500.0,
        "Slowpion_P": 1000,
        "Slowpion_PIDe": 5,
        "Slowpion_PT": 300,
        "Slowpion_TRGHOSTPROB": 0.35,
        "TOSFilter": {
            "Hlt2CharmHad.*HHX.*Decision%TOS": 0
        },
        "TRGHOSTPROB": 0.35,
        "useTOS": True
    },
    "STREAMS": [ "Charm" ],
    "WGs": [ "Charm" ]
}


######################################################################
## StrippingD2KpipimuLine (DST.DST)
## StrippingD2KpipiELine (DST.DST)
## StrippingD2Kpipimu_KLepSSLine (DST.DST)
## StrippingD2Kpipimu_SSLine (DST.DST)
## StrippingD2KpipiE_KLepSSLine (DST.DST)
## StrippingD2KpipiE_SSLine (DST.DST)

######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingD2KPiPiMuNu.py
## Authors: Liang Sun
######################################################################

D2KPiPiMuNu = {
    "BUILDERTYPE": "D2KPiPiMuNuBuilder", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFDCHI2HIGH": 30.0, 
        "BPVVDZcut": 0.0, 
        "BVCHI2DOF": 10, 
        "D0IPCHI2Max": 36.0, 
        "DAUMINIPCHI2": 3, 
        "DELTA_MASS_MAX": 200, 
        "ElectronP": 3000.0, 
        "ElectronPIDe": 2, 
        "ElectronPT": 300, 
        "GEC_nLongTrk": 160.0, 
        "KPiPiMuMassHigh": 2000, 
        "KPiPiMuMassLow": 800, 
        "KaonP": 3000.0, 
        "KaonPIDK": 5.0, 
        "KaonPIDmu": 5.0, 
        "KaonPIDp": 5.0, 
        "KaonPT": 300.0, 
        "KaonTRCHI2": 3.0, 
        "MuonGHOSTPROB": 0.35, 
        "MuonP": 3000.0, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 3.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 300.0, 
        "NoPIDK": False, 
        "NoPIDmu": False, 
        "NoPIDpi": False, 
        "NoPIDpis": True, 
        "PionPIDK": 0.0, 
        "PionPIDmu": 0.0, 
        "PionPIDp": 0.0, 
        "SSonly": 2, 
        "Slowpion_P": 1000, 
        "Slowpion_PIDe": 5, 
        "Slowpion_PT": 120, 
        "Slowpion_TRGHOSTPROB": 0.35, 
        "TOSFilter": {
            "Hlt2CharmHad.*HHX.*Decision%TOS": 0
        }, 
        "TRGHOSTPROB": 0.35, 
        "useTOS": False
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingDstarD2KSHHPi0DstarD2KKPi0KSLLLine (MDST.DST)
## StrippingDstarD2KSHHPi0DstarD2KPiPi0KSDDLine (MDST.DST)
## StrippingDstarD2KSHHPi0DstarD2KPiPi0KSLLLine (MDST.DST)
## StrippingDstarD2KSHHPi0DstarD2PiPiPi0KSDDLine (MDST.DST)
## StrippingDstarD2KSHHPi0DstarD2PiPiPi0KSLLLine (MDST.DST)



######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingDstarD2KSHHPi0.py
## Authors: Maurizio Martinelli, Tommaso Pajero
######################################################################

DstarD2KSHHPi0 = {
    "BUILDERTYPE": "DstarD2KSHHPi0Lines", 
    "CONFIG": {
        "CombMassHigh": 2300.0, 
        "CombMassLow": 1500.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 190.0, 
        "Dstar_MDiff_MAX": 170.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": 0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "KSCutDIRA_DD": 0.99, 
        "KSCutDIRA_LL": 0.99, 
        "KSCutFDChi2_DD": 100, 
        "KSCutFDChi2_LL": 100, 
        "KSCutMass_DD": 50.0, 
        "KSCutMass_LL": 35.0, 
        "LowPIDK": 0, 
        "MassHigh": 2100.0, 
        "MassLow": 1650.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxVCHI2NDOF": 12.0, 
        "MaxZ_DD": 2275.0, 
        "MaxZ_LL": 500.0, 
        "MinBPVDIRA": 0.9998, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 0.0, 
        "MinFDCHI2": 15, 
        "MinTrkIPChi2": 4, 
        "MinTrkPT": 250.0, 
        "MinZ_DD": 300.0, 
        "MinZ_LL": -100.0, 
        "PhotonCL": 0.25, 
        "Pi0MassWin": 35.0, 
        "Pi0PMin": 2000.0, 
        "Pi0PtMin": 1000.0, 
        "PrescaleDstarD2KKPi0KSDD": 1, 
        "PrescaleDstarD2KKPi0KSLL": 1, 
        "PrescaleDstarD2KPiPi0KSDD": 1, 
        "PrescaleDstarD2KPiPi0KSLL": 1, 
        "PrescaleDstarD2PiPiPi0KSDD": 1, 
        "PrescaleDstarD2PiPiPi0KSLL": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingLambdac2V0HLambdac2Lambda0PiLLLine
## StrippingLambdac2V0HLambdac2Lambda0PiDDLine
## StrippingLambdac2V0HLambdac2Lambda0KLLLine
## StrippingLambdac2V0HLambdac2Lambda0KDDLine
## StrippingLambdac2V0HLambdac2pKS0LLLine
## StrippingLambdac2V0HLambdac2pKS0DDLine
## StrippingLambdac2V0HXic2pKS0LLLine
## StrippingLambdac2V0HXic2pKS0DDLine


## -------------------------------------------------------------------
## Lines defined in StrippingLambdac2V0H.py
## Authors: Miroslav Saur
######################################################################

Lambdac2V0H = {
    "BUILDERTYPE": "StrippingLambdac2V0HConf", 
    "CONFIG": {
        "Bach_All_BPVIPCHI2_MIN": 4.0, 
        "Bach_All_PT_MIN": 750.0, 
        "Bach_ETA_MAX": 5.0, 
        "Bach_ETA_MIN": 2.0, 
        "Bach_P_MAX": 100000.0, 
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 90.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Comb_PT_MIN": 1000.0, 
        "Comb_XicPlus_ADAMASS_WIN": 90.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "KS0_FDCHI2_MIN": 100, 
        "KS0_PT_MIN": 500.0, 
        "KS0_P_MIN": 2000.0, 
        "KS0_VCHI2VDOF_MAX": 10.0, 
        "K_PIDK_MIN": 5.0, 
        "Lambda0_FDCHI2_MIN": 100, 
        "Lambda0_PT_MIN": 500.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 10.0, 
        "Lambdac_PVDispCut": "(BPVVDCHI2 > 16.0)", 
        "Lambdac_VCHI2VDOF_MAX": 10.0, 
        "Lambdac_acosBPVDIRA_MAX": 0.14, 
        "Pi_PIDK_MAX": 3.0, 
        "PostscaleLambdac2Lambda0KDD": 1.0, 
        "PostscaleLambdac2Lambda0KLL": 1.0, 
        "PostscaleLambdac2Lambda0PiDD": 1.0, 
        "PostscaleLambdac2Lambda0PiLL": 1.0, 
        "PostscaleLambdac2pKS0DD": 1.0, 
        "PostscaleLambdac2pKS0LL": 1.0, 
        "PostscaleXic2pKS0DD": 1.0, 
        "PostscaleXic2pKS0LL": 1.0, 
        "PrescaleLambdac2Lambda0KDD": 1.0, 
        "PrescaleLambdac2Lambda0KLL": 1.0, 
        "PrescaleLambdac2Lambda0PiDD": 1.0, 
        "PrescaleLambdac2Lambda0PiLL": 1.0, 
        "PrescaleLambdac2pKS0DD": 1.0, 
        "PrescaleLambdac2pKS0LL": 1.0, 
        "PrescaleXic2pKS0DD": 1.0, 
        "PrescaleXic2pKS0LL": 1.0, 
        "Proton_PIDpPIDK_MIN": 2.0, 
        "Proton_PIDpPIDpi_MIN": 7.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingLambdacForNeutronPIDSigmac02LcpiLine (MDST.DST)
## StrippingLambdacForNeutronPIDSigmacpp2LcpiLine (MDST.DST)


######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingLambdacForNeutronPID.py
## Authors: Marco Pappagallo
######################################################################

LambdacForNeutronPID = {
    "BUILDERTYPE": "LambdacForNeutronPIDConf", 
    "CONFIG": {
        "KaonPID": 5.0, 
        "LambdacForNeutronPIDPostScale": 1.0, 
        "LambdacForNeutronPIDPreScale": 1.0, 
        "MaxLcMass": 2200, 
        "MaxLcVertChi2DOF": 16, 
        "MaxSigmacVertChi2DOF": 16, 
        "MaxTrackIPchi2": 9, 
        "MinLcMass": 1400, 
        "MinLcPT": 4000, 
        "MinNeutronPT": 400.0, 
        "MinPionPT": 250, 
        "MinTrackIPchi2": 9, 
        "MinTrackPT": 400.0, 
        "PionPID": 0.0, 
        "QValueSigmacDecay": 120, 
        "SigmacPT": 0, 
        "TrGhostProb": 0.3, 
        "ctau": 0.1
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingNeutralCBaryonsXic02LambdaKS0Line (MDST.DST)
## StrippingNeutralCBaryonsXic02LambdaKpiLine (MDST.DST)
## StrippingNeutralCBaryonsXic02pKLine (MDST.DST)
## StrippingNeutralCBaryonsXic02XipiLine (MDST.DST)
## StrippingNeutralCBaryonsXic02XiKLine (MDST.DST)
## StrippingNeutralCBaryonsOmegac02OmegapiLine (MDST.DST)
## StrippingNeutralCBaryonsOmegac02pKLine (MDST.DST)
## StrippingNeutralCBaryonsOmegac02LambdaKS0Line (MDST.DST)
## StrippingNeutralCBaryonsOmegac02LambdaKpiLine (MDST.DST)

######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingNeutralCbaryons.py
## Authors: Xuesong Liu, Xiao-Rui Lyu, Zhenwei Yang', Miroslav Saur, Ziyi Wang
######################################################################

NeutralCBaryons = {
    "BUILDERTYPE": "StrippingNeutralCBaryonsConf", 
    "CONFIG": {
        "Bachelor_PT_MIN": 50.0, 
        "KaonPIDK": -5.0, 
        "LambdaDDMassWin": 5.7, 
        "LambdaDDMaxVZ": 2275.0, 
        "LambdaDDMinVZ": 400.0, 
        "LambdaDDVtxChi2Max": 5.0, 
        "LambdaDeltaZ_MIN": 5.0, 
        "LambdaLLMassWin": 5.7, 
        "LambdaLLMaxVZ": 400.0, 
        "LambdaLLMinDecayTime": 0.005, 
        "LambdaLLMinVZ": -100.0, 
        "LambdaLLVtxChi2Max": 5.0, 
        "LambdaPi_PT_MIN": 100.0, 
        "LambdaPr_PT_MIN": 500.0, 
        "PionPIDK": 10.0, 
        "ProbNNkMin": 0.1, 
        "ProbNNpMinDD": 0.05, 
        "ProbNNpMinLL": 0.1, 
        "ProtonPIDp": 5, 
        "ProtonPIDpK": -3, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25, 
        "tight_KaonPIDK": 0.0, 
        "tight_KaonPIDpi": 5.0, 
        "tight_ProtonPIDp": 7, 
        "tight_ProtonPIDpK": 0.0, 
        "tight_ProtonPIDppi": 5.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingXcpToLambdaKSpipLine (MDST.DST)

######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingXcpToLambdaKSHp.py
## Authors: Marian Stahl
######################################################################

XcpToLambdaKSHp = {
    "BUILDERTYPE": "XcpToLambdaKSHpConf",
    "CONFIG": {
        "bach_kaon": {
            "filter": "(P>4.5*GeV) & (PT>300*MeV) & (MIPCHI2DV(PRIMARY)>1) & (PROBNNk>0.05)",
            "tes": "Phys/StdAllNoPIDsKaons/Particles"
        },
        "bach_pion": {
            "filter": "(P>2*GeV) & (PT>150*MeV) & (MIPCHI2DV(PRIMARY)>1.5) & (PROBNNpi>0.05)",
            "tes": "Phys/StdAllNoPIDsPions/Particles"
        },
        "descriptor_kaon": [
                            "[Xi_c+ -> Lambda0 KS0 K+]cc"
                            ],
        "descriptor_pion": [
                        "[Xi_c+ -> Lambda0 KS0 pi+]cc"
                        ],
        "kshort_dd": {
            "filter": "(ADMASS('KS0')<25*MeV) & (PT>400*MeV) & (CHI2VXNDF<24) & (CHILDIP(1)<3*mm) & (CHILDIP(2)<3*mm) & (MAXTREE('pi+'==ABSID,PT)>150*MeV)",
            "tes": "Phys/StdLooseKsDD/Particles"
        },
        "kshort_ll": {
            "filter": "(ADMASS('KS0')<25*MeV) & (PT>300*MeV) & (CHI2VXNDF<24) & (BPVVDZ>4*mm) & (BPVVDCHI2>12) & (CHILDIP(1)<0.8*mm) &\n                    (CHILDIP(2)<0.8*mm) & (DOCA(1,2)<0.8*mm) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>12) & (MAXTREE('pi+'==ABSID,PT)>100*MeV)",
            "tes": "Phys/StdVeryLooseKsLL/Particles"
        },
        "lambda_dd": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>1*GeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV) &\n                    (MAXTREE('p+'==ABSID,PT)>800*MeV) & (MAXTREE('pi+'==ABSID,PT)>150*MeV)",
            "tes": "Phys/StdLooseLambdaDD/Particles"
        },
        "lambda_ll": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>800*MeV) & (BPVVDZ>8*mm) & (BPVVDCHI2>32) &\n                    (DOCA(1,2)<0.6*mm) & (DOCACHI2(1,2)<16) & (MAXTREE('p+'==ABSID,PT)>600*MeV) & (MAXTREE('p+'==ABSID,P)>7.5*GeV) &\n                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>16) & (MAXTREE('pi+'==ABSID,PT)>100*MeV) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>16)",
            "tes": "Phys/StdVeryLooseLambdaLL/Particles"
        },
        "xcp_dl": {
            "comb12_cut": "(ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<20) & (AMASS(1,2)<2470*MeV)",
            "comb_cut": "(ADOCA(1,3)<1.4*mm) & (ADOCA(2,3)<0.3*mm) & (ACHI2DOCA(1,3)<16) & (ACHI2DOCA(2,3)<12) & (ASUM(PT)>2.2*GeV) & (in_range(2070*MeV,AMASS(),2610*MeV))",
            "mother_cut": "(P>24*GeV) & (PT>1.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0*mm) & (in_range(2120*MeV,M,2560*MeV)) & (CHILDIP(1)<2*mm) & (CHILDIP(2)<0.4*mm) &\n                             (CHILDIP(3)<0.2*mm) & (CHILDIPCHI2(1)<16) & (CHILDIPCHI2(2)<14) & (CHILDIPCHI2(3)<8) & ((CHILD(VFASPF(VZ),2) - VFASPF(VZ))>4*mm)"
        },
        "xcp_ld": {
            "comb12_cut": "(ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<24) & (AMASS(1,2)<2470*MeV)",
            "comb_cut": "(ADOCA(1,3)<0.3*mm) & (ADOCA(2,3)<1.6*mm) & (ACHI2DOCA(1,3)<12) & (ACHI2DOCA(2,3)<16) & (ASUM(PT)>2.1*GeV) & (in_range(2070*MeV,AMASS(),2610*MeV))",
            "mother_cut": "(P>24*GeV) & (PT>1.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0*mm) & (in_range(2120*MeV,M,2560*MeV)) & (CHILDIP(1)<0.25*mm) & (CHILDIP(2)<3*mm) &\n                             (CHILDIP(3)<0.2*mm) & (CHILDIPCHI2(1)<14) & (CHILDIPCHI2(2)<16) & (CHILDIPCHI2(3)<8) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>8*mm)"
        },
        "xcp_ll": {
            "comb12_cut": "(ACHI2DOCA(1,2)<12) & (ADOCA(1,2)<0.4*mm) & (AMASS(1,2)<2470*MeV)",
            "comb_cut": "(ADOCA(1,3)<0.25*mm) & (ADOCA(2,3)<0.3*mm) & (ACHI2DOCA(1,3)<12) & (ACHI2DOCA(1,3)<12) & (ASUM(PT)>2*GeV) & (in_range(2070*MeV,AMASS(),2610*MeV))",
            "mother_cut": "(P>24*GeV) & (PT>1.5*GeV) & (CHI2VXNDF<12) & (BPVVDZ>0*mm) & (in_range(2120*MeV,M,2560*MeV)) &\n                             (CHILDIP(1)<0.25*mm) & (CHILDIP(2)<0.4*mm) & (CHILDIP(3)<0.2*mm) & (CHILDIPCHI2(1)<12) & (CHILDIPCHI2(2)<12) & (CHILDIPCHI2(3)<8) &\n                             ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>8*mm) & ((CHILD(VFASPF(VZ),2) - VFASPF(VZ))>4*mm)"
}
    },
    "STREAMS": {
        "Charm": [
                  "StrippingXcpToLambdaKSHp_PiLine",
                  "StrippingXcpToLambdaKSHp_KLine"
                  ]
    },
    "WGs": [ "Charm" ]
}



######################################################################
## StrippingXcpToXiPipHp_PiLLLLine (DST.DST)
## StrippingXcpToXiPipHp_PiDDLLine (DST.DST)
## StrippingXcpToXiPipHp_PiDDDLine (DST.DST)
## StrippingXcpToXiPipHp_KLLLLine (DST.DST)
## StrippingXcpToXiPipHp_KDDLLine (DST.DST)
##  StrippingXcpToXiPipHp_KDDDLine (DST.DST)


######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingXcpToXiPipHp.py
## Authors: Marian Stahl, Laurent Dufour
######################################################################

XcpToXiPipHp = {
    "BUILDERTYPE": "XcpToXiPipHpConf", 
    "CONFIG": {
        "RequiredRawEvents": [ "Velo" ], 
        "amass_xikpi": "(in_range(2160*MeV,AMASS(),2580*MeV)) & ", 
        "amass_xipipi": "(in_range(2360*MeV,AMASS(),2580*MeV)) & ", 
        "bach_kaon": {
            "filter": "(P>4.5*GeV) & (PT>300*MeV) & (MIPCHI2DV(PRIMARY)>1) & (PROBNNk>0.1)", 
            "tes": "Phys/StdAllNoPIDsKaons/Particles"
        }, 
        "bach_pion": {
            "filter": "(P>2*GeV) & (PT>100*MeV) & (MIPDV(PRIMARY)>0.01*mm) & (MIPCHI2DV(PRIMARY)>1) & (PROBNNpi>0.03)", 
            "tes": "Phys/StdAllNoPIDsPions/Particles"
        }, 
        "descriptor_xi": [
            "[Xi- -> Lambda0 pi-]cc"
        ], 
        "descriptor_xikpi": [
            "[Xi_c+ -> Xi- K+ pi+]cc"
        ], 
        "descriptor_xipipi": [
            "[Xi_c+ -> Xi- pi+ pi+]cc"
        ], 
        "down_pion": {
            "filter": "(P>2*GeV) & (PT>150*MeV) & (PROBNNpi>0.03)", 
            "tes": "Phys/StdNoPIDsDownPions/Particles"
        }, 
        "lambda_dd": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>1*GeV) & (CHILDIP(1)<2*mm) & (MAXTREE('p+'==ABSID,P)>9*GeV) & (CHI2VXNDF<24) &\n                    (MAXTREE('p+'==ABSID,PT)>800*MeV) & (MAXTREE('pi+'==ABSID,PT)>150*MeV)", 
            "tes": "Phys/StdLooseLambdaDD/Particles"
        }, 
        "lambda_ll": {
            "filter": "(ADMASS('Lambda0')<20*MeV) & (P>12*GeV) & (PT>800*MeV) & (BPVVDZ>8*mm) & (BPVVDCHI2>32) & (CHI2VXNDF<24) &\n                    (DOCA(1,2)<0.6*mm) & (DOCACHI2(1,2)<16) & (MAXTREE('p+'==ABSID,PT)>600*MeV) & (MAXTREE('p+'==ABSID,P)>7.5*GeV) &\n                    (MAXTREE('p+'==ABSID,MIPCHI2DV(PRIMARY))>9) & (MAXTREE('pi+'==ABSID,MIPCHI2DV(PRIMARY))>9)", 
            "tes": "Phys/StdVeryLooseLambdaLL/Particles"
        }, 
        "mass_xikpi": "(in_range(2220*MeV,M,2540*MeV)) & ", 
        "mass_xipipi": "(in_range(2400*MeV,M,2540*MeV)) & ", 
        "xc_ddd": {
            "comb12_cut": "(AMASS(1,2)<2510*MeV) & (ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<16)", 
            "comb_cut": "(APT>1.3*GeV) & (ASUM(PT)>1.9*GeV) & (ADOCA(1,3)<3*mm) & (ACHI2DOCA(1,3)<16) & (ADOCA(2,3)<1*mm) & (ACHI2DOCA(2,3)<16) &\n                        (AHASCHILD(ISBASIC & HASTRACK & (PT>300*MeV) & (P>3*GeV) & (MIPCHI2DV(PRIMARY)>2)))", 
            "mother_cut": "(P>20*GeV) & (PT>1.5*GeV) & (BPVVDZ>0.25*mm) & (BPVVDZ<200*mm) & (CHI2VXNDF<9) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>12*mm) &\n                        (CHILDIP(1)<2*mm) & (CHILDIP(2)<0.3*mm) & (CHILDIP(3)<0.4*mm) & (CHILDIPCHI2(1)<16) & (CHILDIPCHI2(2)<12) & (CHILDIPCHI2(3)<12) &\n                        ((CHILD(2,PX)*CHILD(3,PX)+CHILD(2,PY)*CHILD(3,PY)+CHILD(2,PZ)*CHILD(3,PZ))/(CHILD(2,P)*CHILD(3,P))<0.99999998)"
        }, 
        "xc_ddl": {
            "comb12_cut": "(AMASS(1,2)<2510*MeV) & (ADOCA(1,2)<1.5*mm) & (ACHI2DOCA(1,2)<12)", 
            "comb_cut": "(APT>1.3*GeV) & (ASUM(PT)>1.8*GeV) & (ADOCA(1,3)<1.8*mm) & (ACHI2DOCA(1,3)<12) & (ADOCA(2,3)<0.5*mm) & (ACHI2DOCA(2,3)<12) &\n                        (AHASCHILD(ISBASIC & HASTRACK & (PT>300*MeV) & (P>3*GeV) & (MIPCHI2DV(PRIMARY)>2)))", 
            "mother_cut": "(P>20*GeV) & (PT>1.5*GeV) & (BPVVDZ>0.25*mm) & (BPVVDZ<200*mm) & (CHI2VXNDF<9) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>12*mm) &\n                        (CHILDIP(1)<1.4*mm) & (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.4*mm) & (CHILDIPCHI2(1)<12) & (CHILDIPCHI2(2)<6) & (CHILDIPCHI2(3)<9) &\n                        ((CHILD(2,PX)*CHILD(3,PX)+CHILD(2,PY)*CHILD(3,PY)+CHILD(2,PZ)*CHILD(3,PZ))/(CHILD(2,P)*CHILD(3,P))<0.99999998)"
        }, 
        "xc_lll": {
            "comb12_cut": "(AMASS(1,2)<2510*MeV) & (ADOCA(1,2)<0.24*mm) & (ACHI2DOCA(1,2)<12)", 
            "comb_cut": "(APT>1.3*GeV) & (ASUM(PT)>1.75*GeV) & (ADOCA(1,3)<0.35*mm) & (ACHI2DOCA(1,3)<10) & (ADOCA(2,3)<0.35*mm) & (ACHI2DOCA(2,3)<9) &\n                        (AHASCHILD(ISBASIC & HASTRACK & (PT>300*MeV) & (P>3*GeV) & (MIPCHI2DV(PRIMARY)>2)))", 
            "mother_cut": "(P>20*GeV) & (PT>1.5*GeV) & (BPVVDZ>0.25*mm) & (BPVVDZ<200*mm) & (CHI2VXNDF<9) & ((CHILD(VFASPF(VZ),1) - VFASPF(VZ))>12*mm) &\n                        (CHILDIP(1)<0.15*mm) & (CHILDIP(2)<0.2*mm) & (CHILDIP(3)<0.45*mm) & (CHILDIPCHI2(1)<9) & (CHILDIPCHI2(2)<6) & (CHILDIPCHI2(3)<9) &\n                        ((CHILD(2,PX)*CHILD(3,PX)+CHILD(2,PY)*CHILD(3,PY)+CHILD(2,PZ)*CHILD(3,PZ))/(CHILD(2,P)*CHILD(3,P))<0.99999998)"
        }, 
        "xi_ddd": {
            "comb_cut": "(ADOCA(1,2)<3*mm) & (ACHI2DOCA(1,2)<32) & (ASUM(PT)>1.2*GeV) & (ADAMASS('Xi-')<60*MeV)", 
            "mother_cut": "(ADMASS('Xi-')<30*MeV) & (P>16*GeV) & (PT>800*MeV) & (CHI2VXNDF<16) & (CHILDIP(1)<3*mm) & (CHILDIP(2)<1*mm)"
        }, 
        "xi_ddl": {
            "comb_cut": "(ADOCA(1,2)<2*mm) & (ACHI2DOCA(1,2)<24) & (ASUM(PT)>1.1*GeV) & (ADAMASS('Xi-')<60*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>9)", 
            "mother_cut": "(ADMASS('Xi-')<30*MeV) & (P>15*GeV) & (PT>800*MeV) & (CHI2VXNDF<16) & (CHILDIP(1)<2.5*mm) & (CHILDIP(2)<0.35*mm)"
        }, 
        "xi_lll": {
            "comb_cut": "(ADOCA(1,2)<0.3*mm) & (ACHI2DOCA(1,2)<16) & (ASUM(PT)>1*GeV) & (ADAMASS('Xi-')<60*MeV) & (ACHILD(2,MIPCHI2DV(PRIMARY))>9)", 
            "mother_cut": "(ADMASS('Xi-')<30*MeV) & (P>10*GeV) & (PT>700*MeV) & (BPVVDCHI2>40) & (BPVVD>12*mm) & (CHI2VXNDF<16) &\n                         (CHILDIP(1)<0.16*mm) & (CHILDIP(2)<0.28*mm) & (CHILDIPCHI2(1)<8) & (CHILDIPCHI2(2)<8)"
        }
    }, 
    "STREAMS": {
        "CharmCompleteEvent": [
            "StrippingXcpToXiPipHp_PiLLLLine", 
            "StrippingXcpToXiPipHp_PiDDLLine", 
            "StrippingXcpToXiPipHp_PiDDDLine", 
            "StrippingXcpToXiPipHp_KLLLLine", 
            "StrippingXcpToXiPipHp_KDDLLine", 
            "StrippingXcpToXiPipHp_KDDDLine"
        ]
    }, 
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingXiccSLNEWXicc2LambdacMuNuLine (DST.DST)
##  StrippingXiccSLNEWXicc2XicPlusMuNuLine (DST.DST)
##  StrippingXiccSLNEWXicc2XicZeroMuNuLine (DST.DST)
##  StrippingXiccSLNEWXicc2LambdacMuNuWSLine (DST.DST)
##  StrippingXiccSLNEWXicc2XicPlusMuNuWSLine (DST.DST)
##  StrippingXiccSLNEWXicc2XicZeroMuNuWSLine (DST.DST)
##  StrippingXiccSLNEWOmegacc2OmegacMuNuLine (DST.DST)
##  StrippingXiccSLNEWOmegacc2XicMuNuLine (DST.DST)
##  StrippingXiccSLNEWOmegacc2OmegacNCLine (DST.DST)
##  StrippingXiccSLNEWOmegacc2XicNCLine (DST.DST)
##  StrippingXiccSLNEWOmegacc2OmegacMuNuWSLine (DST.DST)
##  StrippingXiccSLNEWOmegacc2XicMuNuWSLine (DST.DST)

######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingXiccSL.py
## Authors: Xiao-Rui Lyu, Miroslav Saur, Ziyi Wang
######################################################################

XiccSL = {
    "BUILDERTYPE": "StrippingXiccSL", 
    "CONFIG": {
        'LcHlt2TisTosSpec' : { 'Hlt2CharmHadLcpToPpKmPip.*Decision%TOS' : 0, 'Hlt2Global%TIS' : 0 }
        , 'controlPrescaleLc'           : 0.05
        , 'controlPrescaleXic'          : 0.05
        , 'signalPrescaleViaLc'         : 1.0
        , 'signalPrescaleViaXicp'       : 1.0
        , 'signalPrescaleViaXicz'       : 1.0
        , 'signalPrescaleViaOmegaz'     : 0.5
        , 'controlPrescaleViaOmegaczPi' : 0.03
        , 'controlPrescaleViaXiczPi'    : 0.05
        , 'signalPrescaleViaLcWS'       : 0.5
        , 'TRCHI2DOFMax'            :   3.0
        , 'Xicc_Daug_TRCHI2DOF_Max' :   5.0
        , 'Xicc_Daug_P_Min'         :   2.0*GeV
        , 'Xicc_Daug_PT_Min'        : 250.0*MeV
        , 'Xicc_Daug_MIPCHI2DV_Min' :  -1.0
        , 'TrGhostProbMax'          :   0.5 #can be tightened if needed.  < 0.3 -> same for all particles
        , 'Xicc_P_PIDpPIDpi_Min'    :   5.0
        , 'Xicc_P_PIDpPIDK_Min'     :   0.0
        , 'Xicc_Pi_PIDpiPIDK_Min'   :   0.0
        , 'Xicc_K_PIDKPIDpi_Min'    :   5.0
        , 'PionPIDK'                :  10.0
        , 'TRCHI2'                  :   3.0
        , 'MuonP'                   :   6.0*GeV
        , 'MuonPT'                  :  1000*MeV
        , 'MuonIPCHI2'              :   5.0
        , 'MuonPIDmu'               :   0.0
        , 'GhostProb'               :   0.35
        , 'Lc_Daug_TRCHI2DOF_Max'   :   5.0
        , 'Lc_Daug_PT_Min'          : 200.0*MeV
        , 'Lc_Daug_P_Min'           :   2.0*GeV
        , 'Lc_K_PIDKPIDpi_Min'      :   5.0
        , 'Lc_Pi_PIDpiPIDK_Min'     :   0.0
        , 'Lc_P_PIDpPIDpi_Min'      :   5.0
        , 'Lc_P_PIDpPIDK_Min'       :   0.0
        , 'Lc_ADMASS_HalfWin'       :  75.0*MeV
        , 'Lc_Daug_1of3_MIPCHI2DV_Min': 4.0
        , 'Lc_ADOCAMAX_Max'         :   0.5*mm
        , 'Lc_APT_Min'              :   1.0*GeV
        , 'Lc_VCHI2_Max'            :  30.0
        , 'Lc_BPVVDCHI2_Min'        :  16.0
        , 'Lc_BPVDIRA_Min'          :   0.99
        , 'Xicplus_ADAMASS_HalfWin' : 170.0*MeV
        , 'Xicplus_ADMASS_HalfWin'  : 120.0*MeV
        , 'Xicplus_BPVVDCHI2_Min'   :  25.0
        , 'Xicplus_BPVDIRA_Min'     :   0.99
        , 'Xic0_ADAMASS_HalfWin'    : 170.0*MeV
        , 'Xic0_ADMASS_HalfWin'     : 120.0*MeV
        , 'Xic0_BPVVDCHI2_Min'      :  25.0
        , 'Xic0_BPVDIRA_Min'        :   0.9
        , 'Omegac0_ADAMASS_HalfWin' : 170.0*MeV
        , 'Omegac0_ADMASS_HalfWin'  : 120.0*MeV
        , 'Omegac0_4Dau_VCHI2_Max'  :  60.0
        , 'Omegac0_BPVVDCHI2_Min'   :  25.0
        , 'Omegac0_BPVDIRA_Min'     :   0.9
        , 'Xicc_AM_Min'             :   2.3*GeV #maybe 3.1 if rates too high
        , 'Xicc_AM_Max'             :   5.0*GeV #maybe 4.0 if rates too high
        , 'Xicc_APT_Min'            :   2.0*GeV
        , 'Xicc_ADOCAMAX_Max'       :   2.0*mm #increase form 0.5 to 2
        , 'Xicc_Lc_delta_VZ_Min'    :  -5.0*mm #increase to -5 from -2
        , 'Xicc_BPVVDCHI2_Min'      :  -1.0
        , 'Xicc_BPVDIRA_Min'        :   0.9
        , 'Omegacc_AM_Min'          :   2.5*GeV
        , 'Omegacc_AM_Max'          :   5.2*GeV
        , 'Omegacc_APT_Min'         :   2.0*GeV
        , 'Omegacc_ADOCAMAX_Max'    :   2.0*mm #increase form 0.5 to 2
        , 'Omegacc_Lc_delta_VZ_Min' :  -5.0*mm #increase to -5 from -2
        , 'Omegacc_BPVVDCHI2_Min'   :  -1.0
        , 'Omegacc_BPVDIRA_Min'     :   0.9
        , 'BVCHI2DOF'               :  20.0 #increase from 9 to 20
        , 'Omegacc_2Dau_VCHI2_Max'  :  20.0
        , 'Xicc_2Dau_VCHI2_Max'     :  60.0 #set to 60 from 20
        , 'Xicc_3Dau_VCHI2_Max'     :  60.0 #set to 60 form 30
        , 'Xicc_4Dau_VCHI2_Max'     :  60.0
        , 'MINIPCHI2'               :   4.0  # adimensiional, orig. 9
        , 'MINIPCHI2Loose'          :   4.0  # adimensiional
        , 'Xicc_Zero_BPVDIRA_Min'   :   0.0 # Neutral
        , 'Omegacc_Zero_BPVDIRA_Min':   0.0 # Neutral


    },
    "STREAMS": [ "CharmCompleteEvent" ],
    "WGs": [ "Charm" ]
}

######################################################################

## StrippingCharmedBaryonSLLambdac2LambdaMuNuLine (DST.DST)
## StrippingCharmedBaryonSLLambdac2PKMuNuLine (DST.DST)
## StrippingCharmedBaryonSLLambdac2PPiMuNuLine (DST.DST)
## StrippingCharmedBaryonSLLambdac2LambdaPiNCLine (DST.DST)
## StrippingCharmedBaryonSLLambdac2PKPiNCLine (DST.DST)
## StrippingCharmedBaryonSLLambdac2PPiPiNCLine (DST.DST)
## StrippingCharmedBaryonSLOmegac02OmegaMuNuLine (DST.DST)
## StrippingCharmedBaryonSLOmegac02XiMuNuLine (DST.DST)
## StrippingCharmedBaryonSLOmegac02OmegaPiNCLine (DST.DST)
## StrippingCharmedBaryonSLOmegac02XiPiNCLine (DST.DST)
## StrippingCharmedBaryonSLXic2LambdaMuNuLine (DST.DST)
## StrippingCharmedBaryonSLXic02XiMuNuLine (DST.DST)
## StrippingCharmedBaryonSLXic2LambdaPiNCLine (DST.DST)
## StrippingCharmedBaryonSLXic02XiPiNCLine (DST.DST)
######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingCharmedBaryonSL.py
## Authors: Xiao-Rui Lyu, Miroslav Saur, Ziyi Wang
######################################################################


CharmedBaryonSL = {
    "BUILDERTYPE": "StrippingCharmedBaryonSL",
    "CONFIG": {
        'GEC_nLongTrk' : 250    # adimensional
        , 'signalPrescaleViaLc'           :   1.0
        , 'signalPrescaleViaLcL0DDMuNu'   :   0.8
        , 'signalPrescaleViaLcPPiMuNu'    :   0.25
        , 'signalPrescaleViaOmegac0'      :   1.0
        , 'signalPrescaleViaXic'          :   1.0
        , 'signalPrescaleViaXicL0LLMuNu'  :   0.8
        , 'signalPrescaleViaXicL0DDMuNu'  :   0.8
        , 'controlPrescaleViaLcL0LLPiNC'  :   0.4
        , 'controlPrescaleViaLcL0DDPiNC'  :   0.8
        , 'controlPrescaleViaLcPKPiNC'    :   0.09
        , 'controlPrescaleViaLcPPiPiNC'   :   0.06
        , 'controlPrescaleViaOmegac0NC'   :   1.0
        , 'controlPrescaleViaXicL0LLPiNC' :   0.4
        , 'controlPrescaleViaXicL0DDPiNC' :   0.8
        , 'controlPrescaleViaXicXiPiNC'   :   1.0
        , 'TRCHI2DOFMax'            :   3.0
        , 'GhostProb'               :   0.3
        , 'TrGhostProbMax'          :   0.25 # same for all particles
        , 'MINIPCHI2'               :   4.0  # adimensiional, orig. 9
        , 'MuonP'                   :   3.0*GeV
        , 'MuonPT'                  :   500*MeV
        , 'MuonIPCHI2'              :   5.0
        , 'MuonPIDmu'               :   0.0
        , 'PionP'                   :   3.0*GeV
        , 'PionPT'                  :   500*MeV
        , 'PionPIDK'                :  10.0
        , 'Pion_PIDpiPIDK_Min'      :   0.0
        , 'KaonP'                   :   3.0*GeV
        , 'KaonPT'                  :   500*MeV
        , 'KaonPIDK'                :  10.0
        , 'Kaon_PIDKPIDpi_Min'      :   5.0
        , 'ProtonP'                 :   3.0*GeV
        , 'ProtonPT'                :   500*MeV
        , 'Proton_PIDpPIDpi_Min'    :   5.0
        , 'Proton_PIDpPIDK_Min'     :   0.0
        , 'ProbNNpMin'              :   0.2 #ProbNNp cut for proton in L0, to suppress the bkg of L0
        , 'ProbNNp'                 :   0.4
        , 'ProbNNk'                 :   0.4
        , 'ProbNNpi'                :   0.5
        , 'ProbNNpiMax'             :   0.95
        , 'LambdaLLPMin'            :5000.0*MeV
        , 'LambdaLLPTMin'           : 800.0*MeV
        , 'LambdaLLCutMass'         :  20.0*MeV
        , 'LambdaLLCutFDChi2'       : 100.0 ## unitless
        , 'LambdaDDPMin'            :5000.0*MeV
        , 'LambdaDDPTMin'           : 800.0*MeV
        , 'LambdaDDCutMass'         :  20.0*MeV
        , 'LambdaDDCutFDChi2'       : 100.0## unitless
        , 'LambdaLDPMin'            :3000.0*MeV
        , 'LambdaLDPTMin'           : 800.0*MeV
        , 'LambdaLDCutMass'         :  20.0*MeV
        , 'LambdaLDCutFDChi2'       : 100.0## unitless
        , 'LambdaDaugTrackChi2'     :   4.0## unitless
        , 'LambdaVertexChi2'        :   5.0## max chi2/ndf for Lambda0 vertex
        , 'LambdaCutDIRA'           :   0.99## unitless
        , 'LambdaLLMinVZ'           :-100.0*mm
        , 'LambdaLLMaxVZ'           : 400.0*mm
        , 'LambdaDDMinVZ'           : 400.0*mm
        , 'LambdaDDMaxVZ'           :2275.0*mm
        , 'LambdaPr_PT_MIN'         : 500.0*MeV
        , 'LambdaPi_PT_MIN'         : 100.0*MeV
        , 'Lc_ADMASS_HalfWin'       :  80.0*MeV
        , 'Lc_Daug_1of3_MIPCHI2DV_Min': 4.0
        , 'Lc_ADOCAMAX_Max'         :   0.5*mm
        , 'Lc_APT_Min'              :   1.0*GeV
        , 'Lc_VCHI2_Max'            :  30.0
        , 'Lc_BPVVDCHI2_Min'        :  16.0
        , 'Lc_BPVDIRA_Min'          :   0.9
        , 'Omegac0_ADAMASS_HalfWin' : 170.0*MeV
        , 'Omegac0_ADMASS_HalfWin'  : 120.0*MeV
        , 'Omegac0_4Dau_VCHI2_Max'  :  60.0
        , 'Omegac0_BPVVDCHI2_Min'   :  25.0
        , 'Omegac0_BPVDIRA_Min'     :   0.9
        , 'Xic_ADAMASS_HalfWin'     : 170.0*MeV
        , 'Xic_ADMASS_HalfWin'      : 120.0*MeV
        , 'Xic_BPVVDCHI2_Min'       :  25.0
        , 'Xic_BPVDIRA_Min'         :   0.9
        , 'L0Mu_AM_Min'             :  1250.0*MeV
        , 'pKMu_AM_Min'             :  1550.0*MeV
        , 'ppiMu_AM_Min'            :  1200.0*MeV
        , 'XiMu_AM_Min'             :  1430.0*MeV
        , 'OmMu_AM_Min'             :  1780.0*MeV
        , 'Lc_AM_Max'               :  2370.0*MeV
        , 'Oc_AM_Max'               :  2780.0*MeV
        , 'Xic_AM_Max'              :  2550.0*MeV
    },
    "STREAMS": [ "CharmCompleteEvent" ],
    "WGs": [ "Charm" ]
}

######################################################################
## StrippingDstarD2XGammaDstarD2KKGammaLine  (DST.DST)
## StrippingDstarD2XGammaDstarD2KKGamma_CNVDDLine (DST.DST)
## StrippingDstarD2XGammaDstarD2KKGamma_CNVLLLine (DST.DST)
## StrippingDstarD2XGammaDstarD2PiPiGammaLine (DST.DST)
## StrippingDstarD2XGammaDstarD2PiPiGamma_CNVDDLine (DST.DST)
## StrippingDstarD2XGammaDstarD2PiPiGamma_CNVLLLine (DST.DST)
## StrippingDstarD2XGammaDstarD2KPiGammaLine (DST.DST)
## StrippingDstarD2XGammaDstarD2KPiGamma_CNVDDLine (DST.DST)
## StrippingDstarD2XGammaDstarD2KPiGamma_CNVLLLine (DST.DST)
######################################################################

## -------------------------------------------------------------------
## Lines defined in StrippingDstarD2XGamma.py
## Authors: Maurizio Martinelli, Jolanta Brodzicka
######################################################################

DstarD2XGamma = {
    "BUILDERTYPE": "DstarD2XGammaLines",
    "CONFIG": {
        'PrescaleDstarD2PiPiGamma': 1,
        'PrescaleDstarD2KPiGamma' : 1,
        'PrescaleDstarD2KKGamma'  : 1,
        'PrescaleDstarD2PiPiGamma_CNVLL': 1,
        'PrescaleDstarD2KPiGamma_CNVLL' : 1,
        'PrescaleDstarD2KKGamma_CNVLL'  : 1,
        'PrescaleDstarD2PiPiGamma_CNVDD': 1,
        'PrescaleDstarD2KPiGamma_CNVDD' : 1,
        'PrescaleDstarD2KKGamma_CNVDD'  : 1,
         # Gamma
        'photonPT'                : 2.0 * GeV, #1.7->2 in 2021campaign for 2018
        'MaxMass_CNV_LL'          : 100 * MeV,
        'MaxVCHI2_CNV_LL'         : 9,
        'MinPT_CNV_LL'            : 1000 * MeV,
        'MaxMass_CNV_DD'          : 100 * MeV,
        'MaxVCHI2_CNV_DD'         : 9,
        'MinPT_CNV_DD'            : 1000 * MeV,
        'MinPhotonCL'             : 0.25, #added in 2019 campaign
        # X -> HH
        'TrChi2'                  : 4,
        'TrGhostProb'             : 0.5,
        'MinTrkPT'                : 250 * MeV, #500->250 in 2019 campaign2 ##250 in Run1 Restrip
        'MinTrkIPChi2'            : 3, #6->3 in 2019 campaign2 ##3 in Run1
        'HighPIDK'                :-5, #pions #0->-5 in 2019 campaign2 #0 in Run1
        'LowPIDK'                 : 5, #kaons #0->5 in 2019 campaign ##5 in Run1
        'CombMassLow_HH'          : 0 * MeV,
        'CombMassHigh_HH'         : 1300 * MeV, #1820 * MeV,
        'MinCombPT'               : 0 * GeV,
        'MaxADOCACHI2'            : 10.0,
        'MinVDCHI2_HHComb'        : 10.0, #1000->100->10 (!!!) in 2019 campaign2 ##100 in Run1
        'MassLow_HH'              : 0 * MeV,
        'MassHigh_HH'             : 1050 * MeV, #1865 * MeV in Run1 and 2015/2016
        'MaxVCHI2NDOF_HH'         : 16.0,
        'MinVDCHI2_HH'            : 10.0, #1000->100->10 (!!!) in 2019 campaign2 ##100 in Run1
        # D0 -> X Gamma
        'CombMassLow'             : 1610 * MeV, #1665 * MeV,
        'CombMassHigh'            : 2130 * MeV, #2075 * MeV,
        'MassLow'                 : 1660 * MeV, #1685->1610 in 2019campaign2 ->1660 in 2021campaign for 2018 ##1610 in Run1
        'MassHigh'                : 2100 * MeV, #2055 * MeV,
        'MinPT'                   : 2.0 * GeV, #2.0 in Run1
        'MaxVCHI2NDOF'            : 12.0,
        'MinBPVDIRA'              : 0.99995, #0.99995->0.9995->0.99995 in 2019 campaign2, ##0.9995 in Run1
        'MinBPVTAU'               : 0.5 * picosecond, ##0.1 in Run1
        'MaxIPChi2'               : 35., #none->25.->35. in 2019 campaign2, ##35.in Run1
        # D* -> D0 pi+
        'Daug_TRCHI2DOF_MAX'      : 3,
        'Dstar_AMDiff_MAX'        : 180 * MeV,
        'Dstar_VCHI2VDOF_MAX'     : 15.0,
        'Dstar_MDiff_MAX'         : 163 * MeV, #160 * MeV,
        # HLT filters, only process events firing triggers matching the RegEx
        'Hlt1Filter': None,
        'Hlt2Filter': None, #"HLT_PASS_RE('Hlt2CharmHadInclDst2PiD02HHXBDTDecision')", #jb
        'Hlt1Tos': {'Hlt1.*Track.*Decision%TOS' : 0}, #jb
        'Hlt2Tos': {'Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS' : 0, 'Hlt2PhiIncPhiDecision%TOS' : 0, 'Hlt2CharmHadDst2PiD02.*Gamma.*Decision%TOS' : 0} #jb #including an exclusive line
        #
        #'Hlt2Tos': {'Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS' : 0, 'Hlt2PhiIncPhiDecision%TOS' : 0} #jb #test for early 2017

    
    },
    "STREAMS": [ "CharmCompleteEvent" ],
    "WGs": [ "Charm" ]
}
