###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
B2DibaryonMuMu = {
    "BUILDERTYPE": "B2DibaryonMuMuConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFlightCHI2": 36, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BVertexCHI2": 16, 
        "Bu2mmLinePrescale": 1, 
        "DiLeptonFDCHI2": 16, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "K1_MassWindow_Hi": 6300, 
        "K1_MassWindow_Lo": 0, 
        "K1_SumIPChi2Had": 48.0, 
        "K1_SumPTHad": 800, 
        "K1_VtxChi2": 20, 
        "KaonIPCHI2": 9, 
        "KaonPT": 250, 
        "LamLam_VtxChi2": 36, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 300, 
        "Trk_Chi2": 3, 
        "Trk_GhostProb": 0.3, 
        "UpperMass": 5500, 
        "V0TAU": 0.0005
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2KSKSMuMu = {
    "BUILDERTYPE": "B2KSKSMuMuConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFlightCHI2": 36, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BVertexCHI2": 16, 
        "Bu2mmLinePrescale": 1, 
        "DiLeptonFDCHI2": 16, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "K1_MassWindow_Hi": 6100, 
        "K1_MassWindow_Lo": 0, 
        "K1_VtxChi2": 20, 
        "KSDIRA": 0.9, 
        "KSMassWindow": 30, 
        "KsKs_VtxChi2": 36, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 300, 
        "UpperMass": 5500, 
        "V0TAU": 0.0005
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

B2LLXBDTSS = {
    "BUILDERTYPE": "B2LLXBDTSSConf", 
    "CONFIG": {
        "BComCuts": "(in_range(3.7*GeV, AM, 6.8*GeV))", 
        "BMomCuts": "(in_range(4.0*GeV,  M, 6.5*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)", 
        "Bd2LLKsXmlFile": "$TMVAWEIGHTSROOT/data/Bd2eeKs_BDT_v1r0.xml", 
        "Bd2LLKstarXmlFile": "$TMVAWEIGHTSROOT/data/Bd2eeKstar_BDT_v1r0.xml", 
        "Bd2eeKsMVACut": "-0.07", 
        "Bd2eeKstarMVACut": "0.", 
        "Bd2mumuKsMVACut": "-0.07", 
        "Bd2mumuKstarMVACut": "0.", 
        "Bs2LLPhiXmlFile": "$TMVAWEIGHTSROOT/data/Bs2eePhi_BDT_v1r0.xml", 
        "Bs2eePhiMVACut": "-0.06", 
        "Bs2mumuPhiMVACut": "-0.08", 
        "Bu2LLKXmlFile": "$TMVAWEIGHTSROOT/data/Bu2eeK_BDT_v1r0.xml", 
        "Bu2eeKMVACut": "0.", 
        "Bu2mumuKMVACut": "0.", 
        "DiElectronMCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='e-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (PIDe>-2) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "DiElectronPCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='e+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (PIDe>-2) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "DiMuonMCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='mu-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "DiMuonPCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='mu+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "ElectronCuts": "(APT>100*MeV)", 
        "ElectronMuonComCuts": "(APT>100*MeV) & (ADOCACHI2CUT(30, ''))", 
        "ElectronMuonMCuts": "\n\t\t\t     (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n\t\t\t     & (INTREE( (ID=='e+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) & (PIDe>-2) ))\n\t\t\t     & (INTREE( (ID=='mu-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n\t\t\t     ", 
        "ElectronMuonPCuts": "\n\t\t\t     (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n\t\t\t     & (INTREE( (ID=='e-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) & (PIDe>-2) ))\n\t\t\t     & (INTREE( (ID=='mu+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n\t\t\t     ", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "KsDDCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "KsLLComCuts": "(ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(25, ''))", 
        "KsLLCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "KstarCuts": "(VFASPF(VCHI2/VDOF)<16) & (ADMASS('K*(892)0')< 300*MeV)", 
        "LambdaDDCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "LambdaLLComCuts": "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LambdaLLCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "LambdastarComCuts": "(AM < 5.6*GeV)", 
        "LambdastarCuts": "(VFASPF(VCHI2) < 25.)", 
        "Lb2LLLambdaXmlFile": "$TMVAWEIGHTSROOT/data/Lb2eeLambda_BDT_v1r0.xml", 
        "Lb2LLPKXmlFile": "$TMVAWEIGHTSROOT/data/Lb2eePK_BDT_v1r0.xml", 
        "Lb2eeLambdaMVACut": "-0.11", 
        "Lb2eePKMVACut": "-0.03", 
        "Lb2mumuLambdaMVACut": "-0.15", 
        "Lb2mumuPKMVACut": "-0.07", 
        "LbComCuts": "(in_range(3.7*GeV, AM, 7.1*GeV))", 
        "LbMomCuts": "(in_range(4.0*GeV,  M, 6.8*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)", 
        "PhiCuts": "\n                          (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<1.05*GeV) & (MIPCHI2DV(PRIMARY)>2.)\n                          & (INTREE( (ID=='K+') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          & (INTREE( (ID=='K-') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          ", 
        "Pion4LPCuts": "(PROBNNpi> 0.2) & (PT>100*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>9.)", 
        "PionCuts": "(PROBNNpi> 0.2) & (PT>250*MeV) & (TRGHOSTPROB<0.4)", 
        "ProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l+)]CC": "VertexIsoInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l+)]CC": "VertexIsoBDTInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l+)]CC": "TrackIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoInfo05", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l+)]CC": "ConeIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "ConeIsoInfo05", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBs2MMInfo", 
                "Type": "RelInfoBs2MuMuTrackIsolations"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "TrackIsoInfo10", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "TrackIsoInfo15", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "TrackIsoInfo20", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 1.0, 
                "Location": "ConeIsoInfo10", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 1.5, 
                "Location": "ConeIsoInfo15", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 2.0, 
                "Location": "ConeIsoInfo20", 
                "Type": "RelInfoConeIsolation"
            }
        ]
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2LLXBDT_Calib = {
    "BUILDERTYPE": "B2LLXBDT_CalibConf", 
    "CONFIG": {
        "BComCuts": "(in_range(3.7*GeV, AM, 6.8*GeV))", 
        "BMomCuts": "(in_range(4.0*GeV,  M, 6.5*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)", 
        "Bd2JpsieeKstarMVACut": "0.", 
        "Bd2JpsimumuKstarMVACut": "0.", 
        "Bd2LLKstarXmlFile": "$TMVAWEIGHTSROOT/data/Bd2eeKstar_BDT_v1r0.xml", 
        "Bu2JpsieeKMVACut": "0.", 
        "Bu2JpsimumuKMVACut": "0.", 
        "Bu2LLKXmlFile": "$TMVAWEIGHTSROOT/data/Bu2eeK_BDT_v1r0.xml", 
        "EEBothCuts": "\n\t\t\t   (in_range(2196.0*MeV, M, 3596.0*MeV))\n\t\t\t  ", 
        "EEComCuts": "\n\t\t\t   (in_range(2096.0*MeV, AM, 3696.0*MeV)) & (ACHI2DOCA(1,2) <18)\t  \n\t\t\t  ", 
        "EEProbe1Cuts": "\n\t\t\t   (CHILDCUT( (PT>200*MeV),1 )) & (CHILDCUT( (P>3.0*GeV),1 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),1 ))\n\t\t\t   & (CHILDCUT( ISLONG,1 )) & (CHILDCUT( (TRCHI2DOF<4.),1 ))\n\t\t\t  ", 
        "EEProbe2Cuts": "\n\t\t\t   (CHILDCUT( (PT>200*MeV),2 )) & (CHILDCUT( (P>3.0*GeV),2 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),2 ))\n\t\t\t   & (CHILDCUT( ISLONG,2 )) & (CHILDCUT( (TRCHI2DOF<4.),2 ))\n\t\t\t  ", 
        "EETag1Cuts": "\n\t\t\t   (CHILDCUT( (PT>1.5*GeV),1 )) & (CHILDCUT( (PIDe>5.),1 )) & (CHILDCUT( (P>6.0*GeV),1 ))\n\t\t\t   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>25.),1 )) & (CHILDCUT( (TRGHOSTPROB<1.),1 )) & (CHILDCUT( (TRCHI2DOF<4.),1 ))\n\t\t\t   & (CHILDCUT( ISLONG,1 ))\n\t\t\t  ", 
        "EETag2Cuts": "\n\t\t\t   (CHILDCUT( (PT>1.5*GeV),2 )) & (CHILDCUT( (PIDe>5.),2 )) & (CHILDCUT( (P>6.0*GeV),2 ))\n\t\t\t   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>25.),2 )) & (CHILDCUT( (TRGHOSTPROB<1.),2 )) & (CHILDCUT( (TRCHI2DOF<4.),2 ))\n\t\t\t   & (CHILDCUT( ISLONG,2 ))\n\t\t\t  ", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "KstarCuts": "(VFASPF(VCHI2/VDOF)<16) & (ADMASS('K*(892)0')< 300*MeV)", 
        "MMBothCuts": "\n\t\t\t   (in_range(2896.0*MeV, M, 3246.0*MeV))\n\t\t\t  ", 
        "MMComCuts": "\n\t\t\t   (in_range(2796.0*MeV, AM, 3346.0*MeV)) & (ACHI2DOCA(1,2) < 5)\n\t\t\t  ", 
        "MMProbe1Cuts": "\n\t\t\t   (CHILDCUT( (TRCHI2DOF<4.),1 )) & (CHILDCUT( ISLONG,1 )) & (CHILDCUT( (P>3.0*GeV),1 ))\n\t\t\t   & (CHILDCUT( (PT>200*MeV),1 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),1 ))\n\t\t\t  ", 
        "MMProbe2Cuts": "\n\t\t\t   (CHILDCUT( (TRCHI2DOF<4.),2 )) & (CHILDCUT( ISLONG,2 )) & (CHILDCUT( (P>3.0*GeV),2 ))\n\t\t\t   & (CHILDCUT( (PT>200*MeV),2 )) & (CHILDCUT( (MIPCHI2DV(PRIMARY)>1.),2 ))\n\t\t\t  ", 
        "MMTag1Cuts": "\n\t\t\t   (CHILDCUT( ISMUON,1 )) & (CHILDCUT( (P>3.0*GeV),1 )) & (CHILDCUT( (PT>1.2*GeV),1 ))\n\t\t\t   & (CHILDCUT( (TRCHI2DOF<4.),1 )) & (CHILDCUT( ISLONG,1 )) & (CHILDCUT( (TRGHOSTPROB<0.2),1 ))\n\t\t\t   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>9.),1 ))\n\t\t\t  ", 
        "MMTag2Cuts": "\n\t\t\t   (CHILDCUT( ISMUON,2 )) & (CHILDCUT( (P>3.0*GeV),2 )) & (CHILDCUT( (PT>1.2*GeV),2 ))\n\t\t\t   & (CHILDCUT( (TRCHI2DOF<4.),2 )) & (CHILDCUT( ISLONG,2 )) & (CHILDCUT( (TRGHOSTPROB<0.2),2 ))\n\t\t\t   & (CHILDCUT( (MIPCHI2DV(PRIMARY)>9.),2 ))\n\t\t\t  ", 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoInfo05", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "ConeIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "ConeIsoInfo05", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBs2MMInfo", 
                "Type": "RelInfoBs2MuMuTrackIsolations"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "TrackIsoInfo10", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "TrackIsoInfo15", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "TrackIsoInfo20", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 1.0, 
                "Location": "ConeIsoInfo10", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 1.5, 
                "Location": "ConeIsoInfo15", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 2.0, 
                "Location": "ConeIsoInfo20", 
                "Type": "RelInfoConeIsolation"
            }
        ]
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2MuMuMuMu = {
    "BUILDERTYPE": "B2MuMuMuMuLinesConf", 
    "CONFIG": {
        "B23MuPiLinePostscale": 1, 
        "B23MuPiLinePrescale": 0, 
        "B2DetachedDiMuons": {
            "BPVDIRA_MIN": 0.0, 
            "BPVIPCHI2_MAX": 16, 
            "BPVVDCHI2_MIN": 50, 
            "MASS_MAX": {
                "B": "6000*MeV"
            }, 
            "MASS_MIN": {
                "B": "4600*MeV"
            }, 
            "SUMPT_MIN": "2000*MeV", 
            "VCHI2DOF_MAX": 6
        }, 
        "B2DetachedDimuonAndJpsiLinePostscale": 1, 
        "B2DetachedDimuonAndJpsiLinePrescale": 1, 
        "B2JpsiKmumuLinePostscale": 1, 
        "B2JpsiKmumuLinePrescale": 1, 
        "B2JpsiPhimumuLinePostscale": 1, 
        "B2JpsiPhimumuLinePrescale": 1, 
        "B2MuMuMuMuLinePostscale": 1, 
        "B2MuMuMuMuLinePrescale": 0, 
        "B2TwoDetachedDimuonLinePostscale": 1, 
        "B2TwoDetachedDimuonLinePrescale": 0, 
        "D2MuMuMuMuLinePostscale": 1, 
        "D2MuMuMuMuLinePrescale": 0, 
        "DetachedDiMuons": {
            "AMAXDOCA_MAX": "0.5*mm", 
            "ASUMPT_MIN": "1000*MeV", 
            "BPVVDCHI2_MIN": 16, 
            "VCHI2DOF_MAX": 16
        }
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

B2OCMuMu = {
    "BUILDERTYPE": "B2OCMuMuConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFlightCHI2": 50, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BVertexCHI2": 12, 
        "BVertexCHI2Loose": 20, 
        "B_MassWindow_Hi": 5200, 
        "BcFlightCHI2": 25, 
        "Bc_MassWindow_Hi": 6200, 
        "Bu2mmLinePrescale": 1, 
        "DiHadronADOCA": 0.75, 
        "DiHadronADOCACHI2": 30, 
        "DiHadronVtxCHI2": 10, 
        "K1_SumIPChi2Had": 48.0, 
        "KaonIPCHI2": 9, 
        "KaonPT": 250, 
        "Lb_MassWindow_Hi": 5600, 
        "LeptonIPCHI2": 9, 
        "MinProbNN": 0.02, 
        "MinProbNNTight": 0.1, 
        "PTHad": 1000, 
        "Trk_Chi2": 3, 
        "Trk_GhostProb": 0.3
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2XMuMu = {
    "BUILDERTYPE": "B2XMuMuConf", 
    "CONFIG": {
        "A1_Comb_MassHigh": 4200.0, 
        "A1_Comb_MassLow": 0.0, 
        "A1_Dau_MaxIPCHI2": 16.0, 
        "A1_FlightChi2": 36.0, 
        "A1_MassHigh": 4000.0, 
        "A1_MassLow": 0.0, 
        "A1_MinIPCHI2": 4.0, 
        "A1_VtxChi2": 8.0, 
        "B_Comb_MassHigh": 7100.0, 
        "B_Comb_MassLow": 4600.0, 
        "B_DIRA": 0.9999, 
        "B_Dau_MaxIPCHI2": 9.0, 
        "B_FlightCHI2": 64.0, 
        "B_IPCHI2": 16.0, 
        "B_MassHigh": 7000.0, 
        "B_MassLow": 4700.0, 
        "B_VertexCHI2": 8.0, 
        "DECAYS": [
            "B0 -> J/psi(1S) phi(1020)", 
            "[B0 -> J/psi(1S) K*(892)0]cc", 
            "B0 -> J/psi(1S) rho(770)0", 
            "[B+ -> J/psi(1S) rho(770)+]cc", 
            "B0 -> J/psi(1S) f_2(1950)", 
            "B0 -> J/psi(1S) KS0", 
            "[B0 -> J/psi(1S) D~0]cc", 
            "[B+ -> J/psi(1S) K+]cc", 
            "[B+ -> J/psi(1S) pi+]cc", 
            "[B+ -> J/psi(1S) K*(892)+]cc", 
            "[B+ -> J/psi(1S) D+]cc", 
            "[B+ -> J/psi(1S) D*(2010)+]cc", 
            "[Lambda_b0 -> J/psi(1S) Lambda0]cc", 
            "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc", 
            "B0 -> J/psi(1S) pi0", 
            "[B+ -> J/psi(1S) a_1(1260)+]cc", 
            "[B+ -> J/psi(1S) K_1(1270)+]cc", 
            "[B+ -> J/psi(1S) K_2(1770)+]cc", 
            "B0 -> J/psi(1S) K_1(1270)0", 
            "[B+ -> J/psi(1S) K_1(1400)+]cc", 
            "B0 -> J/psi(1S) K_1(1400)0", 
            "[Xi_b- -> J/psi(1S) Xi-]cc", 
            "[Omega_b- -> J/psi(1S) Omega-]cc", 
            "B0 -> J/psi(1S) f_1(1285)", 
            "B0 -> J/psi(1S) omega(782)"
        ], 
        "Dau_DIRA": -0.9, 
        "Dau_VertexCHI2": 12.0, 
        "Dimu_Dau_MaxIPCHI2": 6.0, 
        "Dimu_FlightChi2": 9.0, 
        "DimuonUPPERMASS": 7100.0, 
        "DimuonWS": True, 
        "HLT1_FILTER": None, 
        "HLT2_FILTER": None, 
        "HadronWS": False, 
        "Hadron_MinIPCHI2": 6.0, 
        "HyperonCombWindow": 65.0, 
        "HyperonMaxDocaChi2": 20.0, 
        "HyperonWindow": 50.0, 
        "K12OmegaK_CombMassHigh": 2000, 
        "K12OmegaK_CombMassLow": 400, 
        "K12OmegaK_MassHigh": 2100, 
        "K12OmegaK_MassLow": 300, 
        "K12OmegaK_VtxChi2": 8.0, 
        "KpiVXCHI2NDOF": 8.0, 
        "KsWINDOW": 30.0, 
        "Kstar_Comb_MassHigh": 6200.0, 
        "Kstar_Comb_MassLow": 0.0, 
        "Kstar_Dau_MaxIPCHI2": 6.0, 
        "Kstar_FlightChi2": 16.0, 
        "Kstar_MassHigh": 6200.0, 
        "Kstar_MassLow": 0.0, 
        "Kstar_MinIPCHI2": 0.0, 
        "KstarplusWINDOW": 300.0, 
        "L0DU_FILTER": None, 
        "LambdaWINDOW": 30.0, 
        "LongLivedPT": 0.0, 
        "LongLivedTau": 2, 
        "MuonNoPIDs_PIDmu": 0.0, 
        "MuonPID": -3.0, 
        "Muon_IsMuon": True, 
        "Muon_MinIPCHI2": 6.0, 
        "OmegaChi2Prob": 1e-05, 
        "Omega_CombMassWin": 400, 
        "Omega_MassWin": 400, 
        "Pi0ForOmegaMINPT": 800.0, 
        "Pi0MINPT": 800.0, 
        "RelatedInfoTools": [
            {
                "Location": "KSTARMUMUVARIABLES", 
                "Type": "RelInfoBKstarMuMuBDT", 
                "Variables": [
                    "MU_SLL_ISO_1", 
                    "MU_SLL_ISO_2"
                ]
            }, 
            {
                "Location": "KSTARMUMUVARIABLES2", 
                "Type": "RelInfoBKstarMuMuHad", 
                "Variables": [
                    "K_SLL_ISO_HAD", 
                    "PI_SLL_ISO_HAD"
                ]
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM", 
                    "CONEPT", 
                    "CONEP", 
                    "CONEPASYM", 
                    "CONEDELTAETA", 
                    "CONEDELTAPHI"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation", 
                "Variables": [
                    "VTXISONUMVTX", 
                    "VTXISODCHI2ONETRACK", 
                    "VTXISODCHI2MASSONETRACK", 
                    "VTXISODCHI2TWOTRACK", 
                    "VTXISODCHI2MASSTWOTRACK"
                ]
            }, 
            {
                "Location": "VtxIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Particles": [
                    1, 
                    2
                ], 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "SpdMult": 600, 
        "Track_GhostProb": 0.5, 
        "UseNoPIDsHadrons": True
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2XMuTauMuonic = {
    "BUILDERTYPE": "B2XMuTauMuonicConf", 
    "CONFIG": {
        "B0_VertexCHI2": 50.0, 
        "B_DIRA": 0.999, 
        "Bd_Comb_MassHigh": 6750.0, 
        "Bd_FlightChi2": 25, 
        "Bs_Comb_MassHigh": 7250.0, 
        "Bs_FlightChi2": 16, 
        "Bs_VertexCHI2": 100.0, 
        "HLT1_FILTER": None, 
        "HLT2_FILTER": None, 
        "Hadron_MinIPCHI2": 9.0, 
        "Hadron_P": 3000, 
        "KaonPID": 4.0, 
        "Kaon_Pion_ProbNN_B2Kstar": 0.8, 
        "KstarMu_Comb_MassHigh": 3550.0, 
        "KstarMu_DOCA": 0.6, 
        "KstarMu_DOCACHI2": 6, 
        "KstarMu_VertexCHI2": 6, 
        "Kstar_Comb_MassHigh": 3550.0, 
        "Kstar_Comb_MassLow": 795.0, 
        "Kstar_DOCA": 0.12, 
        "Kstar_DOCACHI2": 3, 
        "Kstar_FlightChi2": 150.0, 
        "Kstar_PT": 1100, 
        "Kstar_VertexCHI2": 3, 
        "L0DU_FILTER": None, 
        "LambdastarMu_Comb_MassHigh": 5000.0, 
        "LambdastarMu_DOCA": 0.8, 
        "LambdastarMu_DOCACHI2": 8, 
        "LambdastarMu_VertexCHI2": 8, 
        "Lambdastar_Comb_MassHigh": 5000.0, 
        "Lambdastar_Comb_MassLow": 1400.0, 
        "Lambdastar_DOCA": 0.15, 
        "Lambdastar_DOCACHI2": 4, 
        "Lambdastar_FlightChi2": 25, 
        "Lambdastar_PT": 1250, 
        "Lambdastar_VertexCHI2": 4, 
        "Lb_Comb_MassHigh": 7750.0, 
        "Lb_DIRA": 0.999, 
        "Lb_FlightChi2": 16, 
        "Lb_VertexCHI2": 100.0, 
        "MuonPID": 0.0, 
        "MuonPT": 500, 
        "Muon_MinIPCHI2": 9.0, 
        "PhiMu_Comb_MassHigh": 3600.0, 
        "PhiMu_DOCA": 0.8, 
        "PhiMu_DOCACHI2": 9, 
        "PhiMu_VertexCHI2": 9, 
        "Phi_Comb_MassHigh": 3600.0, 
        "Phi_DOCA": 0.15, 
        "Phi_DOCACHI2": 4, 
        "Phi_FlightChi2": 25.0, 
        "Phi_PT": 800, 
        "Phi_VertexCHI2": 4, 
        "Pion_ProbNN": 0.3, 
        "Pion_ProbNN_B2Kstar": 0.8, 
        "ProtonPID": 5.0, 
        "SpdMult": 600, 
        "Track_GhostProb": 0.3, 
        "Track_TRCHI2": 3, 
        "UseNoPIDsHadrons": False, 
        "UseNoPIDsMuons": False
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

B2XTauTauMuonic = {
    "BUILDERTYPE": "B2XTauTauMuonicConf", 
    "CONFIG": {
        "B0_VertexCHI2": 100.0, 
        "B_DIRA": 0.999, 
        "Bd_Comb_MassHigh": 6500.0, 
        "Bd_VertDist": -5, 
        "Bs_Comb_MassHigh": 7000.0, 
        "Bs_VertDist": -24, 
        "Bs_VertexCHI2": 150.0, 
        "Dau_DIRA": 0.99, 
        "DimuonUPPERMASS": 4350.0, 
        "Dimuon_DIRA": 0.0, 
        "Dimuon_VertexCHI2": 20, 
        "HLT1_FILTER": None, 
        "HLT2_FILTER": None, 
        "Hadron_MinIPCHI2": 16.0, 
        "Hadron_P": 3000, 
        "KaonPID": 4.0, 
        "KaonPID_B2Kstar": 5.0, 
        "Kaon_Pion_ProbNN_B2Kstar": 0.8, 
        "KstarKstar_Comb_MassHigh": 2250,
        "KstarKstar_DOCACHI2": 25, 
        "Kstar_for_B2KstarKstar_MassWindow": 100, 
        "Kstar_for_B2KstarKstar_PT": 600, 
        "Kstar_for_B2KstarKstar_VertexCHI2": 6, 
        "Kstar_for_B2Kstar_Comb_MassHigh": 1750.0, 
        "Kstar_for_B2Kstar_Comb_MassLow": 795.0, 
        "Kstar_for_B2Kstar_FlightChi2": 150.0, 
        "Kstar_for_B2Kstar_PT": 1500, 
        "Kstar_for_B2Kstar_VertexCHI2": 3, 
        "L0DU_FILTER": None, 
        "Lambdastar_Comb_MassHigh": 5000.0, 
        "Lambdastar_Comb_MassLow": 1400.0, 
        "Lambdastar_FlightChi2": 25, 
        "Lambdastar_PT": 1500, 
        "Lambdastar_VertexCHI2": 3, 
        "Lb_Comb_MassHigh": 7500.0, 
        "Lb_DIRA": 0.999, 
        "Lb_VertDist": -25, 
        "Lb_VertexCHI2": 150.0, 
        "MuonPID": 0.0, 
        "Muon_MinIPCHI2": 9.0, 
        "Phi_Comb_MassHigh": 1850.0, 
        "Phi_FlightChi2": 16.0, 
        "Phi_PT": 600, 
        "Phi_VertexCHI2": 4, 
        "Pion_ProbNN": 0.3, 
        "Pion_ProbNN_B2Kstar": 0.8, 
        "ProtonPID": 5.0, 
        "SpdMult": 600, 
        "Track_GhostProb": 0.3, 
        "Track_TRCHI2": 3, 
        "UseNoPIDsHadrons": False, 
        "UseNoPIDsMuons": False
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

BaryonicLFV = {
    "BUILDERTYPE": "BaryonicLFVConf", 
    "CONFIG": {
        "BaryLFVPostscale": 1.0, 
        "BaryLFVPrescale": 1.0, 
        "DiHadronADOCACHI2": 30, 
        "DiHadronMassLoose": 6000, 
        "DiHadronMassTight": 2600, 
        "DiHadronVtxCHI2": 25, 
        "HadronIPCHI2": 9, 
        "PTCut": 250, 
        "ProbNNCutTight": 0.1, 
        "ProtonP": 5000, 
        "TrackGhostProb": 0.4
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingBaryonicLFVXib22MuTauLine", 
            "StrippingBaryonicLFVXib22ETauLine", 
            "StrippingBaryonicLFVXib2EMuTauLine", 
            "StrippingBaryonicLFVXib23MuLine", 
            "StrippingBaryonicLFVXib25MuLine", 
            "StrippingBaryonicLFVXib22MuELine", 
            "StrippingBaryonicLFVppmumuSSLine", 
            "StrippingBaryonicLFV3p3muSSLine", 
            "StrippingBaryonicLFVDelta3muSSLine", 
            "StrippingBaryonicLFVpipi3muSSLine", 
            "StrippingBaryonicLFVpipipi3muSSLine"
        ], 
        "Leptonic": [ "StrippingBaryonicLFVXib23MuLooseLine" ]
    }, 
    "WGs": [ "RD" ]
}

Beauty2MajoLep = {
    "BUILDERTYPE": "Beauty2MajoLepConf", 
    "CONFIG": {
        "AM_Mhigh": 7200.0, 
        "AM_Mlow": 0.0, 
        "BVtxChi2": 10.0, 
        "B_Bach_PT_DD": 1000.0, 
        "B_DLS": 5.0, 
        "B_IPCHI2wrtPV": 16.0, 
        "B_Mhigh": 6800.0, 
        "B_Mlow": 4800.0, 
        "B_electron_pdg": 500.0, 
        "B_pdg": 300.0, 
        "B_pdg_Ks_DD": 200.0, 
        "Bach_PTmin": 700.0, 
        "D0_pdg": 100.0, 
        "Jpsi_pdg": 100.0, 
        "Ks_Bach_PT_DD": 800.0, 
        "Ks_pdg": 100.0, 
        "Ks_pdg_DD": 50.0, 
        "Lep_IPChi2min": 25.0, 
        "Lep_PTmin": 300.0, 
        "Majo_AMhigh": 7000.0, 
        "Majo_AMlow": 200.0, 
        "Majo_Bach_PTmin": 500.0, 
        "Majo_DocaChi2": 25.0, 
        "Majo_PTmin": 700.0, 
        "Majo_VtxChi2": 10.0, 
        "Postscale": 1.0, 
        "Prescale1": 0.05, 
        "Prescale2": 0.1, 
        "Prescale3": 0.2, 
        "RequiredRawEvents": [
            "Trigger", 
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker"
        ], 
        "Trk_GhostProb": 0.5
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "RD" ]
}

Beauty2XGammaExclTDCPV = {
    "BUILDERTYPE": "Beauty2XGammaExclTDCPVConf", 
    "CONFIG": {
        "B0PVIPchi2": 16.0, 
        "B0VTXchi2": 16.0, 
        "BMassMax": 7000.0, 
        "BMassMin": 4000.0, 
        "B_APT": 3000.0, 
        "B_PT": 2500.0, 
        "Bd2KspipiGammaPostScale": 1.0, 
        "Bd2KspipiGammaPreScale": 0.0, 
        "Bd2KstGammaPostScale": 1.0, 
        "Bd2KstGammaPreScale": 0.0, 
        "BdDIRA": 0.2, 
        "Bs2KsKpiGammaPostScale": 1.0, 
        "Bs2KsKpiGammaPreScale": 0.0, 
        "Bs2PhiGammaPostScale": 1.0, 
        "Bs2PhiGammaPreScale": 0.0, 
        "BsPVIPchi2": 16.0, 
        "BsVTXchi2": 16.0, 
        "DTF_CL": 1e-10, 
        "DTF_CL_tighter": 1e-05, 
        "GhostProb_Max": 0.6, 
        "Hlt1TISTOSLinesDict": {
            "Hlt1(Two)?TrackMVA(Loose)?Decision%TOS": 0
        }, 
        "Hlt1TISTOSLinesDict_Phi": {
            "Hlt1(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt1(Two)?TrackMVA(Loose)?Decision%TOS": 0, 
            "Hlt1B2PhiGamma_LTUNBDecision%TOS": 0
        }, 
        "Hlt2TISTOSLinesDict": {
            "Hlt2Radiative.*Decision%TOS": 0, 
            "Hlt2Topo(2|3|4)Body.*Decision%TOS": 0
        }, 
        "Hlt2TISTOSLinesDict_Phi": {
            "Hlt2(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt2Radiative.*Decision%TOS": 0, 
            "Hlt2Topo(2|3|4)Body.*Decision%TOS": 0
        }, 
        "KsPT": 300.0, 
        "KstMassWin": 100.0, 
        "KstVCHI2": 25.0, 
        "L1520MassWin": 200.0, 
        "Lb2pKGammaPostScale": 1.0, 
        "Lb2pKGammaPreScale": 1.0, 
        "MinProton_P": 4000.0, 
        "MinProton_PT": 300.0, 
        "MinTrack_P": 3000.0, 
        "MinTrack_PT": 250.0, 
        "PhiMassWin": 15.0, 
        "PhiVCHI2": 25.0, 
        "ProbNNk": 0.05, 
        "ProbNNk_tight": 0.1, 
        "ProbNNp": 0.05, 
        "ProbNNp_tight": 0.25, 
        "ProbNNpi": 0.05, 
        "ProbNNpi_tight": 0.4, 
        "SumVec_PT": 500.0, 
        "TrChi2": 4.0, 
        "TrIPchi2": 9.0, 
        "TrIPchi2_tight": 11.0, 
        "mKPhiMax": 4000.0, 
        "mKPhiMax_tight": 2950.0, 
        "mKsKpiMax": 2300.0, 
        "mKspipiMax": 2000.0, 
        "mKsppiMax": 2750.0, 
        "photonPT": 2500.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "RD" ]
}

Bu2LLK = {
    "BUILDERTYPE": "Bu2LLKConf", 
    "CONFIG": {
        "B2K3pill_BFDChi2": 150, 
        "B2K3pill_BFDIRA": 0.9999, 
        "B2K3pill_BIPDChi2": 15, 
        "B2K3pill_BVtxChi2": 40, 
        "B2K3pill_DiLeptonFDChi2": 25, 
        "B2K3pill_DiLeptonPT": 250, 
        "B2K3pill_DiLeptonVtxChi2": 10, 
        "B2K3pill_HadIPCHI2": 4.5, 
        "B2K3pill_KaonPT": 250, 
        "B2K3pill_LeptonIPCHI2": 1, 
        "B2K3pill_LeptonIPCHI2_Max": 7, 
        "B2K3pill_LeptonPT": 150, 
        "B2K3pill_MassWindow_Hi": 6500, 
        "B2K3pill_PionPT": 150, 
        "B2K3pill_achi2doca": 30, 
        "B2KpipipieeLinePrescale": 1, 
        "B2KpipipimmLinePrescale": 1, 
        "BDIRA": 0.9995, 
        "BFlightCHI2": 100, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BMassWindowTau": 5000, 
        "BVertexCHI2": 9, 
        "Bu2eeLine2Prescale": 0, 
        "Bu2eeLine3Prescale": 0, 
        "Bu2eeLine4Prescale": 0, 
        "Bu2eeLinePrescale": 0, 
        "Bu2eeSSLine2Prescale": 1, 
        "Bu2meLinePrescale": 0, 
        "Bu2meSSLinePrescale": 1, 
        "Bu2mmLinePrescale": 0, 
        "Bu2mmSSLinePrescale": 1, 
        "Bu2mtLinePrescale": 0, 
        "Bu2mtSSLinePrescale": 1, 
        "DiHadronADOCACHI2": 30, 
        "DiHadronMass": 2600, 
        "DiHadronVtxCHI2": 25, 
        "DiLeptonFDCHI2": 16, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "K1_MassWindow_Hi": 4200, 
        "K1_MassWindow_Lo": 0, 
        "K1_SumIPChi2Had": 48.0, 
        "K1_SumPTHad": 1200, 
        "K1_VtxChi2": 12, 
        "KaonIPCHI2": 9, 
        "KaonPT": 400, 
        "KaonPTLoose": 250, 
        "KstarPADOCACHI2": 30, 
        "KstarPMassWindow": 300, 
        "KstarPVertexCHI2": 25, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 350, 
        "LeptonPTTight": 500, 
        "PIDe": 0, 
        "Pi0PT": 600, 
        "PionPTRho": 350, 
        "ProbNNCut": 0.05, 
        "ProbNNCutTight": 0.1, 
        "ProtonP": 2000, 
        "RICHPIDe_Up": -5, 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l-  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X  (X ->  l+  ^(l+ -> X+  X-  X+))]CC": "VertexIsoInfoL", 
                    "[Beauty ->  X  (X ->  l-  ^(l- -> X-  X-  X+))]CC": "VertexIsoInfoL", 
                    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC": "VertexIsoInfoL", 
                    "[Beauty ->  X+ ^(X ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X+ ^(X ->  l-  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> (X+ -> X+  X0) ^(X ->  l+  l+)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> (X+ -> X+  X0) ^(X ->  l-  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> K+ X- X+ X- ^(X0 ->  l+  l-)]CC": "VertexIsoInfoLL", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l+  l+)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l-  l-)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC": "VertexIsoInfoH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X ->  l+  l+)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X ->  l-  l-)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l-)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l+  l+)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l-  l-)]CC": "VertexIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l-  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X  (X ->  l+  ^(l+ -> X+  X-  X+))]CC": "VertexIsoBDTInfoL", 
                    "[Beauty ->  X  (X ->  l-  ^(l- -> X-  X-  X+))]CC": "VertexIsoBDTInfoL", 
                    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC": "VertexIsoBDTInfoL", 
                    "[Beauty ->  X+ ^(X ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X ->  l-  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> (X+ -> X+  X0) ^(X ->  l+  l+)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> (X+ -> X+  X0) ^(X ->  l-  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> K+ X- X+ X- ^(X0 ->  l+  l-)]CC": "VertexIsoBDTInfoLL", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l+  l+)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X ->  l-  l-)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X ->  l+  l+)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X ->  l-  l-)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l+  l+)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l-  l-)]CC": "VertexIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "VertexIsoBDTInfoHH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X-  X- ^X+))]CC": "TrackIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X- ^X-  X+))]CC": "TrackIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- -> ^X-  X-  X+))]CC": "TrackIsoInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty ->  X+  (X ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X ->  l- ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+  (X -> ^l-  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  X+ ^(X ->  l+  l+)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  X+ ^(X ->  l-  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l+)]CC": "TrackIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l-)]CC": "TrackIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l-  l-)]CC": "TrackIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "TrackIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "TrackIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l-  l-)]CC": "TrackIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "TrackIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "TrackIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l-  ^l-)]CC": "TrackIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "TrackIsoInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "TrackIsoInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l-  l-)]CC": "TrackIsoInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l- ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l-  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l+  l+)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l-  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l-  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l+  l+)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l-  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l-  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l-  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  l+  ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> K+ X- X+ ^X- (X0 ->  l+  l-)]CC": "TrackIsoInfoH4", 
                    "[Beauty -> K+ X- ^X+ X- (X0 ->  l+  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> K+ ^X- X+ X- (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X ->  l+  l+)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoInfoHH", 
                    "[Beauty -> ^K+ X- X+ X- (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> ^X+  (X ->  l+  l+)]CC": "TrackIsoInfoH", 
                    "[Beauty -> ^X+  (X ->  l-  l-)]CC": "TrackIsoInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoInfo05", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+  X- ^X+))]CC": "ConeIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+ ^X-  X+))]CC": "ConeIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ -> ^X+  X-  X+))]CC": "ConeIsoInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X-  X- ^X+))]CC": "ConeIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X- ^X-  X+))]CC": "ConeIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- -> ^X-  X-  X+))]CC": "ConeIsoInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "ConeIsoInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "ConeIsoInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "ConeIsoInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty ->  X+  (X ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X ->  l- ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+  (X -> ^l-  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  X+ ^(X ->  l+  l+)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  X+ ^(X ->  l-  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "ConeIsoInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l+)]CC": "ConeIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l+  l-)]CC": "ConeIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  ^pi0) (X0 ->  l-  l-)]CC": "ConeIsoInfopi0H2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "ConeIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "ConeIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l-  l-)]CC": "ConeIsoInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "ConeIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "ConeIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l-  ^l-)]CC": "ConeIsoInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "ConeIsoInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "ConeIsoInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l-  l-)]CC": "ConeIsoInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l- ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l-  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l+  l+)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l-  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l-  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l+  l+)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l-  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l-  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l-  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  l+  ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> K+ X- X+ ^X- (X0 ->  l+  l-)]CC": "ConeIsoInfoH4", 
                    "[Beauty -> K+ X- ^X+ X- (X0 ->  l+  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> K+ ^X- X+ X- (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X ->  l+  l+)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "ConeIsoInfoHH", 
                    "[Beauty -> ^K+ X- X+ X- (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> ^X+  (X ->  l+  l+)]CC": "ConeIsoInfoH", 
                    "[Beauty -> ^X+  (X ->  l-  l-)]CC": "ConeIsoInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "ConeIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "ConeIsoInfo05", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoBDTInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoBDTInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoBDTInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X-  X- ^X+))]CC": "TrackIsoBDTInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X- ^X-  X+))]CC": "TrackIsoBDTInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- -> ^X-  X-  X+))]CC": "TrackIsoBDTInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoBDTInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoBDTInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoBDTInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty ->  X+  (X ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  X+  (X ->  l- ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  X+  (X -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  X+  (X -> ^l-  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  X+ ^(X ->  l+  l+)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X ->  l-  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoBDTInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "TrackIsoBDTInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "TrackIsoBDTInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l-  l-)]CC": "TrackIsoBDTInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "TrackIsoBDTInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "TrackIsoBDTInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l-  ^l-)]CC": "TrackIsoBDTInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "TrackIsoBDTInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l-  l-)]CC": "TrackIsoBDTInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l- ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l-  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l+  l+)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l-  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l-  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l+  l+)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l-  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l-  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l-  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  l+  ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> K+ X- X+ ^X- (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH4", 
                    "[Beauty -> K+ X- ^X+ X- (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> K+ ^X- X+ X- (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoHH", 
                    "[Beauty -> ^K+ X- X+ X- (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> ^X+  (X ->  l+  l+)]CC": "TrackIsoBDTInfoH", 
                    "[Beauty -> ^X+  (X ->  l-  l-)]CC": "TrackIsoBDTInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBDTInfo", 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoBs2MMInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoBs2MMInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoBs2MMInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X-  X- ^X+))]CC": "TrackIsoBs2MMInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- ->  X- ^X-  X+))]CC": "TrackIsoBs2MMInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X ->  l-  (l- -> ^X-  X-  X+))]CC": "TrackIsoBs2MMInfoL21", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoBs2MMInfoL23", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoBs2MMInfoL22", 
                    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoBs2MMInfoL21", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty ->  X+  (X ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X ->  l- ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+  (X -> ^l-  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  X+ ^(X ->  l+  l+)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  X+ ^(X ->  l-  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoLL", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "TrackIsoBs2MMInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "TrackIsoBs2MMInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l-  l-)]CC": "TrackIsoBs2MMInfopi0L1", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "TrackIsoBs2MMInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "TrackIsoBs2MMInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l-  ^l-)]CC": "TrackIsoBs2MMInfopi0L2", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfopi0H1", 
                    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l-  l-)]CC": "TrackIsoBs2MMInfopi0H1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X ->  l- ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X -> ^l-  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> K+ X- X+ X- (X0 ->  l+  ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> K+ X- X+ ^X- (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH4", 
                    "[Beauty -> K+ X- ^X+ X- (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> K+ ^X- X+ X- (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoHH", 
                    "[Beauty -> ^K+ X- X+ X- (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> ^X+  (X ->  l+  l+)]CC": "TrackIsoBs2MMInfoH", 
                    "[Beauty -> ^X+  (X ->  l-  l-)]CC": "TrackIsoBs2MMInfoH", 
                    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Location": "TrackIsoBs2MMInfo", 
                "Type": "RelInfoBs2MuMuTrackIsolations"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "TrackIsoInfo10", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "TrackIsoInfo15", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "TrackIsoInfo20", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 1.0, 
                "Location": "ConeIsoInfo10", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 1.5, 
                "Location": "ConeIsoInfo15", 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeSize": 2.0, 
                "Location": "ConeIsoInfo20", 
                "Type": "RelInfoConeIsolation"
            }
        ], 
        "TauPT": 0, 
        "TauVCHI2DOF": 150, 
        "Trk_Chi2": 3, 
        "Trk_GhostProb": 0.35, 
        "UpperBMass": 5280, 
        "UpperBsMass": 5367, 
        "UpperLbMass": 5620, 
        "UpperMass": 5500
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Kshort2Leptons = {
    "BUILDERTYPE": "Kshort2LeptonsConf", 
    "CONFIG": {
        "2mu2e": {
            "DD": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LD": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LL": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 1500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 40, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 1000, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 25, 
                "MaxKsMass": 878.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 1500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 30, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "2mu2pi": {
            "DD": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LD": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LL": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 37, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2000, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 19, 
                "MaxKsMass": 878.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 30, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "2pi2e": {
            "DD": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LD": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LL": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 37, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2000, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 19, 
                "MaxKsMass": 878.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 30, 
                "MaxKsMass": 900.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "3emu": {
            "DD": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LD": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LL": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 30, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "3mue": {
            "DD": {
                "KsDisChi2": 50, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LD": {
                "KsDisChi2": 50, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LL": {
                "KsDisChi2": 50, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 30, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "4e": {
            "DD": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LD": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LL": {
                "KsIP": 5.0, 
                "KsLifetime": 0.8953, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 37, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 2500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 2000, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 20, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 2500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 30.0, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "4mu": {
            "DD": {
                "KsDisChi2": 1500.0, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LD": {
                "KsDisChi2": 1500.0, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 1000.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LL": {
                "KsDisChi2": 1500.0, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LU": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "LV": {
                "KsDisChi2": 1500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 35, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UU": {
                "KsDisChi2": 1500, 
                "KsIP": 5.0, 
                "KsMAXDOCA": 3.0, 
                "KsVtxChi2": 100, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "UV": {
                "KsDisChi2": 1500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 1.0, 
                "KsVtxChi2": 19, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }, 
            "VV": {
                "KsDisChi2": 1500, 
                "KsIP": 1.0, 
                "KsMAXDOCA": 0.8, 
                "KsVtxChi2": 30, 
                "MaxKsMass": 800.0, 
                "Postscale": 1, 
                "Prescale": 1
            }
        }, 
        "TISTOSDict": {
            "Hlt2RareStrangeKsLeptonsTOSDecision%TOS": 0
        }, 
        "e": {
            "D": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 16, 
                "PIDe": -3.5, 
                "PT": 50
            }, 
            "Di": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 16, 
                "PIDe": -4, 
                "PT": 100.0
            }, 
            "L": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 16, 
                "PIDe": -1, 
                "PT": 100.0
            }, 
            "U": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDe": -3.5, 
                "PT": 50
            }, 
            "V": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 40, 
                "PIDe": -1000, 
                "PT": 0
            }
        }, 
        "mu": {
            "D": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDmu": -5, 
                "PT": 50
            }, 
            "L": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 10, 
                "PIDmu": -5, 
                "PT": 50.0
            }, 
            "U": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDmu": -5, 
                "PT": 50
            }, 
            "V": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 16, 
                "PIDmu": -3, 
                "PT": 50
            }
        }, 
        "pi": {
            "D": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 16, 
                "PIDK": 5, 
                "PT": 50
            }, 
            "L": {
                "GhostProb": 0.5, 
                "MINIPCHI2": 16, 
                "PIDK": 5, 
                "PT": 100.0
            }, 
            "U": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDK": 5, 
                "PT": 50
            }, 
            "V": {
                "GhostProb": 0.35, 
                "MINIPCHI2": 10, 
                "PIDK": 3, 
                "PT": 50
            }
        }
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

LFV = {
    "BUILDERTYPE": "LFVLinesConf", 
    "CONFIG": {
        "B2TauMuPrescale": 0, 
        "B2eMuPrescale": 0, 
        "B2eePrescale": 0, 
        "B2hTauMuPrescale": 0, 
        "B2heMuPrescale": 0, 
        "B2pMuPrescale": 1, 
        "Bu2KJPsieePrescale": 0, 
        "D2piphi2MuMuPrescale": 0, 
        "D2piphi2MuMuPromptPrescale": 0, 
        "D2piphi2eMuPrescale": 0, 
        "D2piphi2eMuPromptPrescale": 0., 
        "D2piphi2eePrescale": 0, 
        "D2piphi2eePromptPrescale": 0, 
        "JPsi2MuMuControlPrescale": 0, 
        "JPsi2eMuPrescale": 0, 
        "JPsi2eeControlPrescale": 0., 
        "Phi2MuMuControlPrescale": 0, 
        "Phi2eMuPrescale": 0, 
        "Phi2eeControlPrescale": 0, 
        "Postscale": 1, 
        "PromptJPsi2MuMuControlPrescale": 0., 
        "PromptJPsi2eMuPrescale": 0, 
        "PromptJPsi2eeControlPrescale": 0., 
        "PromptPhi2MuMuControlPrescale": 0, 
        "PromptPhi2eMuPrescale": 0, 
        "PromptPhi2eeControlPrescale": 0., 
        "RelatedInfoTools_B2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[B_s0 -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [mu-]cc]CC": "Electron_TrackIsoBDT", 
                    "[B_s0 -> e+ ^[mu-]cc]CC": "Muon_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_B2ee": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [e-]cc]CC": "Electron1_ISO", 
                    "[B_s0 -> e+ ^[e-]cc]CC": "Electron2_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [e-]cc]CC": "Electron2_TrackIsoBDT", 
                    "[B_s0 -> e+ ^[e-]cc]CC": "Electron1_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_B2hemu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_ISO", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_ISO", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Particles": [
                    1, 
                    2
                ], 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_B2pmu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> X ^[mu-]cc]CC": "Muon_ISO", 
                    "[B_s0 -> ^X [mu-]cc]CC": "Hadron_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> X ^[mu-]cc]CC": "Muon_TrackIsoBDT", 
                    "[B_s0 -> ^X [mu-]cc]CC": "Hadron_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Bu2KJPsiee": [
            {
                "DaughterLocations": {
                    "[B+ -> ^(J/psi(1S) -> e+ e-) K+]CC": "Jpsi_ISO"
                }, 
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B+ -> (J/psi(1S) -> ^e+ e-) K+]CC": "Electron1_ISO", 
                    "[B+ -> (J/psi(1S) -> e+ ^e-) K+]CC": "Electron2_ISO", 
                    "[B+ -> (J/psi(1S) -> e+ e-) ^K+]CC": "Kplus_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B+ -> (J/psi(1S) -> ^e+ e-) K+]CC": "Electron1_TrackIsoBDT", 
                    "[B+ -> (J/psi(1S) -> e+ ^e-) K+]CC": "Electron2_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Particles": [
                    1, 
                    2
                ], 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_D2piphi": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_ISO", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_ISO", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_ISO", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_ISO"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_TrackIso_BDT6vars", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_TrackIso_BDT6vars", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_TrackIso_BDT6vars", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_TrackIso_BDT6vars"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_TrackIso_BDT9vars", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_TrackIso_BDT9vars", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_TrackIso_BDT9vars", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_TrackIso_BDT9vars"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_JPsi2MuMuControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_JPsi2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_JPsi2eeControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Phi2MuMuControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Phi2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Phi2eeControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Tau2MuEtaPrime": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Tau2PhiMu": [
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu05", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus05", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus05", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi05"
                }, 
                "Location": "coneInfoTau05", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.8, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu08", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus08", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus08", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi08"
                }, 
                "Location": "coneInfoTau08", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.0, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu10", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus10", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus10", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi10"
                }, 
                "Location": "coneInfoTau10", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.2, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu12", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus12", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus12", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi12"
                }, 
                "Location": "coneInfoTau12", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation", 
                "Variables": [
                    "VTXISONUMVTX", 
                    "VTXISODCHI2ONETRACK", 
                    "VTXISODCHI2MASSONETRACK", 
                    "VTXISODCHI2TWOTRACK", 
                    "VTXISODCHI2MASSTWOTRACK"
                ]
            }, 
            {
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "MuonTrackIsoBDTInfo", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "KminusTrackIsoBDTInfo", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "KplusTrackIsoBDTInfo"
                }, 
                "Type": "RelInfoTrackIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Upsilon2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[Upsilon(1S) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIsoBDT", 
                    "[Upsilon(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "RelatedInfoTools_Upsilon2ee": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [e-]cc]CC": "Electron1_ISO", 
                    "[Upsilon(1S) -> e+ ^[e-]cc]CC": "Electron2_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [e-]cc]CC": "Electron1_TrackIsoBDT", 
                    "[Upsilon(1S) -> e+ ^[e-]cc]CC": "Electron2_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }, 
            {
                "Location": "TrackIsolationBDT2", 
                "Type": "RelInfoTrackIsolationBDT2"
            }
        ], 
        "Tau2MuEtaPrimePrescale": 1, 
        "Tau2MuMuePrescale": 1, 
        "TauPrescale": 1, 
        "Upsilon2eMuPrescale": 1, 
        "Upsilon2eePrescale": 0.6, 
        "config_B2eMu": {
            "max_ADAMASS": 1200.0, 
            "max_AMAXDOCA": 0.3, 
            "max_BPVIPCHI2": 25.0, 
            "max_TRCHI2DV": 3.0, 
            "max_TRGHOSTPROB": 0.3, 
            "min_BPVDIRA": 0.0, 
            "min_BPVVDCHI2": 225.0, 
            "min_MIPCHI2DV": 25.0
        }, 
        "config_D2piphi": {
            "comb_cuts": "in_range(1300, AM, 2400) & (AMAXDOCA('') < .3*mm)", 
            "comb_cuts_prompt": "in_range(1300, AM, 2400) & (AMAXDOCA('') < .3*mm)", 
            "mother_cuts": "(VFASPF(VCHI2/VDOF) < 3) & (MM > 1300) & (MM < 2400)& (BPVDIRA > 0)", 
            "mother_cuts_prompt": "(VFASPF(VCHI2/VDOF) < 2.5) & (MM > 1300) & (MM < 2400)& (BPVDIRA > 0) & (BPVVD > 0.3)", 
            "pi_cuts": "(PT>250*MeV) & (TRCHI2DOF < 3.) & (TRGHOSTPROB<.3)", 
            "pi_cuts_prompt": "(PT>250*MeV) & (TRCHI2DOF < 2.5) & (TRGHOSTPROB<.2) & (MIPDV(PRIMARY)>0.025) & (MIPCHI2DV(PRIMARY) < 9)"
        }, 
        "config_JPsi2eMu": {
            "max_ADAMASS": 1000.0, 
            "max_AMAXDOCA": 0.3, 
            "max_JpsiMass": 4096.0, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.3, 
            "max_VtxChi2DoF": 2.5, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_BPVDIRA": 0, 
            "min_BPVVDCHI2": 324.0, 
            "min_BPVVDCHI2_phi": 144.0, 
            "min_JpsiMass": 2200.0, 
            "min_MIPCHI2DV": 36.0, 
            "min_MIPCHI2DV_phi": 25.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 300.0, 
            "min_ProbNN": 0.3, 
            "min_ProbNN_phi2ee": 0.05, 
            "min_ProbNN_phi_e": 0.3, 
            "min_ProbNN_phi_mu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_Phi2ll_forD": {
            "max_ADAMASS": 1000.0, 
            "max_AMAXDOCA": 0.3, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.3, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_BPVDIRA": 0, 
            "min_BPVVDCHI2_phi": 9.0, 
            "min_MIPCHI2DV_phi": 9.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 150.0, 
            "min_ProbNN": 0.3, 
            "min_ProbNN_phi2ee": 0.05, 
            "min_ProbNN_phi_e": 0.3, 
            "min_ProbNN_phi_mu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_PromptJPsi2eMu": {
            "max_AMAXDOCA": 0.3, 
            "max_BPVIPCHI2": 9.0, 
            "max_BPVVDCHI2": 9.0, 
            "max_JpsiMass": 4096.0, 
            "max_MIPCHI2DV": 9.0, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_SPD": 350, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.2, 
            "max_VtxChi2DoF": 2.5, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_JpsiMass": 2200.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 300.0, 
            "min_ProbNN": 0.8, 
            "min_ProbNN_Control": 0.65, 
            "min_ProbNN_phi": 0.8, 
            "min_ProbNN_phi2mumu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_Tau2MuEtaPrime": {
            "config_EtaPrime2pipigamma": {
                "etap_cuts": "(PT > 500*MeV) & (VFASPF(VCHI2/VDOF) < 6.0)", 
                "etap_mass_window": "(ADAMASS('eta') < 80*MeV) | (ADAMASS('eta_prime') < 80*MeV)", 
                "gamma_cuts": "(PT > 300*MeV) & (CL > 0.1)", 
                "piminus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)", 
                "pipi_cuts": "(ACHI2DOCA(1,2)<16)", 
                "piplus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)"
            }, 
            "config_EtaPrime2pipipi": {
                "etap_cuts": "(PT > 500*MeV) & (VFASPF(VCHI2/VDOF) < 6.0)", 
                "etap_mass_window": "(ADAMASS('eta') < 80*MeV) | (ADAMASS('eta_prime') < 80*MeV)", 
                "pi0_cuts": "(PT > 250*MeV)", 
                "piminus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)", 
                "pipi_cuts": "ACHI2DOCA(1,2)<16", 
                "piplus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)"
            }, 
            "muplus_cuts": "(ISLONG) & (TRCHI2DOF < 3 )  & (MIPCHI2DV(PRIMARY) >  9.) & (PT > 300*MeV) & (TRGHOSTPROB < 0.3)", 
            "tau_cuts": "(BPVIPCHI2()< 100) & (VFASPF(VCHI2/VDOF)<6.) & (BPVLTIME()*c_light > 50.*micrometer) & (BPVLTIME()*c_light < 400.*micrometer) & (PT>500*MeV) & (D2DVVD(2) < 80*micrometer)", 
            "tau_mass_window": "(ADAMASS('tau+')<150*MeV)"
        }, 
        "config_Upsilon2eMu": {
            "comb_cuts": "in_range(7250, AM, 11000) & ACUTDOCA(0.3*mm,'')", 
            "e_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDe > 1)", 
            "mu_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDmu > 0)", 
            "upsilon_cuts": "(VFASPF(VCHI2) < 25) & (BPVIPCHI2() < 9) & (BPVVDCHI2 < 25)"
        }, 
        "config_Upsilon2ee": {
            "comb_cuts": "in_range(6000, AM, 11000) & ACUTDOCA(0.3*mm,'')", 
            "e_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDe > 1)", 
            "upsilon_cuts": "(VFASPF(VCHI2) < 25) & (BPVIPCHI2() < 9) & (BPVVDCHI2 < 25)"
        }
    }, 
    "STREAMS": {
        "Leptonic": [
            "StrippingLFVTau2PhiMuLine", 
            "StrippingLFVTau2eMuMuLine", 
            "StrippingLFVB2eMuLine", 
            "StrippingLFVJPsi2eMuLine", 
            "StrippingLFVPromptJPsi2eMuLine", 
            "StrippingLFVJPsi2MuMuControlLine", 
            "StrippingLFVJPsi2eeControlLine", 
            "StrippingLFVPromptJPsi2MuMuControlLine", 
            "StrippingLFVPromptJPsi2eeControlLine", 
            "StrippingLFVPhi2eMuLine", 
            "StrippingLFVPromptPhi2eMuLine", 
            "StrippingLFVPhi2MuMuControlLine", 
            "StrippingLFVPhi2eeControlLine", 
            "StrippingLFVPromptPhi2MuMuControlLine", 
            "StrippingLFVPromptPhi2eeControlLine", 
            "StrippingLFVB2eeLine", 
            "StrippingLFVB2heMuLine", 
            "StrippingLFVB2hMuLine", 
            "StrippingLFVBu2KJPsieeLine", 
            "StrippingLFVB2hTauMuLine", 
            "StrippingLFVTau2MuEtaP2pipigLine", 
            "StrippingLFVTau2MuEtaP2pipipiLine", 
            "StrippingLFVupsilon2eMuLine", 
            "StrippingLFVupsilon2eeLine", 
            "StrippingLFVD2piphi2eeLine", 
            "StrippingLFVD2piphi2eMuLine", 
            "StrippingLFVD2piphi2MuMuLine", 
            "StrippingLFVD2piphi2eePromptLine", 
            "StrippingLFVD2piphi2eMuPromptLine", 
            "StrippingLFVD2piphi2MuMuPromptLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

MultiLepton = {
    "BUILDERTYPE": "MultiLeptonConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFlightCHI2": 36, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BVertexCHI2": 16, 
        "Bu2mmLinePrescale": 1, 
        "DiHadronADOCA": 0.75, 
        "DiLeptonFDCHI2": 10, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "K1_MassWindow_Hi": 6300, 
        "K1_MassWindow_Lo": 0, 
        "K1_SumPTHad": 800, 
        "K1_VtxChi2": 25, 
        "KaonIPCHI2": 9, 
        "KaonPT": 250, 
        "KaonPTTight": 400, 
        "LamLam_VtxChi2": 36, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 300, 
        "Mu_SumIPChi2Had": 12.0, 
        "Trk_Chi2": 3, 
        "Trk_GhostProb": 0.3, 
        "UpperMass": 5500, 
        "V0TAU": 0.0005
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingMultiLepton_B24muXTightLine", 
            "StrippingMultiLepton_B22mu2eXTightLine", 
            "StrippingMultiLepton_InclDet4muLine", 
            "StrippingMultiLepton_InclDet2mu2muLine", 
            "StrippingMultiLepton_InclDet6muLine", 
            "StrippingMultiLepton_InclDet5muLine", 
            "StrippingMultiLepton_InclPrompt6muLine", 
            "StrippingMultiLepton_Incl8muLine", 
            "StrippingMultiLepton_Incl8mu4bodyLine", 
            "StrippingMultiLepton_InclDet6mu3bodyLine", 
            "StrippingMultiLepton_Jpsi24MuPromptTightLine", 
            "StrippingMultiLepton_Jpsi22Mu2EDetachedLine", 
            "StrippingMultiLepton_Jpsi22E2EDetachedLine", 
            "StrippingMultiLepton_Jpsi22Mu2ESSDetachedLine", 
            "StrippingMultiLepton_Incl8mu4bodyLongLivedLine", 
            "StrippingMultiLepton_InclDet6mu3bodyLongLivedLine"
        ], 
        "Leptonic": [
            "StrippingMultiLepton_B24muXLine", 
            "StrippingMultiLepton_B24muXUpLine", 
            "StrippingMultiLepton_Jpsi22Mu2EPromptLine", 
            "StrippingMultiLepton_Jpsi23MuEDetachedLine", 
            "StrippingMultiLepton_Jpsi23MuEPromptLine", 
            "StrippingMultiLepton_Jpsi2Mu3EDetachedLine", 
            "StrippingMultiLepton_Jpsi2Mu3EPromptLine", 
            "StrippingMultiLepton_InclDet4muLowMassLine", 
            "StrippingMultiLepton_InclDet4muLowMassUpLine", 
            "StrippingMultiLepton_Jpsi24MuPromptLine", 
            "StrippingMultiLepton_B24muXLongLivedLine", 
            "StrippingMultiLepton_B26muXLongLivedLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

RareBaryonicMuMu = {
    "BUILDERTYPE": "RareBaryonicMuMuConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BDIRA_PR": 0.999, 
        "BFlightCHI2": 50, 
        "BFlightCHI2Ch": 36, 
        "BIPCHI2": 25, 
        "BIPCHI2_PR": 36, 
        "BMassWindow": 1600, 
        "BVertexCHI2": 12, 
        "BVertexCHI2Loose": 20, 
        "BVtxCHI2_PR": 225, 
        "Bu2mmLinePrescale": 1, 
        "DiHadronADOCA": 0.75, 
        "DiHadronADOCACHI2": 30, 
        "DiHadronADOCACHI2_PR": 225, 
        "DiHadronMass": 3000, 
        "DiHadronVtxCHI2": 25, 
        "DiHadronVtxCHI2_PR": 200, 
        "DiLeptonFDCHI2": 10, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "Hadron_MassWindow_Hi": 6300, 
        "Hadron_MassWindow_Lo": 0, 
        "Hadron_MinIPCHI2": 4.0, 
        "Hadron_VtxChi2": 25, 
        "HyperonCombWindow": 75.0, 
        "HyperonMaxDocaChi2": 25.0, 
        "HyperonWindow": 60.0, 
        "KaonIPCHI2": 9, 
        "KaonPT": 250, 
        "KstarPMassWindow": 300, 
        "KstarPVertexCHI2": 36, 
        "Lambda_MassWindow_Hi": 1130, 
        "Lambda_MassWindow_Lo": 1105, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 250, 
        "MinDLLK": -0.5, 
        "MinDLLp": -0.5, 
        "MinProbNN": 0.02, 
        "MinProbNNTight": 0.1, 
        "OmDIRA_PR": 0.999, 
        "OmegaPR_M_Hi_comb": 1700, 
        "OmegaPR_M_Lo_comb": 1370, 
        "ProtonP": 5000, 
        "Trk_Chi2": 3, 
        "Trk_GhostProb": 0.5, 
        "UpperMass": 6500, 
        "V0PT": 0, 
        "V0TAU": 0.0005, 
        "XiPR_M_Hi_comb": 1350, 
        "XiPR_M_Lo_comb": 1000
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Sb2PKMuXPi = {
    "BUILDERTYPE": "Sb2PKMuXPiConf", 
    "CONFIG": {
        "DMpKJpsiPi": 739.57, 
        "DMpKMuPi": 359.57, 
        "DZBPV": 1.0, 
        "DiHadronADOCACHI2": 30, 
        "DiHadronVtxCHI2": 20, 
        "JpsiMassWindow": 80.0, 
        "KaonIPCHI2": 9.0, 
        "KaonPT": 400.0, 
        "KaonPTLoose": 250.0, 
        "MuMinIPChi2": 9.0, 
        "MuP": 3000.0, 
        "MuPIDmu": 0.0, 
        "MuPT": 1000.0, 
        "PiMinIPChi2": 9.0, 
        "PiPIDK": 16.0, 
        "PiPIDmu": 0.0, 
        "PiPIDp": 16.0, 
        "PiPT": 500.0, 
        "PiPTLoose": 250.0, 
        "ProbNNCut": 0.25, 
        "ProtonP": 2000.0, 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l+) pi+]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l+) pi-]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l-) pi+]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l-) pi-]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l+) ^pi+]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l+) ^pi-]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l-) ^pi+]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l-) ^pi-]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l+) pi+]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l+) pi-]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l-) pi+]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l-) pi-]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l+) pi+]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l+) pi-]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l-) pi+]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l-) pi-]CC": "TrackIsoBs2MMInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l+) ^pi+]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l+) ^pi-]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l-) ^pi+]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l-) ^pi-]CC": "TrackIsoBs2MMInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l+) pi+]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l+) pi-]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l-) pi+]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l-) pi-]CC": "TrackIsoBs2MMInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l+) pi+]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l+) pi-]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l-) pi+]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l-) pi-]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l+) pi+]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l+) pi-]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l-) pi+]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l-) pi-]CC": "TrackIsoBs2MMInfo_p", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l+) pi+]CC": "TrackIsoBs2MMInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l+) pi-]CC": "TrackIsoBs2MMInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l-) pi+]CC": "TrackIsoBs2MMInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l-) pi-]CC": "TrackIsoBs2MMInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l+) pi+]CC": "TrackIsoBs2MMInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l+) pi-]CC": "TrackIsoBs2MMInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l-) pi+]CC": "TrackIsoBs2MMInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l-) pi-]CC": "TrackIsoBs2MMInfo_LStar"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS", 
                    "BSMUMUTRACKID"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l+) pi+]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l+) pi-]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l-) pi+]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) ^l-) pi-]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l+) ^pi+]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l+) ^pi-]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l-) ^pi+]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ Xs) l-) ^pi-]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l+) pi+]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l+) pi-]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l-) pi+]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X+ ^Xs) l-) pi-]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l+) pi+]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l+) pi-]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l-) pi+]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) ^l-) pi-]CC": "TrackIsoBDTInfo_mu", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l+) ^pi+]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l+) ^pi-]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l-) ^pi+]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- Xs) l-) ^pi-]CC": "TrackIsoBDTInfo_Pi", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l+) pi+]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l+) pi-]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l-) pi+]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> X- ^Xs) l-) pi-]CC": "TrackIsoBDTInfo_K", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l+) pi+]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l+) pi-]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l-) pi+]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X+ Xs) l-) pi-]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l+) pi+]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l+) pi-]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l-) pi+]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> (X0 -> ^X- Xs) l-) pi-]CC": "TrackIsoBDTInfo_p", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l+) pi+]CC": "TrackIsoBDTInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l+) pi-]CC": "TrackIsoBDTInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l-) pi+]CC": "TrackIsoBDTInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X+ Xs) l-) pi-]CC": "TrackIsoBDTInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l+) pi+]CC": "TrackIsoBDTInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l+) pi-]CC": "TrackIsoBDTInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l-) pi+]CC": "TrackIsoBDTInfo_LStar", 
                    "[Beauty -> (Beauty -> ^(X0 -> X- Xs) l-) pi-]CC": "TrackIsoBDTInfo_LStar"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }
        ], 
        "Sb2PKJpsiPiLinePrescale": 1, 
        "Sb2PKMuXPiLinePrescale": 1, 
        "Sb2PKMuXPiSSLinePrescale": 1, 
        "SbPT": 50.0, 
        "Trk_GhostProb": 0.2, 
        "UpperLbMass": 5620.0, 
        "pKJpsiFdChi2": 25.0, 
        "pKJpsiMassMax": 5850.0, 
        "pKJpsiMassMin": 5400.0, 
        "pKJpsiVChi2Dof": 3.0, 
        "pKMuFdChi2": 100.0, 
        "pKMuMassMax": 6000.0, 
        "pKMuMassMin": 2300.0, 
        "pKMuVChi2Dof": 4.0
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "RD" ]
}

Tau23Mu = {
    "BUILDERTYPE": "Tau23MuLinesConf", 
    "CONFIG": {
        "Ds23PiPrescale": 0.0, 
        "Ds23PiTISPrescale": 0.0, 
        "Ds2PhiPiPrescale": 1.0, 
        "Tau25Prescale": 1.0, 
        "Tau2PMuMuPrescale": 1.0, 
        "TauPostscale": 1.0, 
        "TauPrescale": 1.0, 
        "TrackGhostProb": 0.45
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingTau23MuTau23Mu_DSTLine", 
            "StrippingTau23MuTau23Mu_DST_3muLine", 
            "StrippingTau23MuDs2PhiPi_DSTLine"
        ], 
        "Leptonic": [
            "StrippingTau23MuTau23MuLine", 
            "StrippingTau23MuDs2PhiPiLine", 
            "StrippingTau23MuTau2PMuMuLine", 
            "StrippingTau23MuDs23PiLine", 
            "StrippingTau23MuTau25MuLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

