

2020-05-08 Stripping v14r7
========================================

Release for the 2018 PbPb and PbNe full restrippings (S35r2 and S35r3)
----------------------------------------

Based on Phys v25r11.
This version is released on 2018-patches branch.

- add datafiles for S35r2, !1404 (@nskidmor)   
  

- dbase and archives for S35r2, !1403 (@nskidmor)   
  

- Add database and archive for S35r3, !1402 (@atully)   
  

- Merge S35r3_devel to 2018-patches for stripping for 2018 PbNe data (S35r3), !1401 (@sunj)   
  Added S35r3 dictionaries for 2018 PbNe stripping. The stripping selections are also updated.

- Prepare test data and scripts for s35r3 (2018 PbNe), !1396 (@atully)   
  

- Update reference for StrippingArchive.instantiate-all-strip, !1393 (@nskidmor)   
  Fixes failing StrippingArchive.instantiate-all-strip test
